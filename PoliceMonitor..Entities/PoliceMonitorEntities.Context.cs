﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PoliceMonitor.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class PoliceMonitorDBEntities : DbContext
    {
        public PoliceMonitorDBEntities()
            : base("name=PoliceMonitorDBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CustomerTalkGroup> CustomerTalkGroups { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }
        public virtual DbSet<UserInRole> UserInRoles { get; set; }
        public virtual DbSet<UserPermission> UserPermissions { get; set; }
        public virtual DbSet<UserRadioMap> UserRadioMaps { get; set; }
        public virtual DbSet<CustomerNotificationSetting> CustomerNotificationSettings { get; set; }
        public virtual DbSet<GroupTalkGroupMap> GroupTalkGroupMaps { get; set; }
        public virtual DbSet<UserTalkGroup> UserTalkGroups { get; set; }
        public virtual DbSet<NotificationSettingGroupMap> NotificationSettingGroupMaps { get; set; }
        public virtual DbSet<NotificationSettingRadioMap> NotificationSettingRadioMaps { get; set; }
        public virtual DbSet<NotificationSettingTalkGroup> NotificationSettingTalkGroups { get; set; }
        public virtual DbSet<CustomerNotificationSettingNew> CustomerNotificationSettingNews { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerPermission> CustomerPermissions { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<PolledTalkGroupItem> PolledTalkGroupItems { get; set; }
        public virtual DbSet<PlaybackFavourite> PlaybackFavourites { get; set; }
        public virtual DbSet<TalkGroupItem> TalkGroupItems { get; set; }
        public virtual DbSet<ActivityLog> ActivityLogs { get; set; }
        public virtual DbSet<Chunk> Chunks { get; set; }
        public virtual DbSet<UserRadio> UserRadios { get; set; }
        public virtual DbSet<TblSystem> TblSystems { get; set; }
        public virtual DbSet<Radio> Radios { get; set; }
        public virtual DbSet<TalkGroup> TalkGroups { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<logFBPost> logFBPosts { get; set; }
        public virtual DbSet<Timeformat> Timeformats { get; set; }
        public virtual DbSet<ActivityDash> ActivityDashes { get; set; }
        public virtual DbSet<ActivityTalkDash> ActivityTalkDashes { get; set; }
        public virtual DbSet<CustomerNotificationUserMap> CustomerNotificationUserMaps { get; set; }
        public virtual DbSet<logFBPostDraft> logFBPostDrafts { get; set; }
    
        public virtual ObjectResult<GetTrafficActivity_Result> GetTrafficActivity(string talkgroup, Nullable<System.DateTime> previousTime)
        {
            var talkgroupParameter = talkgroup != null ?
                new ObjectParameter("Talkgroup", talkgroup) :
                new ObjectParameter("Talkgroup", typeof(string));
    
            var previousTimeParameter = previousTime.HasValue ?
                new ObjectParameter("PreviousTime", previousTime) :
                new ObjectParameter("PreviousTime", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTrafficActivity_Result>("GetTrafficActivity", talkgroupParameter, previousTimeParameter);
        }
    
        public virtual ObjectResult<GetCustomerInfo_Result> GetCustomerInfo(string customerID)
        {
            var customerIDParameter = customerID != null ?
                new ObjectParameter("CustomerID", customerID) :
                new ObjectParameter("CustomerID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCustomerInfo_Result>("GetCustomerInfo", customerIDParameter);
        }
    }
}
