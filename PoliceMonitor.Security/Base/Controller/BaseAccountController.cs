﻿using System;
using System.Linq;
using PoliceMonitor.Security.Base.Model;
using System.Web.Routing;
using System.Web.Mvc;
using PoliceMonitor.Security.Principal;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web;
using PoliceMonitor.Security.TwoWayAuth;
using System.Configuration;
using PoliceMonitor.Security.Crypto;
using System.Net.Mail;
using System.Collections.Generic;
using PoliceMonitor.Security;
using PoliceMonitor.Entities;

namespace PoliceMonitor.Security.Base.Controller
{
  
    public abstract class BaseAccountController : BaseController
    {
        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }

        public BaseAccountController()
        {
        }


        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        public ActionResult SessionExpired()
        {
            return View();
        }


        // authTicket issue for Long Mail ID
        // Start
        [HttpPost]
        [ValidateAntiForgeryTokenAttribute]
        public ActionResult Login(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                string encryPassword = PoliceMonitor.Security.Crypto.AESEncrytDecry.EncryptStringAES(model.Password);
                string userName = model.UserName;
                string password = model.Password;
                int Userid;
                string redirectUrl = string.Empty;
                
                if (((AccountMembershipService)MembershipService).ValidateUser(userName, encryPassword))
                {
                    //DB CALL
                    CustomPrincipalSerializeModel serializeModel = ((AccountMembershipService)MembershipService).GetCustomPrincipalSerializeModel(model.UserName);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    string userData = serializer.Serialize(serializeModel);

                    int cookieExpirationInterval = 30;
                    Int32.TryParse(Config("CookieExpirationInterval"), out cookieExpirationInterval);

                    if (cookieExpirationInterval == 0)
                    {
                        cookieExpirationInterval = 30;
                    }

                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                             Convert.ToString(serializeModel.UserId),
                             DateTime.Now,
                             DateTime.Now.AddMinutes(cookieExpirationInterval),
                             false,
                             userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                    Response.Cookies.Add(faCookie);
                    CustomPrincipalSerializeModel serializeModelWithAllData = ((AccountMembershipService)MembershipService).GetCustomPrincipalSerializeModelWithAllData(userName);
                    Session["userName"] = serializeModel.UserName;
                    Session["userId"] = serializeModel.UserId;
                    Session["userMail"] = serializeModel.Email;
                    Session["USER_SEC_INFO"] = serializeModelWithAllData;
                    if (Request.Cookies.AllKeys.Contains("timezoneoffset"))
                    {
                        try
                        {
                            Session["timezoneoffset"] = int.Parse(Request.Cookies["timezoneoffset"].Value);
                        }
                        catch { }
                    }

                    try
                    {
                        using (PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new PoliceMonitor.Entities.PoliceMonitorDBEntities())
                        {
                            ActivityLog activity = new ActivityLog();
                            activity.activity = Constant.LOGIN;
                            activity.userId = serializeModel.UserId;
                            activity.createdOn = DateTime.Now;
                            dbo.ActivityLogs.Add(activity);
                            dbo.SaveChanges();
                        }
                    }
                    catch (Exception ex) {
                        Console.WriteLine(ex);
                    }
                    if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl) && returnUrl.StartsWith("/") && returnUrl != "/"
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\")){
                            redirectUrl = returnUrl;
                    }
                    return Redirect("~/Home/Index");
                    //return RedirectToAction("../Home/Index");

                    //if (!String.IsNullOrEmpty(returnUrl))//Need for ReturnURL
                    //{
                    //    return Redirect(returnUrl);
                    //}
                    //else
                    //{
                    //    return RedirectToAction("Index", "Home");
                    //}
                }
                else
                {
                    ModelState.AddModelError("", "Username or Password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        // End



        // **************************************
        // URL: /Account/LogOff
        // **************************************

        [HttpPost]
        public ActionResult LogOff()
        {
            int userId = 0;
            try
            {
                userId = (int)Session["userId"];
            }
            catch { }
            Session["USER_SEC_INFO"] = null;
            Session.Abandon();
            FormsService.SignOut();

            try
            {
                if (userId > 0)
                {
                    using (PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new PoliceMonitor.Entities.PoliceMonitorDBEntities())
                    {
                        ActivityLog activity = new ActivityLog();
                        activity.activity = Constant.LOGOUT;
                        activity.userId = userId;
                        activity.createdOn = DateTime.Now;
                        dbo.ActivityLogs.Add(activity);
                        dbo.SaveChanges();
                    }
                }
            }
            catch { }

            return RedirectToAction("Login", "Account");
        }

        public ActionResult TwoWayAuthentication()
        {
            return View();
        }


        private bool AdditionalThirdPartyLoginValidation(string userID,string password, string domain)
        {
            if (String.IsNullOrEmpty(userID) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(domain))
            {
                return false;
            }
            else
            {
              
                return false;
                  
            }
        }
       
        // **************************************
        // URL: /Account/Settings
        // **************************************


        public ActionResult Settings()
        {
            PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            Entities.User u = dbo.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            SettingsModel model = new SettingsModel();
            model.HoursAgo = "" + ((u.HoursAgo.HasValue) ? u.HoursAgo.Value : Int32.Parse(ConfigurationManager.AppSettings["DefaultFromHoursAgo"]));
            model.MinTransmissionTime = "" + ((u.MinTransmissionTime.HasValue) ? u.MinTransmissionTime.Value : Int32.Parse(ConfigurationManager.AppSettings["DefaultMinTran"]));
            model.HoursDuration = "" + ((u.HoursDuration.HasValue) ? u.HoursDuration.Value : Int32.Parse(ConfigurationManager.AppSettings["DefaultToHoursAgo"]));
            model.AutoPlay = ((u.AutoPlay.HasValue) ? u.AutoPlay.Value : false);
            Entities.ActivityDash AD = dbo.ActivityDashes.Where(w => w.UserID == User.UserId).FirstOrDefault();
            if (AD != null)
            {
                model.Green = AD.GreenMax.HasValue ? AD.GreenMax.Value : Int32.Parse(ConfigurationManager.AppSettings["GreenMax"]);
                model.YellowMax = AD.YellowMax.HasValue ? AD.YellowMax.Value : Int32.Parse(ConfigurationManager.AppSettings["YellowMax"]);
                model.YellowMin = AD.YellowMin.HasValue ? AD.YellowMin.Value : Int32.Parse(ConfigurationManager.AppSettings["YellowMin"]);
                model.Red = AD.RedMin.HasValue ? AD.RedMin.Value : Int32.Parse(ConfigurationManager.AppSettings["RedMin"]);
                model.TimeDuration = AD.HourDuration.HasValue ? AD.HourDuration.Value : Int32.Parse(ConfigurationManager.AppSettings["HoursDuration"]);
                ViewBag.TimeDuration = AD.HourDuration.HasValue ? AD.HourDuration.Value : Int32.Parse(ConfigurationManager.AppSettings["HoursDuration"]);
            }
            else
            {
                model.Green = Int32.Parse(ConfigurationManager.AppSettings["GreenMax"]);
                model.YellowMin = Int32.Parse(ConfigurationManager.AppSettings["YellowMin"]);
                model.YellowMax = Int32.Parse(ConfigurationManager.AppSettings["YellowMax"]);
                model.Red =  Int32.Parse(ConfigurationManager.AppSettings["RedMin"]);
                model.TimeDuration = Int32.Parse(ConfigurationManager.AppSettings["HoursDuration"]);
                ViewBag.TimeDuration = Int32.Parse(ConfigurationManager.AppSettings["HoursDuration"]);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Settings(SettingsModel model)
        {
            PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            if (!ModelState.IsValid)
            {
                return View("Settings", model);
            }

            int hr, min, duration,green,yellowMin,yellowMax,red,timeDuration;
            
            model.HoursAgo = (model.HoursAgo == null) ? "" : model.HoursAgo.Trim();
            if (Request.Form["TimeDurationHidden"]=="")
            {
                var val= dbo.ActivityDashes.Where(w => w.UserID == User.UserId).Select(s => s.HourDuration).ToArray();
                if (val.Count() == 0)
                {
                    model.TimeDuration = Int32.Parse(ConfigurationManager.AppSettings["HoursDuration"]);
                }
                else
                {
                    model.TimeDuration = Convert.ToInt32(val[0]);
                }
            }
            else
            {
                model.TimeDuration = Convert.ToInt32(Request.Form["TimeDurationHidden"]);
            }            
            green = model.Green;
            yellowMax = model.YellowMax;
            yellowMin = model.YellowMin;
            red = model.Red;
            timeDuration = model.TimeDuration;
            ViewBag.TimeDuration = model.TimeDuration;
            //int.TryParse(model.Green, out green);
            //int.TryParse(model.YellowMin, out yellowMin);
            //int.TryParse(model.YellowMax, out yellowMax);
            //int.TryParse(model.Red, out red);
            //int.TryParse(model.TimeDuration, out timeDuration);


            int.TryParse(model.HoursAgo, out hr);
            int.TryParse(model.MinTransmissionTime, out min);
            int.TryParse(model.HoursDuration, out duration);

            if(!string.IsNullOrEmpty(model.OldPassword))
            {
                model.NewPassword = (model.NewPassword == null) ? "" : model.NewPassword;
                if (model.NewPassword.Equals(""))
                {
                    ModelState.AddModelError("", "Please enter the new password.");
                    return View("Settings", model);
                }
                else if (!model.NewPassword.Equals(model.ConfirmPassword))
                {
                    ModelState.AddModelError("", "Passwords does not match.");
                    return View("Settings", model);
                }

                if (!MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    return View("Settings", model);
                }
            }
            
            if(hr < 1)
            {
                ModelState.AddModelError("", "Default Hours Ago cannot be less than 1.");
                return View("Settings", model);
            }

            if (duration < 0)
            {
                ModelState.AddModelError("", "Duration cannot be less than 0.");
                return View("Settings", model);
            }

            if (min < 0)
            {
                ModelState.AddModelError("", "Minimum Transmission Time cannot be less than 0.");
                return View("Settings", model);
            }
            
            
            
            Entities.User u = dbo.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            u.HoursAgo = hr;
            u.MinTransmissionTime = min;
            u.HoursDuration = duration;
            u.AutoPlay = model.AutoPlay;

            Entities.ActivityDash a = dbo.ActivityDashes.Where(w => w.UserID == User.UserId).FirstOrDefault();
            if (a != null)
            {
                a.UserID = User.UserId;
                a.GreenMax = yellowMin;
                a.YellowMin = yellowMin;
                a.YellowMax = yellowMax;
                a.RedMin = yellowMax;
                a.HourDuration = timeDuration;
            }
            else
            {

                Entities.ActivityDash AD = new Entities.ActivityDash();
                AD.UserID = User.UserId;
                AD.GreenMax = yellowMin;
                AD.YellowMin = yellowMin;
                AD.YellowMax = yellowMax;
                AD.RedMin = yellowMax;
                AD.HourDuration = timeDuration;
                dbo.ActivityDashes.Add(AD);
            }
            dbo.SaveChanges();

            CustomPrincipalSerializeModel serializeModelWithAllData = ((AccountMembershipService)MembershipService).GetCustomPrincipalSerializeModelWithAllData(User.Identity.Name);
            Session["USER_SEC_INFO"] = serializeModelWithAllData;

            return View("Settings", model);
            //return Json(new {success = result }) ;
        }

        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryTokenAttribute]
        public ActionResult ForgotPassword()
        {
            ForgotPwdModel model = new ForgotPwdModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgotPwdModel model)
        {
            string password = ForgotPwd.PwdGenerator.CreateRandomPassword();

            if (ModelState.IsValid)
            {

                if (!System.Text.RegularExpressions.Regex.IsMatch(model.Email, @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$"))
                {
                    ModelState.AddModelError("", "The provided email address is not a valid email address.");
                    return View();
                }

                using (var context = new PoliceMonitorDBEntities())
                {
                    var isEmailExist = (from usr in context.Users
                                        where usr.Active == true && usr.Email == model.Email
                                        select usr).Any();

                    if (!isEmailExist)
                    {
                        ModelState.AddModelError("", "The provided email address is not registered.");
                    }
                    else
                    {
                        //CryptoMgr crypto = new CryptoMgr(CryptoMgr.CryptoTypes.encTypeDES);
                        //string encryptedPwd = crypto.Encrypt(password, "");
                        
                        var user = (from usr in context.Users
                                    where usr.Active == true && usr.Email == model.Email
                                    select usr).FirstOrDefault();

                        if (user != null)
                        {
                            string NewPassword = password;
                            user.Password = PoliceMonitor.Security.Crypto.AESEncrytDecry.EncryptStringAES(password);
                            context.SaveChanges();

                            //NotificationManager NotificationManager = new NotificationManager();
                            //NotificationInfo notificationInfo = new NotificationInfo
                            //{
                            //    Email = user.Email,
                            //    EmailFrom = "",
                            //    Subject = "Reset Password",
                            //    Content = "Your new password is " + password
                            //};
                            //if (NotificationManager.SendNotification(notificationInfo))
                            //{
                            //    return View("PasswordRecoverySuccessful");
                            //}
                            //rohit
                            SmtpClient client = new SmtpClient();
                            MailMessage msg = new MailMessage();
                            msg.To.Add(user.Email);
                            msg.Subject = "Reset Password";
                            msg.IsBodyHtml = true;
                            string name = user.FirstName + " " + user.LastName;
                            name = (name.Trim().Equals("")) ? user.UserName : name;
                            msg.Body = "Dear " + name + ",<br/><br/>Here is your <a href='Radio1033.com'>Radio1033.com</a> new password: " + NewPassword + "<br/><br/><a href='Radio1033.com'>Click here</a> to login.<br/><br/>Note: To change your password after you log in, click the Settings menu option at the top  of the screen.<br/><br/>Thank you.<br/>Radio1033 Team";
                            try
                            {
                                client.Send(msg);
                                ViewBag.Success = "Your password has been sent to your email address.";
                            }
                            catch(Exception ex)
                            {
                                ModelState.AddModelError("", "Some problem exist to send the email.Please contact system administrator.");
                            }
                        }
                    }
                }
            }

                return View();
        }

        private bool HasReportAccess(CustomPrincipalSerializeModel model)
        {
            int privID = 0;
            Int32.TryParse(Config("ReportPrivID"), out privID);
            bool hasReportAccess = false;

            if (model != null)
            {
                hasReportAccess = model.AccessPrivilegeIds.Where(p => p == privID).Any();
            }
            else
            {
                hasReportAccess = User.AccessPrivilegeIds.Where(p => p == privID).Any();
            }

            return hasReportAccess;
        }

    }
}
