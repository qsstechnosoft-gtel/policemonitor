﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceMonitor.Security.Principal;
using PoliceMonitor.Security.Authorization;
using System.Configuration;
using PoliceMonitor.Security.Base.Model;

namespace PoliceMonitor.Security.Base.Controller
{
    
    public abstract class BaseController : System.Web.Mvc.Controller
    {
        protected virtual new CustomPrincipal User
        {
            get
            {
                var res = PageAccessManager.GetFullCustomPrincipal();
                return res;
            }
        }

        /***********************************************************************************************************************************************/

        protected string Config(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        /***********************************************************************************************************************************************/

        protected int ConfigInt(string key)
        {
            int result = 0;
            Int32.TryParse(ConfigurationManager.AppSettings[key], out result);
            return result;
        }

    }
}
