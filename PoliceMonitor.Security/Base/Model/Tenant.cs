﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliceMonitor.Security.Base.Model
{
    public class Tenant
    {
        public int IGTenantID { get; set; }
        public Guid TenantKey { get; set; }
        public string TenantName { get; set; }
    }
}
