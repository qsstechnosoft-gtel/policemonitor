﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliceMonitor.Security.Base.Model
{
    public static class Constant
    {
        public const string SUPERADMIN_ROLENAME = "Super Admin";
        public const string ADMIN_ROLENAME = "Admin";
        public const string USER_ROLENAME = "User";

        public static string SCANLISTENONLY_PERMISSION = "Scan / Listen";

        public static Permission LIVELISTENING = new Permission(1, "Live Listening");
        public static Permission PLAYBACK = new Permission(2, "Playback");
        public static Permission MARKASUNLISTED = new Permission(3, "Mark transmission as unlistened");
        public static Permission QUICKPLAYBACK = new Permission(4, "Quick Playback");
        public static Permission NOTIFICATION_CONFIGURATION = new Permission(5, "Notifications Configuration");
        public static Permission CANDOWNLOADAUDIO = new Permission(6, "Can Download Audio");
        public static Permission SEARCHBYRADIO = new Permission(7, "Search by Radio ID / Literal Name");
        public static Permission ACTIVITYMONITOR = new Permission(8, "Activity Dashboard");


        public static string LOGIN = "LOGIN";

        public static string LOGOUT = "LOGOUT";

        public static string MERGEDTRANSMISSIONS = "mergedtransmissions/";
    }

    
}