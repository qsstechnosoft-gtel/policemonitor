﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace PoliceMonitor.Security.Base.Model
{
    public class TwoWayAuthModel
    {

        [DataType(DataType.Password)]
        [Display(Name = "OTP Code")]
        public string OTP { get; set; }
    }
}
