﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceMonitor.Security.Principal;
using System.Web.Security;
using PoliceMonitor.Security.Provider;

namespace PoliceMonitor.Security.Base.Model
{
  

    public sealed class AccountMembershipService : IMembershipService
    {
        private readonly CentralMembershipProvider _provider;

        public AccountMembershipService()
            : this(null)
        {
        }

        public AccountMembershipService(CentralMembershipProvider provider)
        {
            _provider = provider ?? Membership.Provider as CentralMembershipProvider;
        }

        public int MinPasswordLength
        {
            get
            {
                return _provider.MinRequiredPasswordLength;
            }
        }

        public bool IsADAuthenticatication
        {
            get
            {
                return _provider.IsADauthentication;
            }
        }

        public bool ValidateUser(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            return _provider.ValidateUser(userName, password);
        }
        
        public CustomPrincipalSerializeModel GetCustomPrincipalSerializeModelWithAllData(string userName)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");

            return _provider.GetCustomPrincipalSerializeModelWithAllData(userName);
        }

        public CustomPrincipalSerializeModel GetCustomPrincipalSerializeModel(string userName)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");

            return _provider.GetCustomPrincipalSerializeModel(userName);
        }

        public MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            if (String.IsNullOrEmpty(userName)) throw new ArgumentException("Value cannot be null or empty.", "userName");
            if (String.IsNullOrEmpty(password)) throw new ArgumentException("Value cannot be null or empty.", "password");
            if (String.IsNullOrEmpty(email)) throw new ArgumentException("Value cannot be null or empty.", "email");

            MembershipCreateStatus status;
            _provider.CreateUser(userName, password, email, null, null, true, null, out status);
            return status;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                //if (oldPassword != newPassword) return false;
                //MembershipUser currentUser = _provider.GetUser(userName, true /* userIsOnline */);
                return _provider.ChangePassword(userName, oldPassword, newPassword);
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
            catch (Exception ex)
            {
                throw new MembershipPasswordException(ex.Message);
            }
        }

        public bool IsTwoWayAuthRequired
        {
            get
            {
                return _provider.IsTwoWayAuthRequired;
            }
        }

        public string GenerateOTP()
        {
            return _provider.GenerateOTP();
        }

        public bool ValidateOTP(string otp)
        {
            return _provider.ValidateOTP(otp);
        }

        public string OTP
        {
            get
            {
                return _provider.OTP;
            }
        }

        public string Domain
        {
            get
            {
                return _provider.Domain;
            }
        }



    }
}
