﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace PoliceMonitor.Security.Base.Model
{
    public class SettingsModel
    {
        [Display(Name = "Autoplay")]
        public bool AutoPlay { get; set; }

        [Display(Name = "Default Hours Ago")]
        public string HoursAgo { get; set; }


        [Display(Name = "Green Max Duration")]
        public int Green { get; set; }


        [Display(Name = "Moderate Traffic Range (Transmissions per minute)")]
        public int YellowMin { get; set; }

        public int YellowMax { get; set; }


        [Display(Name = "Red Min Duration")]
        public int Red { get; set; }

        [Display(Name = "Avg. Time Duration for Traffic")]
        public int TimeDuration { get; set; }


        [Display(Name = "Duration")]
        public string HoursDuration { get; set; }

        [Display(Name = "Default Min. Transmission Time")]
        public string MinTransmissionTime { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}
