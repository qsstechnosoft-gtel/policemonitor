﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace PoliceMonitor.Security.Base.Model
{
    public class ForgotPwdModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }
    }
}
