﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoliceMonitor.Security.Base.Model
{
    public class Permission
    {
        public Permission(int id, string value)
        {
            this.id = id;
            this.value = value;
        }

        public int id { get; set; }
        public string value { get; set; }
    }
}
