﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceMonitor.Security.Authorization;
using System.Web.Security;
using System.Web;
using System.Web.Script.Serialization;
using PoliceMonitor.Security.Principal;
using System.Web.Mvc;
using PoliceMonitor.Security.Filter;
using PoliceMonitor.Security.Base.Model;

namespace PoliceMonitor.Security.Base.Global
{  

    public abstract class BaseHttpApplication : System.Web.HttpApplication
    {
        static BaseHttpApplication()
        {
            WhiteListSecurityPolicy = new WhiteListPolicy();
        }

        public static WhiteListPolicy WhiteListSecurityPolicy { get; set; }

        protected void Session_OnEnd(object sender, EventArgs e)
        {
            //Session["USER_SEC_INFO"] = null;
            //Session.Abandon();

            //throw new Exception("Your session has been expired");
            //HttpContext.Current.Response.Redirect("~/Home/Index");   

            //Response.Redirect("~/Account/SessionExpired");   
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CustomPrincipalSerializeModel serializeModel = serializer.Deserialize<CustomPrincipalSerializeModel>(authTicket.UserData);
                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);

                newUser.UserId = serializeModel.UserId;
                newUser.UserName = serializeModel.UserName;
                newUser.Email = serializeModel.Email;
                newUser.CustomerId = serializeModel.CustomerId;
                newUser.Address = serializeModel.Address;
                newUser.City = serializeModel.City;
                newUser.State = serializeModel.State;
                newUser.PostalCode = serializeModel.PostalCode;
                newUser.PhoneNumber = serializeModel.PhoneNumber;
                newUser.UserRoles = serializeModel.UserRoles;
                newUser.HoursAgo = serializeModel.HoursAgo;
                newUser.MinTransmissionTime = serializeModel.MinTransmissionTime;
                newUser.AutoPlay = serializeModel.AutoPlay;

                newUser.DBWhiteList = new List<IWhiteListItem>();
                newUser.DBDenyWhiteList = new List<IWhiteListItem>();
                newUser.Roles = serializeModel.RolePolicies;

                newUser.IsADAuthentication = serializeModel.IsADAuthentication;
                newUser.IsTwoWayAuthentication = serializeModel.IsTwoWayAuthentication;
                newUser.AuthenticationMode = serializeModel.AuthenticationMode;
                newUser.CanShowtheLogOff = serializeModel.CanShowtheLogOff;
                newUser.OTP = serializeModel.OTP;
                newUser.OTPTime = serializeModel.OTPTime;                
                newUser.AccessRoleLevel = serializeModel.AccessRoleLevel;
                newUser.AccessRoleKey = serializeModel.AccessRoleKey;
                newUser.AccessRoleName = serializeModel.AccessRoleName;
                newUser.AccessPrivilegeIds = serializeModel.AccessPrivilegeIds;
                newUser.DefaultMenuID = serializeModel.DefaultMenuID;
                newUser.DefaultController = serializeModel.DefaultController;
                newUser.DefaultAction = serializeModel.DefaultAction;
                newUser.IsSystemAdmin = serializeModel.IsSystemAdmin;
                newUser.IsSystemADAuthentication = serializeModel.IsSystemADAuthentication;
                newUser.DefaultAutoplaySetting = serializeModel.DefaultAutoplaySetting;

                if (serializeModel.DBWhiteList != null)
                {
                    foreach (var item in serializeModel.DBWhiteList)
                    {
                        newUser.DBWhiteList.Add(item);
                    }
                }

                if (serializeModel.DBDenyWhiteList != null)
                {
                    foreach (var item in serializeModel.DBDenyWhiteList)
                    {
                        newUser.DBDenyWhiteList.Add(item);
                    }
                }

                HttpContext.Current.User = newUser;
            }
        }        

        public void RegisterSecurityGlobalFilters(GlobalFilterCollection filters)
        {
            PrepareWhiteListSecurityPolicy();
            filters.Add(new SecurityFilter(WhiteListSecurityPolicy));
        }      

        public abstract void PrepareWhiteListSecurityPolicy();

    }
}
