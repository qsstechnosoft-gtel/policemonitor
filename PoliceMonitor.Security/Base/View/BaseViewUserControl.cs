﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using PoliceMonitor.Security.Helper;

namespace PoliceMonitor.Security.Base.View
{
    public abstract class BaseViewUserControl : ViewUserControl
    {
        public UserHelper AppUser
        {
            get;
            set;
        }
    }

    public abstract class BaseViewUserControl<TModel> : ViewUserControl<TModel>
    {
        public UserHelper<TModel> AppUser
        {
            get;
            set;
        }
    }
}
