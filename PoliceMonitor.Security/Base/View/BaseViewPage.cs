﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using PoliceMonitor.Security.Principal;
using PoliceMonitor.Security.Helper;

namespace PoliceMonitor.Security.Base.View
{   
    public abstract class BaseViewPage : System.Web.Mvc.WebViewPage
    {
        public UserHelper AppUser
        {
            get;
            set;
        }

        public override void InitHelpers()
        {
            base.InitHelpers();
            AppUser = new UserHelper<object>(base.ViewContext, this);
        }         
    }

    public abstract class BaseViewPage<TModel> : System.Web.Mvc.WebViewPage<TModel>
    {
        public UserHelper<TModel> AppUser
        {
            get;
            set;
        }

        public override void InitHelpers()
        {
            base.InitHelpers();
            AppUser = new UserHelper<TModel>(base.ViewContext, this);
        }
    }
}
