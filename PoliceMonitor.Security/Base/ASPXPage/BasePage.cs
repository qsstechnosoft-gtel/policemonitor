﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceMonitor.Security.Principal;
using PoliceMonitor.Security.Authorization;
using System.Configuration;

namespace PoliceMonitor.Security.Base.ASPXPage
{
    public class BasePage : System.Web.UI.Page
    {
        protected virtual new CustomPrincipal User
        {
            get
            {
                var res = PageAccessManager.GetFullCustomPrincipal();
                return res;
            }
        }

        /***********************************************************************************************************************************************/

        protected string Config(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        /***********************************************************************************************************************************************/

        protected int ConfigInt(string key)
        {
            int result = 0;
            Int32.TryParse(ConfigurationManager.AppSettings[key], out result);
            return result;
        }
    }
}
