﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceMonitor.Security.Authorization;
using System.Web.Security;
using System.Web.Mvc;
using System.Web.Routing;
using PoliceMonitor.Security.Principal;
using System.Net;
using log4net;
using PoliceMonitor.Security.Base.Model;

namespace PoliceMonitor.Security.Filter
{

    public class SecurityFilter : FilterAttribute, IAuthorizationFilter, IExceptionFilter, IActionFilter
    {
        WhiteListPolicy _whiteListPolicy = null;
        protected static readonly ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SecurityFilter()
            : base()
        {
            _whiteListPolicy = System.Web.HttpContext.Current.Application["WhiteListSecurityPolicy_FMS"] as WhiteListPolicy;
        }

        public SecurityFilter(WhiteListPolicy whiteListPolicy)
            : base()
        {
            _whiteListPolicy = whiteListPolicy;
        }


        public void OnAuthorization(AuthorizationContext filterContext)
        {

            var Area = GetAreaName(filterContext.RouteData);
            var Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var Action = filterContext.ActionDescriptor.ActionName;
            var User = filterContext.HttpContext.User;
            var IP = filterContext.HttpContext.Request.UserHostAddress;

            var url = ((System.Web.HttpRequestWrapper)filterContext.HttpContext.Request).Params["ReturnUrl"];

            if (!string.IsNullOrEmpty(url))
            {
                System.Diagnostics.Debug.WriteLine("______________________________________________" + url);
                System.Diagnostics.Trace.WriteLine("______________________________________________" + url);
            }

            ValidateAuthenticationAndSession(filterContext);


            var isAccessAllowed = PageAccessManager.IsAccessAllowed(Area, Controller, Action, User, IP, _whiteListPolicy);

            string s = "***" + (string.IsNullOrEmpty(Area) ? "XXX" : Area) + " - " + Controller + " - " + Action + " [" + isAccessAllowed.ToString() + "]";
            System.Diagnostics.Debug.WriteLine(s);
            System.Diagnostics.Trace.WriteLine(s);

            #if DEBUG
            
            #else
            string action = filterContext.ActionDescriptor.ActionName;
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            #endif

            if (!isAccessAllowed)
            {
                //Check Authentication
                var authUser = User as CustomPrincipal;

                //Authorization check || auth
                if (authUser != null && authUser.Identity.IsAuthenticated)
                {
                    filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/Account/Login?ReturnUrl=" + Controller + "/" + Action);
                }
            }
        }

        public void OnException(ExceptionContext filterContext)
        {
            string view = "Error";

            if (!string.IsNullOrEmpty(view))
            {
                 // if the request is AJAX return JSON else view.
                if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    filterContext.Result = new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new
                        {
                            error = true,
                            message = filterContext.Exception.Message,
                            source = filterContext.Exception.Source
                        }
                    };

                }
                else
                {

                    var Area = GetAreaName(filterContext.RouteData);
                    string controllerName = (string)filterContext.RouteData.Values["controller"];

                    if (!string.IsNullOrEmpty(Area))
                    {
                        controllerName = Area + "\\" + controllerName;
                    }

                    string actionName = (string)filterContext.RouteData.Values["action"];
                    HandleErrorInfo model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);
                    ViewResult result = new ViewResult
                    {
                        ViewName = view,
                        ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                        TempData = filterContext.Controller.TempData
                    };
                    filterContext.Result = result;
                }

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

                string loggedInUser = filterContext.HttpContext != null ? filterContext.HttpContext.User.Identity.Name : "AnonymousUser";                
                string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                else
                {
                    ip = "Unknown IP address";
                }

                if (log.IsErrorEnabled)
                    log.ErrorFormat("{0} - {1} -Logged in User {2} - IPAddress({3}) - Stack Trace: {4}", filterContext.Exception.TargetSite.DeclaringType, filterContext.Exception.Message, loggedInUser, ip,filterContext.Exception.StackTrace);
            }

        }

        static string GetAreaName(RouteBase route)
        {
            var area = route as IRouteWithArea;
            if (area != null)
            {
                return area.Area;
            }
            var route2 = route as Route;
            if ((route2 != null) && (route2.DataTokens != null))
            {
                return (route2.DataTokens["area"] as string);
            }
            return null;
        }

        static string GetAreaName(RouteData routeData)
        {
            object obj2;
            if (routeData.DataTokens.TryGetValue("area", out obj2))
            {
                return (obj2 as string);
            }
            return GetAreaName(routeData.Route);
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
            {
            //var rwerwerwerwe = filterContext.HttpContext.Session.IsNewSession;
            //var aaaa = filterContext.HttpContext.Session["USER_SEC_INFO"];
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            //if (_whiteListPolicy != null && _whiteListPolicy.Anonymous != null && _whiteListPolicy.Anonymous.Count() > 0)
            //{
            //    if(!(_whiteListPolicy.Anonymous.Where(p=>p.Controller.ToUpper() == Controller.ToUpper()).Any()))
            //    {
            //    //if (Controller != "Account" || Controller != "License")
            //    //{
            //        System.Web.HttpCookie authCookie = filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];

            //        /*Called when non-authenticated users try to access the application*/
            //        if (authCookie == null)
            //        {
            //            //SetLoginContext(filterContext);
            //        }
            //        else
            //        {
            //            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

            //            /*Called when authentication ticket supplied in request is expired*/
            //            if (authTicket.Expired)
            //            {
            //                FormsAuthentication.SignOut();
            //                //SetLoginContext(filterContext);
            //            }
            //            else
            //            {
            //                FormsAuthenticationTicket newTicket = FormsAuthentication.RenewTicketIfOld(authTicket);

            //                if (authTicket.Expiration != newTicket.Expiration)
            //                {
            //                    string encTicket = FormsAuthentication.Encrypt(newTicket);
            //                    System.Web.HttpCookie faCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            //                    filterContext.HttpContext.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            //                    filterContext.HttpContext.Response.Cookies.Add(faCookie);
            //                }

            //                bool IsNewSession = filterContext.HttpContext.Session.IsNewSession;
            //                bool IsSeesionInit = filterContext.HttpContext.Session["USER_SEC_INFO"] != null ? true : false;

            //                /*Called when session is expired*/
            //                if (IsNewSession == true && IsSeesionInit == false)
            //                {
            //                    FormsAuthentication.SignOut();

            //                    if (filterContext.HttpContext.Request.Headers["X-Requested-With"] != "XMLHttpRequest")
            //                    {
            //                        string view = "Error";

            //                        if (!string.IsNullOrEmpty(view))
            //                        {
            //                            var Area = GetAreaName(filterContext.RouteData);
            //                            string controllerName = (string)filterContext.RouteData.Values["controller"];

            //                            if (!string.IsNullOrEmpty(Area))
            //                            {
            //                                controllerName = Area + "\\" + controllerName;
            //                            }

            //                            string actionName = (string)filterContext.RouteData.Values["action"];
            //                            // HandleErrorInfo model = new HandleErrorInfo(new Exception("Session Expired: Sorry, your session has been expired. Please close the browser and re-login."), controllerName, actionName);
            //                            HandleErrorInfo model = new HandleErrorInfo(new Exception("SessionExpired"), controllerName, actionName);
            //                            ViewResult result = new ViewResult
            //                            {
            //                                ViewName = view,
            //                                ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
            //                                TempData = filterContext.Controller.TempData
            //                            };
            //                            filterContext.Result = result;
            //                            filterContext.HttpContext.Response.Clear();
            //                            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            //                        }
            //                    }
            //                    else
            //                    {
            //                        try
            //                        {
            //                            throw new Exception("SessionExpired");
            //                        }
            //                        catch (Exception)
            //                        {

            //                            throw;
            //                        }
            //                    }



            //                }
            //            }
            //        }
            //    }
            //}
        }

        private void SetLoginContext(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] != "XMLHttpRequest")
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { Action = "Login", Controller = "Account", returnUrl = filterContext.HttpContext.Request.RawUrl }));
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                
            }
            else
            {
                try
                {
                    throw new Exception("AuthenticationTimeout");
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        //authTicket Cookie Not get for long EmailID
        //Start
        private void ValidateAuthenticationAndSession(AuthorizationContext filterContext)
        {
            var Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            if (_whiteListPolicy != null && _whiteListPolicy.Anonymous != null && _whiteListPolicy.Anonymous.Count() > 0)
            {
                if (!(_whiteListPolicy.Anonymous.Where(p => p.Controller.ToUpper() == Controller.ToUpper()).Any()))
                {

                    //if (Controller != "Account" || Controller != "License")
                    //{
                    System.Web.HttpCookie authCookie = filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];

                    /*Called when non-authenticated users try to access the application*/
                    if (authCookie == null)
                    {
                        SetLoginContext(filterContext);
                    }
                    else
                    {
                        FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                        /*Called when authentication ticket supplied in request is expired*/
                        if (authTicket.Expired)
                        {
                            FormsAuthentication.SignOut();
                            SetLoginContext(filterContext);
                        }
                        else
                        {
                            FormsAuthenticationTicket newTicket = FormsAuthentication.RenewTicketIfOld(authTicket);

                            if (authTicket.Expiration != newTicket.Expiration)
                            {
                                string encTicket = FormsAuthentication.Encrypt(newTicket);
                                System.Web.HttpCookie faCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                                filterContext.HttpContext.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                                filterContext.HttpContext.Response.Cookies.Add(faCookie);
                            }

                            bool IsNewSession = filterContext.HttpContext.Session.IsNewSession;
                            bool IsSeesionInit = filterContext.HttpContext.Session["USER_SEC_INFO"] != null ? true : false;

                            /*Called when session is expired*/
                            if (IsNewSession == true && IsSeesionInit == false)
                            {
                                FormsAuthentication.SignOut();

                                if (filterContext.HttpContext.Request.Headers["X-Requested-With"] != "XMLHttpRequest")
                                {

                                    SetLoginContext(filterContext);
                                }
                                else
                                {
                                    try
                                    {
                                        throw new Exception("SessionExpired");
                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }
                                }



                            }
                            else if ((((filterContext.ActionDescriptor).ControllerDescriptor).ControllerType).Namespace == "PoliceMonitor.UI.Areas.Utility.Controllers" && IsSeesionInit == false)//Add check for BM 
                            {
                                try
                                {
                                    throw new Exception("SessionExpired");
                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                            }
                        }
                    }
                }
            }
        }


        //End
    }
}
