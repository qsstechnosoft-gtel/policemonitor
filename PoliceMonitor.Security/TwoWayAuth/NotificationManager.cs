﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace PoliceMonitor.Security.TwoWayAuth
{
    public class NotificationManager
    {

        public bool SendNotification(NotificationInfo notificationInfo)
        {
            try
            {
                SendEmail(notificationInfo);
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SendEmail(NotificationInfo notificationInfo)
        {
            MailAddress to = new MailAddress(notificationInfo.Email);
            MailAddress from = new MailAddress(notificationInfo.EmailFrom);
            MailMessage message = new MailMessage(from, to);
            message.Subject = notificationInfo.Subject;
            message.Body = notificationInfo.Content + notificationInfo.OTP;
            // Use the application or machine configuration to get the 
            // host, port, and credentials.
            SmtpClient client = new SmtpClient();

            try
            {
                client.Send(message);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

    public class NotificationInfo
    {
        public string Email { get; set; }
        public string EmailFrom { get; set; }
        public string PhoneNumber { get; set; }
        public string OTP { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }  
    }
}
