﻿using System.Security.Principal;
using System.Collections.Generic;
using System.Linq;
using PoliceMonitor.Security.Authorization;
using PoliceMonitor.Security.Principal;
using PoliceMonitor.Security.Base.Model;

namespace PoliceMonitor.Security.Authorization
{
    class PageAccessManager
    {
        public static bool IsAccessAllowed(string Area, string Controller, string Action, IPrincipal User, string IP, WhiteListPolicy _whiteListPolicy)
        {
            //Anonmous White List
            if (ProcessWhiteList(Area, Controller, Action, _whiteListPolicy.Anonymous))
                return true;

            //Check Authentication
            var authUser = User as CustomPrincipal;

            //Authorization check || auth
            if (authUser != null && authUser.Identity.IsAuthenticated)
            {

                //Deny List
                if (ProcessWhiteList(Area, Controller, Action, _whiteListPolicy.Deny))
                    return false;

                //Deny Role List
                if (ProcessRoleWhiteList(Area, Controller, Action, authUser.Roles, _whiteListPolicy.DenyWithRolePolicy))
                    return false;


                //Get DB List
                var authUserFull = GetFullCustomPrincipal();

                if (authUserFull != null)
                {
                    //DB White List
                    if (ProcessWhiteList(Area, Controller, Action, authUserFull.DBWhiteList))
                        return true;

                    //DB Deny White List
                    if (ProcessWhiteList(Area, Controller, Action, authUserFull.DBDenyWhiteList))
                        return false;
                }

                //Authorize With Role List All
                if (ProcessRoleWhiteList(Area, Controller, Action, authUser.Roles, _whiteListPolicy.AuthorizeWithRolePolicy))
                    return true;

                //Authorize White List All
                if (ProcessWhiteList(Area, Controller, Action, _whiteListPolicy.Authorize))
                    return true;

                //To Ignore all other action controller which are not required any check
                if (_whiteListPolicy.IgnoreOtherWhenAuthorize)
                    return true;
            }

            return false;
        }

        public static CustomPrincipal GetFullCustomPrincipal()
        {
            CustomPrincipalSerializeModel serializeModel = System.Web.HttpContext.Current.Session["USER_SEC_INFO"] as CustomPrincipalSerializeModel;

            if (serializeModel == null) return null;

            CustomPrincipal newUser = new CustomPrincipal(serializeModel.UserName);

            newUser.UserId = serializeModel.UserId;
            newUser.UserName = serializeModel.UserName;
            newUser.FirstName = serializeModel.FirstName;
            newUser.LastName = serializeModel.LastName;
            newUser.Email = serializeModel.Email;
            newUser.CustomerId = serializeModel.CustomerId;
            newUser.Address = serializeModel.Address;
            newUser.City = serializeModel.City;
            newUser.State = serializeModel.State;
            newUser.PostalCode = serializeModel.PostalCode;
            newUser.PhoneNumber = serializeModel.PhoneNumber;
            newUser.AutoPlay = serializeModel.AutoPlay;
            newUser.HoursDuration = serializeModel.HoursDuration;
            newUser.HoursAgo = serializeModel.HoursAgo;
            newUser.MinTransmissionTime = serializeModel.MinTransmissionTime;
            newUser.DBWhiteList = new List<IWhiteListItem>();
            newUser.DBDenyWhiteList = new List<IWhiteListItem>();
            newUser.Roles = serializeModel.RolePolicies;
            newUser.AuthenticationMode = serializeModel.AuthenticationMode;
            newUser.IsADAuthentication = serializeModel.IsADAuthentication;
            newUser.IsTwoWayAuthentication = serializeModel.IsTwoWayAuthentication;
            newUser.OTP = serializeModel.OTP;
            newUser.OTPTime = serializeModel.OTPTime;
            newUser.AccessRoleLevel = serializeModel.AccessRoleLevel;
            newUser.AccessRoleKey = serializeModel.AccessRoleKey;
            newUser.AccessRoleName = serializeModel.AccessRoleName;
            newUser.AccessPrivilegeIds = serializeModel.AccessPrivilegeIds;
            newUser.CustomerAccessPrivilegeIds = serializeModel.CustomerAccessPrivilegeIds;
            newUser.CanShowtheLogOff = serializeModel.CanShowtheLogOff;
            newUser.IsSystemADAuthentication = serializeModel.IsSystemADAuthentication;
            newUser.UserRoles = serializeModel.UserRoles;
            newUser.ObjTenant = serializeModel.ObjTenant;
            newUser.UserRadioIDs = serializeModel.UserRadioIDs;
            newUser.UserTalkGroupIDs = serializeModel.UserTalkGroupIDs;
            newUser.DefaultAutoplaySetting = serializeModel.DefaultAutoplaySetting;

            //if (serializeModel.CustomerAccessPrivilegeNames != null && serializeModel.CustomerAccessPrivilegeNames.Count > 0)
            //{
            //    newUser.CustomerScanListenOnlyPermission = serializeModel.CustomerAccessPrivilegeNames.Contains(Constant.SCANLISTENONLY_PERMISSION.ToString().ToLower().Trim());              
            //}
            return newUser;
        }

        private static bool ProcessWhiteList(string Area, string Controller, string Action, List<IWhiteListItem> whiteList)
        {
            if (string.IsNullOrEmpty(Area))
            {
                if (whiteList != null && whiteList.Count > 0)
                {
                    var checkOnlyAtControllerLevel = (from w in whiteList
                                                      where string.IsNullOrEmpty(w.Controller) == false &&
                                                      w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim() &&
                                                      w.AllowOnControllerLevel == true
                                                      select w).Any();

                    if (checkOnlyAtControllerLevel)
                    {
                        return true;
                    }

                    var checkAtControllerActionLevel = (from w in whiteList
                                                        where string.IsNullOrEmpty(w.Controller) == false &&
                                                        string.IsNullOrEmpty(w.Action) == false &&
                                                        w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim()
                                                        && w.Action.ToUpper().Trim() == Action.ToUpper().Trim() &&
                                                        w.AllowOnControllerLevel == false
                                                        select w).Any();

                    if (checkAtControllerActionLevel)
                    {
                        return true;
                    }
                }
            }
            else
            {
                if (whiteList != null && whiteList.Count > 0)
                {
                    var checkOnlyAtControllerLevel = (from w in whiteList
                                                      where string.IsNullOrEmpty(w.Area) == false &&
                                                      string.IsNullOrEmpty(w.Controller) == false &&
                                                      w.Area.ToUpper().Trim() == Area.ToUpper().Trim() &&
                                                      w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim() &&
                                                      w.AllowOnControllerLevel == true
                                                      select w).Any();

                    if (checkOnlyAtControllerLevel)
                    {
                        return true;
                    }

                    var checkAtControllerActionLevel = (from w in whiteList
                                                        where string.IsNullOrEmpty(w.Area) == false &&
                                                        string.IsNullOrEmpty(w.Controller) == false &&
                                                        string.IsNullOrEmpty(w.Action) == false &&
                                                        w.Area.ToUpper().Trim() == Area.ToUpper().Trim() &&
                                                        w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim() &&
                                                        w.Action.ToUpper().Trim() == Action.ToUpper().Trim()
                                                        select w).Any();

                    if (checkAtControllerActionLevel)
                    {
                        return true;
                    }
                }

            }

            return false;
        }

        private static bool ProcessRoleWhiteList(string Area, string Controller, string Action, List<RolePolicy> userRoles, List<IWhiteListRoleItem> whiteList)
        {
            if (string.IsNullOrEmpty(Area))
            {
                if (whiteList != null && whiteList.Count > 0)
                {
                    var checkOnlyAtControllerLevel = (from w in whiteList
                                                      where string.IsNullOrEmpty(w.Controller) == false &&
                                                      IsUserRole(userRoles, w.Roles) == true &&
                                                      w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim() &&
                                                      w.AllowOnControllerLevel == true
                                                      select w).Any();

                    if (checkOnlyAtControllerLevel)
                    {
                        return true;
                    }

                    var checkAtControllerActionLevel = (from w in whiteList
                                                        where string.IsNullOrEmpty(w.Controller) == false &&
                                                        IsUserRole(userRoles, w.Roles) == true &&
                                                        string.IsNullOrEmpty(w.Action) == false &&
                                                        w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim()
                                                        && w.Action.ToUpper().Trim() == Action.ToUpper().Trim() &&
                                                        w.AllowOnControllerLevel == false
                                                        select w).Any();

                    if (checkAtControllerActionLevel)
                    {
                        return true;
                    }
                }
            }
            else
            {
                if (whiteList != null && whiteList.Count > 0)
                {
                    var checkOnlyAtControllerLevel = (from w in whiteList
                                                      where string.IsNullOrEmpty(w.Area) == false &&
                                                      IsUserRole(userRoles, w.Roles) == true &&
                                                      string.IsNullOrEmpty(w.Controller) == false &&
                                                      w.Area.ToUpper().Trim() == Area.ToUpper().Trim() &&
                                                      w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim() &&
                                                      w.AllowOnControllerLevel == true
                                                      select w).Any();

                    if (checkOnlyAtControllerLevel)
                    {
                        return true;
                    }

                    var checkAtControllerActionLevel = (from w in whiteList
                                                        where string.IsNullOrEmpty(w.Area) == false &&
                                                        IsUserRole(userRoles, w.Roles) == true &&
                                                        string.IsNullOrEmpty(w.Controller) == false &&
                                                        string.IsNullOrEmpty(w.Action) == false &&
                                                        w.Area.ToUpper().Trim() == Area.ToUpper().Trim() &&
                                                        w.Controller.ToUpper().Trim() == Controller.ToUpper().Trim() &&
                                                        w.Action.ToUpper().Trim() == Action.ToUpper().Trim()
                                                        select w).Any();

                    if (checkAtControllerActionLevel)
                    {
                        return true;
                    }
                }

            }

            return false;
        }

        private static bool IsUserRole(List<RolePolicy> userRoles, List<RolePolicy> policyRoles)
        {
            if (userRoles == null)
                return false;

            try
            {
                foreach (var policyRole in policyRoles)
                {
                    switch (policyRole.MatchPolicy)
                    {
                        case Policy.MatchByRoleName:
                            if ((from r in userRoles
                                 where string.IsNullOrEmpty(r.RoleName) == false && r.RoleName.Trim().ToUpper() == policyRole.RoleName.Trim().ToUpper()
                                 select r).Any() == true)
                                return true;
                            break;
                        case Policy.MatchByRoleKey:
                            if ((from r in userRoles
                                 where string.IsNullOrEmpty(r.RoleKey) == false && r.RoleKey.Trim().ToUpper() == policyRole.RoleKey.Trim().ToUpper()
                                 select r).Any() == true)
                                return true;
                            break;
                        case Policy.MatchByPrevilegeName:
                            if ((from r in userRoles
                                 where string.IsNullOrEmpty(r.PrevilegeName) == false && r.PrevilegeName.Trim().ToUpper() == policyRole.PrevilegeName.Trim().ToUpper()
                                 select r).Any() == true)
                                return true;
                            break;
                        case Policy.MatchByPrevilegeKey:
                            if ((from r in userRoles
                                 where string.IsNullOrEmpty(r.PrevilegeKey) == false && r.PrevilegeKey.Trim().ToUpper() == policyRole.PrevilegeKey.Trim().ToUpper()
                                 select r).Any() == true)
                                return true;
                            break;
                        case Policy.MatchByRoleId:
                            if ((from r in userRoles
                                 where r.RoleId == policyRole.RoleId
                                 select r).Any() == true)
                                return true;
                            break;
                        case Policy.MatchByRoleLevel:
                            if ((from r in userRoles
                                 where r.RoleLevel == policyRole.RoleLevel
                                 select r).Any() == true)
                                return true;
                            break;
                        case Policy.MatchByPrevilegeId:
                            if ((from r in userRoles
                                 where r.PrevilegeId == policyRole.PrevilegeId
                                 select r).Any() == true)
                                return true;
                            break;
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
