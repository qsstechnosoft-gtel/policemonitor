﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PoliceMonitor.Security.Principal;

namespace PoliceMonitor.Security.Attributes
{
    // Summary:
    //     Represents an attribute that is used to restrict access by callers to an
    //     action method.
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public string[] UserRoles { get; set; }

        // Summary:
        //     Initializes a new instance of the PoliceMonitor.Security.AuthAttribute class.
        public CustomAuthorizeAttribute(params string[] UserRolesRequired)
        {
            this.UserRoles = UserRolesRequired;
        }

        //
        // Summary:
        //     Called when a process requests authorization.
        //
        // Parameters:
        //   filterContext:
        //     The filter context, which encapsulates information for using System.Web.Mvc.AuthorizeAttribute.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     The filterContext parameter is null.
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool _authorized = false;
            foreach (var item in this.UserRoles)
                if (HttpContext.Current.User.IsInRole(item.ToString()))
                {
                    _authorized = true;
                    break;
                }
            if (!_authorized)
            {
                //System.Web.Security.FormsAuthentication.SignOut();
                //var url = new UrlHelper(filterContext.RequestContext);
                //var logonUrl = url.Action("Login", "Account", new { ID = 401, Message = "UnAuthorizeAccess" });
                //filterContext.Result = new RedirectResult(logonUrl);
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                return;
            }
        }
    }
}