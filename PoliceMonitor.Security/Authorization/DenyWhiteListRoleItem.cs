﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliceMonitor.Security.Authorization
{
    class DenyWhiteListRoleItem : IWhiteListRoleItem
    {
        public DenyWhiteListRoleItem()
        {
            this.Roles = new List<RolePolicy>();
        }

        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public bool AllowOnControllerLevel { get; set; }
        public List<RolePolicy> Roles { get; set; }
    }
}
