﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliceMonitor.Security.Authorization
{
    public enum Policy
    {
        MatchByRoleName = 1,
        MatchByRoleId = 2,
        MatchByRoleKey = 3,
        MatchByPrevilegeId = 4,
        MatchByPrevilegeKey = 5,
        MatchByPrevilegeName = 6,
        MatchByRoleLevel = 7
    }

    public class RolePolicy
    {
        public RolePolicy()
        {
            this.MatchPolicy = Policy.MatchByRoleName;
        }

        public int PrevilegeId { get; set; }
        public Guid RoleId { get; set; }
        public int RoleLevel { get; set; }
        public string RoleName { get; set; }
        public string PrevilegeName { get; set; }
        public string RoleKey { get; set; }
        public string PrevilegeKey { get; set; }
        public Policy MatchPolicy { get; set; }
    }


    public class Role
    {
        public int RoleId { get; set; }
        public string RoleKey { get; set; }
        public string RoleName { get; set; }
        public int RoleLevel { get; set; }
    }
}
