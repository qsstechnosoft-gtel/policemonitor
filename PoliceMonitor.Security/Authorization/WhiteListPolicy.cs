﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliceMonitor.Security.Authorization
{
    public sealed class WhiteListPolicy
    {
        private List<IWhiteListItem> _Anonymous;
        private List<IWhiteListItem> _Authorize;
        private List<IWhiteListItem> _Deny;
        private List<IWhiteListRoleItem> _AuthorizeWithRolePolicy;
        private List<IWhiteListRoleItem> _DenyWithRolePolicy;

        public WhiteListPolicy()
        {
            _Anonymous = new List<IWhiteListItem>();
            _Authorize = new List<IWhiteListItem>();
            _Deny = new List<IWhiteListItem>();
            _AuthorizeWithRolePolicy = new List<IWhiteListRoleItem>();
            _DenyWithRolePolicy = new List<IWhiteListRoleItem>();
            this.IgnoreOtherWhenAuthorize = false;
        }

        internal List<IWhiteListItem> Anonymous { get { return _Anonymous; } }
        internal List<IWhiteListItem> Authorize { get { return _Authorize; } }
        internal List<IWhiteListItem> Deny { get { return _Deny; } }
        public bool IgnoreOtherWhenAuthorize { get; set; }
        internal List<IWhiteListRoleItem> AuthorizeWithRolePolicy { get { return _AuthorizeWithRolePolicy; } }
        internal List<IWhiteListRoleItem> DenyWithRolePolicy { get { return _DenyWithRolePolicy; } }

        public void AnonymousAccessForAllAction(string controller) { _Anonymous.Add(new AnonymousWhiteListItem() { Controller = controller, AllowOnControllerLevel = true }); }
        public void AnonymousAccessForAllActionInArea(string area, string controller) { _Anonymous.Add(new AnonymousWhiteListItem() { Area = area, Controller = controller, AllowOnControllerLevel = true }); }
        public void AnonymousAccessForAction(string controller, string action) { _Anonymous.Add(new AnonymousWhiteListItem() { Controller = controller, Action = action }); }
        public void AnonymousAccessForActionInArea(string area, string controller, string action) { this.Anonymous.Add(new AnonymousWhiteListItem() { Area = area, Controller = controller, Action = action }); }

        public void AuthorizeAccessForAllAction(string controller) { _Authorize.Add(new AuthorizeWhiteListItem() { Controller = controller, AllowOnControllerLevel = true }); }
        public void AuthorizeAccessForAllActionInArea(string area, string controller) { _Authorize.Add(new AuthorizeWhiteListItem() { Area = area, Controller = controller, AllowOnControllerLevel = true }); }
        public void AuthorizeAccessForAction(string controller, string action) { _Authorize.Add(new AuthorizeWhiteListItem() { Controller = controller, Action = action }); }
        public void AuthorizeAccessForActionInArea(string area, string controller, string action) { _Authorize.Add(new AuthorizeWhiteListItem() { Area = area, Controller = controller, Action = action }); }

        public void DenyAccessForAllAction(string controller) { _Deny.Add(new DenyeWhiteListItem() { Controller = controller, AllowOnControllerLevel = true }); }
        public void DenyAccessForAllActionInArea(string area, string controller) { _Deny.Add(new DenyeWhiteListItem() { Area = area, Controller = controller, AllowOnControllerLevel = true }); }
        public void DenyAccessForAction(string controller, string action) { _Deny.Add(new DenyeWhiteListItem() { Controller = controller, Action = action }); }
        public void DenyAccessForActionInArea(string area, string controller, string action) { _Deny.Add(new DenyeWhiteListItem() { Area = area, Controller = controller, Action = action }); }


        /// <summary>
        /// For role/previlege base access
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void AuthorizeAccessToRoleForAllAction(string controller, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _AuthorizeWithRolePolicy.Add(new AuthorizeWhiteListRoleItem() { Controller = controller, AllowOnControllerLevel = true, Roles = ConvertInToRoles(values, rolePolicy) });
        }
        /// <summary>
        /// For role/previlege base access
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void AuthorizeAccessToRoleForAllActionInArea(string area, string controller, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _AuthorizeWithRolePolicy.Add(new AuthorizeWhiteListRoleItem() { Area = area, Controller = controller, AllowOnControllerLevel = true, Roles = ConvertInToRoles(values, rolePolicy) });
        }
        /// <summary>
        /// For role/previlege base access
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void AuthorizeAccessToRoleForAction(string controller, string action, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _AuthorizeWithRolePolicy.Add(new AuthorizeWhiteListRoleItem() { Controller = controller, Action = action, Roles = ConvertInToRoles(values, rolePolicy) });
        }
        /// <summary>
        /// For role/previlege base access
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void AuthorizeAccessToRoleForActionInArea(string area, string controller, string action, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _AuthorizeWithRolePolicy.Add(new AuthorizeWhiteListRoleItem() { Area = area, Controller = controller, Action = action, Roles = ConvertInToRoles(values, rolePolicy) });
        }


        /// <summary>
        /// For role/previlege base deny
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void DenyAccessToRoleForAllAction(string controller, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _DenyWithRolePolicy.Add(new DenyWhiteListRoleItem() { Controller = controller, AllowOnControllerLevel = true, Roles = ConvertInToRoles(values, rolePolicy) });
        }
        /// <summary>
        /// For role/previlege base deny
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void DenyAccessToRoleForAllActionInArea(string area, string controller, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _DenyWithRolePolicy.Add(new DenyWhiteListRoleItem() { Area = area, Controller = controller, AllowOnControllerLevel = true, Roles = ConvertInToRoles(values, rolePolicy) });
        }
        /// <summary>
        /// For role/previlege base deny
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void DenyAccessToRoleForAction(string controller, string action, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _DenyWithRolePolicy.Add(new DenyWhiteListRoleItem() { Controller = controller, Action = action, Roles = ConvertInToRoles(values, rolePolicy) });
        }
        /// <summary>
        /// For role/previlege base deny
        /// </summary>
        /// <param name="controller">Name of controller</param>
        /// <param name="values">If values are multiple, input comma separated values</param>
        /// <param name="rolePolicy"></param>
        public void DenyAccessToRoleForActionInArea(string area, string controller, string action, string values, Policy rolePolicy)
        {
            if (!string.IsNullOrEmpty(values) && values.Trim().Length > 0)
                _DenyWithRolePolicy.Add(new DenyWhiteListRoleItem() { Area = area, Controller = controller, Action = action, Roles = ConvertInToRoles(values, rolePolicy) });
        }

        private List<RolePolicy> ConvertInToRoles(string values, Policy rolePolicy)
        {
            var valueArray = values.Split(new[] { ',' });
            List<RolePolicy> roles = new List<RolePolicy>();

            foreach (var value in valueArray)
            {
                if (string.IsNullOrEmpty(values) || values.Trim().Length <= 0) continue;

                RolePolicy role = new RolePolicy();

                switch (rolePolicy)
                {
                    case Policy.MatchByRoleName: role.RoleName = value; break;
                    case Policy.MatchByRoleId: role.RoleId = new Guid(Convert.ToString(value)); break;
                    case Policy.MatchByRoleKey: role.RoleKey = value; break;
                    case Policy.MatchByRoleLevel: role.RoleLevel = Convert.ToInt32(value); break;
                    case Policy.MatchByPrevilegeId: role.PrevilegeId = Convert.ToInt32(value); break;
                    case Policy.MatchByPrevilegeName: role.PrevilegeName = value; break;
                    case Policy.MatchByPrevilegeKey: role.PrevilegeKey = value; break;
                    default: role.RoleName = value; break;
                }

                role.MatchPolicy = rolePolicy;
                roles.Add(role);
            }

            return roles;
        }
    }
  
}
