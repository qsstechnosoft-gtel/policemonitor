﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliceMonitor.Security.Authorization
{
    public interface IWhiteListItem
    { 
        string Area { get; set; }
        string Controller { get; set; }
        string Action { get; set; }
        bool AllowOnControllerLevel { get; set; }
    }
}
