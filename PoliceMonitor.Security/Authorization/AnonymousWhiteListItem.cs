﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoliceMonitor.Security.Authorization
{
    class AnonymousWhiteListItem : IWhiteListItem
    {
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public bool AllowOnControllerLevel { get; set; }
    }
}
