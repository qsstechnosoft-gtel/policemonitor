﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Collections.Specialized;
using PoliceMonitor.Security.Principal;
using System.Configuration.Provider;
using PoliceMonitor.Security.Crypto;
using System.Configuration;
using PoliceMonitor.Entities;
using System.Linq;
using PoliceMonitor.Security.Base.Model;



namespace PoliceMonitor.Security.Provider
{
    public sealed class CentralMembershipProvider : MembershipProvider
    {

        #region Private Variable
               
        private string connectionString;
        private string pApplicationName;
        private bool pEnablePasswordReset;
        private bool pEnablePasswordRetrieval;
        private bool pRequiresQuestionAndAnswer;
        private bool pRequiresUniqueEmail;
        private int pMaxInvalidPasswordAttempts;
        private int pPasswordAttemptWindow;
        private MembershipPasswordFormat pPasswordFormat;
        private int pMinRequiredNonAlphanumericCharacters;
        private int pMinRequiredPasswordLength;
        private bool pWriteExceptionsToEventLog;
        private string pPasswordStrengthRegularExpression;
        private bool _IsADauthentication;
        private string _LdapPath;
        private string _EncryptionPrivateKey;
        private string _EntityConnectionString;
        private bool _IsTwoWayAuthRequired;
        private string _OTPString;
        private double _OTPValidityDurationInMin;
        private string _Domain;
        private string _EmailFrom;

        #endregion

        #region Initialize

        public override void Initialize(string name, NameValueCollection config)
        {
            //
            // Initialize values from web.config.
            //

            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "CentralMembershipProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Custom Membership provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            pApplicationName = GetConfigValue(config["applicationName"],
                                            System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            pMaxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            pPasswordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            pMinRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            pMinRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            pPasswordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            pEnablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            pEnablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            pRequiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            pRequiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));
            pWriteExceptionsToEventLog = Convert.ToBoolean(GetConfigValue(config["writeExceptionsToEventLog"], "true"));
            _IsADauthentication = Convert.ToBoolean(GetConfigValue(config["IsADauthentication"], "false"));
            _IsTwoWayAuthRequired = Convert.ToBoolean(GetConfigValue(config["IsTwoWayAuthRequired"], "false"));
            _LdapPath = Convert.ToString(GetConfigValue(config["LdapPath"], ""));
            _Domain = Convert.ToString(GetConfigValue(config["Domain"], ""));
            _EncryptionPrivateKey = Convert.ToString(GetConfigValue(config["EncryptionPrivateKey"], ""));
            _OTPValidityDurationInMin = Convert.ToDouble(GetConfigValue(config["OTPValidityDurationInMin"], "5"));
            _EmailFrom = Convert.ToString(GetConfigValue(config["EmailFrom"], ""));

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Hashed";
            }

            switch (temp_format)
            {
                case "Hashed":
                    pPasswordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    pPasswordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    pPasswordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }

            //
            // Initialize OdbcConnection.
            //

            ConnectionStringSettings ConnectionStringSettings =
              ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            connectionString = ConnectionStringSettings.ConnectionString;

            _EntityConnectionString = string.Format("metadata=res://*/Entity.Security.csdl|res://*/Entity.Security.ssdl|res://*/Entity.Security.msl;provider=System.Data.SqlClient;provider connection string=\"{0}\"", connectionString);

        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        #endregion

        #region Web Config Properties

        public override string ApplicationName
        {
            get { return pApplicationName; }
            set { pApplicationName = value; }
        }

        public bool WriteExceptionsToEventLog
        {
            get { return pWriteExceptionsToEventLog; }
            set { pWriteExceptionsToEventLog = value; }
        }

        public override bool EnablePasswordReset
        {
            get { return pEnablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return pEnablePasswordRetrieval; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return pRequiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return pRequiresUniqueEmail; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return pMaxInvalidPasswordAttempts; }
        }

        public override int PasswordAttemptWindow
        {
            get { return pPasswordAttemptWindow; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return pPasswordFormat; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return pMinRequiredNonAlphanumericCharacters; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return pMinRequiredPasswordLength; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return pPasswordStrengthRegularExpression; }
        }
        #endregion

        #region Membership Methods

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            using (PoliceMonitorDBEntities _context = new PoliceMonitorDBEntities())
            {
                var user = (from u in _context.Users
                            where u.UserName.Trim().ToUpper() == username.Trim().ToUpper()
                            select u).FirstOrDefault();

                if (user != null)
                {
                    //CryptoMgr crypto = new CryptoMgr(CryptoMgr.CryptoTypes.encTypeDES);
                    var encrptedPassword = PoliceMonitor.Security.Crypto.AESEncrytDecry.EncryptStringAES(oldPassword);//crypto.Encrypt(oldPassword, this.EncryptionPrivateKey);
                    //encrptedPassword = oldPassword;
                    if (encrptedPassword == user.Password)
                    {
                        var encryptedNewPwd = PoliceMonitor.Security.Crypto.AESEncrytDecry.EncryptStringAES(newPassword); //crypto.Encrypt(newPassword, this.EncryptionPrivateKey);

                        user.Password = encryptedNewPwd;
                        //user.Password = newPassword;
                        _context.SaveChanges();

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            return (new MembershipUser("", username, "", "", "", "", true, true, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now));
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }
        
        public override bool ValidateUser(string username, string password)
        {
            using(PoliceMonitorDBEntities context = new PoliceMonitorDBEntities())
            {
                bool isValidUser = false;

                var user = (from u in context.Users where u.UserName.Trim().ToUpper() == username.Trim().ToUpper() && u.Active == true
                            select u).FirstOrDefault();
                if (user == null)
                {
                    user = (from u in context.Users
                            where u.Email.Trim().ToUpper() == username.Trim().ToUpper() && u.Active == true
                            select u).FirstOrDefault();
                }
                if (user != null)
                {
                    if(user.CustomerId==0)
                    {
                        isValidUser = true;
                    }
                    else
                    {
                        isValidUser = context.Customers.Where(cust => cust.CustomerId == user.CustomerId && cust.Active == true).Any();
                    }
                    if (isValidUser)
                     {
                         //CryptoMgr crypto = new CryptoMgr(CryptoMgr.CryptoTypes.encTypeDES);
                         //var encrptedPassword = crypto.Encrypt(password, this.EncryptionPrivateKey);
                         var encrptedPassword = password;
                         isValidUser = (from u in context.Users
                                        where u.UserName.Trim().ToUpper() == user.UserName.Trim().ToUpper()
                                        && u.Active == true
                                        && u.Password == encrptedPassword
                                        select u).Any();
                     }
                    if(!isValidUser)
                    {
                        var DecryptPassword = PoliceMonitor.Security.Crypto.AESEncrytDecry.DecryptStringAES( password);
                        isValidUser = (from u in context.Users
                                       where u.UserName.Trim().ToUpper() == user.UserName.Trim().ToUpper()
                                       && u.Active == true
                                       && u.Password == DecryptPassword
                                       select u).Any();

                        if (isValidUser)
                        {
                            user.Password = password;
                            context.SaveChanges();
                        }
 
                    }
                }
                else
                {
                    isValidUser = false;
                }

                return isValidUser;
            }
        }

        #endregion

        public string EncryptionPrivateKey
        {
            get
            {
                return _EncryptionPrivateKey;
            }
        }

        public bool IsADauthentication
        {
            get
            {
                return _IsADauthentication;
            }
        }

        public string LDAPPath
        {
            get
            {

                return _LdapPath;
            }
        }

        #region Custom Methods

        public CustomPrincipalSerializeModel GetCustomPrincipalSerializeModel(string userName)
        {
            using (PoliceMonitorDBEntities _context = new PoliceMonitorDBEntities())
            {

                var serializeModel = (from user in _context.Users
                                      where user.UserName == userName && user.Active == true
                                      select new CustomPrincipalSerializeModel()
                                      {
                                          UserId = user.UserId,
                                          UserName = user.UserName,
                                          FirstName = user.FirstName,
                                          LastName = user.LastName,
                                          Email = user.Email,
                                          CustomerId = user.CustomerId,
                                          DefaultAutoplaySetting = _context.Customers.Where(x => x.CustomerId == user.CustomerId).Select(cust => cust.DefaultAutoplaySetting.Value).FirstOrDefault(),
                                          Address = user.Address,
                                          City = user.City,
                                          State = user.State,
                                          PostalCode = user.PostalCode,
                                          HoursAgo = user.HoursAgo,
                                          HoursDuration = user.HoursDuration,
                                          MinTransmissionTime = user.MinTransmissionTime,
                                          AutoPlay = user.AutoPlay,
                                          PhoneNumber = user.PhoneNumber
                                      }).FirstOrDefault();

                if (serializeModel == null)
                    serializeModel = (from user in _context.Users
                                      where user.Email == userName && user.Active == true
                                      select new CustomPrincipalSerializeModel()
                                      {
                                          UserId = user.UserId,
                                          UserName = user.UserName,
                                          FirstName = user.FirstName,
                                          LastName = user.LastName,
                                          Email = user.Email,
                                          CustomerId = user.CustomerId,
                                          DefaultAutoplaySetting = _context.Customers.Where(x => x.CustomerId == user.CustomerId).Select(cust => cust.DefaultAutoplaySetting.Value).FirstOrDefault(),
                                          Address = user.Address,
                                          City = user.City,
                                          State = user.State,
                                          PostalCode = user.PostalCode,
                                          HoursAgo = user.HoursAgo,
                                          HoursDuration = user.HoursDuration,
                                          MinTransmissionTime = user.MinTransmissionTime,
                                          AutoPlay = user.AutoPlay,
                                          PhoneNumber = user.PhoneNumber
                                      }).FirstOrDefault();

                if (serializeModel != null)
                {
                    serializeModel.UserRoles = (from roles in _context.Roles
                                                join userRole in _context.UserInRoles on roles.RoleId equals userRole.RoleId
                                                where userRole.UserId == serializeModel.UserId
                                                select new PoliceMonitor.Security.Authorization.Role()
                                                {
                                                    RoleId = roles.RoleId,
                                                    RoleName = roles.Name.Replace("\r\n", string.Empty)
                                                }).ToList();

                }
                if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.SUPERADMIN_ROLENAME).Any())
                {
                    serializeModel.AccessPrivilegeIds = _context.Permissions.Select(x => x.PermissionId).ToList();
                }
                else if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.ADMIN_ROLENAME).Any())
                {
                    serializeModel.AccessPrivilegeIds = _context.CustomerPermissions.Where(x => x.CustomerId == serializeModel.CustomerId).Select(x => x.Permission.PermissionId).ToList();
                }
                else if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.USER_ROLENAME).Any())
                {
                    List<int> customerAccessPrivilegeIds = _context.CustomerPermissions.Where(x => x.CustomerId == serializeModel.CustomerId).Select(x => x.Permission.PermissionId).ToList();
                    List<int> userAccessPrivilegeIds = _context.UserPermissions.Where(x => x.UserId == serializeModel.UserId).Select(x => x.Permission.PermissionId).ToList();
                    serializeModel.AccessPrivilegeIds = new List<int>();
                    foreach(int item in userAccessPrivilegeIds)
                    {
                        if(customerAccessPrivilegeIds.Contains(item))
                        {
                            serializeModel.AccessPrivilegeIds.Add(item);
                        }
                    }
                }
                return serializeModel;
            }
        }

        public CustomPrincipalSerializeModel GetCustomPrincipalSerializeModelWithAllData(string userName)
        {
            //Call Database
            using (PoliceMonitorDBEntities _context = new PoliceMonitorDBEntities())
            {
                var serializeModel = (from user in _context.Users
                                      where (user.UserName == userName || user.Email == userName) && user.Active == true
                                      select new CustomPrincipalSerializeModel()
                                      {
                                          UserId = user.UserId,
                                          UserName = user.UserName,
                                          FirstName = user.FirstName,
                                          LastName= user.LastName,
                                          Email = user.Email,
                                          CustomerId = user.CustomerId,
                                          DefaultAutoplaySetting=_context.Customers.Where(x=>x.CustomerId==user.CustomerId).Select(cust=>cust.DefaultAutoplaySetting.Value).FirstOrDefault(),
                                          Address = user.Address,
                                          City = user.City,
                                          State = user.State,
                                          PostalCode = user.PostalCode,
                                          HoursAgo = user.HoursAgo,
                                          HoursDuration = user.HoursDuration,
                                          MinTransmissionTime = user.MinTransmissionTime,
                                          AutoPlay = user.AutoPlay,
                                          PhoneNumber = user.PhoneNumber
                                      }).FirstOrDefault();


                if (serializeModel != null)
                {
                    serializeModel.UserRoles = (from roles in _context.Roles
                                                join userRole in _context.UserInRoles on roles.RoleId equals userRole.RoleId
                                                where userRole.UserId == serializeModel.UserId
                                                select new PoliceMonitor.Security.Authorization.Role()
                                                {
                                                    RoleId = roles.RoleId,
                                                    RoleName = roles.Name.Replace("\r\n", string.Empty)
                                                }).ToList();

                    List<int> roleIDs = new List<int>();
                    roleIDs = serializeModel.UserRoles.Select(p => p.RoleId).ToList();

                    if (serializeModel.UserRoles.Where(x=>x.RoleName== Constant.ADMIN_ROLENAME).Any())
                    {
                        serializeModel.UserTalkGroupIDs = _context.CustomerTalkGroups.Where(x => x.CustomerId == serializeModel.CustomerId)
                            .Select(x => x.TalkGroup).ToList().Select(y=>{
                             int value;
                             bool success = int.TryParse(y, out value);
                             return value;
                         }).ToList();
                    }
                    else if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.USER_ROLENAME).Any())
                    {
                        serializeModel.UserRadioIDs = _context.UserRadioMaps.Where(x => x.UserId == serializeModel.UserId).Select(x => x.RadioId).ToList();
                        serializeModel.UserTalkGroupIDs = _context.UserTalkGroups.Where(x => x.UserId == serializeModel.UserId).Select(x => x.TakGroupId).ToList();
                    }
                    else
                    {
                        serializeModel.IsSystemAdmin = true;
                    }

                    if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.SUPERADMIN_ROLENAME).Any())
                    {
                        serializeModel.AccessPrivilegeIds = _context.Permissions.Select(x => x.PermissionId).ToList();
                    }
                    else if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.ADMIN_ROLENAME).Any())
                    {
                        serializeModel.AccessPrivilegeIds = _context.CustomerPermissions.Where(x => x.CustomerId == serializeModel.CustomerId).Select(x => x.Permission.PermissionId).ToList();
                    }

                    //if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.USER_ROLENAME).Any() || serializeModel.UserRoles.Where(x => x.RoleName == Constant.ADMIN_ROLENAME).Any())
                    //{
                    //    serializeModel.CustomerAccessPrivilegeNames = _context.CustomerPermissions.Where(x => x.CustomerId == serializeModel.CustomerId).Select(x => x.Permission.Permission1.ToLower().Trim()).ToList();
                    //}
                    //if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.SUPERADMIN_ROLENAME).Any() || serializeModel.UserRoles.Where(x => x.RoleName == Constant.ADMIN_ROLENAME).Any())
                    //{
                     //   serializeModel.AccessPrivilegeIds = _context.Permissions.Select(x => x.PermissionId).ToList();
                    //}
                    else if (serializeModel.UserRoles.Where(x => x.RoleName == Constant.USER_ROLENAME).Any())
                    {
                        List<int> customerAccessPrivilegeIds = _context.CustomerPermissions.Where(x => x.CustomerId == serializeModel.CustomerId).Select(x => x.Permission.PermissionId).ToList();
                        List<int> userAccessPrivilegeIds = _context.UserPermissions.Where(x => x.UserId == serializeModel.UserId).Select(x => x.Permission.PermissionId).ToList();
                        serializeModel.AccessPrivilegeIds = new List<int>();
                        foreach (int item in userAccessPrivilegeIds)
                        {
                            if (customerAccessPrivilegeIds.Contains(item))
                            {
                                serializeModel.AccessPrivilegeIds.Add(item);
                            }
                        }
                    }
                }

                return serializeModel;
            }
        }


      
        public string GenerateOTP()
        {
            int randomNumber = (new Random()).Next(0, 1000000);
            _OTPString = randomNumber.ToString();

            CryptoMgr crypto = new CryptoMgr(CryptoMgr.CryptoTypes.encTypeDES);
            var encrptedPassword = crypto.Encrypt(_OTPString, this.EncryptionPrivateKey);

            return encrptedPassword;
        }

        public bool ValidateOTP(string otp)
        {
            try
            {
                var authUser = System.Web.HttpContext.Current.User as CustomPrincipal;

                if (authUser != null && authUser.Identity.IsAuthenticated)
                {
                    var otpSys = authUser.OTP;
                    var otpTimeSys = authUser.OTPTime;

                    if (string.IsNullOrEmpty(otpSys)) return false;
                    if (string.IsNullOrEmpty(otpTimeSys)) return false;

                    var otpTime = DateTime.Parse(otpTimeSys);

                    CryptoMgr crypto = new CryptoMgr(CryptoMgr.CryptoTypes.encTypeDES);
                    otp = crypto.Encrypt(otp, this.EncryptionPrivateKey);

                    if (otp == otpSys)
                    {
                        var current = DateTime.UtcNow;
                        TimeSpan diff = (current - otpTime);

                        var totalSeconds = diff.TotalSeconds;
                        Double comparingSec = (this.OTPValidityDurationInMin * 60);

                        if (totalSeconds <= comparingSec)
                        {
                            return true;
                        }
                    }
                }

                return false; ;
            }
            catch
            {
                return false;
            }
        }

        public string OTP
        {
            get
            {
                return _OTPString;
            }
        }

        #endregion

        public string Domain
        {
            get
            {
                return _Domain;
            }
        }

        public string EntityConnectionString
        {
            get
            {
                return _EntityConnectionString;
            }
        }

        public bool IsTwoWayAuthRequired
        {
            get
            {
                return _IsTwoWayAuthRequired;
            }
        }

        public double OTPValidityDurationInMin
        {
            get
            {
                return _OTPValidityDurationInMin;
            }
        }

        public string EmailFrom
        {
            get
            {
                return _EmailFrom;
            }
        }

    }
}
