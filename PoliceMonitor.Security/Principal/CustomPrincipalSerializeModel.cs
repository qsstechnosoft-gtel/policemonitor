﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceMonitor.Security.Authorization;
using PoliceMonitor.Security.Base.Model;

namespace PoliceMonitor.Security.Principal
{
  
    public sealed class CustomPrincipalSerializeModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CustomerId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool? AutoPlay { get; set; }
        public int? HoursAgo { get; set; }
        public int? HoursDuration { get; set; }
        public int? MinTransmissionTime { get; set; }

        public List<RolePolicy> RolePolicies { get; set; }
        public List<Role> UserRoles { get; set; }
        public List<DBWhiteListItem> DBWhiteList { get; set; }
        public List<DBWhiteListItem> DBDenyWhiteList { get; set; }
        public short AuthenticationMode { get; set; }
        public bool IsADAuthentication { get; set; }
        public bool IsTwoWayAuthentication { get; set; }
        public int AccessRoleLevel { get; set; }
        public string AccessRoleKey { get; set; }
        public string AccessRoleName { get; set; }
        public List<int> AccessPrivilegeIds { get; set; }
        public List<int> CustomerAccessPrivilegeIds { get; set; }
        public string OTP { get; set; }
        public string OTPTime { get; set; }
        public string EmailFrom { get; set; }
        public bool CanShowtheLogOff { get; set; }
        public int DefaultMenuID { get; set; }
        public string DefaultController { get; set; }
        public string DefaultAction { get; set; }
        public bool IsSystemAdmin { get; set; }
        public bool IsSystemADAuthentication { get; set; }
        public Tenant ObjTenant { get; set; }
        public List<int> UserTalkGroupIDs { get; set; }
        public List<int> UserRadioIDs { get; set; }
        public bool DefaultAutoplaySetting { get; set; }
        //public List<string> CustomerAccessPrivilegeNames { get; set; }
        //public List<int> CustomerAccessPrivilegeIds { get; set; }
    }
}
