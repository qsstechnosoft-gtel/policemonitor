﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using PoliceMonitor.Security.Authorization;
using PoliceMonitor.Security.Base.Model;

namespace PoliceMonitor.Security.Principal
{
    public sealed class CustomPrincipal : ICustomPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string role)
        {
            if(this.UserRoles.Where(x=>x.RoleName.ToUpper() == role.ToUpper()).Any())
            {
                return true;
            }

            return false;
        }

        public bool IsInRoleLevel(int role)
        {
            if (role <= this.AccessRoleLevel)
            {
                return true;
            }

            return false;
        }

        public bool IsInRoleByKey(string roleKey)
        {
            if (roleKey == this.AccessRoleKey)
            {
                return true;
            }

            return false;
        }


        public CustomPrincipal(string userName)
        {
            this.UserName = userName;
            this.Identity = new GenericIdentity(userName, "SQL");
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CustomerId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public bool? AutoPlay { get; set; }
        public int? HoursAgo { get; set; }
        public int? HoursDuration { get; set; }
        public int? MinTransmissionTime { get; set; }

        public short AuthenticationMode { get; set; }
        public List<RolePolicy> Roles { get; set; }
        public string Email { get; set; }
        public List<IWhiteListItem> DBWhiteList { get; set; }
        public List<IWhiteListItem> DBDenyWhiteList { get; set; }
        public bool IsADAuthentication { get; set; }
        public bool IsTwoWayAuthentication { get; set; }
        public int AccessRoleLevel { get; set; }
        public string AccessRoleKey { get; set; }
        public string AccessRoleName { get; set; }
        public List<int> AccessPrivilegeIds { get; set; }
        public string OTPTime { get; set; }
        public string OTP { get; set; }
        public List<Role> UserRoles { get; set; }
        public bool CanShowtheLogOff { get; set; }
        public int DefaultMenuID { get; set; }
        public string DefaultController { get; set; }
        public string DefaultAction { get; set; }
        public bool IsSystemAdmin { get; set; }
        public bool IsSystemADAuthentication { get; set; }
        public Tenant ObjTenant { get; set; }
        public List<int> UserTalkGroupIDs { get; set; }
        public List<int> UserRadioIDs { get; set; }

        public bool DefaultAutoplaySetting { get; set; }
        public List<int> CustomerAccessPrivilegeIds { get; set; }
        //public List<string> CustomerAccessPrivilegeNames { get; set; }
        //public bool CustomerScanListenOnlyPermission { get; set; }

        public bool HasPermission(Permission p)
        {
            return AccessPrivilegeIds.Contains(p.id);
        }

        public bool HasCustomerPermission(Permission p)
        {
            return CustomerAccessPrivilegeIds.Contains(p.id);
        }
    }
}
