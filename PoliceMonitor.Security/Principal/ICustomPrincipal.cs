﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PoliceMonitor.Security.Authorization;
using System.Security.Principal;
using PoliceMonitor.Security.Base.Model;

namespace PoliceMonitor.Security.Principal
{    

    public interface ICustomPrincipal : IPrincipal
    {
        int UserId { get; set; }
        bool IsADAuthentication { get; set; }
        bool IsTwoWayAuthentication { get; set; }
        string UserName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        short AuthenticationMode { get; set; }
        string Email { get; set; }
        int AccessRoleLevel { get; set; }
        string AccessRoleKey { get; set; }
        string AccessRoleName { get; set; }
        string OTP { get; set; }
        string OTPTime { get; set; }
        List<int> AccessPrivilegeIds { get; set; }
        List<RolePolicy> Roles { get; set; }
        List<IWhiteListItem> DBWhiteList { get; set; }
        List<IWhiteListItem> DBDenyWhiteList { get; set; }
        List<Role> UserRoles { get; set; }
        bool CanShowtheLogOff { get; set; }
        int DefaultMenuID { get; set; }
        string DefaultController { get; set; }
        string DefaultAction { get; set; }
        bool IsSystemAdmin { get; set; }
        
    }
}
