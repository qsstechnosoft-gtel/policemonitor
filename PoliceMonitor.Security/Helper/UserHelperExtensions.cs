﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PoliceMonitor.Security.Principal;
using PoliceMonitor.Security.Authorization;

namespace PoliceMonitor.Security.Helper
{
  
    public static class UserHelperExtensions
    {
        public static bool IsInRole(this UserHelper html, string roleName)
        {
            var user = HttpContext.Current.User as CustomPrincipal;

            if (user != null)
            {
                return (PageAccessManager.GetFullCustomPrincipal()).IsInRole(roleName);
            }

            return false;
        }

        public static bool IsInRoleLevel(this UserHelper html, int roleLevel)
        {
            var user = HttpContext.Current.User as CustomPrincipal;

            if (user != null)
            {
                return (PageAccessManager.GetFullCustomPrincipal()).IsInRoleLevel(roleLevel);
            }

            return false;
        }

        public static bool IsInRoleKey(this UserHelper html, string roleKey)
        {
            var user = HttpContext.Current.User as CustomPrincipal;

            if (user != null)
            {
                return (PageAccessManager.GetFullCustomPrincipal()).IsInRoleByKey(roleKey);
            }

            return false;
        }

        public static bool IsAuthenticated(this UserHelper html)
        {
            var user = HttpContext.Current.User as CustomPrincipal;

            if (user != null)
            {
                return (PageAccessManager.GetFullCustomPrincipal()).Identity.IsAuthenticated;
            }

            return false;
        }
    }
}
