﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using PoliceMonitor.Security.Principal;
using System.Web;
using PoliceMonitor.Security.Authorization;

namespace PoliceMonitor.Security.Helper
{
  
    public class UserHelper
    {
        public UserHelper(ViewContext viewContext, IViewDataContainer viewDataContainer)
            : this(viewContext, viewDataContainer, RouteTable.Routes)
        {
        }

        public UserHelper(ViewContext viewContext, IViewDataContainer viewDataContainer, RouteCollection routeCollection)
        {
            ViewContext = viewContext;
            ViewData = new ViewDataDictionary(viewDataContainer.ViewData);
        }

        public ViewDataDictionary ViewData
        {
            get;
            private set;
        }

        public ViewContext ViewContext
        {
            get;
            private set;
        }

        public CustomPrincipal Info
        {
            get
            {
                var user = HttpContext.Current.User as CustomPrincipal;

                if (user != null)
                {
                    //return user;
                    return PageAccessManager.GetFullCustomPrincipal();
                }

                return new CustomPrincipal("");
            }
        }
    }

    public class UserHelper<TModel> : UserHelper
    {
        public UserHelper(ViewContext viewContext, IViewDataContainer container)
            : this(viewContext, container, RouteTable.Routes)
        {
        }

        public UserHelper(ViewContext viewContext, IViewDataContainer container, RouteCollection routeCollection)
            : base(viewContext, container,
                routeCollection)
        {
            ViewData = new ViewDataDictionary<TModel>(container.ViewData);
        }

        public new ViewDataDictionary<TModel> ViewData
        {
            get;
            private set;
        }

    }
}
