USE [master]
GO
/****** Object:  Database [PoliceMonitorDB]    Script Date: 31/08/2016 3.08.07 PM ******/
CREATE DATABASE [PoliceMonitorDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PoliceMonitorDB', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PoliceMonitorDB.mdf' , SIZE = 7168KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PoliceMonitorDB_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PoliceMonitorDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PoliceMonitorDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PoliceMonitorDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PoliceMonitorDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [PoliceMonitorDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PoliceMonitorDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PoliceMonitorDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PoliceMonitorDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PoliceMonitorDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PoliceMonitorDB] SET  MULTI_USER 
GO
ALTER DATABASE [PoliceMonitorDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PoliceMonitorDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PoliceMonitorDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PoliceMonitorDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [PoliceMonitorDB]
GO
/****** Object:  Table [dbo].[Archive_PolledTalkGroupItem]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Archive_PolledTalkGroupItem](
	[ID] [bigint] NOT NULL,
	[TalkGroupItemId] [varchar](50) NOT NULL,
	[TransStartMSec] [bigint] NULL,
	[TransLengthMSec] [bigint] NULL,
	[RadioID] [bigint] NULL,
	[TalkGroupID] [bigint] NULL,
	[TransStartUTC] [datetime] NULL,
	[TransTimeDesc] [varchar](500) NOT NULL,
	[RadioDesc] [nvarchar](500) NULL,
	[ChannelName] [nvarchar](500) NULL,
 CONSTRAINT [PK_Archive_PolledTalkGroupItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [nvarchar](250) NOT NULL,
	[StreetAddress] [nvarchar](500) NULL,
	[City] [nvarchar](250) NULL,
	[State] [nvarchar](250) NULL,
	[ZipCode] [nvarchar](50) NULL,
	[ContactName] [nvarchar](250) NULL,
	[ContactPhoneNumber] [nvarchar](50) NULL,
	[UsersCount] [int] NULL,
	[UnlimitedUsers] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerNotificationLastScan]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerNotificationLastScan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[LastScanUniversalDateTime] [datetime] NULL,
 CONSTRAINT [PK_CustomerNotificationLastScan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerNotificationSetting]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerNotificationSetting](
	[CustomerNotificationId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[NotifiyTransmissionsCount] [int] NULL,
	[Interval] [int] NULL,
	[AfterFirstTransmissionWaitInterval] [int] NULL,
	[LastNSecTransmissionsCount] [int] NULL,
	[NSecInterval] [int] NULL,
	[AfterNSecTransmissionWaitInterval] [int] NULL,
 CONSTRAINT [PK_CustomerNotificationSetting] PRIMARY KEY CLUSTERED 
(
	[CustomerNotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerNotificationSettingNew]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerNotificationSettingNew](
	[CustomerNotificationId] [int] IDENTITY(1,1) NOT NULL,
	[NotificationName] [nvarchar](250) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[NotifiyTransmissionsCount] [int] NULL,
	[Interval] [int] NULL,
	[AfterFirstTransmissionWaitInterval] [int] NULL,
	[LastNSecTransmissionsCount] [int] NULL,
	[NSecInterval] [int] NULL,
	[AfterNSecTransmissionWaitInterval] [int] NULL,
 CONSTRAINT [PK_CustomerNotificationSettingNew] PRIMARY KEY CLUSTERED 
(
	[CustomerNotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerTalkGroup]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerTalkGroup](
	[CustTalkGroupId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[TalkGroup] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_CustomerTalkGroup] PRIMARY KEY CLUSTERED 
(
	[CustTalkGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Group]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group](
	[GroupId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[GroupName] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupTalkGroupMap]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupTalkGroupMap](
	[GroupTalkGroupMapId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[TakGroupId] [int] NOT NULL,
 CONSTRAINT [PK_GroupTalkGroupMap] PRIMARY KEY CLUSTERED 
(
	[GroupTalkGroupMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationLog]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NotificationLog](
	[NotificationLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[TalkGroupId] [int] NOT NULL,
	[NotificationMsg] [nvarchar](max) NOT NULL,
	[NotificationDate] [datetime] NOT NULL,
	[MP3File] [varbinary](max) NULL,
 CONSTRAINT [PK_NotificationLog] PRIMARY KEY CLUSTERED 
(
	[NotificationLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NotificationSettingGroupMap]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSettingGroupMap](
	[NotificationSettingGroupMapId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CustomerNotificationId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_NotificationSettingGroupMap] PRIMARY KEY CLUSTERED 
(
	[NotificationSettingGroupMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationSettingRadioMap]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSettingRadioMap](
	[NotificationSettingRadioMapId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CustomerNotificationId] [int] NOT NULL,
	[RadioId] [int] NOT NULL,
 CONSTRAINT [PK_NotificationSettingRadioMap] PRIMARY KEY CLUSTERED 
(
	[NotificationSettingRadioMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotificationSettingTalkGroup]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSettingTalkGroup](
	[NotificationSettingTalkGroupId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[CustomerNotificationId] [int] NOT NULL,
	[TakGroupId] [int] NOT NULL,
 CONSTRAINT [PK_NotificationSettingTalkGroup] PRIMARY KEY CLUSTERED 
(
	[NotificationSettingTalkGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[PermissionId] [int] IDENTITY(1,1) NOT NULL,
	[Permission] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PolledTalkGroupItem]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PolledTalkGroupItem](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TalkGroupItemId] [varchar](50) NOT NULL,
	[TransStartMSec] [bigint] NULL,
	[TransLengthMSec] [bigint] NULL,
	[RadioID] [bigint] NULL,
	[TalkGroupID] [bigint] NULL,
	[TransStartUTC] [datetime] NULL,
	[TransTimeDesc] [varchar](500) NOT NULL,
	[RadioDesc] [nvarchar](500) NULL,
	[ChannelName] [nvarchar](500) NULL,
 CONSTRAINT [PK_PolledTalkGroupItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TalkGroupItem]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TalkGroupItem](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TalkGroupItemId] [varchar](50) NOT NULL,
	[TransStartMSec] [bigint] NULL,
	[TransLengthMSec] [bigint] NULL,
	[RadioID] [bigint] NULL,
	[TalkGroupID] [bigint] NULL,
	[TransStartUTC] [datetime] NULL,
	[TransTimeDesc] [varchar](500) NOT NULL,
	[RadioDesc] [varchar](500) NULL,
	[ReviewStatus] [tinyint] NULL,
	[MarkListen] [bit] NOT NULL,
 CONSTRAINT [PK_TalkGroupItem] PRIMARY KEY CLUSTERED 
(
	[TalkGroupItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[UserName] [nvarchar](250) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[Address] [nvarchar](250) NULL,
	[City] [nvarchar](250) NULL,
	[State] [nvarchar](250) NULL,
	[PostalCode] [nvarchar](250) NULL,
	[PhoneNumber] [nvarchar](250) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[UserGroupId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
 CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[UserGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserInRole]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserPermission]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermission](
	[UserPermissionId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED 
(
	[UserPermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRadioMap]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRadioMap](
	[UserRadioMapId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[RadioId] [int] NOT NULL,
 CONSTRAINT [PK_UserRadioMap] PRIMARY KEY CLUSTERED 
(
	[UserRadioMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserTalkGroup]    Script Date: 31/08/2016 3.08.07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTalkGroup](
	[UserTalkGroupId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[TakGroupId] [int] NOT NULL,
 CONSTRAINT [PK_UserTalkGroup] PRIMARY KEY CLUSTERED 
(
	[UserTalkGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CustomerNotificationLastScan]  WITH CHECK ADD  CONSTRAINT [FK_CustomerNotificationLastScan_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerNotificationLastScan] CHECK CONSTRAINT [FK_CustomerNotificationLastScan_Customer]
GO
ALTER TABLE [dbo].[CustomerNotificationSetting]  WITH CHECK ADD  CONSTRAINT [FK_CustomerNotificationSetting_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerNotificationSetting] CHECK CONSTRAINT [FK_CustomerNotificationSetting_Customer]
GO
ALTER TABLE [dbo].[CustomerNotificationSettingNew]  WITH CHECK ADD  CONSTRAINT [FK_CustomerNotificationSettingNew_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerNotificationSettingNew] CHECK CONSTRAINT [FK_CustomerNotificationSettingNew_Customer]
GO
ALTER TABLE [dbo].[CustomerTalkGroup]  WITH CHECK ADD  CONSTRAINT [FK_CustomerTalkGroup_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerTalkGroup] CHECK CONSTRAINT [FK_CustomerTalkGroup_Customer]
GO
ALTER TABLE [dbo].[Group]  WITH CHECK ADD  CONSTRAINT [FK_Group_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[Group] CHECK CONSTRAINT [FK_Group_Customer]
GO
ALTER TABLE [dbo].[GroupTalkGroupMap]  WITH CHECK ADD  CONSTRAINT [FK_GroupTalkGroupMap_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([GroupId])
GO
ALTER TABLE [dbo].[GroupTalkGroupMap] CHECK CONSTRAINT [FK_GroupTalkGroupMap_Group]
GO
ALTER TABLE [dbo].[NotificationLog]  WITH CHECK ADD  CONSTRAINT [FK_NotificationLog_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[NotificationLog] CHECK CONSTRAINT [FK_NotificationLog_Customer]
GO
ALTER TABLE [dbo].[NotificationSettingGroupMap]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettingGroupMap_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[NotificationSettingGroupMap] CHECK CONSTRAINT [FK_NotificationSettingGroupMap_Customer]
GO
ALTER TABLE [dbo].[NotificationSettingGroupMap]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettingGroupMap_CustomerNotificationSettingNew] FOREIGN KEY([CustomerNotificationId])
REFERENCES [dbo].[CustomerNotificationSettingNew] ([CustomerNotificationId])
GO
ALTER TABLE [dbo].[NotificationSettingGroupMap] CHECK CONSTRAINT [FK_NotificationSettingGroupMap_CustomerNotificationSettingNew]
GO
ALTER TABLE [dbo].[NotificationSettingGroupMap]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettingGroupMap_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([GroupId])
GO
ALTER TABLE [dbo].[NotificationSettingGroupMap] CHECK CONSTRAINT [FK_NotificationSettingGroupMap_Group]
GO
ALTER TABLE [dbo].[NotificationSettingRadioMap]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettingRadioMap_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[NotificationSettingRadioMap] CHECK CONSTRAINT [FK_NotificationSettingRadioMap_Customer]
GO
ALTER TABLE [dbo].[NotificationSettingRadioMap]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettingRadioMap_CustomerNotificationSettingNew] FOREIGN KEY([CustomerNotificationId])
REFERENCES [dbo].[CustomerNotificationSettingNew] ([CustomerNotificationId])
GO
ALTER TABLE [dbo].[NotificationSettingRadioMap] CHECK CONSTRAINT [FK_NotificationSettingRadioMap_CustomerNotificationSettingNew]
GO
ALTER TABLE [dbo].[NotificationSettingTalkGroup]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettingTalkGroup_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[NotificationSettingTalkGroup] CHECK CONSTRAINT [FK_NotificationSettingTalkGroup_Customer]
GO
ALTER TABLE [dbo].[NotificationSettingTalkGroup]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettingTalkGroup_CustomerNotificationSettingNew] FOREIGN KEY([CustomerNotificationId])
REFERENCES [dbo].[CustomerNotificationSettingNew] ([CustomerNotificationId])
GO
ALTER TABLE [dbo].[NotificationSettingTalkGroup] CHECK CONSTRAINT [FK_NotificationSettingTalkGroup_CustomerNotificationSettingNew]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Customer]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Group] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Group] ([GroupId])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Group]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_User]
GO
ALTER TABLE [dbo].[UserInRole]  WITH CHECK ADD  CONSTRAINT [FK_UserInRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
ALTER TABLE [dbo].[UserInRole] CHECK CONSTRAINT [FK_UserInRole_Role]
GO
ALTER TABLE [dbo].[UserInRole]  WITH CHECK ADD  CONSTRAINT [FK_UserInRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserInRole] CHECK CONSTRAINT [FK_UserInRole_User]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_Customer]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([PermissionId])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_Permission]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_User]
GO
ALTER TABLE [dbo].[UserRadioMap]  WITH CHECK ADD  CONSTRAINT [FK_UserRadioMap_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserRadioMap] CHECK CONSTRAINT [FK_UserRadioMap_User]
GO
ALTER TABLE [dbo].[UserTalkGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserTalkGroup_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[UserTalkGroup] CHECK CONSTRAINT [FK_UserTalkGroup_User]
GO
USE [master]
GO
ALTER DATABASE [PoliceMonitorDB] SET  READ_WRITE 
GO
