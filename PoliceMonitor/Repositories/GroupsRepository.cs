﻿using PoliceMonitor.Entities;
using PoliceMonitor.Models;
using PoliceMonitor.Security.Principal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoliceMonitor.Repositories
{
    public class GroupsRepository
    {
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }

        public int CustomerID 
        {
            get 
            {
                return this._userInfo.CustomerId;
            }
        }
        /// <summary>
        /// GroupsRepository class constructor
        /// </summary>
        /// <param name="userInformation">CustomPrincipal</param>
        public GroupsRepository(CustomPrincipal userInformation)
        {
            _userInfo = userInformation;
        }

        #region Groups


        //public IList<Group> ListGroups()
        //{
        //    try
        //    {
        //        using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
        //        {
        //            var groups = db.Groups.Where(x => x.CustomerId == CustomerID);
        //            return groups.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        public IList<GroupViewModels> ListGroups()
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    return  db.Groups.Where(x=> x.CustomerId == CustomerID).Select(grp => new GroupViewModels() {
                        GroupId = grp.GroupId,
                        GroupName = grp.GroupName,
                        TalkGroupList = db.GroupTalkGroupMaps.Where(x => x.CustomerId == CustomerID && x.GroupId == grp.GroupId).Select(x => new SelectListItem() { Value = x.TakGroupId.ToString(), Text = x.TakGroupId.ToString() }).ToList(),
                        Users = db.UserGroups.Where(x => x.CustomerId == CustomerID && x.GroupId == grp.GroupId).Select(x => new UserViewModels() { UserId = x.UserId, UserName = x.User.UserName, Active=x.User.Active }).ToList()
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Add(GroupViewModels groupModel, string[] SelectedUser,params string[] selectedTalkGroup)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var group = new Group()
                        {
                            CustomerId = CustomerID,
                            GroupName = groupModel.GroupName,
                        };
                        db.Groups.Add(group);
                        foreach (var select in selectedTalkGroup)
                        {
                            var groupTalkGroupMap = new GroupTalkGroupMap() { GroupId = groupModel.GroupId, CustomerId = CustomerID, TakGroupId = Convert.ToInt32(select) };
                            db.GroupTalkGroupMaps.Add(groupTalkGroupMap);
                        }

                        foreach (var userId in SelectedUser)
                        {
                            var userGroup = new UserGroup() { GroupId = groupModel.GroupId, CustomerId = CustomerID, UserId = Convert.ToInt32(userId) };
                            db.UserGroups.Add(userGroup);
                        }
                        db.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public GroupViewModels Find(int? id)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {

                    Group group = db.Groups.Where(x => x.CustomerId==CustomerID && x.GroupId==id).FirstOrDefault();

                    GroupViewModels groupModel = new GroupViewModels();
                    groupModel.GroupId = group.GroupId;
                    groupModel.GroupName = group.GroupName;
                    groupModel.TalkGroupList = db.GroupTalkGroupMaps.Where(x => x.CustomerId == CustomerID && x.GroupId == group.GroupId).Select(x => new SelectListItem() { Value = x.TakGroupId.ToString(), Text = x.TakGroupId.ToString() }).ToList();
                    groupModel.Users = db.UserGroups.Where(x => x.CustomerId == CustomerID && x.GroupId == group.GroupId).Select(x => new UserViewModels() { UserId = x.UserId, UserName = x.User.UserName }).ToList();
                    return groupModel;
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public bool Update(GroupViewModels groupModel,  string[] selectedUser,params string[] selectedTalkGroup)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Group group = db.Groups.Where(x => x.GroupId == groupModel.GroupId && x.CustomerId == CustomerID).FirstOrDefault();
                        if (group != null)
                        {
                            group.GroupName = groupModel.GroupName;

                            #region userTalkGroup
                            var groupTalkGroupList = db.GroupTalkGroupMaps.Where(x => x.GroupId == group.GroupId).ToList();
                            //Add item that if does not exist in db
                            foreach (var select in selectedTalkGroup)
                            {
                                if (!groupTalkGroupList.Where(x => x.TakGroupId == Convert.ToInt32(select)).Any())
                                {
                                    var groupTalkGroupMap = new GroupTalkGroupMap() { GroupId = groupModel.GroupId, CustomerId = CustomerID, TakGroupId = Convert.ToInt32(select) };
                                    db.GroupTalkGroupMaps.Add(groupTalkGroupMap);
                                }
                            }
                            //Remove the does not exist in userTalkGroupList
                            foreach (var talkGroup in groupTalkGroupList)
                            {
                                if (!selectedTalkGroup.Where(x => x == talkGroup.TakGroupId.ToString()).Any())
                                {
                                    db.GroupTalkGroupMaps.Remove(talkGroup);
                                }
                            }
                            #endregion

                            #region user
                            var userList = db.UserGroups.Where(x => x.GroupId == group.GroupId).ToList();
                            //Add item that if does not exist in db
                            foreach (var userId in selectedUser)
                            {
                                if (!userList.Where(x => x.UserId == Convert.ToInt32(userId)).Any())
                                {
                                    var userGroup = new UserGroup() { GroupId = groupModel.GroupId, CustomerId = CustomerID, UserId = Convert.ToInt32(userId) };
                                    db.UserGroups.Add(userGroup);
                                }
                            }
                            //Remove the does not exist in userList
                            foreach (var user in userList)
                            {
                                if (!selectedUser.Where(x => x == user.UserId.ToString()).Any())
                                {
                                    db.UserGroups.Remove(user);
                                }
                            }
                            #endregion

                            db.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }

            }
            return false;
        }
        public void Delete(int id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var group = db.Groups.Find(id);
                        db.Groups.Remove(group);

                        var userGroups = db.UserGroups.Where(x => x.GroupId==group.GroupId);
                        foreach (var userGroup in userGroups)
                        {
                            db.UserGroups.Remove(userGroup);
                        }

                        var groupTalkGroupList = db.GroupTalkGroupMaps.Where(x => x.GroupId == group.GroupId);
                        foreach (var groupTalkGroup in groupTalkGroupList)
                        {
                            db.GroupTalkGroupMaps.Remove(groupTalkGroup);
                        }

                        db.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        #endregion

    }
}