﻿using PoliceMonitor.Entities;
using PoliceMonitor.Models;
using PoliceMonitor.Security.Principal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoliceMonitor.Repositories
{
    public class CustomersRepository
    {
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }

        public int CustomerID
        {
            get
            {
                return this._userInfo.CustomerId;
            }
        }
        /// <summary>
        /// GroupsRepository class constructor
        /// </summary>
        /// <param name="userInformation">CustomPrincipal</param>
        public CustomersRepository(CustomPrincipal userInformation)
        {
            _userInfo = userInformation;
        }


        #region Customers
        public IList<CustomersViewModels> ListCustomers()
        {
            IList<CustomersViewModels> customers = new List<CustomersViewModels>();

            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var listCustomers = db.Customers.ToList();
                    foreach (var cust in listCustomers)
                    {
                        customers.Add(new CustomersViewModels()
                        {
                            CustomerId = cust.CustomerId,
                            CustomerName = cust.CustomerName,
                            StreetAddress = cust.StreetAddress,
                            City = cust.City,
                            State = cust.State,
                            ZipCode = cust.ZipCode,
                            ContactName = cust.ContactName,
                            ContactPhoneNumber = cust.ContactPhoneNumber,
                            UnlimitedUsers = cust.UnlimitedUsers,
                            UsersCount = cust.UsersCount,
                            Active = cust.Active
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return customers;
        }

        public string GetCustomerPermission(int? customerId)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var customerperssion = db.CustomerPermissions.Where(w => w.CustomerId == customerId).Select(x => x.Permission).FirstOrDefault();
                    return customerperssion != null ? customerperssion.Permission1 : string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IList<Permission> GetCustomerAssignedPermissions(int? customerId)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    return db.CustomerPermissions.Where(w => w.CustomerId == customerId).Select(x => x.Permission).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Add(CustomersViewModels customer, string[] selectedPermissionLevel, string[] selectedTalkGroup, params string[] selectSysID)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var cust = new Customer()
                        {
                            CustomerName = customer.CustomerName,
                            StreetAddress = customer.StreetAddress,
                            City = customer.City,
                            State = customer.State,
                            ZipCode = customer.ZipCode,
                            ContactName = customer.ContactName,
                            ContactPhoneNumber = customer.ContactPhoneNumber,
                            UnlimitedUsers = customer.UnlimitedUsers,
                            UsersCount = customer.UnlimitedUsers == false ? customer.UsersCount : null,
                            Active = true,
                            DefaultAutoplaySetting = customer.DefaultAutoplaySetting
                        };
                        db.Customers.Add(cust);
                        db.SaveChanges();
                        int CustomerId = cust.CustomerId;
                        if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                        {
                            foreach (var select in selectedPermissionLevel)
                            {
                                var customerPermission = new CustomerPermission() { CustomerId = CustomerId, PermissionId = Convert.ToInt32(select) };
                                db.CustomerPermissions.Add(customerPermission);
                                db.SaveChanges();
                            }
                        }
                        int count = 0;
                        foreach (var select in selectedTalkGroup)
                        {
                            var customerTalkGroup = new CustomerTalkGroup() { CustomerId = CustomerId, TalkGroup = select, SystemID = Convert.ToInt32(selectSysID[count]) };
                            db.CustomerTalkGroups.Add(customerTalkGroup);
                            db.SaveChanges();
                            count++;
                        }
                        dbContextTransaction.Commit();
                        return true;
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public CustomersViewModels Find(int? id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                Customer customer = db.Customers.Find(id);
                if (customer != null)
                {
                    return new CustomersViewModels()
                        {
                            CustomerId = customer.CustomerId,
                            CustomerName = customer.CustomerName,
                            StreetAddress = customer.StreetAddress,
                            City = customer.City,
                            State = customer.State,
                            ZipCode = customer.ZipCode,
                            ContactName = customer.ContactName,
                            ContactPhoneNumber = customer.ContactPhoneNumber,
                            UsersCount = customer.UsersCount,
                            UnlimitedUsers = customer.UnlimitedUsers,
                            Active = customer.Active,
                            DefaultAutoplaySetting = customer.DefaultAutoplaySetting.Value,
                            PermissionLevelList = customer.CustomerPermissions.Select(x => new SelectListItem() { Value = x.PermissionId.ToString(), Text = x.Permission.Permission1.ToString() }).ToList(),
                            TalkGroupList = db.CustomerTalkGroups.Where(x => x.CustomerId == id).Select(x => new SelectListItem() { Value = x.SystemID.ToString() + "_" + x.TalkGroup, Text = x.SystemID.ToString() }).ToList()
                        };
                }
                return null;
            }

        }


        public bool UpdateEdit(int customer, FilterViewModel Usertalkgroups, string[] selectedTalkGroup, params string[] selectSysId)
        {
            List<string> selectedTalkGroupList = new List<string>(selectedTalkGroup);
            List<string> selectedSystemID = new List<string>(selectSysId);
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Customer cust = db.Customers.Find(customer);
                        if (cust != null)
                        {

                            int customerId = cust.CustomerId;
                            var customerTalkGroupList = db.CustomerTalkGroups.Where(x => x.CustomerId == customerId).ToList();

                            if (customerTalkGroupList == null)//if null then add
                            {
                                int count = 0;
                                foreach (var select in selectedTalkGroupList)
                                {
                                    var customerTalkGroup = new CustomerTalkGroup() { CustomerId = customerId, TalkGroup = select, SystemID = Convert.ToInt32(selectSysId[count]) };
                                    db.CustomerTalkGroups.Add(customerTalkGroup);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                int count = 0;
                                //Add item that if does not exist in db
                                List<CustomerTalkGroup> talkGroupList = new List<CustomerTalkGroup>();
                                foreach (var select in selectedTalkGroupList)
                                {
                                    if (!customerTalkGroupList.Where(x => x.TalkGroup == select && x.CustomerId == customerId && x.SystemID == Convert.ToInt32(selectSysId[count])).Any())
                                    {
                                        var talkGroup = new CustomerTalkGroup() { CustomerId = customerId, TalkGroup = select, SystemID = Convert.ToInt32(selectSysId[count]) };
                                        talkGroupList.Add(talkGroup);
                                        db.CustomerTalkGroups.Add(talkGroup);
                                        db.SaveChanges();
                                    }
                                    count++;
                                }
                                if(Usertalkgroups.AddUserTalkgroup==true)
                                {
                                List<UserTalkGroup> addNewUserTalkGroup = new List<Entities.UserTalkGroup>();
                                var CustomerUser = db.Users.Where(w => w.CustomerId == customer).ToList();
                                    foreach (var item in CustomerUser)
                                    {
                                        foreach (var Talkitem in talkGroupList)
                                        {
                                            UserTalkGroup Talk = new UserTalkGroup();
                                            Talk.CustomerId = customer;
                                            Talk.SystemID = Talkitem.SystemID;
                                            Talk.TakGroupId = Convert.ToInt32(Talkitem.TalkGroup);
                                            Talk.UserId = item.UserId;
                                            addNewUserTalkGroup.Add(Talk);
                                        }
                                    }
                                    db.UserTalkGroups.AddRange(addNewUserTalkGroup);
                                    db.SaveChanges();
                                }

                                //Remove the does not exist in selectedTalkGroupList
                                Dictionary<int, List<string>> dict1 = new Dictionary<int, List<string>>();

                                for (int i = 0; i < selectedTalkGroup.Count(); i++)
                                {
                                    int systemid = Convert.ToInt32(selectSysId[i]);

                                    List<string> talkgroups = null;
                                    if (!dict1.Keys.Contains(systemid))
                                    {
                                        talkgroups = new List<string>();
                                        dict1.Add(systemid, talkgroups);
                                    }
                                    else
                                    {
                                        talkgroups = dict1[systemid];
                                    }

                                    talkgroups.Add(selectedTalkGroup[i]);
                                }
                                foreach (var talkGroup in customerTalkGroupList)
                                {
                                    if (talkGroup.SystemID == null)
                                        talkGroup.SystemID = 1039;
                                    if (dict1.ContainsKey(talkGroup.SystemID.Value))
                                    {
                                        if (!dict1[talkGroup.SystemID.Value].Contains(talkGroup.TalkGroup))
                                        {
                                            db.CustomerTalkGroups.Remove(talkGroup);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        db.CustomerTalkGroups.Remove(talkGroup);
                                        db.SaveChanges();
                                    }
                                }
                            }
                            dbContextTransaction.Commit();
                            return true;
                        }
                    }
                    catch(Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }

            }
            return false;
        }






        public bool Update(CustomersViewModels customer, string[] selectedPermissionLevel, string[] selectedTalkGroup, params string[] selectSysId)
        {
            List<string> selectedTalkGroupList = new List<string>(selectedTalkGroup);
            List<string> selectedSystemID = new List<string>(selectSysId);
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Customer cust = db.Customers.Find(customer.CustomerId);
                        if (cust != null)
                        {
                            cust.CustomerName = customer.CustomerName;
                            cust.StreetAddress = customer.StreetAddress;
                            cust.City = customer.City;
                            cust.State = customer.State;
                            cust.ZipCode = customer.ZipCode;
                            cust.ContactName = customer.ContactName;
                            cust.ContactPhoneNumber = customer.ContactPhoneNumber;
                            cust.UnlimitedUsers = customer.UnlimitedUsers;
                            cust.UsersCount = customer.UnlimitedUsers == false ? customer.UsersCount : null;
                            cust.Active = customer.Active;
                            cust.DefaultAutoplaySetting = customer.DefaultAutoplaySetting;
                            db.SaveChanges();


                            #region selectedPermission

                            var customerPermissionList = db.CustomerPermissions.Where(x => x.CustomerId == customer.CustomerId).ToList();
                            foreach (var customerPermission in customerPermissionList)
                            {
                                db.CustomerPermissions.Remove(customerPermission);
                                db.SaveChanges();
                            }
                            //Add item that if does not exist in db
                            if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                            {
                                foreach (var select in selectedPermissionLevel)
                                {
                                    // if (!customerPermissionList.Where(x => x.PermissionId == Convert.ToInt32(select)).Any())
                                    //{
                                    var customerPermission = new CustomerPermission() { CustomerId = customer.CustomerId, PermissionId = Convert.ToInt32(select) };
                                    db.CustomerPermissions.Add(customerPermission);
                                    db.SaveChanges();
                                    //  }
                                }
                            }

                            #endregion

                            //int customerId = cust.CustomerId;
                            //var customerTalkGroupList = db.CustomerTalkGroups.Where(x => x.CustomerId == customerId).ToList();

                            //if (customerTalkGroupList == null)//if null then add
                            //{
                            //    int count = 0;
                            //    foreach (var select in selectedTalkGroupList)
                            //    {
                            //        var customerTalkGroup = new CustomerTalkGroup() { CustomerId = customerId, TalkGroup = select,SystemID=Convert.ToInt32(selectSysId[count]) };
                            //        db.CustomerTalkGroups.Add(customerTalkGroup);
                            //        db.SaveChanges();
                            //    }
                            //}
                            //else
                            //{
                            //    int count = 0;
                            //    //Add item that if does not exist in db
                            //    foreach (var select in selectedTalkGroupList)
                            //    { 
                            //        if (!customerTalkGroupList.Where(x => x.TalkGroup == select && x.CustomerId == customerId && x.SystemID == Convert.ToInt32(selectSysId[count])).Any())
                            //        {
                            //            var talkGroup = new CustomerTalkGroup() { CustomerId = customerId, TalkGroup = select, SystemID = Convert.ToInt32(selectSysId[count]) };
                            //            db.CustomerTalkGroups.Add(talkGroup);
                            //            db.SaveChanges();
                            //        }
                            //        count++;
                            //    }

                            //    //Remove the does not exist in selectedTalkGroupList
                            //    Dictionary<int, List<string>> dict1 = new Dictionary<int, List<string>>();

                            //    for (int i = 0; i < selectedTalkGroup.Count(); i++)
                            //    {
                            //        int systemid = Convert.ToInt32(selectSysId[i]);

                            //        List<string> talkgroups = null;
                            //        if (!dict1.Keys.Contains(systemid))
                            //        {
                            //            talkgroups = new List<string>();
                            //            dict1.Add(systemid, talkgroups);
                            //        }
                            //        else
                            //        {
                            //            talkgroups = dict1[systemid];
                            //        }

                            //        talkgroups.Add(selectedTalkGroup[i]);
                            //    }
                            //    foreach (var talkGroup in customerTalkGroupList)
                            //    {
                            //        if (talkGroup.SystemID == null)
                            //            talkGroup.SystemID = 1039;
                            //        if (dict1.ContainsKey(talkGroup.SystemID.Value))
                            //        {
                            //            if (!dict1[talkGroup.SystemID.Value].Contains(talkGroup.TalkGroup))
                            //            {
                            //                db.CustomerTalkGroups.Remove(talkGroup);
                            //                db.SaveChanges();
                            //            }
                            //        }
                            //        else
                            //        {
                            //            db.CustomerTalkGroups.Remove(talkGroup);
                            //            db.SaveChanges();
                            //        }
                            //    }
                            //}
                            dbContextTransaction.Commit();
                            return true;
                        }
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }

            }
            return false;
        }
        public void Delete(int id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                Customer customer = db.Customers.Find(id);
                if (customer != null)
                {
                    customer.Active = false;
                    db.SaveChanges();
                }
            }
        }

        public void Active(int id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                Customer customer = db.Customers.Find(id);
                if (customer != null)
                {
                    customer.Active = true;
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region NotificationSetting New


        public IList<CustomerNotificationSettingNew> ListNotificationSettingsNew()
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var customerNotificationSettings = db.CustomerNotificationSettingNews.Include(c => c.Customer).Include(c => c.NotificationSettingTalkGroups)
                        .Where(x => x.Customer.Active == true && x.Active == true && x.CustomerId == CustomerID);
                    return customerNotificationSettings.ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public void AddNotificationSettingNew(NotificationViewModelsNew notificationModel, string[] selectedRadio, string[] selectedTalkGroup, params string[] selectsysval)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var notificationSetting = new CustomerNotificationSettingNew()
                        {
                            CustomerId = CustomerID,
                            NotificationName = notificationModel.NotificationName,
                            NotifiyTransmissionsCount = notificationModel.NotifiyTransmissionsCount,
                            Interval = notificationModel.Interval,
                            AfterFirstTransmissionWaitInterval = notificationModel.AfterFirstTransmissionWaitInterval,
                            LastNSecTransmissionsCount = notificationModel.LastNSecTransmissionsCount,
                            NSecInterval = notificationModel.NSecInterval,
                            AfterNSecTransmissionWaitInterval = notificationModel.AfterNSecTransmissionWaitInterval,
                            Active = true
                        };
                        db.CustomerNotificationSettingNews.Add(notificationSetting);
                        int count = 0;
                        foreach (var select in selectedTalkGroup)
                        {
                            var notificationTalkGroup = new NotificationSettingTalkGroup()
                            {
                                CustomerNotificationId = notificationSetting.CustomerNotificationId,
                                CustomerId = CustomerID,
                                TakGroupId = Convert.ToInt32(select),
                                SystemID = Convert.ToInt32(selectsysval[count]),
                                SelectedCaseID = notificationModel.SelectedCaseId
                            };
                            count++;
                            db.NotificationSettingTalkGroups.Add(notificationTalkGroup);
                        }
                        if (selectedRadio != null && selectedRadio.Length > 0)
                        {
                            foreach (var select in selectedRadio)
                            {
                                var notificationRadioMap = new NotificationSettingRadioMap()
                                {
                                    CustomerNotificationId = notificationSetting.CustomerNotificationId,
                                    CustomerId = CustomerID,
                                    RadioId = Convert.ToInt32(select)
                                };
                                db.NotificationSettingRadioMaps.Add(notificationRadioMap);
                            }
                        }
                        if (notificationModel.SelectedGroupValues != null && notificationModel.SelectedGroupValues.Count > 0)
                        {
                            foreach (var group in notificationModel.SelectedGroupValues)
                            {
                                var notificationUserGroupMap = new NotificationSettingGroupMap()
                                {
                                    CustomerNotificationId = notificationSetting.CustomerNotificationId,
                                    CustomerId = CustomerID,
                                    GroupId = Convert.ToInt32(group)
                                };
                                db.NotificationSettingGroupMaps.Add(notificationUserGroupMap);
                            }
                        }

                        //Add Users
                        List<int> SelectedUsers = notificationModel.SelectedUserValues.Select(int.Parse).ToList();
                        foreach (var selectedUser in SelectedUsers)
                        {
                            db.CustomerNotificationUserMaps.Add(new CustomerNotificationUserMap()
                            {
                                CustomerNotificationId = notificationModel.CustomerNotificationId,
                                UserId = selectedUser,
                                Active = true
                            });
                        }

                        db.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public NotificationViewModelsNew FindNotificationSettingsNew(int? id)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {

                    CustomerNotificationSettingNew customerNotificationSetting = db.CustomerNotificationSettingNews.Include(c => c.Customer).Where(x => x.CustomerNotificationId == id).FirstOrDefault();

                    NotificationViewModelsNew notificationModel = new NotificationViewModelsNew();
                    notificationModel.CustomerNotificationId = customerNotificationSetting.CustomerNotificationId;
                    notificationModel.NotificationName = customerNotificationSetting.NotificationName;

                    notificationModel.NotifiyTransmissionsCount = customerNotificationSetting.NotifiyTransmissionsCount;
                    notificationModel.NotifiyTransmissionsCount = customerNotificationSetting.NotifiyTransmissionsCount;
                    notificationModel.Interval = customerNotificationSetting.Interval;
                    notificationModel.AfterFirstTransmissionWaitInterval = customerNotificationSetting.AfterFirstTransmissionWaitInterval;
                    notificationModel.LastNSecTransmissionsCount = customerNotificationSetting.LastNSecTransmissionsCount;
                    notificationModel.NSecInterval = customerNotificationSetting.NSecInterval;
                    notificationModel.AfterNSecTransmissionWaitInterval = customerNotificationSetting.AfterNSecTransmissionWaitInterval;
                    notificationModel.Active = customerNotificationSetting.Active;
                    notificationModel.Customer = new CustomersViewModels()
                    {
                        CustomerId = customerNotificationSetting.Customer.CustomerId,
                        CustomerName = customerNotificationSetting.Customer.CustomerName,
                        StreetAddress = customerNotificationSetting.Customer.StreetAddress,
                        City = customerNotificationSetting.Customer.City,
                        State = customerNotificationSetting.Customer.State,
                        ZipCode = customerNotificationSetting.Customer.ZipCode,
                        ContactName = customerNotificationSetting.Customer.ContactName,
                        ContactPhoneNumber = customerNotificationSetting.Customer.ContactPhoneNumber,
                        UnlimitedUsers = customerNotificationSetting.Customer.UnlimitedUsers,
                        UsersCount = customerNotificationSetting.Customer.UsersCount,
                        Active = customerNotificationSetting.Customer.Active
                    };

                    notificationModel.TalkGroupList = db.NotificationSettingTalkGroups.Where(x => x.CustomerId == CustomerID && x.CustomerNotificationId == customerNotificationSetting.CustomerNotificationId)
                        .Select(x => new SelectListItem() { Value = x.TakGroupId.ToString(), Text = x.TakGroupId.ToString() }).ToList();

                    notificationModel.GroupList = db.NotificationSettingGroupMaps.Include(c => c.Group).Where(x => x.CustomerId == CustomerID && x.CustomerNotificationId == customerNotificationSetting.CustomerNotificationId)
                        .Select(x => new SelectListItem() { Value = x.Group.GroupId.ToString(), Text = x.Group.GroupName.ToString() }).ToList();

                    notificationModel.RadioList = db.NotificationSettingRadioMaps.Where(x => x.CustomerId == CustomerID && x.CustomerNotificationId == customerNotificationSetting.CustomerNotificationId)
                        .Select(x => new SelectListItem() { Value = x.RadioId.ToString(), Text = x.RadioId.ToString() }).ToList();

                    var notificationUsers = customerNotificationSetting.CustomerNotificationUserMaps.Where(u => u.CustomerNotificationId == id && u.Active == true).ToList();
                    //if(notificationUsers.Any())
                    //{
                    //    notificationModel.Users = notificationUsers.
                    //}
                    notificationModel.SelectedUserValues = new List<string>();
                    foreach (var user in notificationUsers)
                    {
                        notificationModel.SelectedUserValues.Add(Convert.ToString(user.UserId));
                    }
                    return notificationModel;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool UpdateNotificationSettingNew(NotificationViewModelsNew notificationModel, string[] selectedRadio, string[] selectedTalkGroup, params string[] selectsysval)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var notificationSetting = db.CustomerNotificationSettingNews.Find(notificationModel.CustomerNotificationId);
                        if (notificationSetting != null)
                        {
                            notificationSetting.NotificationName = notificationModel.NotificationName;
                            notificationSetting.NotifiyTransmissionsCount = notificationModel.NotifiyTransmissionsCount;
                            notificationSetting.Interval = notificationModel.Interval;
                            notificationSetting.AfterFirstTransmissionWaitInterval = notificationModel.AfterFirstTransmissionWaitInterval;
                            notificationSetting.LastNSecTransmissionsCount = notificationModel.LastNSecTransmissionsCount;
                            notificationSetting.NSecInterval = notificationModel.NSecInterval;
                            notificationSetting.AfterNSecTransmissionWaitInterval = notificationModel.AfterNSecTransmissionWaitInterval;
                            notificationSetting.Active = notificationModel.Active;
                            #region Notification User Group
                            var notificationUserGroupList = db.NotificationSettingGroupMaps.Where(x => x.CustomerNotificationId == notificationSetting.CustomerNotificationId).ToList();
                            //Add item that if does not exist in db
                            if (notificationModel.SelectedGroupValues != null)
                            {
                                foreach (var select in notificationModel.SelectedGroupValues)
                                {
                                    if (!notificationUserGroupList.Where(x => x.GroupId == Convert.ToInt32(select)).Any())
                                    {
                                        var notificationUserGroupMap = new NotificationSettingGroupMap()
                                        {
                                            CustomerNotificationId = notificationSetting.CustomerNotificationId,
                                            CustomerId = CustomerID,
                                            GroupId = Convert.ToInt32(select)
                                        };
                                        db.NotificationSettingGroupMaps.Add(notificationUserGroupMap);
                                    }
                                }
                            }
                            //Remove the does not exist in notificationUserGroupList
                            foreach (var userGroup in notificationUserGroupList)
                            {
                                if (!notificationModel.SelectedGroupValues.Where(x => x == userGroup.GroupId.ToString()).Any())
                                {
                                    db.NotificationSettingGroupMaps.Remove(userGroup);
                                }
                            }
                            #endregion

                            #region NotificationTalkGroup
                            var notificationTalkGroupList = db.NotificationSettingTalkGroups.Where(x => x.CustomerNotificationId == notificationSetting.CustomerNotificationId && x.CustomerId == notificationSetting.CustomerId).ToList();
                            //Add item that if does not exist in db
                            int count = 0;
                            foreach (var select in selectedTalkGroup)
                            {
                                int systemid = Convert.ToInt32(selectsysval[count]);
                                if (!notificationTalkGroupList.Where(x => x.TakGroupId == Convert.ToInt32(select) && x.SystemID == systemid).Any())
                                {
                                    var notificationTalkGroup = new NotificationSettingTalkGroup()
                                    {
                                        CustomerNotificationId = notificationSetting.CustomerNotificationId,
                                        CustomerId = CustomerID,
                                        TakGroupId = Convert.ToInt32(select),
                                        SystemID = systemid,
                                        SelectedCaseID = notificationModel.SelectedCaseId

                                    };
                                    db.NotificationSettingTalkGroups.Add(notificationTalkGroup);
                                    count++;
                                }
                            }

                            //Remove the does not exist in notificationTalkGroupList
                            foreach (var talkGroup in notificationTalkGroupList)
                            {
                                count = 0;
                                int systemid = Convert.ToInt32(selectsysval[count]);
                                //for(var i=0;i<selectedTalkGroup.Count();i++)
                                //{
                                //    if(talkGroup.TakGroupId.ToString()==selectedTalkGroup[i] && talkGroup.SystemID.ToString()==selectsysval[i])
                                //    {
                                //        count++;
                                //    }
                                //    //else
                                //    //{
                                //    //    db.NotificationSettingTalkGroups.Remove(talkGroup);
                                //    //}
                                //}
                                //if(count>0)
                                //{
                                //    db.NotificationSettingTalkGroups.Remove(talkGroup);
                                //}
                                if (!selectedTalkGroup.Where(x => x == talkGroup.TakGroupId.ToString()).Any())
                                {
                                    db.NotificationSettingTalkGroups.Remove(talkGroup);
                                }
                            }
                            #endregion

                            #region selectedRadio
                            if (selectedRadio != null && selectedRadio.Length > 0)
                            {
                                var notificationRadioList = db.NotificationSettingRadioMaps.Where(x => x.CustomerNotificationId == notificationSetting.CustomerNotificationId).ToList();
                                //Add item that if does not exist in db
                                foreach (var select in selectedRadio)
                                {
                                    if (!notificationRadioList.Where(x => x.RadioId == Convert.ToInt32(select)).Any())
                                    {
                                        var notificationRadioMap = new NotificationSettingRadioMap()
                                        {
                                            CustomerNotificationId = notificationSetting.CustomerNotificationId,
                                            CustomerId = CustomerID,
                                            RadioId = Convert.ToInt32(select)
                                        };
                                        db.NotificationSettingRadioMaps.Add(notificationRadioMap);
                                    }
                                }
                                //Remove the does not exist in notificationRadioList
                                foreach (var radio in notificationRadioList)
                                {
                                    if (!selectedRadio.Where(x => x == radio.RadioId.ToString()).Any())
                                    {
                                        db.NotificationSettingRadioMaps.Remove(radio);
                                    }
                                }
                            }
                            else
                            {
                                var notificationRadioList = db.NotificationSettingRadioMaps.Where(x => x.CustomerNotificationId == notificationSetting.CustomerNotificationId).ToList();
                                foreach (var radio in notificationRadioList)
                                {
                                    if (selectedRadio == null)
                                    {
                                        db.NotificationSettingRadioMaps.Remove(radio);
                                    }
                                }
                            }
                            #endregion

                            #region

                            List<int> SelectedUsers = notificationModel.SelectedUserValues.Select(int.Parse).ToList();
                            //Remove Users
                            var deselectUser = notificationSetting.CustomerNotificationUserMaps.Where(u => !SelectedUsers.Contains(u.UserId) && u.CustomerNotificationId == notificationModel.CustomerNotificationId && u.Active == true);
                            foreach (var item in deselectUser)
                            {
                                var userMap = db.CustomerNotificationUserMaps.FirstOrDefault(u => u.NotificationUserId == item.NotificationUserId);
                                userMap.Active = false;
                            }

                            //Add Users
                            foreach (var selectedUser in SelectedUsers)
                            {
                                var existingUser = notificationSetting.CustomerNotificationUserMaps.Where(u => u.CustomerNotificationId == notificationModel.CustomerNotificationId && u.UserId == selectedUser && u.Active == true).ToList();
                                if (!existingUser.Any())
                                {
                                    db.CustomerNotificationUserMaps.Add(new CustomerNotificationUserMap()
                                    {
                                        CustomerNotificationId = notificationModel.CustomerNotificationId,
                                        UserId = selectedUser,
                                        Active = true
                                    });
                                }
                            }

                            #endregion

                            db.SaveChanges();
                            dbContextTransaction.Commit();
                            return true;
                        }
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }

                }
            }
            return false;
        }
        public void DeleteNotificationSettingNew(int id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                CustomerNotificationSettingNew customerNotificationSetting = db.CustomerNotificationSettingNews.Find(id);
                customerNotificationSetting.Active = false;
                //db.CustomerNotificationSettingNews.Remove(customerNotificationSetting);

                db.SaveChanges();
            }
        }

        #endregion
        #region NotificationSetting Old

        public bool CheckNotificationSettingsExistForCustomer(int customerId)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    return db.CustomerNotificationSettings.Where(x => x.CustomerId == customerId).Any();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IList<CustomerNotificationSetting> ListNotificationSettings()
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var customerNotificationSettings = db.CustomerNotificationSettings.Include(c => c.Customer).Where(x => x.Customer.Active == true);
                    return customerNotificationSettings.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        public void AddNotificationSetting(NotificationViewModels notificationModel)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var notificationSetting = new CustomerNotificationSetting()
                    {
                        CustomerId = notificationModel.SelectedCustomerId,
                        NotifiyTransmissionsCount = notificationModel.NotifiyTransmissionsCount,
                        Interval = notificationModel.Interval,
                        AfterFirstTransmissionWaitInterval = notificationModel.AfterFirstTransmissionWaitInterval,
                        LastNSecTransmissionsCount = notificationModel.LastNSecTransmissionsCount,
                        NSecInterval = notificationModel.NSecInterval,
                        AfterNSecTransmissionWaitInterval = notificationModel.AfterNSecTransmissionWaitInterval
                    };
                    db.CustomerNotificationSettings.Add(notificationSetting);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public NotificationViewModels FindNotificationSettings(int? id)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {

                    CustomerNotificationSetting customerNotificationSetting = db.CustomerNotificationSettings.Include(c => c.Customer).Where(x => x.CustomerNotificationId == id).FirstOrDefault();

                    NotificationViewModels notificationModel = new NotificationViewModels();
                    notificationModel.CustomerNotificationId = customerNotificationSetting.CustomerNotificationId;
                    notificationModel.SelectedCustomerId = customerNotificationSetting.CustomerId;

                    notificationModel.NotifiyTransmissionsCount = customerNotificationSetting.NotifiyTransmissionsCount;
                    notificationModel.Interval = customerNotificationSetting.Interval;
                    notificationModel.AfterFirstTransmissionWaitInterval = customerNotificationSetting.AfterFirstTransmissionWaitInterval;
                    notificationModel.LastNSecTransmissionsCount = customerNotificationSetting.LastNSecTransmissionsCount;
                    notificationModel.NSecInterval = customerNotificationSetting.NSecInterval;
                    notificationModel.AfterNSecTransmissionWaitInterval = customerNotificationSetting.AfterNSecTransmissionWaitInterval;
                    notificationModel.Customer = new CustomersViewModels()
                        {
                            CustomerId = customerNotificationSetting.Customer.CustomerId,
                            CustomerName = customerNotificationSetting.Customer.CustomerName,
                            StreetAddress = customerNotificationSetting.Customer.StreetAddress,
                            City = customerNotificationSetting.Customer.City,
                            State = customerNotificationSetting.Customer.State,
                            ZipCode = customerNotificationSetting.Customer.ZipCode,
                            ContactName = customerNotificationSetting.Customer.ContactName,
                            ContactPhoneNumber = customerNotificationSetting.Customer.ContactPhoneNumber,
                            UnlimitedUsers = customerNotificationSetting.Customer.UnlimitedUsers,
                            UsersCount = customerNotificationSetting.Customer.UsersCount,
                            Active = customerNotificationSetting.Customer.Active
                        };
                    return notificationModel;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateNotificationSetting(NotificationViewModels notificationModel)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var notificationSetting = db.CustomerNotificationSettings.Find(notificationModel.CustomerNotificationId);
                    if (notificationSetting != null)
                    {
                        notificationSetting.NotifiyTransmissionsCount = notificationModel.NotifiyTransmissionsCount;
                        notificationSetting.Interval = notificationModel.Interval;
                        notificationSetting.AfterFirstTransmissionWaitInterval = notificationModel.AfterFirstTransmissionWaitInterval;
                        notificationSetting.LastNSecTransmissionsCount = notificationModel.LastNSecTransmissionsCount;
                        notificationSetting.NSecInterval = notificationModel.NSecInterval;
                        notificationSetting.AfterNSecTransmissionWaitInterval = notificationModel.AfterNSecTransmissionWaitInterval;
                        db.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }
        public void DeleteNotificationSetting(int id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                CustomerNotificationSetting customerNotificationSetting = db.CustomerNotificationSettings.Find(id);
                db.CustomerNotificationSettings.Remove(customerNotificationSetting);
                db.SaveChanges();
            }
        }

        #endregion


        internal List<Usage> GetCustomerStats(DateTime start, DateTime end, int customerId)
        {
            long TransStartMSec = (long)(Utils.Utils.ToServerTime(start) - new DateTime(1970, 1, 1)).TotalMilliseconds;
            long TransEndMSec = (long)(Utils.Utils.ToServerTime(end) - new DateTime(1970, 1, 1)).TotalMilliseconds;
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                // get customers
                List<Customer> customers;
                if (customerId > 0)
                {
                    customers = db.Customers.Where(x => x.CustomerId == customerId && x.Active == true).ToList();
                }
                else
                {
                    customers = db.Customers.Where(x => x.Active == true).ToList();
                }

                // Get all the users of customer
                //List<User> users;
                dynamic users;
                if (customerId <= 0)
                {
                    users = db.Users.Join(db.Customers, c => c.CustomerId, u => u.CustomerId, (c, u) => new { c, u }).
                        Where(x => x.u.Active == true && x.u.CustomerId > 0).OrderBy(x => x.u.CustomerId).Select(s => s.c).ToList();
                }
                else
                {
                    users = db.Users.Where(x => x.Active == true && x.CustomerId == customerId).ToList();
                }

                List<Usage> result = new List<Usage>();
                foreach (User u in users)
                {
                    int totalTransmissions = db.TalkGroupItems.Where(x => (x.UserName == u.UserId.ToString()
                         || x.UserName.StartsWith(u.UserId + ",")
                         || x.UserName.EndsWith("," + u.UserId)
                         || x.UserName.StartsWith(u.UserId + ",")
                         || x.UserName.Contains("," + u.UserId + ","))
                         && x.TransStartMSec >= TransStartMSec
                         && x.TransStartMSec < TransEndMSec
                         ).Count();
                    long? totalDuration = db.TalkGroupItems.Where(x => (x.UserName == u.UserId.ToString()
                         || x.UserName.StartsWith(u.UserId + ",")
                         || x.UserName.EndsWith("," + u.UserId)
                         || x.UserName.StartsWith(u.UserId + ",")
                         || x.UserName.Contains("," + u.UserId + ","))
                         && x.TransStartMSec >= TransStartMSec
                         && x.TransStartMSec < TransEndMSec
                         ).Sum(x => x.TransLengthMSec);

                    int totalLogins = db.ActivityLogs.Where(x => x.userId == u.UserId && x.createdOn > start && x.createdOn < end).Count();

                    Usage usage = new Usage();
                    usage.CustomerID = u.CustomerId;
                    usage.Duration = (totalDuration == null) ? 0 : totalDuration.Value;
                    usage.UserID = u.UserId;
                    usage.UserName = u.FirstName + " " + u.LastName;
                    usage.TotalTransmissions = totalTransmissions;
                    usage.TotalLogins = totalLogins;
                    result.Add(usage);
                }

                return result;
            }
        }
    }
}