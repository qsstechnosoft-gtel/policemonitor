﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PoliceMonitor.Entities;

namespace PoliceMonitor.Repositories
{
    public class GeneralRepository
    {
        public void SaveActivityLog(string activityName, int userId, string description)
        {
            try
            {
                using (PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new PoliceMonitor.Entities.PoliceMonitorDBEntities())
                {
                    ActivityLog activity = new ActivityLog();
                    activity.activity = activityName;
                    activity.userId = userId;
                    activity.description = description;
                    activity.createdOn = DateTime.Now;
                    dbo.ActivityLogs.Add(activity);
                    dbo.SaveChanges();
                }
            }
            catch { }
        }
    }
}