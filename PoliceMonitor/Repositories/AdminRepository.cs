﻿using PoliceMonitor.Entities;
using PoliceMonitor.Models;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Security.Principal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace PoliceMonitor.Repositories
{
    public class AdminRepository
    {
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }

        public int CustomerID
        {
            get
            {
                return this._userInfo.CustomerId;
            }

        }
        /// <summary>
        /// AdminRepository class constructor
        /// </summary>
        /// <param name="userInformation">CustomPrincipal</param>
        public AdminRepository(CustomPrincipal userInformation)
        {
            _userInfo = userInformation;
        }

        #region Admin
        public IList<AdminViewModels> ListAdmins()
        {
            IList<AdminViewModels> admins = new List<AdminViewModels>();
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var listAdmins = (from u in db.Users
                                      join uir in db.UserInRoles on u.UserId equals uir.UserId
                                      join r in db.Roles on uir.RoleId equals r.RoleId
                                      where (r.Name == Constant.ADMIN_ROLENAME || r.Name == Constant.SUPERADMIN_ROLENAME)
                                      select u).ToList();
                    foreach (var admin in listAdmins)
                    {
                        admins.Add(new AdminViewModels()
                        {
                            UserId = admin.UserId,
                            SelectedCustomerId = admin.CustomerId,
                            Customer = db.Customers.Where(x => x.CustomerId == admin.CustomerId).Select(cust => new CustomersViewModels()
                            {
                                CustomerId = cust.CustomerId,
                                CustomerName = cust.CustomerName,
                                StreetAddress = cust.StreetAddress,
                                City = cust.City,
                                State = cust.State,
                                ZipCode = cust.ZipCode,
                                ContactName = cust.ContactName,
                                ContactPhoneNumber = cust.ContactPhoneNumber,
                                UnlimitedUsers = cust.UnlimitedUsers,
                                UsersCount = cust.UsersCount,
                                Active = cust.Active
                            }).FirstOrDefault(),
                            UserName = admin.UserName,
                            UserFirstName = admin.FirstName,
                            UserLastName = admin.LastName,
                            Email = admin.Email,
                            Address = admin.Address,
                            City = admin.City,
                            State = admin.State,
                            PostalCode = admin.PostalCode,
                            PhoneNumber = admin.PhoneNumber,
                            Active = admin.Active
                        });
                    }
                    admins = admins.Where(w => w.Customer != null).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return admins;
        }

        public bool Add(AdminViewModels admin)
        {
            int RoleId = 2;
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var usr = new User()
                        {
                            CustomerId = admin.SelectedCustomerId,
                            UserName = admin.Email,
                            FirstName = admin.UserFirstName,
                            LastName = admin.UserLastName,
                            Password = admin.Password,
                            Email = admin.Email,
                            Address = admin.Address,
                            City = admin.City,
                            State = admin.State,
                            PostalCode = admin.PostalCode,
                            PhoneNumber = admin.PhoneNumber,
                            Active = true
                        };
                        db.Users.Add(usr);
                        db.SaveChanges();
                        int CustomerId = usr.CustomerId;
                        if (admin.SelectedCustomerId != 0)
                        {
                            RoleId = 2;
                        }
                        else
                        {
                            RoleId = 1;
                        }
                        var userInRole = new UserInRole
                        {
                            CustomerId = CustomerId,
                            UserId = usr.UserId,
                            RoleId = RoleId//0 ? db.Roles.Where(x => x.Name == Constant.ADMIN_ROLENAME).Select(x => x.RoleId).First() : db.Roles.Where(x => x.Name == Constant.SUPERADMIN_ROLENAME).Select(x => x.RoleId).First()
                        };
                        db.UserInRoles.Add(userInRole);
                        db.SaveChanges();
                        dbContextTransaction.Commit();
                        return true;
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }


        public bool AddSuperAdmin(SuperAdminViewModel admin)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var usr = new User()
                        {
                            CustomerId = 0,
                            UserName = admin.Email,
                            FirstName = admin.UserFirstName,
                            LastName = admin.UserLastName,
                            Password = admin.Password,
                            Email = admin.Email,
                            Address = admin.Address,
                            City = admin.City,
                            State = admin.State,
                            PostalCode = admin.PostalCode,
                            PhoneNumber = admin.PhoneNumber,
                            Active = true
                        };
                        db.Users.Add(usr);
                        db.SaveChanges();
                        int CustomerId = usr.CustomerId;
                        var userInRole = new UserInRole
                        {
                            CustomerId = CustomerId,
                            UserId = usr.UserId,
                            RoleId = db.Roles.Where(x => x.Name == Constant.ADMIN_ROLENAME).Select(x => x.RoleId).First()
                        };
                        db.UserInRoles.Add(userInRole);
                        db.SaveChanges();
                        dbContextTransaction.Commit();
                        return true;
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public AdminViewModels Find(int? id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                User admin = db.Users.Find(id);
                if (admin != null)
                {
                    return new AdminViewModels()
                    {
                        UserId = admin.UserId,
                        SelectedCustomerId = admin.CustomerId,
                        Customer = db.Customers.Where(x => x.CustomerId == admin.CustomerId).Select(cust => new CustomersViewModels()
                        {
                            CustomerId = cust.CustomerId,
                            CustomerName = cust.CustomerName,
                            StreetAddress = cust.StreetAddress,
                            City = cust.City,
                            State = cust.State,
                            ZipCode = cust.ZipCode,
                            ContactName = cust.ContactName,
                            ContactPhoneNumber = cust.ContactPhoneNumber,
                            UnlimitedUsers = cust.UnlimitedUsers,
                            UsersCount = cust.UsersCount,
                            Active = cust.Active
                        }).FirstOrDefault(),
                        UserName = admin.UserName,
                        UserFirstName = admin.FirstName,
                        UserLastName = admin.LastName,
                        Password = admin.Password,
                        ConfirmPassword = admin.Password,
                        Email = admin.Email,
                        Address = admin.Address,
                        City = admin.City,
                        State = admin.State,
                        PostalCode = admin.PostalCode,
                        PhoneNumber = admin.PhoneNumber,
                        Active = admin.Active
                    };
                }
                return null;
            }

        }

        public bool Update(AdminViewModels user)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        User usr = null;
                        UserInRole usrRole = null;
                        usrRole = db.UserInRoles.Where(w => w.UserId == user.UserId).FirstOrDefault();
                        if (CustomerID == 0)//Only Super Admin
                        {
                            usr = db.Users.Find(user.UserId);
                        }
                        else
                        {
                            usr = db.Users.Where(x => x.UserId == user.UserId && x.CustomerId == CustomerID).FirstOrDefault();
                        }
                        if (user.SelectedCustomerId != 0)
                        {
                            usrRole.RoleId = 2;
                        }
                        else
                        {
                            usrRole.RoleId = 1;
                        }

                        if (usr != null)
                        {
                            if (CustomerID == 0)//Only Super Admin
                            {
                                usr.CustomerId = user.SelectedCustomerId;
                            }
                            usr.Email = user.Email;
                            usr.Address = user.Address;
                            usr.City = user.City;
                            usr.State = user.State;
                            usr.PostalCode = user.PostalCode;
                            usr.PhoneNumber = user.PhoneNumber;
                            usr.Active = user.Active;
                            if (usr.Password != user.Password && !string.IsNullOrEmpty(user.Password))
                            {
                                usr.Password = user.Password;
                            }

                            db.SaveChanges();
                            dbContextTransaction.Commit();
                            return true;
                        }
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }

            }
            return false;
        }
        public void Delete(int id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                User user = null;
                if (CustomerID == 0)//Only Super Admin
                {
                    user = db.Users.Find(id);
                }
                else
                {
                    user = db.Users.Where(x => x.UserId == id && x.CustomerId == CustomerID).FirstOrDefault();
                }
                if (user != null)
                {
                    user.Active = false;
                    db.SaveChanges();
                }
            }
        }

        public bool IsExist(string userName)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                try
                {
                    return db.Users.Where(x => x.Email.ToUpper() == userName.ToUpper()).Any();
                }
                catch
                {
                    throw;
                }
            }
        }


        public bool IsExistEmail(string userName)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                try
                {
                    return db.Users.Where(x => x.Email.ToUpper() == userName.ToUpper()).Any();
                }
                catch
                {
                    throw;
                }
            }
        }

        public bool IsExistUserName(string userName)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                try
                {
                    return db.Users.Where(x => x.UserName.ToUpper() == userName.ToUpper()).Any();
                }
                catch
                {
                    throw;
                }
            }
        }

        #endregion

        #region Users

        public IList<UserViewModels> ListUsers()
        {
            IList<UserViewModels> users = new List<UserViewModels>();
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var listUsers = (from u in db.Users
                                     join uir in db.UserInRoles on u.UserId equals uir.UserId
                                     join r in db.Roles on uir.RoleId equals r.RoleId
                                     where r.Name == Constant.USER_ROLENAME || r.Name == Constant.ADMIN_ROLENAME
                                     select u).OrderBy(c => c.CustomerId).ToList();

                    //&& u.CustomerId == CustomerID
                    if (CustomerID > 0)
                        listUsers = listUsers.Where(w => w.CustomerId == CustomerID).ToList();

                    foreach (var usr in listUsers)
                    {
                        users.Add(new UserViewModels()
                        {
                            UserId = usr.UserId,
                            Customer = db.Customers.Where(x => x.CustomerId == usr.CustomerId).Select(cust => new CustomersViewModels()
                            {
                                CustomerId = cust.CustomerId,
                                CustomerName = cust.CustomerName,
                                StreetAddress = cust.StreetAddress,
                                City = cust.City,
                                State = cust.State,
                                ZipCode = cust.ZipCode,
                                ContactName = cust.ContactName,
                                ContactPhoneNumber = cust.ContactPhoneNumber,
                                UnlimitedUsers = cust.UnlimitedUsers,
                                UsersCount = cust.UsersCount,
                                Active = cust.Active
                            }).FirstOrDefault(),
                            UserName = usr.UserName,
                            UserFirstName = usr.FirstName,
                            UserLastName = usr.LastName,
                            Email = usr.Email,
                            Address = usr.Address,
                            City = usr.City,
                            State = usr.State,
                            PostalCode = usr.PostalCode,
                            PhoneNumber = usr.PhoneNumber,
                            Active = usr.Active
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return users;
        }

        public bool AddUser(UserViewModels user, string[] selectedRadio, string[] selectedPermissionLevel, string[] selectedTalkGroup, params string[] selectsysid)
        {

            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        string PasswordEecr = PoliceMonitor.Security.Crypto.AESEncrytDecry.EncryptStringAES(user.Password);
                        int custId = user.SelectedCustomerId > 0 ? user.SelectedCustomerId : CustomerID;
                        var usr = new User()
                        {
                            CustomerId = custId,
                            UserName = user.Email,
                            FirstName = user.UserFirstName,
                            LastName = user.UserLastName,
                            Password = PasswordEecr,
                            Email = user.Email,
                            Address = user.Address,
                            City = user.City,
                            State = user.State,
                            PostalCode = user.PostalCode,
                            PhoneNumber = user.PhoneNumber,
                            Active = true
                        };
                        db.Users.Add(usr);

                        int CustomerId = usr.CustomerId;
                        var userInRole = new UserInRole
                        {
                            CustomerId = CustomerId,
                            UserId = usr.UserId,
                            RoleId = db.Roles.Where(x => x.Name == Constant.USER_ROLENAME).Select(x => x.RoleId).First()
                        };
                        db.UserInRoles.Add(userInRole);

                        if (selectedPermissionLevel != null)
                        {
                            foreach (var select in selectedPermissionLevel)
                            {
                                var userPermission = new UserPermission() { UserId = usr.UserId, CustomerId = CustomerId, PermissionId = Convert.ToInt32(select) };
                                db.UserPermissions.Add(userPermission);
                            }

                        }
                        int count = 0;
                        foreach (var select in selectedTalkGroup)
                        {
                            var userTalkGroup = new UserTalkGroup() { UserId = usr.UserId, CustomerId = CustomerId, TakGroupId = Convert.ToInt32(select), SystemID = Convert.ToInt32(selectsysid[count]) };
                            db.UserTalkGroups.Add(userTalkGroup);
                            count++;
                        }
                        if (selectedRadio != null && selectedRadio.Length > 0)
                        {
                            foreach (var select in selectedRadio)
                            {
                                var userRadioMap = new UserRadioMap() { UserId = usr.UserId, CustomerId = CustomerId, RadioId = Convert.ToInt32(select) };
                                db.UserRadioMaps.Add(userRadioMap);
                            }
                        }
                        db.SaveChanges();
                        dbContextTransaction.Commit();
                        return true;
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public UserViewModels FindUser(int? id, int? custId)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                User user;
                if (CustomerID > 0)
                    user = db.Users.Where(x => x.UserId == id && x.CustomerId == CustomerID).FirstOrDefault();
                else
                    user = db.Users.Where(x => x.UserId == id && x.CustomerId == custId).FirstOrDefault();


                if (user != null)
                {
                    return new UserViewModels()
                    {
                        UserId = user.UserId,
                        Customer = db.Customers.Where(x => x.CustomerId == user.CustomerId).Select(cust => new CustomersViewModels()
                        {
                            CustomerId = cust.CustomerId,
                            CustomerName = cust.CustomerName,
                            StreetAddress = cust.StreetAddress,
                            City = cust.City,
                            State = cust.State,
                            ZipCode = cust.ZipCode,
                            ContactName = cust.ContactName,
                            ContactPhoneNumber = cust.ContactPhoneNumber,
                            UnlimitedUsers = cust.UnlimitedUsers,
                            UsersCount = cust.UsersCount,
                            Active = cust.Active
                        }).FirstOrDefault(),
                        UserName = user.UserName,
                        UserFirstName = user.FirstName,
                        UserLastName = user.LastName,
                        Password = user.Password,
                        ConfirmPassword = user.Password,
                        Email = user.Email,
                        Address = user.Address,
                        City = user.City,
                        State = user.State,
                        PostalCode = user.PostalCode,
                        PhoneNumber = user.PhoneNumber,
                        Active = user.Active,
                        TalkGroupList = db.UserTalkGroups.Where(x => x.CustomerId == user.CustomerId && x.UserId == user.UserId).Select(x => new SelectListItem() { Value = x.SystemID + "_" + x.TakGroupId.ToString(), Text = x.TakGroupId.ToString() }).ToList(),
                        PermissionLevelList = user.UserPermissions.Select(x => new SelectListItem() { Value = x.PermissionId.ToString(), Text = x.Permission.Permission1.ToString() }).ToList()
                    };
                }
                return null;
            }

        }

        public bool UpdateUser(UserViewModels user, string[] selectedRadio, string[] selectedPermissionLevel, string[] selectedTalkGroup, params string[] selectSysID)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                using (var dbContextTransaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        int custId = user.SelectedCustomerId > 0 ? user.SelectedCustomerId : CustomerID;
                        User usr = db.Users.Where(x => x.UserId == user.UserId && x.CustomerId == custId).FirstOrDefault();
                        if (usr != null)
                        {
                            usr.UserName = user.UserName;
                            usr.FirstName = user.UserFirstName;
                            usr.LastName = user.UserLastName;
                            usr.Email = user.Email;
                            usr.Address = user.Address;
                            usr.City = user.City;
                            usr.State = user.State;
                            usr.PostalCode = user.PostalCode;
                            usr.PhoneNumber = user.PhoneNumber;
                            usr.Active = user.Active;
                            if (usr.Password != user.Password && !string.IsNullOrEmpty(user.Password))
                            {
                                usr.Password = user.Password;
                            }

                            #region selectedPermission

                            var userPermissionList = db.UserPermissions.Where(x => x.UserId == usr.UserId).ToList();
                            //Add item that if does not exist in db

                            //Remove the does not exist in userPermissionList
                            if (userPermissionList.Any())
                            {
                                foreach (var userPermission in userPermissionList)
                                {
                                    // if (!selectedPermissionLevel.Where(x => x == userPermission.PermissionId.ToString()).Any())
                                    // {
                                    db.UserPermissions.Remove(userPermission);
                                    //}
                                }
                            }
                            if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                            {
                                foreach (var select in selectedPermissionLevel)
                                {
                                    //if (!userPermissionList.Where(x => x.PermissionId == Convert.ToInt32(select)).Any())
                                    //{
                                    var userPermission = new UserPermission() { UserId = usr.UserId, CustomerId = custId, PermissionId = Convert.ToInt32(select) };
                                    db.UserPermissions.Add(userPermission);
                                    // }
                                }
                            }
                            //  }
                            #endregion

                            #region userTalkGroup
                            var userTalkGroupList = db.UserTalkGroups.Where(x => x.UserId == usr.UserId).ToList();
                            //Add item that if does not exist in db
                            int count = 0;
                            foreach (var select in selectedTalkGroup)
                            {
                                if (!userTalkGroupList.Where(x => x.TakGroupId == Convert.ToInt32(select) && x.SystemID == Convert.ToInt32(selectSysID[count])).Any())
                                {
                                    var userTalkGroup = new UserTalkGroup() { UserId = usr.UserId, CustomerId = custId, TakGroupId = Convert.ToInt32(select), SystemID = Convert.ToInt32(selectSysID[count]) };
                                    db.UserTalkGroups.Add(userTalkGroup);
                                }
                                count++;
                            }
                            //Remove the does not exist in userTalkGroupList
                            foreach (var talkGroup in userTalkGroupList)
                            {
                                if (!selectedTalkGroup.Where(x => x == talkGroup.TakGroupId.ToString()).Any())
                                {
                                    db.UserTalkGroups.Remove(talkGroup);
                                }
                            }
                            #endregion

                            #region selectedRadio
                            if (selectedRadio != null && selectedRadio.Length > 0)
                            {
                                var userRadioList = db.UserRadioMaps.Where(x => x.UserId == usr.UserId).ToList();
                                //Add item that if does not exist in db
                                foreach (var select in selectedRadio)
                                {
                                    if (!userRadioList.Where(x => x.RadioId == Convert.ToInt32(select)).Any())
                                    {
                                        var userRadioMap = new UserRadioMap() { UserId = usr.UserId, CustomerId = custId, RadioId = Convert.ToInt32(select) };
                                        db.UserRadioMaps.Add(userRadioMap);
                                    }
                                }
                                //Remove the does not exist in userRadioList
                                foreach (var talkRadio in userRadioList)
                                {
                                    if (!selectedRadio.Where(x => x == talkRadio.RadioId.ToString()).Any())
                                    {
                                        db.UserRadioMaps.Remove(talkRadio);
                                    }
                                }
                            }
                            #endregion


                            db.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                    }
                    catch
                    {
                        dbContextTransaction.Rollback();
                        throw;
                    }
                }

            }
            return false;
        }

        public void DeleteUser(int id, int? custId)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                User user;
                if (custId > 0)
                    user = db.Users.Where(x => x.UserId == id && x.CustomerId == custId).FirstOrDefault();
                else
                    user = db.Users.Where(x => x.UserId == id && x.CustomerId == CustomerID).FirstOrDefault();
                if (user != null)
                {
                    user.Active = false;
                    db.SaveChanges();
                }
            }
        }

        public IList<int> GetRadioListUser(int? id)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    return db.UserRadioMaps.Where(x => x.UserId == id && x.CustomerId == CustomerID).Select(x => x.RadioId).ToList<int>();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region permission level

        public IList<PoliceMonitor.Entities.Permission> GetPermissionLevel(CustomPrincipal user)
        {
            int customerId = user.CustomerId;
            bool isSuperAdmin = user.IsInRole(Constant.SUPERADMIN_ROLENAME);

            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    if (isSuperAdmin)
                    {
                        return db.Permissions.ToList();
                    }
                    else
                    {
                        List<int> permissions = db.CustomerPermissions.Where(x => x.CustomerId == customerId).Select(x => x.Permission.PermissionId).ToList();
                        return db.Permissions.Where(x => permissions.Contains(x.PermissionId)).ToList();
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Customer GetTalkGroups

        public IList<CustomerTalkGroup> GetCustomerTalkGroups()
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    return db.CustomerTalkGroups.Where(x => x.CustomerId == CustomerID).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IList<CustomerTalkGroup> GetCustomerTalkGroups(int customerId)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    return db.CustomerTalkGroups.Where(x => x.CustomerId == customerId).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}