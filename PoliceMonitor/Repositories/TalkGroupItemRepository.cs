﻿using PoliceMonitor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PoliceMonitor.Repositories
{
    public class TalkGroupItemRepository
    {       
        public IList<TalkGroupItem> FetchTalkGroupItemsFromDB(string[] ID)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {

                    db.Database.CommandTimeout = 600;
                    int range = 2000;
                    if (ID.Length > range)
                    {
                        List<TalkGroupItem> list = new List<TalkGroupItem>();
                        // var batchs = ID.Batch<string>(range);

                        List<string[]> batchs = new List<string[]>();
                        for (int i = 0; i < ID.Length; i += range)
                        {
                            batchs.Add(ID.Skip(i).Take(range).ToArray());
                        }

                        foreach (var batch in batchs)
                        {
                            string sqlQuery = string.Format(@"select * from  TalkGroupItem where TalkGroupItemId in ('{0}')", string.Join("','", batch.ToArray()));
                            list.AddRange(db.Database.SqlQuery<TalkGroupItem>(sqlQuery).ToList());
                        }
                        return list;
                    }
                    else
                    {
                        string sqlQuery = string.Format(@"select * from  TalkGroupItem where TalkGroupItemId in ('{0}')", string.Join("','", ID));
                        return db.Database.SqlQuery<TalkGroupItem>(sqlQuery).ToList();
                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        public List<Models.TalkGroupItem> GetTalkGroupItems(DateTime? startDate, DateTime? endDate, List<int> talkgroupIds,int[] SystemID, int startHour, int endHour, int minTransmissionSec, bool isServerTime)
        {
            List<Models.TalkGroupItem> result = new List<Models.TalkGroupItem>();
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {                    
                    List<PolledTalkGroupItem> tgItems=new List<PolledTalkGroupItem>(); 
                    DateTime sd;
                    DateTime? ed = null;
                    if(startDate == null || endDate == null)
                    {
                        sd = DateTime.UtcNow.AddHours(-1 * startHour);
                        if(endHour > 0)
                            ed = sd.AddHours(endHour);
                    }
                    else if (!isServerTime)
                    {
                        sd = Utils.Utils.ToServerTime(startDate.Value).ToUniversalTime();
                        ed = Utils.Utils.ToServerTime(endDate.Value).ToUniversalTime();
                    }
                    else
                    {
                        sd = startDate.Value;
                        ed = endDate.Value;
                    }

                    int id = SystemID[0];
                    var lst = db.PolledTalkGroupItems.Where(t => t.TransStartUTC >= sd && talkgroupIds.Contains((int)t.TalkGroupID.Value)).ToList();

                    for (int i = 0; i < SystemID.Distinct().ToArray().Length; i++)
                    {
                        if (ed == null)
                        {
                            tgItems.AddRange(lst.Where(t => t.SystemID == SystemID[i]).ToList());
                        }
                        else
                        {
                            tgItems.AddRange(lst.Where(t => t.SystemID == SystemID[i] && (t.TransStartUTC.Value.Year <= ed.Value.Year && t.TransStartUTC.Value.Month <= ed.Value.Month && t.TransStartUTC.Value.Date <= ed.Value.Date)).ToList());
                        }
                    }
                    foreach (PolledTalkGroupItem item in tgItems)
                    {
                        Models.TalkGroupItem it = new Models.TalkGroupItem();
                        it.ID = item.TalkGroupItemId.ToString();
                        it.RadioDesc = item.RadioDesc;
                        it.RadioID = item.RadioID.Value;
                        it.TalkGroupID = item.TalkGroupID.Value;
                        it.TransLengthMSec = item.TransLengthMSec.Value;
                        it.TransStartMSec = item.TransStartMSec.Value;
                        it.SystemID = item.SystemID.Value + "_" + item.TalkGroupID.Value;

                        result.Add(it);
                    }
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TalkGroupItem GetTalkGroupItem(string ID)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var talkGroupObj = db.TalkGroupItems.Where(t => t.TalkGroupItemId == ID).FirstOrDefault();
                    if (talkGroupObj != null)
                    {
                        return talkGroupObj;  
                    }
               }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }

        public bool MarkedReview(PoliceMonitor.Models.TalkGroupItem talkGroupItem)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var talkGroupObj = db.TalkGroupItems.Where(t => t.TalkGroupItemId == talkGroupItem.ID.ToString()).FirstOrDefault();
                    if (talkGroupObj == null)
                    {
                        var talkItem = Add(talkGroupItem);
                        db.TalkGroupItems.Add(talkItem);
                        db.SaveChanges();
                    }
                    else
                    {
                        talkGroupObj.ReviewStatus = talkGroupItem.ReviewStatus;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch(Exception)
            {
                throw;
            }
        }

        public bool MarkListen(PoliceMonitor.Models.TalkGroupItem talkGroupItem)
        {
            try
            {
                using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
                {
                    var talkGroupObj = db.TalkGroupItems.Where(t => t.TalkGroupItemId == talkGroupItem.ID.ToString()).FirstOrDefault();
                    if (talkGroupObj == null)
                    {
                        var talkItem = Add(talkGroupItem);
                        db.TalkGroupItems.Add(talkItem);
                        db.SaveChanges();
                    }
                    else
                    {
                        talkGroupObj.MarkListen = talkGroupItem.MarkListen;
                        if(talkGroupObj.UserName.IndexOf(talkGroupItem.UserIds) < 0)
                            talkGroupObj.UserName = talkGroupObj.UserName + ";" + talkGroupItem.UserIds;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public User GetUserDetails(int id)
        {
            using (PoliceMonitorDBEntities db = new PoliceMonitorDBEntities())
            {
                User user = db.Users.Where(x => x.UserId == id).FirstOrDefault();
                return user;
            }
        }

        private TalkGroupItem Add(PoliceMonitor.Models.TalkGroupItem talkGroupItem)
        {
            var talkItem = new TalkGroupItem
            {
                TalkGroupItemId = talkGroupItem.ID,
                TransStartMSec = talkGroupItem.TransStartMSec,
                TransLengthMSec = talkGroupItem.TransLengthMSec,
                RadioID = talkGroupItem.RadioID,
                TalkGroupID = talkGroupItem.TalkGroupID,
                TransStartUTC = talkGroupItem.TransStartUTC,
                TransTimeDesc = talkGroupItem.TransTimeDesc,
                RadioDesc = talkGroupItem.RadioDesc,
                ReviewStatus = talkGroupItem.ReviewStatus,
                MarkListen = talkGroupItem.MarkListen,
                UserName = talkGroupItem.UserIds 
                //talkGroupItem.UserFirstName + " " + talkGroupItem.UserLastName               
            };
            return talkItem;
        }
    }
}