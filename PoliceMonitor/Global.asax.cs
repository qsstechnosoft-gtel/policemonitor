﻿using PoliceMonitor.Security.Base.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PoliceMonitor
{
    public class MvcApplication : BaseHttpApplication//System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(Server.MapPath("~/Web.config")));
            AreaRegistration.RegisterAllAreas();
            RegisterSecurityGlobalFilters(GlobalFilters.Filters);  //Add to incorporate prommis security
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        public override void PrepareWhiteListSecurityPolicy()
        {
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("Account");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("Home");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("Manage");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("Customers");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("Reports");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("Users");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("CustomerNotification");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("Group");
            WhiteListSecurityPolicy.AuthorizeAccessForAllAction("MyRadios");
            WhiteListSecurityPolicy.AnonymousAccessForAction("Account", "Login");
            WhiteListSecurityPolicy.AnonymousAccessForAction("Account", "AccessDenied");
            WhiteListSecurityPolicy.AnonymousAccessForAction("Account", "ForgotPassword");
            

        }
    }
}
