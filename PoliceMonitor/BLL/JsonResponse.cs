﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoliceMonitor.BLL
{
    public class JsonResponseIndexMatch
    {
        public List<List<object>> matches { get; set; }
    }

    public class JsonResponseTalkGroup
    {
        public List<List<object>> talkgroups { get; set; }
    }


    public class JsonResponseRadio
    {
        public string __invalid_name__100003 { get; set; }
        public string __invalid_name__100012 { get; set; }
        public string __invalid_name__100014 { get; set; }
    }
}
