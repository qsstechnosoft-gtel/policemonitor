﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PoliceMonitor.Entities;
using PoliceMonitor.Security.Base.Controller;
using PoliceMonitor.Security.Attributes;
using PoliceMonitor.Models;
using PoliceMonitor.Repositories;
using PoliceMonitor.BLL;
using System.Threading.Tasks;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Utils;

namespace PoliceMonitor.Controllers
{
    public class GroupController : BaseController
    {
        private GroupsRepository groupRepository = null;
        private AdminRepository adminRepository;
        public GroupController()
        {
            groupRepository = new GroupsRepository(User);
            adminRepository = new AdminRepository(User);
        }

        // GET: CustomerNotification
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Index()
        {
            var groups = groupRepository.ListGroups();

            foreach(var group in groups)
            {
                foreach (var talkGroup in group.TalkGroupList)
                {
                    foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User, false))
                    {
                        if (x.Value == talkGroup.Value)
                        {
                            talkGroup.Text = x.Text;
                            break;
                        }
                    }
                }
            }
            return View(groups);
        }

        // GET: CustomerNotification/Details/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var group = groupRepository.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }

            foreach (var talkGroup in group.TalkGroupList)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User, false))
                {
                    if (x.Value == talkGroup.Value)
                    {
                        talkGroup.Text = x.Text;
                        break;
                    }
                }
            }
            return View(group);
        }

        // GET: CustomerNotification/Create
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Create()
        {
            GroupViewModels groupModel = new GroupViewModels();
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            List<SelectListItem> listUsers = new List<SelectListItem>();
         
            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User, false))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text });
                        break;
                    }
                }
            }

            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName });
            }
            ViewBag.TalkGroups = listTalkGroups;
            ViewBag.AllUsers = listUsers;
            return View(groupModel);
        }

        // POST: CustomerNotification/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Create([Bind(Include = "GroupId,GroupName")] GroupViewModels groupModel,
            string[] SelectedUser,params string[] selectedTalkGroup)
        {
            if (ModelState.IsValid)
            {
                if (SelectedUser != null && SelectedUser.Length > 0)
                {
                    if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                    {
                        groupRepository.Add(groupModel,SelectedUser, selectedTalkGroup);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please must select any talk group.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please must select any user.");
                }
            }
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            List<SelectListItem> listUsers = new List<SelectListItem>();

            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        var selected = false;
                        if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                        {
                            selected = selectedTalkGroup.Where(z => z == talkGroup.TalkGroup.ToString()).Any();
                        }
                            listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text, Selected = selected });
                            break;
                    }
                }
            }
            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                var selected = false;
                if (SelectedUser != null && SelectedUser.Length > 0)
                {
                    selected = SelectedUser.Where(z => z == user.UserId.ToString()).Any();
                }
                    listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName, Selected = selected });
                
            }
            ViewBag.TalkGroups = listTalkGroups;
            ViewBag.AllUsers = listUsers;
            return View(groupModel);
        }

        // GET: CustomerNotification/Edit/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Edit(int? id)
        {
           List<SelectListItem> listTalkGroups = new List<SelectListItem>();
           List<SelectListItem> listUsers = new List<SelectListItem>();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var group = groupRepository.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text });
                        break;
                    }
                }
            }

            foreach (var talkGroup in group.TalkGroupList)
            {
                foreach (var _talkGroup in listTalkGroups)
                {
                    if (_talkGroup.Value == talkGroup.Value)
                    {
                        _talkGroup.Selected = true;
                    }
                }
            }
            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName });
            }
            foreach (var usr in group.Users)
            {
                foreach (var _user in listUsers)
                {
                    if (_user.Value == usr.UserId.ToString())
                    {
                        _user.Selected = true;
                    }
                }
            }
            ViewBag.TalkGroups = listTalkGroups;
            ViewBag.AllUsers = listUsers;
            return View(group);
        }

        // POST: CustomerNotification/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Edit([Bind(Include = "GroupId,GroupName")] GroupViewModels groupModel, string[] SelectedUser, params string[] selectedTalkGroup)
        {
            if (ModelState.IsValid)
            {
                if (SelectedUser != null && SelectedUser.Length > 0)
                {
                    if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                    {
                        groupRepository.Update(groupModel,SelectedUser, selectedTalkGroup);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please must select any talk group.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please must select any user.");
                }
            }
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            List<SelectListItem> listUsers = new List<SelectListItem>();
            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        var selected = false;
                        if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                        {
                            selected = selectedTalkGroup.Where(z => z == talkGroup.TalkGroup.ToString()).Any();
                        }
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text, Selected = selected });
                        break;
                    }
                }
            }
            var group = groupRepository.Find(groupModel.GroupId);
            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                var selected = false;
                if (SelectedUser != null && SelectedUser.Length > 0)
                {
                    selected = SelectedUser.Where(z => z == user.UserId.ToString()).Any();
                }
                listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName, Selected = selected });
            }
            foreach (var usr in group.Users)
            {
                foreach (var _user in listUsers)
                {
                    if (_user.Value == usr.UserId.ToString())
                    {
                        _user.Selected = true;
                    }
                }
            }
            ViewBag.TalkGroups = listTalkGroups;
            ViewBag.AllUsers = listUsers;
            return View(groupModel);
        }

        // GET: CustomerNotification/Delete/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var group = groupRepository.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            foreach (var talkGroup in group.TalkGroupList)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.Value)
                    {
                        talkGroup.Text = x.Text;
                        break;
                    }
                }
            }
            return View(group);
        }

        // POST: CustomerNotification/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public ActionResult DeleteConfirmed(int id)
        {
            groupRepository.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
