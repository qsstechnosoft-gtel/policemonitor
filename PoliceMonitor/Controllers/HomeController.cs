﻿using Newtonsoft.Json;
using PoliceMonitor.BLL;
using PoliceMonitor.Models;
using PoliceMonitor.Repositories;
using PoliceMonitor.Security.Base.Controller;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Security.Principal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using PoliceMonitor.Security.Attributes;
using Newtonsoft.Json.Linq;
using System.IO;
using PoliceMonitor.Utils;
using System.Collections;

namespace PoliceMonitor.Controllers
{
    public class HomeController : BaseController
    {
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }

        private TalkGroupItemRepository talkGroupItemRepository = new TalkGroupItemRepository();
        private static bool TEST = true;

        private IList<SelectListItem> reviewStatusListItems = new List<SelectListItem>{
                //new SelectListItem{Text = "Post audio", Value = "1"},
                //new SelectListItem{Text = "Post to FB", Value = "2"},
                new SelectListItem{Text = "Follow up", Value = "3"}
           };
        private IList<SelectListItem> markListenListItems = new List<SelectListItem>{
                new SelectListItem{Text = "Listened", Value = "Listened"},
                new SelectListItem{Text = "Not Listen", Value = "Not Listen"}
           };

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public HomeController()
        {
            _userInfo = User;
        }
        PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();


        public ActionResult Index()
        {
            //try
            //{
            //    if (Session["UserTalkGroups"] == null)
            //    {
            //        AdminRepository adminRepository = new AdminRepository(User);
            //        var listTalkGroups = new List<SelectListItem>();
            //        {
            //            Utility.Get().TalkGroupView = new TalkGroupView();
            //            {
            //               PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();

            //               var talkgroups = dbo.TalkGroups.Join(dbo.CustomerTalkGroups, tkg => tkg.ID.ToString(), ctg => ctg.TalkGroup, (tkg, ctg) => new { TKG = tkg, CTG = ctg }).Where(x => x.TKG.ID.ToString() == x.CTG.TalkGroup && x.CTG.CustomerId==User.CustomerId).Select(x => x.TKG).ToList();

            //                if(UserInfo.IsInRole(Constant.SUPERADMIN_ROLENAME))
            //                {
            //                    talkgroups = dbo.TalkGroups.ToList();
            //                }

            //                foreach (var info in talkgroups)
            //                {
            //                    string tmp = " (" + (string)info.County + ") " + (string)info.Name;
            //                    CacheUtil.GetCustomerTalkgroupsSelectList(User).Add(new SelectListItem() { Value = info.ID.ToString(), Text = string.Format("{0}", tmp) });

            //                    if (UserInfo.IsInRole(Constant.SUPERADMIN_ROLENAME))
            //                    {
            //                        listTalkGroups.Add(new SelectListItem() { Value = "" + info.ID, Text = string.Format("{0}", tmp) });
            //                    }
            //                    else
            //                    {
            //                        if (UserInfo.UserTalkGroupIDs.Exists(c => c == info.ID) == true)
            //                        {
            //                            listTalkGroups.Add(new SelectListItem() { Value = "" + info.ID, Text = string.Format("{0}", tmp) });
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        Session["UserTalkGroups"] = listTalkGroups;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    Utility.Get().TalkGroupView.ErrorMessage = ex.Message;
            //}
            var date= dbo.ActivityLogs.Where(w => w.userId == _userInfo.UserId && w.activity=="LOGIN").OrderByDescending(o=>o.id).Take(2).ToList();
            ViewBag.LoginDate = Convert.ToString(date.OrderBy(o=>o.id).Select(s=>s.createdOn).FirstOrDefault());
            return View();
        }



        public ActionResult Tyler_Technology()
        {
            return View();
        }



        public ActionResult Playback()
        {
            // check if user has permission for playback
            if (!(_userInfo.HasPermission(PoliceMonitor.Security.Base.Model.Constant.PLAYBACK)))
            {
                AuthorizationContext filterContext = new AuthorizationContext();
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                Response.Redirect("~/Account/AccessDenied");
                return null;
            }

            var talkGroupView = new TalkGroupView();

            talkGroupView.StartHour = GetValue(_userInfo.HoursAgo, Utility.Get().StartHour);
            talkGroupView.EndHour = GetValue(_userInfo.HoursDuration, Utility.Get().EndHour);
            talkGroupView.MinTranSeconds = GetValue(_userInfo.MinTransmissionTime, Utility.Get().MinTranSeconds);

            List<TalkGroupModel> model = new List<TalkGroupModel>();
            List<RadioGroupModel> model1 = new List<RadioGroupModel>();

            // Check if there are talkgroups from session, set them on view if available
            TalkGroupView Result = this.HttpContext.Session["_SavedTalkGroupView"] as TalkGroupView;
            if (Result == null)
            {
                Result = talkGroupView;
            }
            Result.TalkGroups = Utils.CacheUtil.GetUserTalkgroups(User);

            // fetch the favourite list
            Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var favouriteList = dbo.PlaybackFavourites.Where(x => x.UserId == User.UserId && x.Active == true);
            List<int> termsList = new List<int>();
            List<int> termsListSysID = new List<int>();
            if (favouriteList.Any())
            {
                //if (Result.SelectedFavouriteID <= 0)
                {
                    Entities.PlaybackFavourite selectedFavourite = null;
                    if (Result.SelectedFavouriteID <= 0)
                        selectedFavourite = favouriteList.Where(x => x.IsPrimary == true).FirstOrDefault();
                    else
                        selectedFavourite = favouriteList.Where(x => x.FavouriteID == Result.SelectedFavouriteID).FirstOrDefault();
                    if (selectedFavourite != null)
                    {
                        var itemToAdd = new Newtonsoft.Json.Linq.JObject();
                        Object json = JsonConvert.DeserializeObject(selectedFavourite.FavouriteView);
                        JArray jsonarr = json as JArray;

                        foreach (var item in jsonarr)
                        {
                            JObject obj = item as JObject;

                            if ("SelectedTalkGroups".Equals(obj["name"].ToString()))
                            {
                                termsList.Add(Convert.ToInt32(obj["value"].ToString()));
                                if (obj["SysID"] != null)
                                {
                                    termsListSysID.Add(Convert.ToInt32(obj["SysID"].ToString()));
                                }
                                else
                                {
                                    termsListSysID.Add(1039);
                                }
                            }
                        }

                        Result.SelectedTalkGroups = termsList.ToArray();
                        Result.SelectedSystemID = termsListSysID.ToArray();

                        Result.SelectedFavouriteID = selectedFavourite.FavouriteID;
                    }
                }
            }

            Result.PlayBackFavouriteList.Clear();
            //if (Result.PlayBackFavouriteList.Count <= 0)
            {
                List<SelectListItem> favList = favouriteList.ToList().Select(rT => new SelectListItem() { Value = rT.FavouriteID.ToString(), Text = rT.Name.Trim() }).ToList();
                Result.PlayBackFavouriteList.AddRange(favList);
            }
            var systId = dbo.TblSystems.ToList();

            bool isSuperAdmin = false || User.IsInRole(Constant.SUPERADMIN_ROLENAME);

            if (!isSuperAdmin)
            {

                model = Utils.Utils.GetTalkgroupModelList(termsList, termsListSysID, User, UserInfo);
            }
            else
            {
                int i = 1;
                foreach (var item in Result.TalkGroups)
                {
                    var a = item.Text.Split('(', ')');
                    string county = a[1];
                    string sysName = a[0];
                    var sysID = systId.Where(z => z.Name.Trim() == sysName.Trim()).ToList();
                    int chkduplicate = model.FindIndex(m => m.ChannelName == county);
                    if (chkduplicate <= -1)
                    {
                        model.Add(new TalkGroupModel() { TalkGroupID = long.Parse(item.Value), ChannelName = county, SystemName = sysName, SysID = sysID[0].ID });
                        i++;
                    }
                    var addChildModel = model.First(m => m.ChannelName == county);
                    int selectedRadio = 0;
                    if (Result.SelectedTalkGroups != null)
                    {
                        selectedRadio = Array.Find(Result.SelectedTalkGroups, element => element.Equals(int.Parse(item.Value)));
                    }
                    if (sysID[0].ID == 16961)
                    {
                        addChildModel.RadioGroupModel.Add(new RadioGroupModel()
                        {
                            RadioID = int.Parse(item.Value),
                            RadioDesc = a[4],
                            IsSelect = (selectedRadio > 0 ? true : false)
                        });
                    }
                    else
                    {
                        addChildModel.RadioGroupModel.Add(new RadioGroupModel()
                        {
                            RadioID = int.Parse(item.Value),
                            RadioDesc = a[2],
                            IsSelect = (selectedRadio > 0 ? true : false)
                        });
                    }
                    int selectedListCount = addChildModel.RadioGroupModel.Where(aa => aa.IsSelect == true).ToList().Count;
                    if (selectedListCount == addChildModel.RadioGroupModel.Count)
                    {
                        addChildModel.IsSelect = true;
                        addChildModel.AllNotChecked = false;
                    }
                    else if (selectedListCount == 0)
                    {
                        addChildModel.AllNotChecked = false;
                    }
                    else if (selectedListCount < addChildModel.RadioGroupModel.Count)
                    {
                        addChildModel.AllNotChecked = true;
                    }
                    else
                    {
                        addChildModel.AllNotChecked = false;
                    }
                }
            }
            string[] SystemNames = model.Select(x => x.SystemName).Distinct().ToArray();//dbo.TblSystems.Select(x => x.Name).ToArray();
            // Sort talkgroups and radios
            Hashtable ht = new Hashtable();
            for (int x = 0; x < SystemNames.Count(); x++)
            {
                int total = 0;
                List<TalkGroupModel> mm = model.Where(a => a.SystemName.Trim() == SystemNames[x].Trim()).ToList();
                if (mm.Count != 0)
                {
                    foreach (TalkGroupModel m in mm)
                    {
                        m.RadioGroupModel = m.RadioGroupModel.OrderBy(t => t.RadioDesc).ToList();
                        total += m.RadioGroupModel.Count() + 1;
                    }

                    // calculate the size at which left and right split of displayed talkgroups should be done
                    total = total / 2;
                    int count = 0;
                    int size = 0;
                    foreach (TalkGroupModel m in mm)
                    {
                        count += m.RadioGroupModel.Count() + 1;
                        size++;
                        if (count >= total)
                            break;
                    }
                    ht.Add(SystemNames[x], size);

                }
            }
            Result.TalkGroupModel = model.OrderBy(t => t.ChannelName).ToList(); ;
            Result.Size = ht;
            ViewBag.UserInfo = UserInfo;
            
            Result.SystemNam = SystemNames;
            Result.RadioID = "";
            this.HttpContext.Session["_PlaybackView"] = Result;
            return View(Result);
        }

        private int GetValue(int? val1, int val2)
        {
            if (val1 != null && val1.HasValue)
                return val1.Value;

            return val2;
        }

        [HttpPost]
        public ActionResult SaveView(string model, string name, bool isPrimary, int id, bool ListenLivePrimary)
        {
            var itemToAdd = new Newtonsoft.Json.Linq.JObject();


            if (ModelState.IsValid)
            {
                using (PoliceMonitor.Entities.PoliceMonitorDBEntities db = new PoliceMonitor.Entities.PoliceMonitorDBEntities())
                {
                    try
                    {
                        PoliceMonitor.Entities.PlaybackFavourite fav = new PoliceMonitor.Entities.PlaybackFavourite();
                        if (name.Trim() == "")
                        {
                            return Json(new { Response = "Name is required." });
                        }
                        if (id > 0)
                        {
                            fav = db.PlaybackFavourites.SingleOrDefault(f => f.FavouriteID == id);
                        }
                        else
                        {
                            db.PlaybackFavourites.Add(fav);
                        }

                        var chkDuplicate = db.PlaybackFavourites.Where(f => f.Name == name.Trim() && f.UserId == User.UserId && f.Active == true && f.FavouriteID != id).ToList();
                        if (chkDuplicate.Any())
                        {
                            return Json(new { Response = "Duplicate Name." });
                        }

                        if (isPrimary)
                        {
                            var update = db.PlaybackFavourites.SingleOrDefault(f => f.UserId == User.UserId && f.IsPrimary == true && f.Active == true);
                            if (update != null)
                            {
                                update.IsPrimary = false;
                            }
                        }
                        else if (ListenLivePrimary)
                        {
                            var update = db.PlaybackFavourites.SingleOrDefault(f => f.UserId == User.UserId && f.ListenLivePrimary == true && f.Active == true);
                            if (update != null)
                            {
                                update.ListenLivePrimary = false;
                            }
                        }
                        fav.Name = name.Trim();
                        fav.IsPrimary = isPrimary;
                        if (model.Length > 2)
                        {
                            fav.FavouriteView = model;
                        }
                        else
                        {
                            fav.FavouriteView = fav.FavouriteView;
                        }

                        fav.UserId = User.UserId;
                        fav.Active = true;
                        fav.ListenLivePrimary = ListenLivePrimary;

                        var flag = db.SaveChanges();
                        if (flag >= 1)
                        {
                            return Json(new { Response = "Success" });
                        }
                        return Json(new { Response = "Error" });
                    }
                    catch
                    {
                        return Json(new { Response = "Error" });
                    }

                }
            }
            return Json(new { Response = "Error" });
        }

        [HttpGet]
        public string ShowFavouriteData(int id)
        {
            using (var db = new PoliceMonitor.Entities.PoliceMonitorDBEntities())
            {
                try
                {
                    var favSelectedData = db.PlaybackFavourites.Where(a => a.FavouriteID == id).ToList();

                    var itemToAdd = new Newtonsoft.Json.Linq.JObject();
                    if (favSelectedData.Any())
                    {
                        itemToAdd["View"] = favSelectedData.First().FavouriteView;
                        itemToAdd["Name"] = favSelectedData.First().Name.Trim();
                        itemToAdd["FavouriteID"] = favSelectedData.First().FavouriteID;
                        //itemToAdd["sysid"] = favSelectedData.First().SysID;
                        itemToAdd["IsPrimary"] = favSelectedData.First().IsPrimary;
                    }
                    return JsonConvert.SerializeObject(itemToAdd); ;
                }
                catch
                {
                    return "Error";
                }

            }
        }

        [HttpGet]
        public ActionResult DeleteView(int id)
        {
            using (var db = new PoliceMonitor.Entities.PoliceMonitorDBEntities())
            {
                try
                {
                    var favSelectedData = db.PlaybackFavourites.Where(a => a.FavouriteID == id);
                    if (favSelectedData.Any())
                    {
                        favSelectedData.SingleOrDefault().Active = false;
                        if (db.SaveChanges() > 0)
                        {
                            // this.Json("you result", JsonRequestBehavior.AllowGet);
                            return Json(new { Response = "Success" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Response = "Error" }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new { Response = "Error" }, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(new { Response = "Error" }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpGet]
        public ActionResult ActivityDashboard()
        {
            if (!(_userInfo.HasPermission(PoliceMonitor.Security.Base.Model.Constant.LIVELISTENING)))
            {
                AuthorizationContext filterContext = new AuthorizationContext();
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                Response.Redirect("~/Account/AccessDenied");
            }
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var favouriteList = dbo.PlaybackFavourites.Where(x => x.UserId == User.UserId && x.Active == true);

            PlayBackIndexModel pbm = new PlayBackIndexModel();
            List<SelectListItem> favList = new List<SelectListItem>();

            favList = favouriteList.ToList().Select(rT => new SelectListItem() { Value = rT.FavouriteID.ToString(), Text = rT.Name.Trim() }).ToList();
            pbm.ListenLiveFavouriteList.AddRange(favList);
            var selectedID = dbo.PlaybackFavourites.Where(x => x.UserId == User.UserId && x.ListenLivePrimary == true && x.Active == true).ToList();
            List<int> termsList = new List<int>();
            List<int> termsListSys = new List<int>();
            if (selectedID.Any())
            {
                var FavView = selectedID.FirstOrDefault().FavouriteView;
                var itemToAdd = new Newtonsoft.Json.Linq.JObject();
                Object json = JsonConvert.DeserializeObject(FavView);
                JArray jsonarr = json as JArray;

                foreach (var item in jsonarr)
                {
                    JObject obj = item as JObject;

                    if ("SelectedTalkGroups".Equals(obj["name"].ToString()))
                    {
                        string n = obj["value"].ToString();
                        termsList.Add(Convert.ToInt32(n));
                        if (obj["SysID"] != null)
                        {
                            termsListSys.Add(Convert.ToInt32(obj["SysID"].ToString()));
                        }
                        else
                        {
                            termsListSys.Add(Convert.ToInt32("1039"));
                        }
                    }

                }
                pbm.SelectedSystemID = termsListSys.ToArray();
                pbm.SelectedTalkGroups = termsList.ToArray();


                if (selectedID.Any())
                {
                    pbm.SelectedFavouriteID = selectedID.FirstOrDefault().FavouriteID;
                }
            }
            var atd = dbo.ActivityTalkDashes.Where(w => w.UserID == User.UserId && w.Active == true).ToList();
            logger.Debug(string.Format("Activity Dashboard atd : "));
            if (atd != null)
            {
                string sysid = "";
                string talkid = "";
                for (int i = 0; i < atd.Count; i++)
                {
                    sysid += atd[i].SystemID + ",";
                    talkid += atd[i].TalkGroupID + ",";

                }
                if (sysid.Length > 0)
                {
                    sysid.Remove(sysid.Length - 1, 1);
                    talkid.Remove(talkid.Length - 1, 1);
                    ViewBag.TalkSelect = talkid;
                    ViewBag.selectSystem = sysid;
                }
            }


            Entities.ActivityDash ad = dbo.ActivityDashes.Where(w => w.UserID == User.UserId).FirstOrDefault();
            logger.Debug(string.Format("Activity Dashboard ad : "));
            if (ad != null)
            {
                ViewBag.Green = ad.GreenMax.HasValue ? ad.GreenMax.Value : Int32.Parse(ConfigurationManager.AppSettings["GreenMax"]);
                ViewBag.Yellowmin = ad.YellowMin.HasValue ? ad.YellowMin.Value : Int32.Parse(ConfigurationManager.AppSettings["YellowMin"]);
                ViewBag.YellowMax = ad.YellowMax.HasValue ? ad.YellowMax.Value : Int32.Parse(ConfigurationManager.AppSettings["YellowMax"]);
                ViewBag.red = ad.RedMin.HasValue ? ad.RedMin.Value : Int32.Parse(ConfigurationManager.AppSettings["RedMin"]);
                ViewBag.TimeDuration = ad.HourDuration.HasValue ? ad.HourDuration.Value : Int32.Parse(ConfigurationManager.AppSettings["HoursDuration"]);
            }
            else
            {
                // ViewBag.Green = Int32.Parse(ConfigurationManager.AppSettings["GreenMax"]);
                ViewBag.Yellowmin = Int32.Parse(ConfigurationManager.AppSettings["YellowMin"]);
                ViewBag.YellowMax = Int32.Parse(ConfigurationManager.AppSettings["YellowMax"]);
                //ViewBag.red = Int32.Parse(ConfigurationManager.AppSettings["RedMin"]);
                ViewBag.TimeDuration = Int32.Parse(ConfigurationManager.AppSettings["HoursDuration"]);
            }

            pbm.TalkGroupModel = Utils.Utils.GetTalkgroupModelList(termsList, termsListSys, User, UserInfo);
            logger.Debug(string.Format("Activity Dashboard: "));
            return View("ActivityDashboard", pbm);
        }



        [HttpGet]
        public ActionResult PlaybackIndex(string ReturnUrl)
        {
            if (!(_userInfo.HasPermission(PoliceMonitor.Security.Base.Model.Constant.LIVELISTENING)))
            {
                AuthorizationContext filterContext = new AuthorizationContext();
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                Response.Redirect("~/Account/AccessDenied");
            }
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var favouriteList = dbo.PlaybackFavourites.Where(x => x.UserId == User.UserId && x.Active == true);

            PlayBackIndexModel pbm = new PlayBackIndexModel();
            List<SelectListItem> favList = new List<SelectListItem>();

            favList = favouriteList.ToList().Select(rT => new SelectListItem() { Value = rT.FavouriteID.ToString(), Text = rT.Name.Trim() }).ToList();
            pbm.ListenLiveFavouriteList.AddRange(favList);
            var selectedID = dbo.PlaybackFavourites.Where(x => x.UserId == User.UserId && x.ListenLivePrimary == true && x.Active == true).ToList();
            List<int> termsList = new List<int>();
            List<int> termsListSys = new List<int>();
            if (selectedID.Any())
            {
                var FavView = selectedID.FirstOrDefault().FavouriteView;
                var itemToAdd = new Newtonsoft.Json.Linq.JObject();
                Object json = JsonConvert.DeserializeObject(FavView);
                JArray jsonarr = json as JArray;

                foreach (var item in jsonarr)
                {
                    JObject obj = item as JObject;

                    if ("SelectedTalkGroups".Equals(obj["name"].ToString()))
                    {
                        string n = obj["value"].ToString();
                        termsList.Add(Convert.ToInt32(n));
                        if (obj["SysID"] != null)
                        {
                            termsListSys.Add(Convert.ToInt32(obj["SysID"].ToString()));
                        }
                        else
                        {
                            termsListSys.Add(Convert.ToInt32("1039"));
                        }
                    }

                }
                pbm.SelectedSystemID = termsListSys.ToArray();
                pbm.SelectedTalkGroups = termsList.ToArray();
                if (selectedID.Any())
                {
                    pbm.SelectedFavouriteID = selectedID.FirstOrDefault().FavouriteID;
                }
            }
            pbm.TalkGroupModel = Utils.Utils.GetTalkgroupModelList(termsList, termsListSys, User, UserInfo);
            //pbm.TalkGroupModel.Add(mm);

            //if (!string.IsNullOrEmpty(ReturnUrl))
            //{
            //    ViewBag.ReturnUrl = ReturnUrl;
            //}
            //else
            //{
            //    ViewBag.ReturnUrl = "PlaybackIndex";
            //}

            //return View("PlaybackIndex", mm);
            return View("PlaybackIndex", pbm);
        }

        //[HttpGet]
        //public string GetData()
        //{
        //    Response.ContentType = "text/event-stream";
        //    Response.Expires = -1;
        //    Response.Write("data: The server time is: " + DateTime.Now.ToString());
        //    Response.Flush();
        //    return "data: The server time is: " + DateTime.Now.ToString();
        //}

        //Live Playback
        [HttpGet]
        public string GetRadios(string idTalkGroups, string idSystemIds, long lastID)
        {// var result1 = (from it in dbo.PolledTalkGroupItems where it.RadioID == id && it.TransStartUTC >= prevTimeT select it).ToList();
            var time = getTime();
            var prevTimeT = time;
            //if (prevTime != "")
            //    prevTimeT = DateTime.ParseExact(prevTime, "ddMMyyyy-HHmmss", CultureInfo.InvariantCulture);
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();

            List<long> idAudioList = idTalkGroups.Split(',').Select(x => Int64.Parse(x)).ToList();
            List<long> idSystemList = idSystemIds.Split(',').Select(x => Int64.Parse(x)).ToList();

            List<Entities.PolledTalkGroupItem> result1;
            long max = dbo.PolledTalkGroupItems.Max(x => x.ID);

            StringBuilder query;
            if (lastID > 0)
            {
                query = new StringBuilder("select * from dbo.PolledTalkGroupItem where ID > " + lastID + " and ID <= " + max + " and (");
                //result1 = (from it in dbo.PolledTalkGroupItems where idAudioList.Contains(it.TalkGroupID.Value) && it.ID > lastID && it.ID <= max select it).ToList();
            }
            else
            {
                query = new StringBuilder("select * from dbo.PolledTalkGroupItem where TransStartUTC > '" + prevTimeT + "' and ID <= " + max + " and (");
                //result1 = (from it in dbo.PolledTalkGroupItems where idAudioList.Contains(it.TalkGroupID.Value) && it.TransStartUTC >= prevTimeT && it.ID <= max select it).ToList();
            }

            for (int i = 0; i < idAudioList.Count; i++)
            {
                query.Append("(TalkGroupID = " + idAudioList[i] + " and SystemId=" + idSystemList[i] + ") or ");
            }
            query.Remove(query.Length - 3, 3);
            query.Append(")");
            result1 = dbo.PolledTalkGroupItems.SqlQuery(query.ToString()).ToList();

            foreach (var item in result1)
            {
                var RadioDesc = item.RadioDesc;
                item.RadioDesc = RadioDesc.Replace("DISPATCH:::", "");

                DateTime MyDT = Convert.ToDateTime(item.TransStartUTC);
                //DateTime MyDT = Utility.Get().ConvertJsonDT(this.TransStartMSec);
                if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
                {
                    double offset = TimeZone.CurrentTimeZone.GetUtcOffset(MyDT).TotalHours;
                    bool isdaylight = IsDaylight(MyDT.AddHours(offset));
                    if (isdaylight)
                    {
                        DateTime newdate = MyDT;
                        MyDT = new DateTime();
                        MyDT = newdate.AddHours(-1);
                    }
                    else
                    {
                        DateTime newdate = MyDT;
                        MyDT = new DateTime();
                        MyDT = newdate.AddHours(-2);
                    }
                }
                item.TransStartUTC = Convert.ToDateTime(PoliceMonitor.Utils.Utils.ToClientTime((MyDT)));
            }

            var itemToAdd = new Newtonsoft.Json.Linq.JObject();
            itemToAdd["result"] = JsonConvert.SerializeObject(result1);
            itemToAdd["lastID"] = max;

            return JsonConvert.SerializeObject(itemToAdd);
        }



        public static bool IsDaylight(DateTime MyDT)
        {
            return (Between(MyDT, TimeZone.CurrentTimeZone.GetDaylightChanges(MyDT.Year).Start, TimeZone.CurrentTimeZone.GetDaylightChanges(MyDT.Year).End));
        }
        public static bool Between(DateTime input, DateTime date1, DateTime date2)
        {
            return (input > date1 && input < date2);
        }
        [HttpGet]
        public string GetLatestActivity(string lastValue)
        {
            int val = Convert.ToInt32(lastValue);
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var maxtime = dbo.PolledTalkGroupItems.Max(x => x.TransStartUTC).ToString();

            //DateTime myDateTime = DateTime.Parse(maxtime).AddSeconds(-val);

            DateTime dtime = Convert.ToDateTime(maxtime);//DateTime.UtcNow;
            logger.Debug(string.Format("UTC Time in GetLatestActivity {0} ", dtime));

            int ServerTimeDiffInSeconds = Convert.ToInt32(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerTimeDiffInSeconds"].ToString()) ? "0" : ConfigurationManager.AppSettings["ServerTimeDiffInSeconds"].ToString());
            // dtime = dtime.AddSeconds(ServerTimeDiffInSeconds);

            logger.Debug(string.Format("UTC Time in GetLatestActivity after ServerTimeDiffInSeconds{0} ", dtime));
            dtime = dtime.AddSeconds(-(Convert.ToInt32(5)));

            logger.Debug(string.Format("UTC Time in GetLatestActivity after add default time{0} ", dtime));
            var result1 = dbo.PolledTalkGroupItems.Where(x => x.TransStartUTC > dtime).ToList();
            foreach (var item in result1)
            {
                var RadioDesc = item.RadioDesc;
                item.RadioDesc = RadioDesc.Replace("DISPATCH:::", "");
            }
            var itemToAdd = new Newtonsoft.Json.Linq.JObject();
            itemToAdd["result"] = JsonConvert.SerializeObject(result1);
            logger.Debug(string.Format("Json for selected data  GetLatestActivity", itemToAdd));
            return JsonConvert.SerializeObject(itemToAdd);
        }

        [HttpPost]
        public string getSelectLatest(string TalkIDs, string systemIDs)
        {
            int sysid = Convert.ToInt32(systemIDs);
            int talkid = Convert.ToInt32(TalkIDs);
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var atd = dbo.ActivityTalkDashes.Where(w => w.UserID == User.UserId && w.Active == true && w.SystemID == sysid && w.TalkGroupID == talkid).ToList();
            return JsonConvert.SerializeObject(atd);
        }

        [HttpPost]
        public string ResetTalkTraffic(string systemID, string TalkID)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            int talkID = Convert.ToInt32(TalkID);
            int sysID = Convert.ToInt32(systemID);
            var activity = dbo.ActivityTalkDashes.Where(w => w.UserID == User.UserId && w.TalkGroupID == talkID && w.SystemID == sysID).FirstOrDefault();
            if (activity != null)
            {

                activity.Active = false;
                dbo.SaveChanges();
                return JsonConvert.SerializeObject(500);
            }
            else
            {
                return JsonConvert.SerializeObject(200);
            }
        }

        [HttpPost]
        public string AddTalkTraffic(string MinTime, string MaxTime, string TimeDuration, string systemID, string TalkID)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            try
            {
                int talkID = Convert.ToInt32(TalkID);
                int sysID = Convert.ToInt32(systemID);
                var activity = dbo.ActivityTalkDashes.Where(w => w.UserID == User.UserId && w.TalkGroupID == talkID && w.SystemID == sysID).FirstOrDefault();
                if (activity != null)
                {
                    activity.GreenMax = Convert.ToInt32(MinTime);
                    activity.YellowMin = Convert.ToInt32(MinTime);
                    activity.YellowMax = Convert.ToInt32(MaxTime);
                    activity.RedMin = Convert.ToInt32(MaxTime);
                    activity.HourDuration = Convert.ToInt32(TimeDuration);
                    activity.Active = true;
                }
                else
                {
                    Entities.ActivityTalkDash atd = new Entities.ActivityTalkDash();
                    atd.GreenMax = Convert.ToInt32(MinTime);
                    atd.UserID = User.UserId;
                    atd.YellowMin = Convert.ToInt32(MinTime);
                    atd.YellowMax = Convert.ToInt32(MaxTime);
                    atd.RedMin = Convert.ToInt32(MaxTime);
                    atd.HourDuration = Convert.ToInt32(TimeDuration);
                    atd.SystemID = Convert.ToInt32(systemID);
                    atd.TalkGroupID = Convert.ToInt32(TalkID);
                    atd.Active = true;
                    dbo.ActivityTalkDashes.Add(atd);
                }
                dbo.SaveChanges();
                return JsonConvert.SerializeObject("save");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return JsonConvert.SerializeObject(ex);
            }
            //return "";
        }

        [HttpPost]
        public string GetTrafficActivity(string lastValue, int TimeDuration, string systemid)
        {
            //int val = Convert.ToInt32(lastValue);
            lastValue = lastValue.Remove(lastValue.Length - 1, 1);
            string[] value = (lastValue.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries));
            List<long> valueTalk = value.Select(long.Parse).ToList();

            systemid = lastValue.Remove(lastValue.Length - 1, 1);
            string[] system = (systemid.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries));
            List<long> systemTalk = system.Select(long.Parse).ToList();


            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var maxtime = dbo.PolledTalkGroupItems.Max(x => x.TransStartUTC).ToString();

            var atd = dbo.ActivityTalkDashes.Where(w => w.UserID == User.UserId && w.Active == true).ToList();
            var atd1 = JsonConvert.SerializeObject(atd);



            //DateTime TimeLastMin = DateTime.Parse(maxtime).AddMinutes(-1);            
            //var LastMinutes = dbo.PolledTalkGroupItems.Where(x => valueTalk.Contains(x.TalkGroupID.Value) && x.TransStartUTC > TimeLastMin).GroupBy(g => g.TalkGroupID).Select(s => new { TalkGroupID = s.Key, Val = (s.Count()/60) }).ToList();
            //var lastMinutesJson = JsonConvert.SerializeObject(LastMinutes);

            DateTime dtime = DateTime.UtcNow;
            logger.Debug(string.Format("UTC Time in GetTrafficActivity {0} ", dtime));
            int ServerTimeDiffInSeconds = Convert.ToInt32(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerTimeDiffInSeconds"].ToString()) ? "0" : ConfigurationManager.AppSettings["ServerTimeDiffInSeconds"].ToString());
            dtime = dtime.AddSeconds(ServerTimeDiffInSeconds);
            logger.Debug(string.Format("UTC Time in GetTrafficActivity after ServerTimeDiffInSeconds{0} ", dtime));
            dtime = dtime.AddMinutes(-(Convert.ToInt32(TimeDuration)));
            logger.Debug(string.Format("UTC Time in GetTrafficActivity {0} ", dtime));
            //DateTime saveUtcNow = DateTime.UtcNow;
            //saveUtcNow=saveUtcNow(-ConfigurationManager.AppSettings)
            //DateTime myDateTime = DateTime.Parse(maxtime).AddMinutes(-(Convert.ToInt32(TimeDuration)));



            var result1 = dbo.PolledTalkGroupItems.Where(x => valueTalk.Contains(x.TalkGroupID.Value) && x.TransStartUTC > dtime).GroupBy(g => g.TalkGroupID).Select(s => new { TalkGroupID = s.Key, Val = (s.Count()), SystemId = s.FirstOrDefault().SystemID.Value }).ToList();

            //var result1 = dbo.PolledTalkGroupItems.Where(x => valueTalk.Contains(x.TalkGroupID.Value) && x.TransStartUTC > dtime).ToList();
            //for (int i = 0; i < valueTalk.Count(); i++)
            //{

            //}

            var itemToAdd = new Newtonsoft.Json.Linq.JObject();
            itemToAdd["result"] = JsonConvert.SerializeObject(result1);
            var result2 = JsonConvert.SerializeObject(itemToAdd);
            logger.Debug(string.Format("Json for selected data  GetTrafficActivity", itemToAdd));
            var j = JsonConvert.SerializeObject(new
            {
                obj1 = itemToAdd,//JObject.Parse(result2),
                obj2 = atd,//JObject.Parse(atd1)
                //obj3=lastMinutesJson
            });



            return j;
        }

        [HttpPost]
        public string GetActivity(string idTalkGroups, string idSystemIds)
        {// var result1 = (from it in dbo.PolledTalkGroupItems where it.RadioID == id && it.TransStartUTC >= prevTimeT select it).ToList();
            var time = getTime();
            var prevTimeT = time;
            //if (prevTime != "")
            //    prevTimeT = DateTime.ParseExact(prevTime, "ddMMyyyy-HHmmss", CultureInfo.InvariantCulture);
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();

            List<long> idAudioList = idTalkGroups.Split(',').Select(x => Int64.Parse(x)).ToList();
            List<long> idSystemList = idSystemIds.Split(',').Select(x => Int64.Parse(x)).ToList();

            var maxtime = dbo.PolledTalkGroupItems.Max(x => x.TransStartUTC).ToString();
            DateTime myDateTime = DateTime.Parse(maxtime).AddHours(-24);
            var polledTable = dbo.PolledTalkGroupItems.Where(w => w.TransStartUTC > myDateTime && idAudioList.Contains(w.TalkGroupID.Value)).ToList();
            //var result1 = dbo.PolledTalkGroupItems.Where(x => x.TransStartUTC > myDateTime).ToList();
            StringBuilder result = new StringBuilder("");
            for (int i = 0; i < idAudioList.Count; i++)
            {
                var syst = Convert.ToInt32(idSystemList[i]);
                var talk = Convert.ToInt32(idAudioList[i]);
                var pollData = polledTable.Where(x => x.SystemID == syst && x.TalkGroupID == talk && x.TransStartUTC > myDateTime).ToList();
                if (pollData.Count() > 0)
                {
                    result.Append(pollData.Max(m => m.ID == null ? 0 : m.ID).ToString() + ",");
                }


            }
            result.Remove(result.Length - 1, 1);


            int[] arrayMax = new int[idAudioList.Count];
            List<Entities.PolledTalkGroupItem> result1;
            StringBuilder querymax = new StringBuilder(" select * from dbo.PolledTalkGroupItem where [ID] IN  (" + result + ")");
            //for (int i = 0; i < idAudioList.Count; i++)
            //{
            //    var syst = Convert.ToInt32(idSystemList[i]);
            //    var talk = Convert.ToInt32(idAudioList[i]);
            //    querymax.Append("(TalkGroupID = " + talk + " and SystemId=" + syst + ") or");//(TalkGroupID = " + idAudioList[i] + " and SystemId=" + idSystemList[i] + ")");

            //}
            //querymax.Remove(querymax.Length - 3, 3);
            //querymax.Append("group by [TalkGroupID] )");

            result1 = dbo.PolledTalkGroupItems.SqlQuery(querymax.ToString()).ToList();
            foreach (var item in result1)
            {
                var RadioDesc = item.RadioDesc;
                item.RadioDesc = RadioDesc.Replace("DISPATCH:::", "");
            }
            var itemToAdd = new Newtonsoft.Json.Linq.JObject();
            itemToAdd["result"] = JsonConvert.SerializeObject(result1);
            return JsonConvert.SerializeObject(itemToAdd);
            //return "";
        }


        [HttpGet]
        public ActionResult QuickPlayback()
        {
            if (!(_userInfo.HasPermission(PoliceMonitor.Security.Base.Model.Constant.QUICKPLAYBACK)))
            {
                AuthorizationContext filterContext = new AuthorizationContext();
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                Response.Redirect("~/Account/AccessDenied");
            }
            List<int> termsList = new List<int>();
            List<int> termsListSysID = new List<int>();
            List<TalkGroupModel> mm = Utils.Utils.GetTalkgroupModelList(termsList, termsListSysID, User, UserInfo);
            var rr = mm.Select(a => a.SystemName).Distinct().ToList();
            ViewBag.Countsys = rr;
            ViewBag.SysID = mm.Select(a => a.SysID).Distinct().ToList();// mm.Select(a => a.SystemName).Distinct().ToList();
            return View("QuickPlayback", mm);
        }

        [HttpGet]
        public async Task<string> GetLastNTransmissions(int talkGroupID, int size, int time, int SystemId)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            List<Entities.PolledTalkGroupItem> WorkingList = new List<Entities.PolledTalkGroupItem>();

            var itemToAdd = new Newtonsoft.Json.Linq.JObject();
            List<Entities.PolledTalkGroupItem> result1 = null;
            string URLSearch = "";
            //if (size > 0)
            {
                // Pull more than required items just to make sure after duplicates size remain the same
                //result1 = dbo.PolledTalkGroupItems.Where(it => it.TalkGroupID == talkGroupID && it.SystemID == SystemId).OrderByDescending(t => t.ID).Take(size + 20).ToList();
                using (var client = new HttpClient())
                {
                    if (size > 0)
                    {
                        client.BaseAddress = new Uri(Utility.Get().BaseURL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        URLSearch += string.Format("/radio/index_search.cgi?system_id={0}&tg={1}", SystemId, talkGroupID);
                        Int64 start = 0;
                        Int64 end = 0;
                        DateTime startdate = DateTime.Now.Date.AddDays(-10);
                        start = Utility.Get().GetJsonDT(startdate);
                        DateTime enddate = DateTime.Now.Date.AddDays(+1);
                        end = Utility.Get().GetJsonDT(enddate);
                        if (start > 0)
                        {
                            URLSearch += string.Format("&start_time={0}", start);
                        }
                        if (end > 0)
                        {
                            URLSearch += string.Format("&end_time={0}", end);
                        }
                        URLSearch += string.Format("&reverse=1");
                        URLSearch += string.Format("&max_results=20");
                    }
                    else
                    {
                        DateTime dtime = DateTime.UtcNow;
                        dtime = dtime.AddSeconds(-1 * time);
                        int ServerTimeDiffInSeconds = Convert.ToInt32(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerTimeDiffInSeconds"].ToString()) ? "0" : ConfigurationManager.AppSettings["ServerTimeDiffInSeconds"].ToString());
                        dtime = dtime.AddSeconds(ServerTimeDiffInSeconds);
                        //result1 = dbo.PolledTalkGroupItems.Where(it => it.TalkGroupID == talkGroupID && it.TransStartUTC > dtime && it.SystemID == SystemId).OrderByDescending(t => t.ID).ToList();
                        // using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(Utility.Get().BaseURL);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            URLSearch = string.Format("/radio/index_search.cgi?system_id={0}&tg={1}", SystemId, talkGroupID);
                            Int64 start = 0;
                            Int64 end = 0;
                            //DateTime startdate = DateTime.Now.Date.AddDays(-10);
                            start = Utility.Get().GetJsonDT(dtime);
                            DateTime enddate = DateTime.Now.Date.AddDays(+1);
                            end = Utility.Get().GetJsonDT(enddate);
                            if (start > 0)
                            {
                                URLSearch += string.Format("&start_time={0}", start);
                            }
                            if (end > 0)
                            {
                                URLSearch += string.Format("&end_time={0}", end);
                            }
                            URLSearch += string.Format("&reverse=1");
                        }
                    }
                    HttpResponseMessage response = await client.GetAsync(URLSearch);
                    if (response.IsSuccessStatusCode == false)
                    {
                        //Result.ErrorMessage = response.ReasonPhrase;
                    }
                    else
                    {
                        var json = await response.Content.ReadAsStringAsync();
                        var respItem = JsonConvert.DeserializeObject<JsonResponseIndexMatch>(json);
                        List<Tuple<Entities.PolledTalkGroupItem, string>> tmpList = new List<Tuple<Entities.PolledTalkGroupItem, string>>();
                        foreach (var info in respItem.matches)
                        {
                            int Cnt = -1;
                            Entities.PolledTalkGroupItem item = new Entities.PolledTalkGroupItem();
                            item.RawTransmission = JsonConvert.SerializeObject(info);
                            item.SystemID = SystemId;
                            foreach (Object col in info)
                            {
                                Cnt++;
                                if (Cnt == 0)
                                    item.TalkGroupItemId = (string)col;
                                else if (Cnt == 1)
                                    item.TransStartMSec = (long)col;
                                else if (Cnt == 2)
                                    item.TransLengthMSec = (long)col;
                                else if (Cnt == 3)
                                    item.TalkGroupID = (long)col;
                                else if (Cnt == 6)
                                    item.RadioID = (long)col;
                                else if (Cnt == 10)
                                {
                                    JObject obj = (Newtonsoft.Json.Linq.JObject)(col);
                                    JToken token;
                                    obj.TryGetValue("tones", out token);
                                    if (token != null)
                                    {
                                        Newtonsoft.Json.Linq.JArray arr = token as Newtonsoft.Json.Linq.JArray;
                                        List<JToken> l = arr.ToList();
                                        if (l.Count > 0)
                                        {
                                            foreach (JToken t in l)
                                            {
                                                item.Tones += t.ToString() + ", ";
                                            }
                                            item.Tones = item.Tones.Substring(0, item.Tones.Length - 2);
                                        }
                                    }
                                }
                            }
                            tmpList.Add(new Tuple<Entities.PolledTalkGroupItem, string>(item, string.Format("{0}{1}{2}", item.TransStartMSec, item.TransLengthMSec, item.RadioID)));
                        }
                        string LastUniqueID = "";
                        foreach (var c in tmpList)
                        {
                            if (LastUniqueID == c.Item2)
                                continue;
                            else
                            {
                                WorkingList.Add(c.Item1);
                                LastUniqueID = c.Item2;
                            }
                        }

                    }
                }
            }
            List<TalkGroup> talkgroups = CacheUtil.GetCustomerTalkgroups(User, true);
            var poolTalkGroups = Utils.CacheUtil.GetUserTalkgroups(User);
            List<Entities.Radio> RadioCachList = Utils.CacheUtil.GetRadios();
            var radioIDs = RadioCachList.Select(x => x.ID).ToList();


            var UpdateDesc = from c in WorkingList
                             join d in talkgroups on c.TalkGroupID equals d.ID
                             where d.SystemId == SystemId
                             select new Tuple<Entities.PolledTalkGroupItem, TalkGroup>(c, d);



            foreach (var info in UpdateDesc.ToList())
            {
                string tmpRadioDesc = "";
                string RadioNum = info.Item1.RadioID.ToString();

                var myRadio = RadioCachList.FirstOrDefault(c => c.ID == info.Item1.RadioID);
                if (myRadio != null)
                {
                    tmpRadioDesc = myRadio.Description;
                }

                string ChannelDesc = "(" + info.Item2.County + ") " + info.Item2.Name;
                string ChannelNum = info.Item2.ID.ToString();

                if (tmpRadioDesc.ToUpper().IndexOf("DISPATCH") > -1)
                {
                    info.Item1.RadioDesc = ChannelDesc;
                }
                else
                {
                    if (info.Item1.RadioID == 0)
                    {
                        info.Item1.RadioDesc = "TONES" + (string.IsNullOrEmpty(info.Item1.Tones) ? "" : ": " + info.Item1.Tones);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(tmpRadioDesc) == true)
                        {
                            info.Item1.RadioDesc = string.Format("{0} ({1})", ChannelDesc, RadioNum);
                        }
                        else
                        {
                            info.Item1.RadioDesc = string.Format("{0} ({1})", tmpRadioDesc, RadioNum);
                        }
                    }
                }
                var tt = poolTalkGroups.Where(x => Convert.ToInt64(x.Value) == info.Item1.TalkGroupID).FirstOrDefault();
                info.Item1.ChannelName = tt.Text; //"(" + tt.County + ") " + tt.Name;// (string)tkgroups["" + info.Item1.TalkGroupID];
                info.Item1.SystemID = info.Item1.SystemID;
                info.Item1.TransStartUTC = Utility.Get().ConvertJsonDT(info.Item1.TransStartMSec.Value);
                info.Item1.TransTimeDesc = TransTimeDesc(info.Item1.TransLengthMSec.Value);
            }
            List<Entities.PolledTalkGroupItem> FilteredList = new List<Entities.PolledTalkGroupItem>();
            foreach (var info in UpdateDesc.ToList())
            {
                FilteredList.Add(info.Item1);
            }
            FilteredList.Reverse();
            Utils.Utils.RemoveDuplicates(FilteredList);
            if (FilteredList.Count > size && size > 0)
                FilteredList.RemoveRange(0, FilteredList.Count - size);
            if (FilteredList.Any())
            {
                itemToAdd["result"] = JsonConvert.SerializeObject(FilteredList);
            }
            return JsonConvert.SerializeObject(itemToAdd);
        }


        public string TransTimeDesc(long TransLengthMSec)
        {
            if (TransLengthMSec > 0)
            {
                TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)TransLengthMSec);
                return string.Format("{0:F1}s", ts.TotalSeconds);
            }
            else
                return "NA";
        }

        [HttpPost]
        public async Task<ActionResult> TalkGroup(TalkGroupView value)
        {
            try
            {

                string[] systemID = Request.Form["HdfSystemID"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                int[] SelSystemID = systemID.Select(int.Parse).ToArray();

                logger.Debug(string.Format("Start Execution of TalkGroup Search"));

                logger.Debug(string.Format("Start Search WS and Create model list"));
                TalkGroupView Result = this.HttpContext.Session["_PlaybackView"] as TalkGroupView;
                if (Result == null)
                    Result = new TalkGroupView();
                ViewBag.UserInfo = UserInfo;
                var myList = value.SelectedTalkGroups;

                if (value.radioFilter == "dateTimeRange")
                {
                    logger.Debug(string.Format("Log for check dateTimeRange!"));
                    if (value.StartDatetime.Value.Year < 100)
                        value.StartDatetime = value.StartDatetime.Value.AddYears(2000);

                    if (value.EndDatetime.Value.Year < 100)
                        value.EndDatetime = value.EndDatetime.Value.AddYears(2000);

                    bool validateFailed = false;
                    StringBuilder sb = new StringBuilder();
                    if (!value.StartDatetime.HasValue)
                    {
                        validateFailed = true;
                        sb.Append("Please select start date. <br/>");
                    }
                    if (!value.EndDatetime.HasValue)
                    {
                        validateFailed = true;
                        sb.Append("Please select end date. <br/>");
                    }
                    if (!value.StartTime.HasValue)
                    {
                        validateFailed = true;
                        sb.Append("Please enter start time. <br/>");
                    }
                    if (!value.EndTime.HasValue)
                    {
                        validateFailed = true;
                        sb.Append("Please enter end time. <br/>");
                    }
                    TimeSpan ts = value.EndDatetime.Value.Subtract(value.StartDatetime.Value);
                    if (ts.TotalHours > 72)
                    {
                        validateFailed = true;
                        sb.Append("Difference between start datetime and end datetime should be less than 72 hours. <br/>");
                    }

                    if (validateFailed == true)
                    {
                        Result.ErrorMessage = sb.ToString();
                        return View("Playback", Result);
                    }
                }
                else
                {
                    logger.Debug(string.Format("Log for check HourDuration!"));
                    bool validateFailed = false;
                    StringBuilder sb = new StringBuilder();
                    if (value.StartHour < 1)
                    {
                        validateFailed = true;
                        sb.Append("Hours ago should be greater than 1. <br/>");
                    }
                    if (value.StartHour > 72)
                    {
                        validateFailed = true;
                        sb.Append("Hours ago should be Less than or Equal to 72. <br/>");
                    }
                    if (value.EndHour < 0)
                    {
                        validateFailed = true;
                        sb.Append("Hours Duration should be greater than 0. <br/>");
                    }
                    if (value.MinTranSeconds < 0)
                    {
                        validateFailed = true;
                        sb.Append("Min Transmission Time should be greater than 0. <br/>");
                    }
                    if (validateFailed == true)
                    {
                        Result.ErrorMessage = sb.ToString();
                        return View("Playback", Result);
                    }
                }
                if (myList == null || myList.Count() <= 0)
                {
                    Result.ErrorMessage = "Select one or more filters";
                    return View(Result);
                }
                int TstForNum = 0;
                Result.AutoPlay = (_userInfo.AutoPlay != null && _userInfo.AutoPlay.HasValue) ? _userInfo.AutoPlay.Value : _userInfo.DefaultAutoplaySetting;
                value.TalkGroups.Clear();
                value.TalkGroups.AddRange(Utils.CacheUtil.GetUserTalkgroups(_userInfo));
                var counts = SelSystemID.GroupBy(item => item).Select(grp => new { sysid = grp.Key, Count = grp.Count() });
                string tgQuery = "";
                string[] systgQuery = new string[counts.Count()];
                int first = 0;
                int z = 0;
                foreach (var i in counts)
                {
                    var second = i.Count;
                    int count = 0;
                    tgQuery = "";
                    foreach (var info in myList)
                    {
                        if (count >= first)
                        {
                            if (second > 0)
                            {
                                var tgGroup = value.TalkGroups.FirstOrDefault(c => c.Value == TstForNum.ToString());
                                if (tgGroup != null)
                                {
                                    value.TalkGroups.Remove(tgGroup);
                                    value.TalkGroupsSelected.Add(tgGroup);
                                }
                                if (tgQuery != "")
                                    tgQuery += "," + info;
                                else
                                    tgQuery = info.ToString();
                                second--;
                            }
                        }
                        count++;
                    };
                    systgQuery[z] = tgQuery;
                    first += i.Count;
                    z++;
                }
                this.HttpContext.Session["_SavedTalkGroupView"] = value;
                int minSec = value.MinTranSeconds;
                List<TalkGroupItem> WorkingList = new List<TalkGroupItem>();
                z = 0;
                foreach (var i in counts)
                {
                    logger.Debug(string.Format("Start for URL Creation!"));
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(Utility.Get().BaseURL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        if (TEST)
                        {
                            String username = "admin";
                            String password = "P5zHfKBpj3";
                            String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                            client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
                        }

                        string URLSearch = string.Format("radio/index_search.cgi?system_id={0}&tg={1}", i.sysid, systgQuery[z]);
                        if (value.RadioID != null && value.RadioID != "")
                        {
                            URLSearch += string.Format("&radio={0}", value.RadioID);
                        }
                        if (value.radioFilter == "dateTimeRange" && value.StartDatetime != null && value.EndDatetime != null)
                        {
                            Int64 Start = 0;
                            Int64 End = 0;
                            if (value.StartDatetime.HasValue && value.StartTime.HasValue)
                            {
                                DateTime startDatetime = value.StartDatetime.Value + value.StartTime.Value;
                                double offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).TotalHours;
                                startDatetime = startDatetime.AddHours(offset);
                                int offsetclient = PoliceMonitor.Utils.Utils.GetTimezoneOffset();
                                startDatetime = startDatetime.AddMinutes(offsetclient);                           
                                Start = Utility.Get().GetJsonDT(startDatetime);
                                URLSearch += string.Format("&start_time={0}", Start);
                            }

                            if (value.EndDatetime.HasValue && value.EndTime.HasValue)
                            {
                                DateTime endDatetime = value.EndDatetime.Value + value.EndTime.Value;
                                double offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).TotalHours;
                                endDatetime = endDatetime.AddHours(offset);
                                int offsetclient = PoliceMonitor.Utils.Utils.GetTimezoneOffset();
                                endDatetime = endDatetime.AddMinutes(offsetclient);
                                End = Utility.Get().GetJsonDT(endDatetime);
                                URLSearch += string.Format("&end_time={0}", End);
                            }
                        }
                        else
                        {
                            Int64 StartAgo = 0;
                            if (value.StartHour > 0)
                            {
                                StartAgo = Utility.Get().GetJsonDT(value.StartHour);
                                URLSearch += string.Format("&start_time={0}", StartAgo);
                            }

                            if (value.EndHour > 0 && value.StartHour > 0)
                            {
                                int Seconds = value.EndHour * 60 * 60;
                                // Int64 tmpAgo = Utility.Get().GetJsonDT(value.EndHour);
                                URLSearch += string.Format("&end_time={0}", StartAgo + Seconds);
                            }
                        }
                        logger.Debug(string.Format("URL to search Talkgroup {0}", URLSearch));
                        // New code:
                        HttpResponseMessage response = await client.GetAsync(URLSearch);

                        if (response.IsSuccessStatusCode == false)
                        {
                            logger.Debug(string.Format("Search Talkgroup Fail {0}", response.RequestMessage));
                            Result.ErrorMessage = response.ReasonPhrase;
                        }
                        else
                        {
                            //ListTalkGroups tmp = await response.Content.ReadAsAsync<ListTalkGroups>();
                            var json = await response.Content.ReadAsStringAsync();
                            logger.Debug(string.Format("Talkgroup Success Json {0}", json));
                            var respItem = JsonConvert.DeserializeObject<JsonResponseIndexMatch>(json);
                            List<Tuple<TalkGroupItem, string>> tmpList = new List<Tuple<TalkGroupItem, string>>();
                            foreach (var info in respItem.matches)
                            {
                                int Cnt = -1;
                                TalkGroupItem item = new TalkGroupItem();
                                foreach (Object col in info)
                                {
                                    Cnt++;
                                    if (Cnt == 0)
                                        item.ID = (string)col;
                                    else if (Cnt == 1)
                                    {
                                        item.TransStartMSec = (long)col;
                                        //if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
                                        //{
                                        //    item.TransStartUTC.AddHours(-1);
                                        //}
                                        // item.TransStartUTC = item.TransStartUTC.AddHours(-1);
                                    }
                                    else if (Cnt == 2)
                                        item.TransLengthMSec = (long)col;
                                    else if (Cnt == 3)
                                    {
                                        item.TalkGroupID = (long)col;
                                        item.SystemID = i.sysid + "_" + (col.ToString());
                                    }
                                    else if (Cnt == 6)
                                        item.RadioID = (long)col;
                                }
                                //item.TransStartUTC.AddHours(-1);
                                tmpList.Add(new Tuple<TalkGroupItem, string>(item, string.Format("{0}{1}{2}", item.TransStartMSec, item.TransLengthMSec, item.RadioID)));
                            }
                            string LastUniqueID = "";
                            foreach (var c in tmpList)
                            {
                                if (LastUniqueID == c.Item2)
                                    continue;
                                else
                                {
                                    WorkingList.Add(c.Item1);
                                    LastUniqueID = c.Item2;
                                }
                            }
                        }
                    }
                    z++;
                }
                //List<TalkGroupItem> WorkingList;

                //if ((value.radioFilter == "dateTimeRange" && value.StartDatetime.Value.CompareTo(new DateTime(2016, 11, 1)) > 0) || value.radioFilter != "dateTimeRange")
                //{
                //    WorkingList = talkGroupItemRepository.GetTalkGroupItems((value.radioFilter == "dateTimeRange") ? value.StartDatetime + value.StartTime : null, value.EndDatetime + value.EndTime, myList.ToList(), SelSystemID, value.StartHour, value.EndHour, value.MinTranSeconds, false);
                //}
                //else
                //{
                //    WorkingList = await GetTransmissionsFromCGI(value, Result, tgQuery);
                //}

                var UserTalkGroups = Utils.CacheUtil.GetUserTalkgroups1(_userInfo);
                List<Entities.Radio> RadioCachList = Utils.CacheUtil.GetRadios();
                var radioIDs = RadioCachList.Select(x => x.ID).ToList();

                //var sysid = UserTalkGroups.Select(x => x.Value).ToArray();
                //int[] val = new int[sysid.Length];
                //for(int i=0;i<sysid.Length;i++)
                //{
                //    val=sysid.Split(new[] {""}, StringSplitOptions.None);
                //}

                var UpdateDesc = from c in WorkingList
                                 join d in UserTalkGroups on c.SystemID.Trim().ToString() equals d.Value.Trim().ToString()
                                 select new Tuple<TalkGroupItem, SelectListItem>(c, d);
                logger.Debug(string.Format("Start add Radio and Tones"));
                foreach (var info in UpdateDesc.ToList())
                {
                    string tmpRadioDesc = "";
                    string RadioNum = info.Item1.RadioID.ToString();
                    var myRadio = RadioCachList.FirstOrDefault(c => c.ID == info.Item1.RadioID);
                    if (myRadio != null)
                    {
                        tmpRadioDesc = myRadio.Description;
                    }
                    string ChannelDesc = info.Item2.Text;
                    string ChannelNum = info.Item2.Value;
                    if (tmpRadioDesc.ToUpper().IndexOf("DISPATCH") > -1)
                    {
                        info.Item1.RadioDesc = "DISPATCH:::" + ChannelDesc;
                    }
                    else
                    {
                        if (info.Item1.RadioID == 0)
                        {
                            info.Item1.RadioDesc = "TONES" + (string.IsNullOrEmpty(info.Item1.Tones) ? "" : ": " + info.Item1.Tones);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(tmpRadioDesc) == true)
                            {
                                info.Item1.RadioDesc = string.Format("{0} ({1})", ChannelDesc, RadioNum);
                            }

                            else
                            {
                                info.Item1.RadioDesc = string.Format("{0} ({1})", tmpRadioDesc, RadioNum);
                            }
                        }
                    }
                }
                logger.Debug(string.Format("End Search WS and Create model list"));
                List<TalkItemGrouping> NewList = new List<TalkItemGrouping>();
                logger.Debug(string.Format("Start WorkingList to TalkItemGrouping List with db check for MarkListen AND ReviewStatus "));
                int cnt = 0;
                logger.Debug(string.Format("Start Fetch TalkItemGrouping Listfrom db. "));
                var talkGroupItemsDB = talkGroupItemRepository.FetchTalkGroupItemsFromDB(WorkingList.Select(x => x.ID).ToList().ToArray());
                logger.Debug(string.Format("End Fetch TalkItemGrouping Listfrom db. "));
                logger.Debug(string.Format("Start WorkingList to TalkItemGrouping List with db check for MarkListen AND ReviewStatus"));
                List<TalkGroupItem> items = WorkingList.OrderBy(c => c.TransStartMSec).ToList();
                Utils.Utils.RemoveDuplicates(items);
                foreach (var info in items)
                {
                    var talkGroupItem = talkGroupItemsDB.Where(x => x.TalkGroupItemId == info.ID).FirstOrDefault();
                    if (talkGroupItem != null)
                    {
                        info.ReviewStatus = talkGroupItem.ReviewStatus;
                        string[] ids = talkGroupItem.UserName.Split(';');
                        foreach (string s in ids)
                        {
                            PoliceMonitor.Entities.User u = talkGroupItemRepository.GetUserDetails(Convert.ToInt16(s));
                            if (u.CustomerId == UserInfo.CustomerId)
                            {
                                if (!string.IsNullOrEmpty(u.FirstName) && !string.IsNullOrEmpty(u.LastName))
                                    info.UserName = info.UserName + "," + u.FirstName + " " + u.LastName;
                                else
                                    info.UserName = info.UserName + "," + u.UserName;

                                info.MarkListen = talkGroupItem.MarkListen;
                            }
                        }
                        if (!string.IsNullOrEmpty(info.UserName))
                            info.UserName = info.UserName.Substring(1, info.UserName.Length - 1);
                    }
                    else
                    {
                        info.MarkListen = false;
                    }
                    if (info.TransLengthMSec > (minSec * 1000))
                    {
                        NewList.Add(new TalkItemGrouping() { ID = cnt++, Primary = info, Secondary = null });
                    }
                    else
                    {
                        var find = NewList.Where(c => c.Primary.TalkGroupID == info.TalkGroupID).OrderByDescending(c => c.ID).FirstOrDefault();
                        if (find != null && find.Secondary == null)
                            find.Secondary = info;
                    }
                }
                logger.Debug(string.Format("End WorkingList to TalkItemGrouping List with db check for MarkListen AND ReviewStatus"));
                foreach (var info in NewList)
                {
                    Result.FilteredList.Add(info.Primary);
                    if (info.Secondary != null)
                        Result.FilteredList.Add(info.Secondary);
                }
                Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
                //TempData["Cities"] = dbo.Cities.Select(x => new SelectListItem() { Value = x.cities,Text=x.cities }).ToList();
                //TempData["Cities"] = dbo.Cities.ToList();
               //-----------------------------------------------------------------------------
                if (value.RadioID != null)
                {
                    long FindRadio = Convert.ToInt64(value.RadioID);
                    Result.FilteredList = Result.FilteredList.Where(w => w.RadioID == FindRadio).ToList();
                }
                //----------------------------------------------------------------------------
                TempData["Result"] = Result;
                TempData.Keep();
                Result.Timeformat = dbo.Timeformats.ToList();
                Result.Cities = dbo.Cities.ToList();
                ViewBag.ReviewStatusList = reviewStatusListItems;
                ViewBag.MarkListenList = markListenListItems;
                Result.BaseURL = Utility.Get().BaseURL;
                logger.Debug(string.Format("End Execution of TalkGroup Search"));
                ViewBag.UserInfo = UserInfo;
                return View(Result);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return View("" + ex);
            }
        }

        private static TimeSpan ToTimezone(int mins)
        {
            int h = mins / 60;
            int m = mins - (h * 60);
            return new TimeSpan(h, m, 0);
        }

        private static async Task<List<TalkGroupItem>> GetTransmissionsFromCGI(TalkGroupView value, TalkGroupView Result, string tgQuery)
        {
            List<TalkGroupItem> WorkingList = new List<TalkGroupItem>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Utility.Get().BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string URLSearch = string.Format("/radio/index_search.cgi?tg={0}", tgQuery);

                if (value.radioFilter == "dateTimeRange" && value.StartDatetime != null && value.EndDatetime != null)
                {
                    Int64 Start = 0;
                    Int64 End = 0;
                    if (value.StartDatetime.HasValue && value.StartTime.HasValue)
                    {
                        DateTime startDatetime = value.StartDatetime.Value + value.StartTime.Value;
                        Start = Utility.Get().GetJsonDT(startDatetime);
                        URLSearch += string.Format("&start_time={0}", Start);
                    }

                    if (value.EndDatetime.HasValue && value.EndTime.HasValue)
                    {
                        DateTime endDatetime = value.EndDatetime.Value + value.EndTime.Value;
                        End = Utility.Get().GetJsonDT(endDatetime);
                        URLSearch += string.Format("&end_time={0}", End);
                    }
                }
                else
                {
                    Int64 StartAgo = 0;
                    if (value.StartHour > 0)
                    {
                        StartAgo = Utility.Get().GetJsonDT(value.StartHour);
                        URLSearch += string.Format("&start_time={0}", StartAgo);
                    }

                    if (value.EndHour > 0 && value.StartHour > 0)
                    {
                        int Seconds = value.EndHour * 60 * 60;
                        // Int64 tmpAgo = Utility.Get().GetJsonDT(value.EndHour);
                        URLSearch += string.Format("&end_time={0}", StartAgo + Seconds);
                    }
                }
                //add radio search
                // New code:
               
                HttpResponseMessage response = await client.GetAsync(URLSearch);

                if (response.IsSuccessStatusCode == false)
                {
                    Result.ErrorMessage = response.ReasonPhrase;
                }
                else
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var respItem = JsonConvert.DeserializeObject<JsonResponseIndexMatch>(json);

                    List<Tuple<TalkGroupItem, string>> tmpList = new List<Tuple<TalkGroupItem, string>>();

                    foreach (var info in respItem.matches)
                    {
                        int Cnt = -1;
                        TalkGroupItem item = new TalkGroupItem();
                        foreach (Object col in info)
                        {
                            Cnt++;
                            if (Cnt == 0)
                                item.ID = (string)col;
                            else if (Cnt == 1)
                                item.TransStartMSec = (long)col;
                            else if (Cnt == 2)
                                item.TransLengthMSec = (long)col;
                            else if (Cnt == 3)
                                item.TalkGroupID = (long)col;
                            else if (Cnt == 6)
                                item.RadioID = (long)col;
                            else if (Cnt == 10)
                            {
                                JObject obj = (Newtonsoft.Json.Linq.JObject)(col);
                                JToken token;
                                obj.TryGetValue("tones", out token);
                                if (token != null)
                                {
                                    Newtonsoft.Json.Linq.JArray arr = token as Newtonsoft.Json.Linq.JArray;
                                    List<JToken> l = arr.ToList();
                                    if (l.Count > 0)
                                    {
                                        foreach (JToken t in l)
                                        {
                                            item.Tones += t.ToString() + ", ";
                                        }

                                        item.Tones = item.Tones.Substring(0, item.Tones.Length - 2);
                                    }
                                }
                            }
                        }

                        tmpList.Add(new Tuple<TalkGroupItem, string>(item, string.Format("{0}{1}{2}", item.TransStartMSec, item.TransLengthMSec, item.RadioID)));
                    }

                    string LastUniqueID = "";
                    foreach (var c in tmpList)
                    {
                        if (LastUniqueID == c.Item2)
                            continue;
                        else
                        {
                            WorkingList.Add(c.Item1);
                            LastUniqueID = c.Item2;
                        }
                    }

                }
            }
            return WorkingList;
        }

        private static int diff = 0;
        private DateTime getTime()
        {
            if (diff == 0)
            {
                DateTime nextV1Scan = DateTime.UtcNow.ToUniversalTime();
                DateTime NtpDateTime = NtpClient.GetNetworkTime().ToUniversalTime();
                int timeDiffInSeconds = nextV1Scan.Ticks > NtpDateTime.Ticks ? -Convert.ToInt32((nextV1Scan - NtpDateTime).TotalSeconds) : Convert.ToInt32((NtpDateTime - nextV1Scan).TotalSeconds);
                diff = -40 + (timeDiffInSeconds);
            }

            return DateTime.UtcNow.AddSeconds(diff);
        }
        [HttpGet]
        public async Task<ActionResult> LivePlayback(object obj)
        {
            logger.Debug(string.Format("Start Execution of TalkGroup Search"));

            logger.Debug(string.Format("Start Search WS and Create model list"));
            TalkGroupView Result = new TalkGroupView();
            var items = (TalkGroupView)obj;
            var myList = items.SelectedItem as IEnumerable<string>;

            string tgQuery = "";
            int TstForNum = 0;

            items.TalkGroups.Clear();
            items.TalkGroups.AddRange(Utils.CacheUtil.GetUserTalkgroups(User));

            foreach (var info in myList)
            {

                if (int.TryParse(info, out TstForNum) == true)
                {
                    var tgGroup = items.TalkGroups.FirstOrDefault(c => c.Value == TstForNum.ToString());
                    if (tgGroup != null)
                    {
                        items.TalkGroups.Remove(tgGroup);
                        items.TalkGroupsSelected.Add(tgGroup);
                    }

                    if (tgQuery != "")
                        tgQuery += "," + info;
                    else
                        tgQuery = info;
                }
            };

            this.HttpContext.Session["_SavedTalkGroupView"] = items;

            int minSec = items.MinTranSeconds;
            List<TalkGroupItem> WorkingList = new List<TalkGroupItem>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Utility.Get().BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string URLSearch = string.Format("/radio/index_search.cgi?tg={0}", tgQuery);

                Int64 StartAgo = 0;

                StartAgo = 3; //Utility.Get().GetJsonDT(value.StartHour);
                URLSearch += string.Format("&start_time={0}", StartAgo);

                //int Seconds = value.EndHour * 60 * 60;
                // Int64 tmpAgo = Utility.Get().GetJsonDT(value.EndHour);
                //URLSearch += string.Format("&end_time={0}", StartAgo + Seconds);

                // New code:
                HttpResponseMessage response = await client.GetAsync(URLSearch);

                if (response.IsSuccessStatusCode == false)
                {
                    Result.ErrorMessage = response.ReasonPhrase;
                }
                else
                {
                    //ListTalkGroups tmp = await response.Content.ReadAsAsync<ListTalkGroups>();

                    var json = await response.Content.ReadAsStringAsync();
                    var respItem = JsonConvert.DeserializeObject<JsonResponseIndexMatch>(json);

                    List<Tuple<TalkGroupItem, string>> tmpList = new List<Tuple<TalkGroupItem, string>>();

                    foreach (var info in respItem.matches)
                    {
                        int Cnt = -1;
                        TalkGroupItem item = new TalkGroupItem();
                        foreach (Object col in info)
                        {
                            Cnt++;
                            if (Cnt == 0)
                                item.ID = (string)col;
                            else if (Cnt == 1)
                                item.TransStartMSec = (long)col;
                            else if (Cnt == 2)
                                item.TransLengthMSec = (long)col;
                            else if (Cnt == 3)
                                item.TalkGroupID = (long)col;
                            else if (Cnt == 6)
                                item.RadioID = (long)col;
                        }

                        tmpList.Add(new Tuple<TalkGroupItem, string>(item, string.Format("{0}{1}{2}", item.TransStartMSec, item.TransLengthMSec, item.RadioID)));
                    }

                    string LastUniqueID = "";
                    foreach (var c in tmpList)
                    {
                        if (LastUniqueID == c.Item2)
                            continue;
                        else
                        {
                            WorkingList.Add(c.Item1);
                            LastUniqueID = c.Item2;
                        }
                    }

                }
            }

            var UserTalkGroups = Utils.CacheUtil.GetUserTalkgroups(User);
            List<Entities.Radio> RadioCachList = Utils.CacheUtil.GetRadios();
            var radioIDs = RadioCachList.Select(x => x.ID).ToList();

            var UpdateDesc = from c in WorkingList
                             join d in UserTalkGroups on c.TalkGroupID.ToString() equals d.Value
                             select new Tuple<TalkGroupItem, SelectListItem>(c, d);


            foreach (var info in UpdateDesc.ToList())
            {
                string tmpRadioDesc = "";
                string RadioNum = info.Item1.RadioID.ToString();

                var myRadio = RadioCachList.FirstOrDefault(c => c.ID == info.Item1.RadioID);
                if (myRadio != null)
                {
                    tmpRadioDesc = myRadio.Description;
                }

                string ChannelDesc = info.Item2.Text;
                string ChannelNum = info.Item2.Value;


                if (tmpRadioDesc.ToUpper().IndexOf("DISPATCH") > -1)
                {
                    info.Item1.RadioDesc = ChannelDesc;
                }
                else
                {
                    if (string.IsNullOrEmpty(tmpRadioDesc) == true)
                    {
                        info.Item1.RadioDesc = string.Format("{0} ({1})", ChannelDesc, RadioNum);
                    }

                    else
                    {
                        info.Item1.RadioDesc = string.Format("{0} ({1})", tmpRadioDesc, RadioNum);
                    }

                }

            }

            logger.Debug(string.Format("End Search WS and Create model list"));
            List<TalkItemGrouping> NewList = new List<TalkItemGrouping>();
            logger.Debug(string.Format("Start WorkingList to TalkItemGrouping List with db check for MarkListen AND ReviewStatus "));
            int cnt = 0;
            logger.Debug(string.Format("Start Fetch TalkItemGrouping Listfrom db. "));
            var talkGroupItemsDB = talkGroupItemRepository.FetchTalkGroupItemsFromDB(WorkingList.Select(x => x.ID).ToList().ToArray());
            logger.Debug(string.Format("End Fetch TalkItemGrouping Listfrom db. "));
            logger.Debug(string.Format("Start WorkingList to TalkItemGrouping List with db check for MarkListen AND ReviewStatus"));
            foreach (var info in WorkingList.OrderBy(c => c.TransStartMSec))
            {
                var talkGroupItem = talkGroupItemsDB.Where(x => x.TalkGroupItemId == info.ID).FirstOrDefault();

                if (talkGroupItem != null)
                {
                    info.ReviewStatus = talkGroupItem.ReviewStatus;
                    info.MarkListen = talkGroupItem.MarkListen;
                    info.UserName = talkGroupItem.UserName;
                }
                else
                {
                    info.MarkListen = false;
                }
                if (info.TransLengthMSec > (minSec * 1000))
                {
                    NewList.Add(new TalkItemGrouping() { ID = cnt++, Primary = info, Secondary = null });
                }
                else
                {
                    var find = NewList.Where(c => c.Primary.TalkGroupID == info.TalkGroupID).OrderByDescending(c => c.ID).FirstOrDefault();
                    if (find != null && find.Secondary == null)
                        find.Secondary = info;
                }
            }
            logger.Debug(string.Format("End WorkingList to TalkItemGrouping List with db check for MarkListen AND ReviewStatus"));
            foreach (var info in NewList)
            {
                Result.FilteredList.Add(info.Primary);
                if (info.Secondary != null)
                    Result.FilteredList.Add(info.Secondary);

            }
            //Result.FilteredList.AddRange(WorkingList.Where(c => c.TransLengthMSec >= (minSec * 1000)).OrderBy(c => c.TransStartMSec).ToList());


            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<JsonResult> MarkedForReview(PoliceMonitor.Models.TalkGroupItem talkGroupItem)
        {
            talkGroupItem.UserIds = UserInfo.UserId.ToString();
            var response = talkGroupItemRepository.MarkedReview(talkGroupItem);
            if (response == true)
            {
                var result = TempData["Result"] as TalkGroupView;
                List<TalkGroupItem> listmodel = result.FilteredList;
                List<TalkGroupItem> newList = new List<TalkGroupItem>();
                foreach (var info in listmodel.ToList())
                {
                    if (info.ID == talkGroupItem.ID)
                    {
                        info.ReviewStatus = talkGroupItem.ReviewStatus;
                    }
                    newList.Add(info);
                }
                result.FilteredList.Clear();
                foreach (var info in newList)
                {
                    result.FilteredList.Add(info);
                }
                TempData["Result"] = result;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> MarkListen(TalkGroupItem talkGroupItem)
        {
            talkGroupItem.UserName = User.UserName;
            talkGroupItem.UserFirstName = User.FirstName;
            talkGroupItem.UserLastName = User.LastName;
            talkGroupItem.UserIds = Convert.ToString(User.UserId);
            var response = talkGroupItemRepository.MarkListen(talkGroupItem);
            if (response == true)
            {
                var result = TempData["Result"] as TalkGroupView;
                List<TalkGroupItem> listmodel = result.FilteredList;
                List<TalkGroupItem> newList = new List<TalkGroupItem>();
                foreach (var info in listmodel.ToList())
                {
                    if (info.ID == talkGroupItem.ID)
                    {
                        info.MarkListen = talkGroupItem.MarkListen;
                    }
                    newList.Add(info);
                }
                result.FilteredList.Clear();
                foreach (var info in newList)
                {
                    result.FilteredList.Add(info);
                }
                TempData["Result"] = result;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> FilterTalkGroup(byte? ReviewStatusList, string MarkListenList)
        {
            byte? reviewStatus = ReviewStatusList;
            bool? markListen = null;
            TalkGroupView Result = new TalkGroupView();

            if (MarkListenList == "Listened")
            {
                markListen = true;
            }
            else if (MarkListenList == "Not Listen")
            {
                markListen = false;
            }

            ViewBag.ReviewStatusList = reviewStatusListItems;
            ViewBag.MarkListenList = markListenListItems;

            if (TempData["Result"] != null)
            {
                var result = TempData["Result"] as TalkGroupView;
                List<TalkGroupItem> listmodel = result.FilteredList;
                TempData.Keep();
                dynamic fileterlistmodel = null;
                if (reviewStatus != null && markListen != null)
                {
                    fileterlistmodel = listmodel.Where(x => x.ReviewStatus == reviewStatus && x.MarkListen == markListen).ToList();
                }
                else if (reviewStatus != null && markListen == null)
                {
                    fileterlistmodel = listmodel.Where(x => x.ReviewStatus == reviewStatus).ToList();
                }
                else if (markListen != null && reviewStatus == null)
                {
                    fileterlistmodel = listmodel.Where(x => x.MarkListen == markListen).ToList();
                }
                else
                {
                    fileterlistmodel = listmodel;
                }
                foreach (var info in fileterlistmodel)
                {
                    Result.FilteredList.Add(info);
                }
            }

            ViewBag.UserInfo = UserInfo;
            using (PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities())
            {
                Result.Cities = dbo.Cities.ToList();
            }
            Result.BaseURL = Utility.Get().BaseURL;
            return View("~/Views/Home/TalkGroup.cshtml", Result);
        }

        public async Task<ActionResult> GoToTalkgroup(int id, int SystemId, DateTime transStartUTC, string itemid)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            TalkGroupView Result = new TalkGroupView();
            int tgQuery = id;
            Int64 StartAgo = 0;
            Int64 EndAfter = 0;
            List<TalkGroupItem> WorkingList = new List<TalkGroupItem>();
            int channelClipintervalInSecondsBefore = 0;
            if (transStartUTC.Year == 1970)
            {
                transStartUTC = DateTime.UtcNow;
                channelClipintervalInSecondsBefore = Convert.ToInt32(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ChannelClipintervalIn24HoursBefore"].ToString()) ? "0" : ConfigurationManager.AppSettings["ChannelClipintervalIn24HoursBefore"].ToString());
            }
            else
            {
                channelClipintervalInSecondsBefore = Convert.ToInt32(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ChannelClipintervalInSecondsBefore"].ToString()) ? "0" : ConfigurationManager.AppSettings["ChannelClipintervalInSecondsBefore"].ToString());
            }
            int channelClipintervalInSecondsAfter = Convert.ToInt32(string.IsNullOrEmpty(ConfigurationManager.AppSettings["ChannelClipintervalInSecondsAfter"].ToString()) ? "0" : ConfigurationManager.AppSettings["ChannelClipintervalInSecondsAfter"].ToString());
            Result.AutoPlay = (_userInfo.AutoPlay != null && _userInfo.AutoPlay.HasValue) ? _userInfo.AutoPlay.Value : _userInfo.DefaultAutoplaySetting;

            //if ((value.radioFilter == "dateTimeRange" && value.StartDatetime.Value.CompareTo(new DateTime(2016, 11, 1)) > 0) || value.radioFilter != "dateTimeRange")
            //{
            //    DateTime fromTimeUTC = transStartUTC;
            //    fromTimeUTC = fromTimeUTC.AddSeconds(-channelClipintervalInSecondsBefore);

            //    DateTime toTimeUTC = transStartUTC;
            //    toTimeUTC = toTimeUTC.AddSeconds(channelClipintervalInSecondsAfter);
            //    EndAfter = (Int64)(toTimeUTC.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            //    List<int> talkgroups = new List<int>();
            //    talkgroups.Add(id);                
            //    int[] sysid = new int[1];
            //    sysid[0] = SystemId;
            //    WorkingList = talkGroupItemRepository.GetTalkGroupItems(fromTimeUTC, toTimeUTC, talkgroups, sysid, 0, 0, 0, true);
            //}
            //            else
            //{
            //    //WorkingList = await GetTransmissionsFromCGI(value, Result, tgQuery);
            //}            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Utility.Get().BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (TEST)
                {
                    String username = "admin";
                    String password = "P5zHfKBpj3";
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                    client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
                }

                string URLSearch = string.Format("/radio/index_search.cgi?system_id={0}&tg={1}", SystemId, tgQuery);
                DateTime fromTimeUTC = transStartUTC;
                fromTimeUTC = fromTimeUTC.AddSeconds(-channelClipintervalInSecondsBefore);

                StartAgo = (Int64)(fromTimeUTC.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                if (StartAgo > 0)
                {
                    URLSearch += string.Format("&start_time={0}", StartAgo);
                }

                DateTime toTimeUTC = transStartUTC;
                toTimeUTC = toTimeUTC.AddSeconds(channelClipintervalInSecondsAfter);
                EndAfter = (Int64)(toTimeUTC.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                if (EndAfter > 0)
                {
                    URLSearch += string.Format("&end_time={0}", EndAfter);
                }
                HttpResponseMessage response = await client.GetAsync(URLSearch);

                if (response.IsSuccessStatusCode == false)
                {
                    Result.ErrorMessage = response.ReasonPhrase;
                }
                else
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var respItem = JsonConvert.DeserializeObject<JsonResponseIndexMatch>(json);

                    List<Tuple<TalkGroupItem, string>> tmpList = new List<Tuple<TalkGroupItem, string>>();
                    foreach (var info in respItem.matches)
                    {
                        int Cnt = -1;
                        TalkGroupItem item = new TalkGroupItem();

                        foreach (Object col in info)
                        {
                            Cnt++;
                            if (Cnt == 0)
                                item.ID = (string)col;
                            else if (Cnt == 1)
                                item.TransStartMSec = (long)col;
                            else if (Cnt == 2)
                                item.TransLengthMSec = (long)col;
                            else if (Cnt == 3)
                            {
                                item.TalkGroupID = (long)col;
                                item.SystemID = SystemId.ToString() + "_" + (col.ToString());
                            }
                            else if (Cnt == 6)
                                item.RadioID = (long)col;
                            else if (Cnt == 10)
                            {
                                JObject obj = (Newtonsoft.Json.Linq.JObject)(col);
                                JToken token;
                                obj.TryGetValue("tones", out token);
                                if (token != null)
                                {
                                    Newtonsoft.Json.Linq.JArray arr = token as Newtonsoft.Json.Linq.JArray;
                                    List<JToken> l = arr.ToList();
                                    if (l.Count > 0)
                                    {
                                        foreach (JToken t in l)
                                        {
                                            item.Tones += t.ToString() + ", ";
                                        }

                                        item.Tones = item.Tones.Substring(0, item.Tones.Length - 2);
                                    }
                                }
                            }
                        }

                        tmpList.Add(new Tuple<TalkGroupItem, string>(item, string.Format("{0}{1}{2}", item.TransStartMSec, item.TransLengthMSec, item.RadioID)));
                    }

                    string LastUniqueID = "";
                    foreach (var c in tmpList)
                    {
                        if (LastUniqueID == c.Item2)
                            continue;
                        else
                        {
                            WorkingList.Add(c.Item1);
                            LastUniqueID = c.Item2;
                        }
                    }
                }
            }
            var UserTalkGroups = Utils.CacheUtil.GetUserTalkgroups1(User);
            //List<TalkGroup> UserTalkGroups = PoliceMonitor.Utils.CacheUtil.GetCustomerTalkgroups(User, false);
            List<Entities.Radio> RadioCachList = Utils.CacheUtil.GetRadios();
            var radioIDs = RadioCachList.Select(x => x.ID).ToList();

            var UpdateDesc = from c in WorkingList
                             join d in UserTalkGroups on c.SystemID.ToString() equals d.Value
                             select new Tuple<TalkGroupItem, SelectListItem>(c, d);
            foreach (var info in UpdateDesc.ToList())
            {
                string tmpRadioDesc = "";
                string RadioNum = info.Item1.RadioID.ToString();

                var myRadio = RadioCachList.FirstOrDefault(c => c.ID == info.Item1.RadioID);
                if (myRadio != null)
                {
                    tmpRadioDesc = myRadio.Description;
                }
                string ChannelDesc = info.Item2.Text;
                string ChannelNum = info.Item2.Value;

                if (tmpRadioDesc.ToUpper().IndexOf("DISPATCH") > -1)
                {
                    info.Item1.RadioDesc = "DISPATCH:::" + ChannelDesc;
                }
                else
                {
                    if (info.Item1.RadioID == 0)
                    {
                        info.Item1.RadioDesc = "TONES" + (string.IsNullOrEmpty(info.Item1.Tones) ? "" : ": " + info.Item1.Tones);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(tmpRadioDesc) == true)
                        {
                            info.Item1.RadioDesc = string.Format("{0} ({1})", ChannelDesc, RadioNum);
                        }
                        else
                        {
                            info.Item1.RadioDesc = string.Format("{0} ({1})", tmpRadioDesc, RadioNum);
                        }
                    }
                }
            }
            var systemName = dbo.TblSystems.ToList();
            List<TalkItemGrouping> NewList = new List<TalkItemGrouping>();
            int cnt = 0;
            var talkGroupItemsDB = talkGroupItemRepository.FetchTalkGroupItemsFromDB(WorkingList.Select(x => x.ID).ToList().ToArray());

            foreach (var info in WorkingList.OrderBy(c => c.TransStartMSec))
            {
                var talkGroupItem = talkGroupItemsDB.Where(x => x.TalkGroupItemId == info.ID).FirstOrDefault();
                //var talkGroupItem= talkGroupItemRepository.GetTalkGroupItem(info.ID);
                info.MarkListen = false;
                if (talkGroupItem != null)
                {
                    string[] ids = talkGroupItem.UserName.Split(';');
                    foreach (string s in ids)
                    {
                        PoliceMonitor.Entities.User u = talkGroupItemRepository.GetUserDetails(Convert.ToInt16(s));
                        if (u.CustomerId == UserInfo.CustomerId)
                        {
                            if (!string.IsNullOrEmpty(u.FirstName) && !string.IsNullOrEmpty(u.LastName))
                                info.UserName = info.UserName + "," + u.FirstName + " " + u.LastName;
                            else
                                info.UserName = info.UserName + "," + u.UserName;
                        }
                    }
                    if (!string.IsNullOrEmpty(info.UserName))
                    {
                        info.UserName = info.UserName.Substring(1, info.UserName.Length - 1);
                        info.ReviewStatus = talkGroupItem.ReviewStatus;
                        info.MarkListen = talkGroupItem.MarkListen;
                    }
                }

                if (info.RadioID == 0)
                {
                    info.RadioDesc = "TONES" + (string.IsNullOrEmpty(info.Tones) ? "" : ": " + info.Tones);
                }
                //string[] sysmID = info.SystemID.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries).ToArray();

                //var sysID = systemName.Where(z => z.ID ==Convert.ToInt32(sysmID[0])).ToList();
                if (info.RadioDesc != "")
                {
                    info.RadioDesc = info.RadioDesc;
                }
                NewList.Add(new TalkItemGrouping() { ID = cnt++, Primary = info, Secondary = null });
            }

            foreach (var info in NewList)
            {
                Result.FilteredList.Add(info.Primary);
                if (info.Secondary != null)
                    Result.FilteredList.Add(info.Secondary);
            }
            ViewBag.ItemId = itemid;
            Result.BaseURL = Utility.Get().BaseURL;
            ViewBag.UserInfo = UserInfo;
            Result.Cities = dbo.Cities.ToList();
            Result.Timeformat = dbo.Timeformats.ToList();
            //TempData["Cities"] = dbo.Cities.ToList();
            TempData["Result"] = Result;
            return View(Result);
        }

        [HttpPost]
        public async Task<string> GetVideo(string url)
        {
            dynamic respItem = "";
            dynamic json = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Utility.Get().BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (TEST)
                {
                    String username = "admin";
                    String password = "P5zHfKBpj3";
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                    client.DefaultRequestHeaders.Add("Authorization", "Basic " + encoded);
                }

                string URLSearch = string.Format(url);
                HttpResponseMessage response = await client.GetAsync(URLSearch);
                if (response.IsSuccessStatusCode == false)
                {
                    //Result.ErrorMessage = response.ReasonPhrase;
                }
                json = await response.Content.ReadAsStringAsync();
                var json1 = json;
                respItem = ((Newtonsoft.Json.Linq.JContainer)(JObject.Parse(json.ToString()))).First.First;
                Session["DownlodeUrl"] = ((((Newtonsoft.Json.Linq.JContainer)(JObject.Parse(json.ToString())))).Last).Last;
            }
            return respItem;
        }


        //public PartialViewResult GetData()
        //{
        //    Thread.Sleep(5000);
        //    MyDB db = new MyDB();
        //    return PartialView("DParticalView", db.UsersRecords.ToList());
        //}


        [HttpPost]
        public ActionResult UploadFiles()
        {
            // Checking no of files injected in Request object 

            if (User.CustomerId != 7)
            {
                return Json("User not Authenticated!!");
            }
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            if (Request.Files.Count > 0)
            {
                try
                {
                    string path = "";
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        var userName = dbo.Users.Where(x => x.CustomerId == User.CustomerId).Select(x => x.UserName).FirstOrDefault().ToString();
                        HttpPostedFileBase file = files[i];
                        string fname;
                        //string UserName=dbo.Users.Where(x=>x.UserId==User).Select(x=>x.UserName)
                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                            fname = fname.Replace(' ', '-');
                        }
                        // Get the complete folder path and store the file inside it.  
                        string url = Utility.Get().BaseURL;
                        string strpath = Server.MapPath("~/Images/" + User.UserName + "");
                        if (!(Directory.Exists(strpath)))
                        {
                            Directory.CreateDirectory(strpath);
                        }
                        fname = Path.Combine(strpath, fname);
                        file.SaveAs(fname);
                        path = fname.Substring(fname.Replace("Images", " ").LastIndexOf(' '));
                        //path = path.Replace("PoliceMonitor", " ").Substring(path.Replace("PoliceMonitor", " ").LastIndexOf(' '));
                    }
                    // Returns message that successfully uploaded  
                    return Json(path);
                }
                catch (Exception ex)
                {
                    return Json("Error");
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        [HttpPost]
        public async Task<JsonResult> SearchCity(string Prefix)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var radio = dbo.Cities.Where(x => x.cities.StartsWith(Prefix)).ToList();
            return Json(radio, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> SearchEvent(string Prefix)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var radio = dbo.Events.Where(x => x.EventType.StartsWith(Prefix)).ToList();
            return Json(radio, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<string> SaveDraft(string result)
        {

            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();

            var lastRecord = dbo.logFBPostDrafts.OrderByDescending(o => o.ID).Take(1).FirstOrDefault();
            Entities.logFBPostDraft FbD = new Entities.logFBPostDraft();
            if (lastRecord != null)
            {
                if (lastRecord.Active == true)
                {
                    lastRecord.PostData = result;
                }
                else
                {
                    FbD.PostData = result;
                    FbD.Active = true;
                    FbD.UserName = User.UserName;
                    dbo.logFBPostDrafts.Add(FbD);
                }

            }
            else
            {
                FbD.PostData = result;
                FbD.Active = true;
                FbD.UserName = User.UserName;
                dbo.logFBPostDrafts.Add(FbD);
            }
            dbo.SaveChanges();


            return "save";//Json(radio, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> GetDraft(string Prefix)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var getDraft = dbo.logFBPostDrafts.Where(w => w.Active == true).OrderByDescending(o => o.ID).Select(s => s.PostData).Take(1).FirstOrDefault();
            //var getDraft = dbo.logFBPostDrafts.Where(w => w.Active == true).OrderByDescending(o => o.ID).Take(1).FirstOrDefault();
            if (getDraft == null)
            {
                return Json("empty", JsonRequestBehavior.AllowGet);
            }
            return Json(getDraft, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> ViewPostID(string result)
        {
            int ID = Convert.ToInt32(result);
            var getDraft = dbo.logFBPosts.Where(w => w.Postid == ID && w.IsActive == true).OrderByDescending(o => o.ID).ToList();

            if (getDraft == null)
            {
                return Json("empty", JsonRequestBehavior.AllowGet);
            }
            else
            {
                JsonConvert.SerializeObject(getDraft);
            }

            return Json(getDraft, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> GetDraftFBPop(string result)
        {


            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            //var getDraft = dbo.logFBPostDrafts.Where(w=>w.Active==true).OrderByDescending(o => o.ID).Select(s => s.PostData).Take(1).FirstOrDefault();
            dynamic getDraft = "";
            if (result == "")
            {
                getDraft = dbo.logFBPostDrafts.Where(x => x.Active == true).OrderByDescending(o => o.ID).ToList();
            }
            else
            {
                int ID = Convert.ToInt32(result);
                getDraft = dbo.logFBPostDrafts.Where(w => w.ID == ID && w.Active == true).OrderByDescending(o => o.ID).Take(1).FirstOrDefault();
            }
            if (getDraft == null)
            {
                return Json("empty", JsonRequestBehavior.AllowGet);
            }
            return Json(getDraft, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<string> DeleteDraft(string Prefix)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var lastRecord = dbo.logFBPostDrafts.OrderByDescending(o => o.ID).Take(1).FirstOrDefault();
            Entities.logFBPostDraft FbD = new Entities.logFBPostDraft();
            if (lastRecord != null)
            {
                if (lastRecord.Active == true)
                {
                    lastRecord.Active = false;
                }
                dbo.SaveChanges();
            }
            return "delete";
        }

        [HttpPost]
        public async Task<string> DeletePostID(string result)
        {
            int id = Convert.ToInt32(result);
            var lastRecord = dbo.logFBPosts.Where(s => s.ID == id).FirstOrDefault();
            Entities.logFBPostDraft FbD = new Entities.logFBPostDraft();
            if (lastRecord != null)
            {
                if (lastRecord.IsActive == true)
                {
                    lastRecord.IsActive = false;
                }
                dbo.SaveChanges();
            }
            return "delete";
        }

        //rohit
        [HttpGet]
        public ActionResult PostFaceBook()
        {
            List<PostFacebookModel> pf = new List<PostFacebookModel>();
            //PostFacebookModel model = new PostFacebookModel();
            List<Entities.logFBPost> Pfm = new List<Entities.logFBPost>();
            Pfm = dbo.logFBPosts.GroupBy(g => g.Postid.Value).Select(s => s.FirstOrDefault()).ToList();
            Pfm = Pfm.OrderByDescending(x => x.Postid).ToList();
            ViewBag.PostFB = Pfm;
            ViewBag.FirstName = User.FirstName;
            ViewBag.LastName = User.LastName;
            ViewBag.UserName = User.UserName;
            ViewBag.BaseURL = PoliceMonitor.BLL.Utility.Get().BaseURL;


            return View();
        }

        [HttpPost]
        public async Task<JsonResult> ViewSaved(string ID)
        {
            List<PostFacebookModel> pf = new List<PostFacebookModel>();
            int Id = Convert.ToInt32(ID);
            List<Entities.logFBPost> Pfm = new List<Entities.logFBPost>();
            Pfm = dbo.logFBPosts.Where(w => w.ID == Id).ToList();

            return Json(Pfm, JsonRequestBehavior.AllowGet);




        }



        [HttpPost]
        public async Task<JsonResult> PostFb(List<PostFBModel> result)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            try
            {
                result = result.OrderByDescending(o => o.postedOn).ThenByDescending(t => t.Time).ToList();
                if (User.CustomerId != 7)
                {
                    return Json("User not Authenticated", JsonRequestBehavior.AllowGet);
                    //return "User not Authenticated";
                }
                dynamic Postid = 1;
                var postid1 = dbo.logFBPosts.Select(x => x.Postid.HasValue);
                if (postid1.Any() == true)
                {
                    Postid = dbo.logFBPosts.OrderByDescending(obj => obj.Postid).Select(x => x.Postid.HasValue ? x.Postid : 1).First();
                }
                else
                {
                    Postid = 0;
                }
                foreach (var item in result)
                {
                    Entities.logFBPost saveData = new Entities.logFBPost();
                    saveData.AudioURL = item.url;
                    saveData.FBPostUrl = item.common_img;
                    saveData.Categories = item.categoryId.ToString();
                    saveData.City = item.city;
                    saveData.Description = item.description;
                    saveData.Event = item.event_type;
                    string[] imageurl = new string[5];
                    if (item.imageurl != null)
                    {
                        saveData.ImageURL = item.imageurl;
                        item.imageurl = Server.MapPath("~/" + item.imageurl);
                    }
                    if (item.ismember != null)
                    {
                        saveData.IsMember = true;
                    }
                    else
                    {
                        saveData.IsMember = false;
                    }
                    saveData.IsActive = true;
                    saveData.Location = item.location;
                    if (item.postedOn != null)
                    {
                        saveData.PostDate = Convert.ToDateTime(item.postedOn + " " + item.Time);
                    }
                    saveData.TransmissionUrl = item.TalkURl;
                    saveData.PostedBy = User.Email;
                    saveData.UserID = User.UserId;
                    saveData.Postid = Postid + 1;
                    saveData.TransmissionID = (item.transid);
                    dbo.logFBPosts.Add(saveData);
                }
                //dbo.SaveChanges();
                int count = 0;
                foreach (var item in result)
                {
                    if (count == 0)
                    {
                        if (item.common_img != null)
                        {
                            item.common_img = Server.MapPath("~/" + item.common_img);
                            var ImageName = PostDataFB(item.common_img);
                            var pic = JObject.Parse(ImageName).Last.First;
                            item.common_img = "./uploads/" + pic.ToString();
                            result[0].common_img = "./uploads/" + pic.ToString();
                        }
                    }
                    if (item.imageurl != null)
                    {
                        var ImageName = PostDataFB(item.imageurl);
                        var pic = JObject.Parse(ImageName).Last.First;
                        item.imageurl = "./uploads/" + pic.ToString();
                    }
                    if (item.postedOn != null)
                    {
                        item.postedOn = Convert.ToDateTime(item.postedOn + " " + item.Time).ToString("yyyy-MM-dd hh:mm:ss tt");
                    }
                    else
                    {
                        item.postedOn = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt");
                    }
                    if (item.city == null)
                    {
                        item.city = " ";
                    }
                    if (item.ismember != null)
                    {
                        item.ismember = false;
                    }
                    else
                    {
                        item.ismember = true;
                    }
                    if (item.description == null)
                    {
                        item.description = " ";
                    }
                    item.postid = Convert.ToInt32(Postid + 1);
                    item.postedBy = User.Email;
                }
                string result1 = "";
                string jsonFb = JsonConvert.SerializeObject(result);
                string RestwebURl = ConfigurationManager.AppSettings["RestAPIWebSiteUrl"];
                var URlPost = RestwebURl + "/api/Transmissions/postEvent";
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    result1 = client.UploadString(URlPost, jsonFb);

                }
                if (result1 == "success")
                {
                    dbo.SaveChanges();
                }

                logger.Debug(string.Format("Post to Facebook URL{0} and data {1}", URlPost, jsonFb));
                // return "" + result1;
                return Json(result1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                //return ""+ex;
                return Json(ex, JsonRequestBehavior.AllowGet);
            }
            //return "Done";
        }
        public string PostDataFB(string urlPost)
        {
            string fileToUpload = urlPost;//Server.MapPath(urlPost);
            string responseAsString = "";
            string RestwebURl = ConfigurationManager.AppSettings["RestAPIWebSiteUrl"];
            string url = RestwebURl + "/upload";
            using (var client = new WebClient())
            {
                byte[] result1 = client.UploadFile(url, fileToUpload);
                responseAsString = Encoding.Default.GetString(result1);
                logger.Debug(string.Format("Post to Facebook Image Uplode response{0}", responseAsString));
            }
            return responseAsString;
        }
        [HttpPost]
        public async Task<string> downloadVideo()
        {
            var downlodeurl = Session["DownlodeUrl"].ToString();
            downlodeurl = downlodeurl.Replace("http://rtc.radio1033.com/", "");
            string fileName = "";
            dynamic respItem = "";
            dynamic json = "";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Utility.Get().BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string URLSearch = string.Format(downlodeurl);
                HttpResponseMessage response = await client.GetAsync(URLSearch);
                if (response.IsSuccessStatusCode == false)
                {
                    //Result.ErrorMessage = response.ReasonPhrase;
                }

                json = await response.Content.ReadAsStringAsync();

                if ((((Newtonsoft.Json.Linq.JContainer)(JObject.Parse(json.ToString()))).First).Path == "status")
                {
                    respItem = "fail";
                    Session["DownlodeUrl"] = ((((Newtonsoft.Json.Linq.JContainer)(JObject.Parse(json.ToString())))).Last).Last;
                    return respItem;
                }
                else
                {
                    respItem = ((Newtonsoft.Json.Linq.JContainer)(JObject.Parse(json.ToString()))).First.First;
                    string VedioUrl = respItem;
                    VedioUrl = VedioUrl.Replace("http://rtc.radio1033.com/", "");
                    string name = Utils.Utils.RandomString(16);
                    fileName = name + ".mp4";
                    string filePath = Server.MapPath("~/") + ConfigurationManager.AppSettings["TransmissionsFolder"] + "/" + fileName;
                    await Utils.Utils.DownloadFile(VedioUrl, filePath);

                    return fileName + "__" + respItem; //Json(new { filepath = Session["DownlodeUrl"].ToString(), fileName = (fileName) }, JsonRequestBehavior.AllowGet);
                }
            }
        }



        [HttpPost]
        public async Task<JsonResult> GenerateMp3(string chunks, Boolean mergeTransmissions)
        {
            string URLMP3 = "";
            string fileName = "";
            using (PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities())
            {
                string[] chunkArr = chunks.Split(',');
                foreach (string c in chunkArr)
                {
                    TalkGroupItem talkGroupItem = new TalkGroupItem();
                    talkGroupItem.ID = c;
                    talkGroupItem.MarkListen = true;
                    talkGroupItem.UserName = User.UserName;
                    talkGroupItem.UserFirstName = User.FirstName;
                    talkGroupItem.UserLastName = User.LastName;
                    talkGroupItem.UserIds = Convert.ToString(User.UserId);
                    talkGroupItemRepository.MarkListen(talkGroupItem);
                }

                try
                {
                    int count = dbo.Chunks.Where(x => x.transmission == chunks).Count();
                    if (count > 0)
                    {
                        List<Entities.Chunk> files = dbo.Chunks.Where(x => x.transmission == chunks).ToList();
                        return Json(new { success = true, fileName = (files.First().fileName) }, JsonRequestBehavior.AllowGet);
                    }

                    string name = Utils.Utils.RandomString(16);
                    while (dbo.Chunks.Where(x => x.fileName == name).Count() > 0)
                        name = Utils.Utils.RandomString(16);

                    fileName = name + ".mp3";
                    string filePath = Server.MapPath("~/") + ConfigurationManager.AppSettings["TransmissionsFolder"] + "/" + fileName;
                    URLMP3 = string.Format("radio/get_audio.cgi?chunk={0}", chunks);
                    await Utils.Utils.DownloadFile(URLMP3, filePath);
                    Entities.Chunk c = new Entities.Chunk();
                    c.fileName = fileName;
                    c.transmission = chunks;
                    dbo.Chunks.Add(c);
                    dbo.SaveChanges();
                }
                catch (Exception e)
                {
                    logger.Error(e);
                }
            }
            return Json(new { success = true, fileName = (fileName), AudioURL = URLMP3 }, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public FileResult DownloadMp3File(string fName, Boolean mergedTransmissions)
        //{
        //    string filePath = ConfigurationManager.AppSettings["TransmissionsFolder"] + ((mergedTransmissions)?Constant.MERGEDTRANSMISSIONS:"") + fName;
        //    byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
        //    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fName);
        //}

        [HttpGet]
        public ActionResult DownloadMp3File(string fName, Boolean mergedTransmissions)
        {
            string filePath = "~/" + ConfigurationManager.AppSettings["TransmissionsFolder"] + "/" + ((mergedTransmissions) ? Constant.MERGEDTRANSMISSIONS : "") + fName;
            DateTime d = DateTime.Now;
            string displayName = "Audio-" + Utils.Utils.TwoDigit(d.Month) + "-" + Utils.Utils.TwoDigit(d.Day) + "-" + Utils.Utils.TwoDigit(d.Year)
                + "-" + Utils.Utils.TwoDigit(d.Hour) + Utils.Utils.TwoDigit(d.Minute) + Utils.Utils.TwoDigit(d.Second) + "-" + UserInfo.UserName + ".mp3";
            return new DownloadResult
            {
                VirtualPath = filePath,
                FileDownloadName = displayName
            };
        }

        [HttpGet]
        public ActionResult DownloadMp4File(string fName, Boolean mergedTransmissions)
        {
            string filePath = "~/" + ConfigurationManager.AppSettings["TransmissionsFolder"] + "/" + ((mergedTransmissions) ? Constant.MERGEDTRANSMISSIONS : "") + fName;
            DateTime d = DateTime.Now;
            string displayName = "Video-" + Utils.Utils.TwoDigit(d.Month) + "-" + Utils.Utils.TwoDigit(d.Day) + "-" + Utils.Utils.TwoDigit(d.Year)
                + "-" + Utils.Utils.TwoDigit(d.Hour) + Utils.Utils.TwoDigit(d.Minute) + Utils.Utils.TwoDigit(d.Second) + "-" + UserInfo.UserName + ".mp4";
            return new DownloadResult
            {
                VirtualPath = filePath,
                FileDownloadName = displayName
            };
        }

        //private async Task<byte[]> GetMP3File(string chunks)
        //{
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri(Utility.Get().BaseURL);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.Timeout = new TimeSpan(0, 59, 0);

        //        string URLMP3 = string.Format("radio/get_audio.cgi?chunk={0}", chunks);

        //        HttpResponseMessage response = await client.GetAsync(URLMP3);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var responseData = await response.Content.ReadAsByteArrayAsync();
        //            return responseData;
        //        }
        //    }
        //    return null;
        //}

        public class NtpClient
        {
            public static DateTime GetNetworkTime()
            {
                //default Windows time server
                const string ntpServer = "time.windows.com";

                // NTP message size - 16 bytes of the digest (RFC 2030)
                var ntpData = new byte[48];

                //Setting the Leap Indicator, Version Number and Mode values
                ntpData[0] = 0x1B; //LI = 0 (no warning), VN = 3 (IPv4 only), Mode = 3 (Client Mode)

                var addresses = Dns.GetHostEntry(ntpServer).AddressList;

                //The UDP port number assigned to NTP is 123
                var ipEndPoint = new IPEndPoint(addresses[0], 123);
                //NTP uses UDP
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(ipEndPoint);

                //Stops code hang if NTP is blocked
                socket.ReceiveTimeout = 3000;

                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();

                //Offset to get to the "Transmit Timestamp" field (time at which the reply 
                //departed the server for the client, in 64-bit timestamp format."
                const byte serverReplyTime = 40;

                //Get the seconds part
                ulong intPart = BitConverter.ToUInt32(ntpData, serverReplyTime);

                //Get the seconds fraction
                ulong fractPart = BitConverter.ToUInt32(ntpData, serverReplyTime + 4);

                //Convert From big-endian to little-endian
                intPart = SwapEndianness(intPart);
                fractPart = SwapEndianness(fractPart);

                var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

                //**UTC** time
                var networkDateTime = (new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddMilliseconds((long)milliseconds);

                return networkDateTime.ToLocalTime();
            }

            // stackoverflow.com/a/3294698/162671
            static uint SwapEndianness(ulong x)
            {
                return (uint)(((x & 0x000000ff) << 24) +
                               ((x & 0x0000ff00) << 8) +
                               ((x & 0x00ff0000) >> 8) +
                               ((x & 0xff000000) >> 24));
            }
        }
    }







    public class DownloadResult : ActionResult
    {
        public DownloadResult()
        {
        }
        public DownloadResult(string virtualPath)
        {
            this.VirtualPath = virtualPath;
        }
        public string VirtualPath { get; set; }
        public string FileDownloadName { get; set; }
        public override void ExecuteResult(ControllerContext context)
        {
            if (!String.IsNullOrEmpty(FileDownloadName))
            {
                context.HttpContext.Response.AddHeader("content-disposition",
                  "attachment; filename=" + this.FileDownloadName);
            }

            context.HttpContext.Response.ContentType = "audio/mp3, audio/wave";

            string filePath = context.HttpContext.Server.MapPath(this.VirtualPath);
            context.HttpContext.Response.TransmitFile(filePath);
            context.HttpContext.Response.Flush();
        }
    }

}