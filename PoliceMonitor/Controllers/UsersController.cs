﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PoliceMonitor.Entities;
using PoliceMonitor.Repositories;
using PoliceMonitor.Models;
using PoliceMonitor.Security.Base.Controller;
using PoliceMonitor.Security.Attributes;
using PoliceMonitor.BLL;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Security.Principal;
using PoliceMonitor.Utils;
using System.Runtime.Caching;

namespace PoliceMonitor.Controllers
{
    public class UsersController : BaseController
    {

        private AdminRepository adminRepository;
        private CustomersRepository customersRepository;
        static ObjectCache cache = MemoryCache.Default;
        public UsersController()
        {
            _userInfo = User;
            customersRepository = new CustomersRepository(User);
            adminRepository = new AdminRepository(User);
        }
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }
        #region Admins

        // GET: AdminIndex
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminIndex()
        {
            var listAdmins = adminRepository.ListAdmins();
            if (User.IsInRole(Constant.ADMIN_ROLENAME))
            {
                listAdmins = listAdmins.Where(x => x.Customer.CustomerId == User.CustomerId).ToList();
            }
            return View(listAdmins);
        }

        // GET: Users/AdminDetails/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = adminRepository.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (User.IsInRole(Constant.ADMIN_ROLENAME) && user.Customer.CustomerId != User.CustomerId)
            {
                return HttpNotFound();
            }
            return View(user);
        }




        // GET: Users/AdminCreate
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminCreate()
        {
            AdminViewModels admin = new AdminViewModels();
            CustomersViewModels customer = new CustomersViewModels();
            var customers=customersRepository.ListCustomers();
            customer.CustomerId = 0;
            customer.CustomerName = "Super Admin";
            customer.Active = true;
            customers.Add(customer);
            admin.CustomerList = customers.Where(x => x.Active == true);
            admin.SelectedCustomerId = -1;
            return View(admin);
        }

        // POST: Users/AdminCreate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminCreate([Bind(Include = "UserId,SelectedCustomerId,UserName,UserFirstName,UserLastName,Password,ConfirmPassword,Email,Address,City,State,PostalCode,PhoneNumber,Active")] AdminViewModels admin
            )
        {
            CustomersViewModels customer = new CustomersViewModels();
            if (User.IsInRole(Constant.ADMIN_ROLENAME))
            {
                admin.SelectedCustomerId = User.CustomerId;
            }
            if (ModelState.IsValid)
            {
                if (!adminRepository.IsExist(admin.Email))
                {
                    adminRepository.Add(admin);
                    return RedirectToAction("AdminIndex");
                }
                else
                {
                    ModelState.AddModelError("", string.Format("'{0}' user name already exist.", admin.UserName));
                }
            }
            var customers = customersRepository.ListCustomers();
            customer.CustomerId = 0;
            customer.CustomerName = "Super Admin";
            customer.Active = true;
            customers.Add(customer);           
            admin.CustomerList = customers.Where(x => x.Active == true);
            return View(admin);
        }

        //// GET: Users/SuperAdminCreate
        //[CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        //public ActionResult SuperAdminCreate()
        //{
        //    SuperAdminViewModel admin = new SuperAdminViewModel();
        //    var customers = customersRepository.ListCustomers().Where(x => x.Active == true);
        //    admin.CustomerList = customers;
        //    return View(admin);
        //}

        //// POST: Users/SuperAdminCreate
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        //public ActionResult SuperAdminCreate([Bind(Include = "UserId,SelectedCustomerId,UserName,UserFirstName,UserLastName,Password,ConfirmPassword,Email,Address,City,State,PostalCode,PhoneNumber,Active")] SuperAdminViewModel admin
        //    )
        //{
          
        //    if (ModelState.IsValid)
        //    {
        //        if (!adminRepository.IsExist(admin.Email))
        //        {
        //            adminRepository.AddSuperAdmin(admin);
        //            return RedirectToAction("AdminIndex");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", string.Format("'{0}' user name already exist.", admin.UserName));
        //        }
        //    }
        //    var customers = customersRepository.ListCustomers().Where(x => x.Active == true);
        //    admin.CustomerList = customers;
        //    return View(admin);
        //}

        // GET: Users/AdminEdit/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminEdit(int? id)
        {
            CustomersViewModels customer = new CustomersViewModels();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = adminRepository.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (User.IsInRole(Constant.ADMIN_ROLENAME) && user.Customer.CustomerId != User.CustomerId)
            {
                return HttpNotFound();
            }            
            var customers = customersRepository.ListCustomers();
            customer.CustomerId = 0;
            customer.CustomerName = "Super Admin";
            customer.Active = true;
            customers.Add(customer);
            user.CustomerList = customers.Where(x => x.Active == true);
            return View(user);
        }

        // POST: Users/AdminEdit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminEdit([Bind(Include = "UserId,SelectedCustomerId,UserName,UserFirstName,UserLastName,Password,ConfirmPassword,Email,Address,City,State,PostalCode,PhoneNumber,Active")] AdminViewModels admin
           )
        {
            if (User.IsInRole(Constant.ADMIN_ROLENAME))
            {
                admin.SelectedCustomerId = User.CustomerId;
            }
            if (ModelState.IsValid)
            {
                adminRepository.Update(admin);
                return RedirectToAction("AdminIndex");
            }
            var customers = customersRepository.ListCustomers().Where(x => x.Active == true);
            admin.CustomerList = customers;           
            return View(admin);
        }

        // GET: Users/AdminDelete/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = adminRepository.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (User.IsInRole(Constant.ADMIN_ROLENAME) && user.Customer.CustomerId != User.CustomerId)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/AdminDelete/5
        [HttpPost, ActionName("AdminDelete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult AdminDeleteConfirmed(int id)
        {
            adminRepository.Delete(id);
            return RedirectToAction("AdminIndex");
        }

        #endregion

        #region Users

        // GET: Index
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult Index()
        {
            return View(adminRepository.ListUsers());
        }

        // GET: Users/Details/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult Details(int? id, int? custId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = adminRepository.FindUser(id, custId);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Create()
        {
            UserViewModels user = new UserViewModels();
            List<SelectListItem> listPermisssion=new List<SelectListItem>();
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();

            string customerPermission = customersRepository.GetCustomerPermission(User.CustomerId);
            var permissions = adminRepository.GetPermissionLevel(UserInfo);
            foreach(var permiss in permissions)
            {
                if (permiss.Permission1.Equals(customerPermission) || !permiss.Permission1.Contains(Constant.SCANLISTENONLY_PERMISSION))
                    listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString() });             
            }
            user.PermissionLevelList = listPermisssion;

            var talkGroups = adminRepository.GetCustomerTalkGroups();
            
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User,true))
                {
                    var val = talkGroup.SystemID + "_" + talkGroup.TalkGroup;

                    if (x.Value == val)
                    {
                        listTalkGroups.Add(new SelectListItem() { Value = val.ToString(), Text = x.Text, Selected=true });
                        break;
                    }
                }
            }
            ViewBag.TalkGroups = listTalkGroups;
           // ViewBag.Radios = new string[0];
            //ViewBag.Role = User.IsInRole(Constant.SUPERADMIN_ROLENAME) ? Constant.SUPERADMIN_ROLENAME : Constant.ADMIN_ROLENAME;

            var customers = customersRepository.ListCustomers().Where(x => x.Active == true);
            //var admins = adminRepository.ListAdmins();
            ViewBag.CustomerList = customers;

            return View(user);
        }
        //Prasun
        //[CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        [HttpGet]
        public async Task<JsonResult> FillAdmin(int customerId)
        {
            var listAdmins = adminRepository.ListAdmins();

            if (User.IsInRole(Constant.SUPERADMIN_ROLENAME))
            {
                listAdmins = listAdmins.Where(x => x.Customer.CustomerId == customerId).ToList();
            }
            return Json(listAdmins, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> FillTalkGroup(int id, int customerId)
        {
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            var talkGroups = adminRepository.GetCustomerTalkGroups(customerId);
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User,true))
                {
                    var val = talkGroup.SystemID + "_" + talkGroup.TalkGroup;
                    if (x.Value.Trim() == val.Trim())
                    {
                        listTalkGroups.Add(new SelectListItem() { Value = val.ToString(), Text = x.Text, Selected = (id <= 0) });
                        break;
                    }
                }
            }
            if(id > 0)
            {
                var user = adminRepository.FindUser(id, customerId);

                foreach (var talkGroup in user.TalkGroupList)
                {
                    foreach (var _talkGroup in listTalkGroups)
                    {
                        if (_talkGroup.Value == talkGroup.Value)
                        {
                            _talkGroup.Selected = true;
                            break;
                        }
                    }
                }

            }
            return Json(listTalkGroups, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> FillPermissions(int customerId)
        {
            List<SelectListItem> listPermisssion = new List<SelectListItem>();
           
            //string customerPermission = customersRepository.GetCustomerPermission(customerId);
            //var permissions = adminRepository.GetPermissionLevel();
            //foreach (var permiss in permissions)
            //{
            //    if (permiss.Permission1.Equals(customerPermission) || !permiss.Permission1.Contains(Constant.SCANLISTENONLY_PERMISSION))
            //        listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString() , Selected = false});
            //}
            var customerPermission = customersRepository.GetCustomerAssignedPermissions(customerId);
            foreach (var permiss in customerPermission)
            {
               listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = false });
            }
            return Json(listPermisssion, JsonRequestBehavior.AllowGet);
        }
        //End


        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> Create([Bind(Include = "SelectedCustomerId,UserId,UserName,UserFirstName,UserLastName,Password,ConfirmPassword,Email,Address,City,State,PostalCode,PhoneNumber,Active")] UserViewModels user,
            string[] SelectedRadio, string[] selectedPermissionLevel,params string[] selectedTalkGroup)
        {
            if (ModelState.IsValid)
            {
                //if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                //{
                    //if (SelectedRadio != null && SelectedRadio.Length > 0)
                    //{

                
                    if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                    {
                        string[] selectsysval = new string[selectedTalkGroup.Length];
                        string[] selectId = new string[selectedTalkGroup.Length];
                        for (int i = 0; i < selectedTalkGroup.Length; i++)
                        {
                            var a = selectedTalkGroup[i].Split('_');
                            selectsysval[i] = a[0];
                            selectId[i] = a[1];
                        }


                        if (!adminRepository.IsExistEmail(user.Email))
                        {
                            adminRepository.AddUser(user, SelectedRadio, selectedPermissionLevel, selectId,selectsysval);
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError("Email", string.Format("'{0}' Email already exist.", user.Email));
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please must select any talk group.");
                    }
                    //}
                    //else
                    //{
                    //    ModelState.AddModelError("", "Please must add a radio.");
                    //}
                //}
                //else
                //{
                //    ModelState.AddModelError("", "Please must select any permission level.");
                //}
            }
           // List<SelectListItem> listPermisssion = new List<SelectListItem>();
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            
            //string customerPermission = customersRepository.GetCustomerPermission(User.CustomerId);
            //var permissions = adminRepository.GetPermissionLevel();
            //foreach (var permiss in permissions)
            //{
            //    var selected = false;
            //    if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
            //    {
            //        selected = selectedPermissionLevel.Where(x => x == permiss.PermissionId.ToString()).Any();
            //    }
            //    if (permiss.Permission1.Equals(customerPermission) || (!permiss.Permission1.Contains(Constant.SCANLISTENONLY_PERMISSION)))
            //    {                   
            //        listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = selected });
            //    } 
            //}
            //var customerPermission = customersRepository.GetCustomerAssignedPermissions(User.CustomerId);
            //foreach (var permiss in customerPermission)
            //{
            //    listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = false });
            //}
            //user.PermissionLevelList = listPermisssion;

            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        var selected = false;
                        if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                        {
                            selected = selectedTalkGroup.Where(z => z == talkGroup.TalkGroup.ToString()).Any();
                        }
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text, Selected = selected });
                        break;
                    }
                }
            }
            ViewBag.TalkGroups = listTalkGroups;
            var customers = customersRepository.ListCustomers().Where(x => x.Active == true);           
            ViewBag.CustomerList = customers;
            //ViewBag.Radios = SelectedRadio != null && SelectedRadio.Length > 0 ? SelectedRadio.ToList() : new List<string>();
            user.SelectedCustomerId = 0;
            return View(user);
        }

        // GET: Users/Edit/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME, Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Edit(int? id, int? custId)
        {
            List<SelectListItem> listPermisssion = new List<SelectListItem>();
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = adminRepository.FindUser(id, custId);
            if (user == null)
            {
                return HttpNotFound();
            }
            //string customerPermission = customersRepository.GetCustomerPermission(custId);
            
            //var permissions = adminRepository.GetPermissionLevel();
            //foreach (var permiss in permissions)
            //{
            //    if (permiss.Permission1.Equals(customerPermission) || (!permiss.Permission1.Contains(Constant.SCANLISTENONLY_PERMISSION)))
            //    {
            //        var selected = user.PermissionLevelList.Where(x => x.Value == permiss.PermissionId.ToString()).Any();
            //        listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = selected });
            //    }
            //}
            var customerPermission = customersRepository.GetCustomerAssignedPermissions(custId);
            foreach (var permiss in customerPermission)
            {
                var selected = user.PermissionLevelList.Where(x => x.Value == permiss.PermissionId.ToString()).Any();
                listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = selected });
            }
            user.PermissionLevelList = listPermisssion;

            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User,true))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text });
                        break;
                    }
                }
            }

            foreach (var talkGroup in user.TalkGroupList)
            {
                foreach (var _talkGroup in listTalkGroups)
                {
                    if (_talkGroup.Value == talkGroup.Value)
                    {
                        _talkGroup.Selected = true;
                    }
                }
            }
            ViewBag.TalkGroups = listTalkGroups;
            //ViewBag.Radios = adminRepository.GetRadioListUser(id);
            var customers = customersRepository.ListCustomers().Where(x => x.Active == true);
            ViewBag.CustomerList = customers;
            user.SelectedCustomerId = (int) custId;
            return View(user);
        }

        // POST: Users/AdminEdit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME, Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Edit([Bind(Include = "SelectedCustomerId,UserId,UserFirstName,UserLastName,UserName,Password,ConfirmPassword,Email,Address,City,State,PostalCode,PhoneNumber,Active")] UserViewModels user,
            string[] SelectedRadio, string[] selectedPermissionLevel , params string[] selectedTalkGroup)
        {

            if (ModelState.IsValid)
            {
                //if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                //{
                    //if (SelectedRadio != null && SelectedRadio.Length > 0)
                    //{          
                if (!adminRepository.IsExistUserName(user.UserName))
                {
                    if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                    {
                        string[] selectsysval = new string[selectedTalkGroup.Length];
                        string[] selectId = new string[selectedTalkGroup.Length];
                        for (int i = 0; i < selectedTalkGroup.Length; i++)
                        {
                            var a = selectedTalkGroup[i].Split('_');
                            selectsysval[i] = a[0];
                            selectId[i] = a[1];
                        }


                        adminRepository.UpdateUser(user, SelectedRadio, selectedPermissionLevel, selectId, selectsysval);
                        foreach (var element in MemoryCache.Default)
                        {
                            MemoryCache.Default.Remove(element.Key);
                        }
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please must select any talk group.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "UserName already exist.");
                }
                    //}
                    //else
                    //{
                    //    ModelState.AddModelError("", "Please must add a radio.");
                    //}
                //}
                //else
                //{
                //    ModelState.AddModelError("", "Please must select any permission level.");
                //}
            }

            List<SelectListItem> listPermisssion = new List<SelectListItem>();
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();

            string customerPermission = customersRepository.GetCustomerPermission(User.CustomerId);
            var permissions = adminRepository.GetPermissionLevel(UserInfo);
            foreach (var permiss in permissions)
            {
                if (permiss.Permission1.Equals(customerPermission) || (!permiss.Permission1.Contains(Constant.SCANLISTENONLY_PERMISSION)))
                {
                    var selected = false;
                    if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                    {
                        selected = selectedPermissionLevel.Where(x => x == permiss.PermissionId.ToString()).Any();
                    }
                    listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = selected });
                }
            }
            user.PermissionLevelList = listPermisssion;

            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        var selected = false;
                        if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                        {
                            selected = selectedTalkGroup.Where(z => z == talkGroup.TalkGroup.ToString()).Any();
                        }
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text, Selected = selected });
                        break;
                    }
                }
            }
            ViewBag.TalkGroups = listTalkGroups;
            ViewBag.CustomerList = customersRepository.ListCustomers().Where(x => x.Active == true);
            //ViewBag.Radios = SelectedRadio != null && SelectedRadio.Length > 0 ? SelectedRadio.ToList() : new List<string>();
            return View(user);
        }

        // GET: Users/Delete/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME, Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Delete(int? id, int? custId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = adminRepository.FindUser(id, custId);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME, Constant.SUPERADMIN_ROLENAME)]
        public ActionResult DeleteConfirmed(int id, int? custId)
        {
            adminRepository.DeleteUser(id, custId);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<JsonResult> SearchRadio(string Prefix)
        {
            Utility.Get().RadioCachList = Utils.CacheUtil.GetRadios();

            //Searching records from list using LINQ query  
            var radio = (from N in Utility.Get().RadioCachList
                         where N.Description.ToLower().StartsWith(Prefix.ToLower()) || N.ID.Equals(Prefix)
                         select new {N.ID, N.Description });
            return Json(radio, JsonRequestBehavior.AllowGet);
        }  
        #endregion
    }
}
