﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PoliceMonitor.Entities;
using PoliceMonitor.Security.Base.Controller;
using PoliceMonitor.Security.Attributes;
using PoliceMonitor.Models;
using PoliceMonitor.Repositories;
using PoliceMonitor.Security.Base.Model;
using System.Threading.Tasks;
using PoliceMonitor.BLL;

namespace PoliceMonitor.Controllers
{
    public class CustomerNotificationController : BaseController
    {
        private CustomersRepository customersRepository = null;
        private AdminRepository adminRepository=null;
        private GroupsRepository groupRepository = null;
        public CustomerNotificationController()
        {
            customersRepository = new CustomersRepository(User);
            adminRepository = new AdminRepository(User);
            groupRepository = new GroupsRepository(User);
        }

        #region New Code
        // GET: CustomerNotification
        public ActionResult IndexNew()
        {
            var _userInfo = User as PoliceMonitor.Security.Principal.CustomPrincipal;
            if (!(_userInfo.HasPermission(PoliceMonitor.Security.Base.Model.Constant.NOTIFICATION_CONFIGURATION)))
            {
                AuthorizationContext filterContext = new AuthorizationContext();
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                Response.Redirect("~/Account/AccessDenied");
            }
            var customerNotificationSettings = customersRepository.ListNotificationSettingsNew();
            return View(customerNotificationSettings);
        }

        // GET: CustomerNotification/Details/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public ActionResult DetailsNew(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customerNotificationSetting = customersRepository.FindNotificationSettingsNew(id);
            if (customerNotificationSetting == null)
            {
                return HttpNotFound();
            }
            return View(customerNotificationSetting);
        }

        // GET: CustomerNotification/Create
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> CreateNew()
        {
            NotificationViewModelsNew notificationModel = new NotificationViewModelsNew();
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            List<SelectListItem> listUserGroups = new List<SelectListItem>();
            List<SelectListItem> listUsers = new List<SelectListItem>();
            notificationModel.SelectedCaseId = 1;

            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, false))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        string value = talkGroup.SystemID.ToString() + "_" + talkGroup.TalkGroup.ToString();
                        listTalkGroups.Add(new SelectListItem() { Value = value, Text = x.Text });
                        break;
                    }
                }
            }
            var groups = groupRepository.ListGroups();
            foreach (var grp in groups)
            {
                listUserGroups.Add(new SelectListItem() { Value = grp.GroupId.ToString(), Text = grp.GroupName});
            }
            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName });
            }
            notificationModel.Users = listUsers;
            notificationModel.GroupList = listUserGroups;
            notificationModel.TalkGroupList = listTalkGroups;
            ViewBag.Radios = new string[0];
            return View(notificationModel);
        }

        // POST: CustomerNotification/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> CreateNew([Bind(Include = "CustomerNotificationId,NotificationName,SelectedCaseId,NotifiyTransmissionsCount,Interval,AfterFirstTransmissionWaitInterval,LastNSecTransmissionsCount,NSecInterval,AfterNSecTransmissionWaitInterval,SelectedGroupValues,SelectedUserValues")] NotificationViewModelsNew notificationModel
            ,string[] SelectedRadio,  params string[] selectedTalkGroup)
        {
            ModelValidateNew(notificationModel);

            if (ModelState.IsValid)
            {
                if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                {
                    string[] selectsysval = new string[selectedTalkGroup.Length];
                    string[] selectId = new string[selectedTalkGroup.Length];
                    for (int i = 0; i < selectedTalkGroup.Length; i++)
                    {
                        var a = selectedTalkGroup[i].Split('_');
                        selectsysval[i] = a[0];
                        selectId[i] = a[1];
                    }


                    customersRepository.AddNotificationSettingNew(notificationModel, SelectedRadio, selectId,selectsysval);
                    return RedirectToAction("IndexNew");
                }
                else
                {
                    ModelState.AddModelError("", "Please must select any talk group.");
                }
            }
            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            List<SelectListItem> listUserGroups = new List<SelectListItem>();
            List<SelectListItem> listUsers = new List<SelectListItem>();
            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, false))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        var selected = false;
                        if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                        {
                            selected = selectedTalkGroup.Where(z => z == talkGroup.TalkGroup.ToString()).Any();
                        }
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text, Selected = selected });
                        break;
                    }
                }
            }
            var groups = groupRepository.ListGroups();
            foreach (var grp in groups)
            {
                listUserGroups.Add(new SelectListItem() { Value = grp.GroupId.ToString(), Text = grp.GroupName });
            }
            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName });
            }
            notificationModel.Users = listUsers;
            notificationModel.GroupList = listUserGroups;
            notificationModel.TalkGroupList = listTalkGroups;
            ViewBag.Radios = SelectedRadio != null && SelectedRadio.Length > 0 ? SelectedRadio.ToList() : new List<string>();
            return View(notificationModel);
        }

        // GET: CustomerNotification/Edit/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> EditNew(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotificationViewModelsNew notificationModel = customersRepository.FindNotificationSettingsNew(id);
            if (notificationModel == null)
            {
                return HttpNotFound();
            }

            if (notificationModel.NotifiyTransmissionsCount != null && notificationModel.Interval == null)
            {
                notificationModel.SelectedCaseId = 1;
            }
            else if (notificationModel.AfterFirstTransmissionWaitInterval != null)
            {
                notificationModel.SelectedCaseId = 2;
            }
            else if (notificationModel.LastNSecTransmissionsCount != null && notificationModel.NSecInterval != null && notificationModel.AfterNSecTransmissionWaitInterval != null)
            {
                notificationModel.SelectedCaseId = 3;
            }

            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            List<SelectListItem> listUserGroups = new List<SelectListItem>();
            List<SelectListItem> listUsers = new List<SelectListItem>();
            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        var selected = false;
                        selected = notificationModel.TalkGroupList.Where(z => z.Value == talkGroup.TalkGroup.ToString()).Any();
                        string value = talkGroup.SystemID.ToString() + "_" + talkGroup.TalkGroup.ToString();
                        listTalkGroups.Add(new SelectListItem() { Value = value, Text = x.Text, Selected = selected });
                        break;
                    }
                }
            }
            var groups = groupRepository.ListGroups();
            foreach (var grp in groups)
            {
                listUserGroups.Add(new SelectListItem() { Value = grp.GroupId.ToString(), Text = grp.GroupName });
            }
            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName });
            }
           
            notificationModel.Users = listUsers;

            notificationModel.SelectedGroupValues = notificationModel.GroupList!=null ? notificationModel.GroupList.Select(x => x.Value).ToList():null;
            notificationModel.GroupList = listUserGroups;

            notificationModel.TalkGroupList = listTalkGroups;
            ViewBag.Radios = notificationModel.RadioList != null ? notificationModel.RadioList.Select(x => x.Value).ToList() : null;

            return View(notificationModel);
        }

        // POST: CustomerNotification/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public async Task<ActionResult> EditNew([Bind(Include = "CustomerNotificationId,NotificationName,SelectedCustomerId,SelectedCaseId,NotifiyTransmissionsCount,Interval,AfterFirstTransmissionWaitInterval,LastNSecTransmissionsCount,NSecInterval,AfterNSecTransmissionWaitInterval,SelectedGroupValues,SelectedUserValues,Active")] NotificationViewModelsNew notificationModel
            , string[] SelectedRadio, params string[] selectedTalkGroup)
        {
            ModelValidateNew(notificationModel);

            if (ModelState.IsValid)
            {
                if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                {
                    string[] selectsysval = new string[selectedTalkGroup.Length];
                    string[] selectId = new string[selectedTalkGroup.Length];
                    for (int i = 0; i < selectedTalkGroup.Length; i++)
                    {
                        var a = selectedTalkGroup[i].Split('_');
                        selectsysval[i] = a[0];
                        selectId[i] = a[1];
                    }
                    customersRepository.UpdateNotificationSettingNew(notificationModel, SelectedRadio, selectId,selectsysval);
                    return RedirectToAction("IndexNew");
                }
                else
                {
                    ModelState.AddModelError("", "Please must select any talk group.");
                }
            }
            if (notificationModel.NotifiyTransmissionsCount != null && notificationModel.Interval == null)
            {
                notificationModel.SelectedCaseId = 1;
            }
            else if (notificationModel.AfterFirstTransmissionWaitInterval != null)
            {
                notificationModel.SelectedCaseId = 2;
            }
            else if (notificationModel.LastNSecTransmissionsCount != null && notificationModel.NSecInterval != null && notificationModel.AfterNSecTransmissionWaitInterval != null)
            {
                notificationModel.SelectedCaseId = 3;
            }

            List<SelectListItem> listTalkGroups = new List<SelectListItem>();
            List<SelectListItem> listUserGroups = new List<SelectListItem>();
            List<SelectListItem> listUsers = new List<SelectListItem>();
            var talkGroups = adminRepository.GetCustomerTalkGroups();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (x.Value == talkGroup.TalkGroup)
                    {
                        var selected = false;
                        if (selectedTalkGroup != null && selectedTalkGroup.Length > 0)
                        {
                            selected = selectedTalkGroup.Where(z => z == talkGroup.TalkGroup.ToString()).Any();
                        }
                        listTalkGroups.Add(new SelectListItem() { Value = talkGroup.TalkGroup.ToString(), Text = x.Text, Selected = selected });
                        break;
                    }
                }
            }
            var groups = groupRepository.ListGroups();
            foreach (var grp in groups)
            {
                listUserGroups.Add(new SelectListItem() { Value = grp.GroupId.ToString(), Text = grp.GroupName });
            }
            var users = adminRepository.ListUsers();
            foreach (var user in users)
            {
                listUsers.Add(new SelectListItem() { Value = user.UserId.ToString(), Text = user.UserName });
            }
           // notificationModel.SelectedUserValues = notificationModel.Users != null ? notificationModel.Users.Select(x => x.Value).ToList() : null;
            notificationModel.Users = listUsers;

            //notificationModel.SelectedGroupValues = notificationModel.GroupList != null ? notificationModel.GroupList.Select(x => x.Value).ToList() : null;
            notificationModel.GroupList = listUserGroups;

            notificationModel.TalkGroupList = listTalkGroups;
            ViewBag.Radios = SelectedRadio != null ? SelectedRadio.ToList() : null;
            return View(notificationModel);
        }

        // GET: CustomerNotification/Delete/5
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public ActionResult DeleteNew(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //customersRepository.DeleteNotificationSettingNew((int)id);
            var customerNotificationSetting = customersRepository.FindNotificationSettingsNew(id);
            if (customerNotificationSetting == null)
            {
                return HttpNotFound();
            }
            return View(customerNotificationSetting);
        }

        // POST: CustomerNotification/Delete/5
        [HttpPost, ActionName("DeleteNew")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.ADMIN_ROLENAME)]
        public ActionResult DeleteConfirmedNew(int id)
        {
            customersRepository.DeleteNotificationSettingNew(id);
            return RedirectToAction("IndexNew");
        }

        private void ModelValidateNew(NotificationViewModelsNew notificationModel)
        {
            if (notificationModel.SelectedCaseId == 1)
            {
                if (notificationModel.NotifiyTransmissionsCount == null || notificationModel.NotifiyTransmissionsCount <= 0)
                    ModelState.AddModelError("NotifiyTransmissionsCount", "Please enter valid Notifiy Transmissions Count.");
                if (notificationModel.Interval == null || notificationModel.Interval <= 0)
                    ModelState.AddModelError("Interval", "Please enter valid Interval.");
            }
            else if (notificationModel.SelectedCaseId == 2)
            {
                if (notificationModel.AfterFirstTransmissionWaitInterval == null || notificationModel.AfterFirstTransmissionWaitInterval <= 0)
                    ModelState.AddModelError("AfterFirstTransmissionWaitInterval", "Please enter valid After First Transmission Wait Interval.");
            }
            else if (notificationModel.SelectedCaseId == 3)
            {
                if (notificationModel.LastNSecTransmissionsCount == null || notificationModel.LastNSecTransmissionsCount <= 0)
                    ModelState.AddModelError("LastNSecTransmissionsCount", "Please enter valid Last N Sec Transmissions Count.");
                if (notificationModel.NSecInterval == null || notificationModel.NSecInterval <= 0)
                    ModelState.AddModelError("NSecInterval", "Please enter valid N Sec Interval.");
                if (notificationModel.AfterNSecTransmissionWaitInterval == null || notificationModel.AfterNSecTransmissionWaitInterval <= 0)
                    ModelState.AddModelError("Interval", "Please enter valid After N Sec Transmission Wait Interval.");
            }
        }

        #endregion

        #region Old Code
        // GET: CustomerNotification
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Index()
        {
            var customerNotificationSettings = customersRepository.ListNotificationSettings();
            return View(customerNotificationSettings);
        }

        // GET: CustomerNotification/Details/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customerNotificationSetting = customersRepository.FindNotificationSettings(id);
            if (customerNotificationSetting == null)
            {
                return HttpNotFound();
            }
            return View(customerNotificationSetting);
        }

        // GET: CustomerNotification/Create
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Create()
        {
            //ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerName");
            NotificationViewModels notificationModel = new NotificationViewModels();
            var customers = customersRepository.ListCustomers().Where(x => x.Active == true && customersRepository.CheckNotificationSettingsExistForCustomer(x.CustomerId)==false);
            notificationModel.CustomerList = customers;
            notificationModel.SelectedCaseId = 1;
            return View(notificationModel);
        }

        // POST: CustomerNotification/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Create([Bind(Include = "CustomerNotificationId,SelectedCustomerId,SelectedCaseId,NotifiyTransmissionsCount,Interval,AfterFirstTransmissionWaitInterval,LastNSecTransmissionsCount,NSecInterval,AfterNSecTransmissionWaitInterval")] NotificationViewModels notificationModel)
        {
            ModelValidate(notificationModel);

            if (ModelState.IsValid)
            {
                if (customersRepository.CheckNotificationSettingsExistForCustomer(notificationModel.SelectedCustomerId) == false)
                {
                    customersRepository.AddNotificationSetting(notificationModel);
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Selected customer already has notification setting in database.");
                }
            }
            var customers = customersRepository.ListCustomers().Where(x => x.Active == true);
            notificationModel.CustomerList = customers;
            return View(notificationModel);
        }

        // GET: CustomerNotification/Edit/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotificationViewModels notificationModel = customersRepository.FindNotificationSettings(id);
            if (notificationModel == null)
            {
                return HttpNotFound();
            }

            if (notificationModel.NotifiyTransmissionsCount != null && notificationModel.Interval == null)
            {
                notificationModel.SelectedCaseId = 1;
            }
            else if (notificationModel.AfterFirstTransmissionWaitInterval != null)
            {
                notificationModel.SelectedCaseId = 2;
            }
            else if (notificationModel.LastNSecTransmissionsCount != null && notificationModel.NSecInterval != null &&  notificationModel.AfterNSecTransmissionWaitInterval != null)
            {
                notificationModel.SelectedCaseId = 3;
            }

            notificationModel.CustomerList = customersRepository.ListCustomers().Where(x => x.CustomerId == notificationModel.SelectedCustomerId);
            return View(notificationModel);
        }

        // POST: CustomerNotification/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Edit([Bind(Include = "CustomerNotificationId,SelectedCustomerId,SelectedCaseId,NotifiyTransmissionsCount,Interval,AfterFirstTransmissionWaitInterval,LastNSecTransmissionsCount,NSecInterval,AfterNSecTransmissionWaitInterval")] NotificationViewModels notificationModel)
        {
            ModelValidate(notificationModel);
            if (ModelState.IsValid)
            {
                customersRepository.UpdateNotificationSetting(notificationModel);
                return RedirectToAction("Index");
            }

            notificationModel.CustomerList = customersRepository.ListCustomers().Where(x => x.Active == true);
            return View(notificationModel);
        }

        // GET: CustomerNotification/Delete/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customerNotificationSetting = customersRepository.FindNotificationSettings(id);
            if (customerNotificationSetting == null)
            {
                return HttpNotFound();
            }
            return View(customerNotificationSetting);
        }

        // POST: CustomerNotification/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult DeleteConfirmed(int id)
        {
            customersRepository.DeleteNotificationSetting(id);
            return RedirectToAction("Index");
        }

        private void ModelValidate(NotificationViewModels notificationModel)
        {
            if (notificationModel.SelectedCaseId == 1)
            {
                if (notificationModel.NotifiyTransmissionsCount == null || notificationModel.NotifiyTransmissionsCount <= 0)
                    ModelState.AddModelError("NotifiyTransmissionsCount", "Please enter valid Notifiy Transmissions Count.");
                if (notificationModel.Interval == null || notificationModel.Interval <= 0)
                    ModelState.AddModelError("Interval", "Please enter valid Interval.");
            }
            else if (notificationModel.SelectedCaseId == 2)
            {
                if (notificationModel.AfterFirstTransmissionWaitInterval == null || notificationModel.AfterFirstTransmissionWaitInterval <= 0)
                    ModelState.AddModelError("AfterFirstTransmissionWaitInterval", "Please enter valid After First Transmission Wait Interval.");
            }
            else if (notificationModel.SelectedCaseId == 3)
            {
                if (notificationModel.LastNSecTransmissionsCount == null || notificationModel.LastNSecTransmissionsCount <= 0)
                    ModelState.AddModelError("LastNSecTransmissionsCount", "Please enter valid Last N Sec Transmissions Count.");
                if (notificationModel.NSecInterval == null || notificationModel.NSecInterval <= 0)
                    ModelState.AddModelError("NSecInterval", "Please enter valid N Sec Interval.");
                if (notificationModel.AfterNSecTransmissionWaitInterval == null || notificationModel.AfterNSecTransmissionWaitInterval <= 0)
                    ModelState.AddModelError("Interval", "Please enter valid After N Sec Transmission Wait Interval.");
            }
        }

        #endregion
    }
}
