﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PoliceMonitor.Entities;
using PoliceMonitor.Repositories;
using PoliceMonitor.Models;
using System.Net.Http;
using PoliceMonitor.BLL;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using PoliceMonitor.Security.Base.Controller;
using PoliceMonitor.Security.Attributes;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Security.Principal;

namespace PoliceMonitor.Controllers
{
    public class ReportsController : BaseController
    {
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }

        CustomersRepository customersRepository;
        public ReportsController()
        {
            _userInfo = User;
            customersRepository = new CustomersRepository(User);
            //adminRepository = new AdminRepository(User);
        }

        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Index()
        {
            return View(customersRepository.ListCustomers().Where(x => x.Active == true));
        }

        [HttpPost]
        public string GetCustomerStats(string startDate, string endDate, string startTime, string endTime, int customerId)
        {
            DateTime start = DateTime.ParseExact(startDate + " " + startTime, "dd/MM/yyyy HH : mm", null);
            DateTime end = DateTime.ParseExact(endDate + " " + endTime, "dd/MM/yyyy HH : mm", null);

            List<Usage> data = customersRepository.GetCustomerStats(Utils.Utils.ToServerTime(start), Utils.Utils.ToServerTime(end), customerId);
            // get customers
            List<CustomersViewModels> customers;
            if (customerId > 0)
            {
                customers = customersRepository.ListCustomers().Where(x => x.CustomerId == customerId && x.Active == true).ToList();
            }
            else
            {
                customers = customersRepository.ListCustomers().Where(x => x.Active == true).ToList();
            }

            List<Usage> result = new List<Usage>();
            List<int> customerIds = data.Select(x=>x.CustomerID).Distinct().ToList();
            foreach(int customer in customerIds)
            {
                Usage usage = new Usage();
                usage.CustomerID = customer;
                usage.CustomerName = customers.Where(x => x.CustomerId == customer).Select(x => x.CustomerName).FirstOrDefault();
                usage.Duration = data.Where(x => x.CustomerID == customer).Sum(x => x.Duration);
                usage.TotalTransmissions = data.Where(x => x.CustomerID == customer).Sum(x=>x.TotalTransmissions);
                usage.TotalLogins = data.Where(x => x.CustomerID == customer).Sum(x => x.TotalLogins);
                usage.userwiseUsage = data.Where(x => x.CustomerID == customer).ToList();
                result.Add(usage);
            }

            return JsonConvert.SerializeObject(result);
        }
    }
}
