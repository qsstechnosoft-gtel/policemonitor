﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PoliceMonitor.Entities;
using PoliceMonitor.Repositories;
using PoliceMonitor.Models;
using System.Net.Http;
using PoliceMonitor.BLL;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using PoliceMonitor.Security.Base.Controller;
using PoliceMonitor.Security.Attributes;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Security.Principal;
using System.Runtime.Caching;
using System.Text;
using System.Data.SqlClient;

namespace PoliceMonitor.Controllers
{
    public class CustomersController : BaseController
    {
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }
        static ObjectCache cache = MemoryCache.Default;
        private CustomersRepository customersRepository;
        private AdminRepository adminRepository;
        public CustomersController()
        {
            _userInfo = User;
            customersRepository = new CustomersRepository(User);
            adminRepository = new AdminRepository(User);
        }

        // GET: Customers
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Index()
        {
            List<SelectListItem> ObjItem = new List<SelectListItem>()  
            {  
                  new SelectListItem {Text="Active",Value="Active",Selected=true },  
                  new SelectListItem {Text="In-Active",Value="InActive" },  
                  new SelectListItem {Text="All",Value="All"},   
            };
            ViewBag.ListItem = ObjItem;
            return View(customersRepository.ListCustomers());
        }

        //Get:Customers Log
        [HttpGet]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME, Constant.ADMIN_ROLENAME)]
        public ActionResult CustomerLog()
        {
            CustomerLog log = new Models.CustomerLog();
            var role = _userInfo.UserRoles[0].RoleName;
            if (role.Trim() == "Admin".Trim())
            {
                var Customerlist = customersRepository.ListCustomers().Where(x => x.Active == true);
                ViewBag.CustomerList = Customerlist.Where(w => w.CustomerId == _userInfo.CustomerId);
            }
            else
            {
                var Customerlist = customersRepository.ListCustomers().Where(x => x.Active == true);
                ViewBag.CustomerList = Customerlist;
            }
            //Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();

            return View();
        }

        [HttpGet]
        public async Task<JsonResult> CustomerLoginDetail(string customerid, string StartDate, string EndDate)
        {//rohit
            CustomerLog log = new Models.CustomerLog();
            try
            {

                var cust = Convert.ToInt32(customerid);
                var start = Convert.ToDateTime(StartDate);
                var end = Convert.ToDateTime(EndDate);
                Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
                StringBuilder Query = new StringBuilder("  select u.[UserId],u.[UserName],c.counts from [dbo].[User]  as u right join   ( select count(*) as counts, [UserId] from [dbo].[ActivityLogs] where userId in (select [UserId] from [dbo].[User] where [CustomerId] =" + cust + " and [createdOn] >'" + start + "' and [createdOn]<'" + end + "' ) group by userId) as c on u.UserId=c.userId");
                //int[] userList = dbo.Users.Where(w => w.CustomerId == cust).Select(s => s.UserId).ToArray();
                //var CustomerDetail = dbo.ActivityLogs.Where(w => (userList.Contains(w.userId ?? 0)) && w.createdOn > start && w.createdOn < end).GroupBy(g => g.userId).ToList();
                //var clientIdParameter = new SqlParameter("@CustomerID", cust);
                //var result = dbo.Database.SqlQuery<CutomerDetail>("GetCustomerInfo @CustomerID", clientIdParameter).ToList();
                var Customerlist = customersRepository.ListCustomers().Where(x => x.Active == true);
                ViewBag.CustomerList = Customerlist;
                var userlist = dbo.Database.SqlQuery<CutomerDetail>(Query.ToString()).ToList();
                log.Detail = userlist.ToList();

            }
            catch (Exception ex)
            {

                throw;
            }
            return Json(log, JsonRequestBehavior.AllowGet);
        }

        // GET: Customers/Details/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = customersRepository.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            await GetTalkGroups();

            foreach (var talkGroup in customer.TalkGroupList)
            {
                talkGroup.Text = Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, false).Where(x => x.Value == talkGroup.Value).Select(x => x.Text).FirstOrDefault();
            }
            return View(customer);
        }

        // GET: Customers/Create
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Create()
        {
            var id = (TempData["ID"]);
            string systemID = Request.Form["FilterSystemID"];
            await GetTalkGroups();
            //IEnumerable<SelectListItem> selectList = from x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, true)
            //                                         select new SelectListItem
            //                                         {
            //                                             Value = x.Value,
            //                                             Text = x.Text + " ( " + x.Value + " )",
            //                                             Selected = x.Selected
            //                                         };
            //ViewBag.TalkGroups = new SelectList(selectList, "Value", "Text");
            List<SelectListItem> listPermisssion = new List<SelectListItem>();
            var permissions = adminRepository.GetPermissionLevel(UserInfo);
            foreach (var permiss in permissions)
            {
                //if(permiss.Permission1 == "Scan / listen only")
                listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString() });
            }
            if (Session["EditTalkGroup"] != null)
            {
                Session.Remove("EditTalkGroup");
            }
            ViewBag.PermissionLevelList = listPermisssion;
            return View();
        }


        [HttpGet]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Filter(string id)
        {
            Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            List<PoliceMonitor.Models.TalkGroup> talkgroups = PoliceMonitor.Utils.CacheUtil.GetCustomerTalkgroups(User, false);


            var Countys = dbo.TalkGroups.Select(s => s.County).OrderBy(m => m).Distinct().ToList();
            List<string> county = new List<string>();
            county.AddRange(Countys);
            ViewBag.County = county.OrderBy(o => o).ToList();

            var systemID = dbo.TblSystems.OrderBy(x => x.Name).ToList();
            //var sysID = dbo.TblSystems.ToList();
            if (Session["EditTalkGroup"] != null)
            {
                ViewBag.SelectID = Session["EditTalkGroup"].ToString();

            }
            if (Session["Selectedvalue"] != null)
            {
                ViewBag.SelectID = Session["Selectedvalue"].ToString();
            }

            ViewBag.SysID = systemID;
            return View("Filter");
        }

        [HttpPost]
        //[CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Filter(FilterViewModel Usertalkgroups)
        {
            string[] systemID = Request.Form["FilterSystemID"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (Session["Selectedvalue"] != null)
            {
                Session.Remove("Selectedvalue");
            }

            Session["Selectedvalue"] = Request.Form["FilterSystemID"];
            TempData["ID"] = Request.Form["FilterSystemID"]; //add data as properties of ViewBag
            if (Request.QueryString["name"] == "Create")
            {
                return RedirectToAction(Request.QueryString["name"]);
            }
            else
            {
                string val = Request.Form["FilterSystemID"];
                string[] systemID1 = val.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                string[] selectsysval = new string[systemID1.Length];
                string[] selectId = new string[systemID1.Length];
                for (int i = 0; i < systemID1.Length; i++)
                {
                    var a = systemID1[i].Split('_');
                    selectsysval[i] = a[0];
                    selectId[i] = a[1];
                }
                int customer = Convert.ToInt32(Request.QueryString["customerid"]);
                customersRepository.UpdateEdit(customer, Usertalkgroups, selectId.ToArray(), selectsysval.ToArray());
                string val1 = Request.QueryString["name"].ToString() + "/" + Request.QueryString["customerid"].ToString();
                foreach (var element in MemoryCache.Default)
                {
                    MemoryCache.Default.Remove(element.Key);
                }
                if (Session["Selectedvalue"] != null)
                {
                    Session.Remove("Selectedvalue");
                }
                return RedirectToAction(val1);
            }
        }

        [HttpPost]
        public async Task<JsonResult> getCounty(string SystemId)
        {

            Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            var SystemID = SystemId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList();
            int[] SelSystemID = SystemID.Select(int.Parse).ToArray();
            var county = dbo.TalkGroups.Where(x => SelSystemID.Contains(x.SystemId)).Select(x => x.County).Distinct().ToList();
            var SelectCounty = county.OrderBy(o => o).ToList();
            return Json(SelectCounty, JsonRequestBehavior.AllowGet);

        }




        [HttpPost]
        public async Task<JsonResult> GetSelectedTalkGroupval(string SystemId, string talkgroupid)
        {
            var systemID = SystemId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            var talkgroupID = talkgroupid.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            int[] SelSystemID = systemID.Select(int.Parse).ToArray();
            int[] SeltalkID = talkgroupID.Select(int.Parse).ToArray();
            Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            //var talkgroup = dbo.TalkGroups.Where(w => SelSystemID.Contains(w.SystemId) && SeltalkID.Contains(w.ID)).Distinct().ToList();



            var talkgr = dbo.TalkGroups.Where(w => SelSystemID.Contains(w.SystemId) && SeltalkID.Contains(w.ID)).Distinct().ToList();
            List<Entities.TalkGroup> talkgroup = new List<Entities.TalkGroup>();
            for (int i = 0; i < SeltalkID.Length; i++)
            {
                talkgroup.AddRange(talkgr.Where(w => (w.SystemId == SelSystemID[i]) && (w.ID == SeltalkID[i])).Distinct().ToList());
            }


            return Json(talkgroup, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public async Task<JsonResult> GetTalkGroupval(string SystemId, string County)
        {
            int sysID = Convert.ToInt32(SystemId);
            Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            //List <PoliceMonitor.Entities.TalkGroup> talkgroup = new List<PoliceMonitor.Entities.TalkGroup>();
            dynamic talkgroup = "";
            if (County == "")
            {
                talkgroup = dbo.TalkGroups.Where(x => x.SystemId == sysID).Select(a => new { a.Name, a.ID, a.County }).OrderBy(a => a.Name).ToList();
            }
            else if (SystemId == "")
            {
                talkgroup = dbo.TalkGroups.Where(x => x.County.Trim() == County.Trim()).Select(a => new { a.Name, a.ID, a.County }).OrderBy(a => a.Name).ToList();

            }
            else
                talkgroup = dbo.TalkGroups.Where(x => x.SystemId == sysID && x.County.Trim() == County.Trim()).Select(a => new { a.Name, a.ID, a.County }).OrderBy(a => a.Name).ToList();

            return Json(talkgroup, JsonRequestBehavior.AllowGet);

        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Create([Bind(Include = "CustomerId,CustomerName,StreetAddress,City,State,ZipCode,ContactName,ContactPhoneNumber,UsersCount,UnlimitedUsers,SelectedValues,DefaultAutoplaySetting")] CustomersViewModels customer,
           string[] selectedPermissionLevel /*, params string[] selectedTalkGroup*/)
        {
            if (ModelState.IsValid)
            {
                if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                {
                    string val = Request.Form["FilterSystemID"];
                    string[] systemID = val.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    string[] selectsysval = new string[systemID.Length];
                    string[] selectId = new string[systemID.Length];
                    for (int i = 0; i < systemID.Length; i++)
                    {
                        var a = systemID[i].Split('_');
                        selectsysval[i] = a[0];
                        selectId[i] = a[1];
                    }

                    if (systemID != null && systemID.Length > 0)
                    {
                        customersRepository.Add(customer, selectedPermissionLevel, selectId.ToArray(), selectsysval.ToArray());
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please must select any talk group.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please must select any permission level.");
                }
            }
            await GetTalkGroups();
            //IEnumerable<SelectListItem> selectList = from x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, true)
            //                                         select new SelectListItem
            //                                         {
            //                                             Value = x.Value,
            //                                             Text = x.Text + " ( " + x.Value + " )",
            //                                             Selected = x.Selected
            //                                         };
            //ViewBag.TalkGroups = new SelectList(selectList, "Value", "Text");
            List<SelectListItem> listPermisssion = new List<SelectListItem>();
            var permissions = adminRepository.GetPermissionLevel(UserInfo);
            foreach (var permiss in permissions)
            {
                //if (permiss.Permission1 == "Scan / listen only")
                listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString() });
            }
            if (Session["EditTalkGroup"] != null)
            {
                Session.Remove("EditTalkGroup");
            }
            if (Session["Selectedvalue"] != null)
            {
                Session.Remove("Selectedvalue");
            }
            ViewBag.PermissionLevelList = listPermisssion;
            return View();
        }

        // GET: Customers/Edit/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Edit(int? id)
        {
            var ids = (TempData["ID"]);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = customersRepository.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            await GetTalkGroups();

            List<SelectListItem> listPermisssion = new List<SelectListItem>();
            foreach (var talkGroup in customer.TalkGroupList)
            {
                foreach (var _talkGroup in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, true))
                {
                    if (_talkGroup.Value == talkGroup.Value)
                    {
                        _talkGroup.Selected = true;
                    }
                }
            }
            customer.SelectedValues = customer.TalkGroupList.Select(x => x.Value).ToList();


            IEnumerable<SelectListItem> selectList = from x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, true)
                                                     select new SelectListItem
                                                     {
                                                         Value = x.Value,
                                                         Text = x.Text + " ( " + x.Value + " )",
                                                         Selected = x.Selected
                                                     };

            var permissions = adminRepository.GetPermissionLevel(UserInfo);
            foreach (var permiss in permissions)
            {
                var selected = customer.PermissionLevelList.Where(x => x.Value == permiss.PermissionId.ToString()).Any();
                //  if (permiss.Permission1 == "Scan / listen only")
                listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = selected });
            }
            customer.PermissionLevelList = listPermisssion;
            ViewBag.TalkGroups = selectList;

            var EditTalkGroup = "";
            foreach (var val in customer.SelectedValues)
            {
                EditTalkGroup += val.ToString() + ",";

            }
            if (Session["Selectedvalue"] != null)
            {
                Session.Remove("Selectedvalue");
            }
            if (Session["EditTalkGroup"] != null)
            {
                Session.Remove("EditTalkGroup");
            }

            Session["EditTalkGroup"] = EditTalkGroup;
            TempData["ID"] = EditTalkGroup;

            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public async Task<ActionResult> Edit([Bind(Include = "CustomerId,CustomerName,StreetAddress,City,State,ZipCode,ContactName,ContactPhoneNumber,UsersCount,UnlimitedUsers,Active,SelectedValues,DefaultAutoplaySetting")] CustomersViewModels customer
            , string[] selectedPermissionLevel/*,params string[] selectedTalkGroup*/)
        {
            if (ModelState.IsValid)
            {
                if (selectedPermissionLevel != null && selectedPermissionLevel.Length > 0)
                {
                    //string val = "";
                    //if((Request.Form["FilterSystemID"]) != "" && (Request.Form["FilterSystemID"]) != null)
                    //{
                    //    val = Request.Form["FilterSystemID"];
                    //}
                    //else
                    //{
                    //    val = Request.Form["FilterSystemID1"];
                    //}
                    string val = Request.Form["FilterSystemID"];
                    string[] systemID = val.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    string[] selectsysval = new string[systemID.Length];
                    string[] selectId = new string[systemID.Length];
                    for (int i = 0; i < systemID.Length; i++)
                    {
                        var a = systemID[i].Split('_');
                        selectsysval[i] = a[0];
                        selectId[i] = a[1];
                    }

                    //string[] selectsysval = new string[customer.SelectedValues.Count];
                    //string[] selectId = new string[customer.SelectedValues.Count];
                    //for (int i = 0; i < customer.SelectedValues.Count; i++)
                    //{
                    //    var a = customer.SelectedValues[i].Split('_');
                    //    selectsysval[i] = a[0];
                    //    selectId[i] = a[1];
                    //}

                    if (systemID != null && systemID.Length > 0)
                    {
                        customersRepository.Update(customer, selectedPermissionLevel, selectId.ToArray(), selectsysval.ToArray());
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Please must select any talk group.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please must select any permission level.");
                }
            }
            await GetTalkGroups();
            IEnumerable<SelectListItem> selectList = from x in Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, true)
                                                     select new SelectListItem
                                                     {
                                                         Value = x.Value,
                                                         Text = x.Text + " ( " + x.Value + " )",
                                                         Selected = x.Selected
                                                     };
            ViewBag.TalkGroups = new SelectList(selectList, "Value", "Text");

            List<SelectListItem> listPermisssion = new List<SelectListItem>();
            var permissions = adminRepository.GetPermissionLevel(UserInfo);
            foreach (var permiss in permissions)
            {
                var selected = customer.PermissionLevelList.Where(x => x.Value == permiss.PermissionId.ToString()).Any();
                listPermisssion.Add(new SelectListItem() { Value = permiss.PermissionId.ToString(), Text = permiss.Permission1.ToString(), Selected = selected });
            }
            if (Session["Selectedvalue"] != null)
            {
                Session.Remove("Selectedvalue");
            }
            if (Session["EditTalkGroup"] != null)
            {
                Session.Remove("EditTalkGroup");
            }
            ViewBag.PermissionLevelList = listPermisssion;
            return View();
        }

        // GET: Customers/Delete/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = customersRepository.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult DeleteConfirmed(int id)
        {
            customersRepository.Delete(id);
            return RedirectToAction("Index");
        }

        // GET: Customers/Active/5
        [CustomAuthorize(Constant.SUPERADMIN_ROLENAME)]
        public ActionResult Active(int? id)
        {
            if (id != null)
            {
                customersRepository.Active(Convert.ToInt32(id));
            }
            return RedirectToAction("Index");
        }



        public async Task GetTalkGroups()
        {
            if (User != null)
            {
                Utils.CacheUtil.GetCustomerTalkgroupsSelectList(User, false).All(x => { x.Selected = false; return true; });
            }
        }
    }
}
