﻿var AudioPlayer = function (audioControl, baseURL, display, onplay, onPlayEnd, generateMp3Url, downloadMp3Url) {
    this.audio = audioControl;
    this.baseURL = baseURL;
    this.displayDiv = display;
    this.audio.player = this;
    this.isPlaying = true;
    this.playlistEnd = true;
    this.playlist = [];
    this.current = 0;
    this.filterId = 0;
    this.onPlay = onplay;
    this.onPlayEnd = onPlayEnd;
    this.generateMp3Url = generateMp3Url;
    this.downloadMp3Url = downloadMp3Url;
    this.resetCurrentTime = function () {
        try{
            this.audio.currentTime = 0;
        }
        catch (e) { }
    };
    this.play = function () {
        if (this.audio.src == undefined || this.audio.src == '')
            this.resetSrc();
        this.isPlaying = true;
        this.audio.play();
        if (this.onPlay) {
            this.displayDiv.style.color = '#000000';
            this.onPlay(this.playlist[this.current]);
        }
    };
    this.pause = function () {
        this.isPlaying = false;
        this.audio.pause();
        this.displayDiv.style.color = 'rgba(90, 90, 90, 0.27)';
    }
    this.continuePlay = function () {
        if (this.isPlaying == false)
            return;
       // console.log("this.playlistEnd:" + this.playlistEnd);
        if (this.playlistEnd) {
         //if (this.audio.ended) {
            this.next();
        }
        else {
            if (this.audio.paused)
                this.audio.play();
        }
    }
    this.addToPlaylist = function(items)
    {
        this.playlist = this.playlist.concat(items);
    }
    this.clearPlaylist = function()
    {
        this.playlist = [];
        this.current = 0;
    }
    this.stop = function () {
        try{
            this.isPlaying = false;
            this.audio.pause();
            this.resetCurrentTime();
        }catch(e){}
    }
    this.previous = function () {
        this.stop();
        var flag = this.changeCounter(false);
        if (flag == false) {
            console.log("Nothing to play");
            return;
        }
        this.resetSrc();
        this.play();
    }
    this.next = function () {
        this.audio.pause();
        this.resetCurrentTime();
        var flag = this.changeCounter(true);
        if (flag == false) {
            this.playlistEnded();
            return;
        }
        flag = this.resetSrc();
        if (flag == false) {
            this.playlistEnded();
            return;
        }
        this.playlistEnd = false;
        this.play();
    }
    this.playlistEnded = function () {
        this.playlistEnd = true;
        this.displayDiv.style.color = 'rgba(90, 90, 90, 0.27)';
        console.log("Nothing to play");
    }
    this.resetSrc = function () {
        if (this.playlist.length < this.current || this.playlist.length <= 0) {
            this.playlistEnd = true;
            return false;
        }

        if (this.filterId != 0)
        {
            if (this.playlist[this.current].TalkGroupID != this.filterId)
            {
                while (true)
                {
                    var flag = this.changeCounter(true);
                    if (flag == false) {
                        this.playlistEnded();
                        return false;
                    }
                    this.playlistEnd = false;
                    if (this.playlist[this.current].TalkGroupID == this.filterId)
                        break;
                }
            }
        } 

        this.audio.src = this.baseURL + "/radio/get_audio.cgi?chunk=" + this.playlist[this.current].TalkGroupItemId;
/*
        var player = this;
        var getUrl = downloadMp3Url;

        if (player.prevRequest)
            player.prevRequest.abort();

        player.prevRequest = $.ajax({
            url: this.generateMp3Url,
            type: "POST",
            async: false,
            data: { chunks: this.playlist[this.current].TalkGroupItemId, mergeTransmissions: false },
            dataType: "json",
            success: function (data) {
                $('#loading').hide();
                if (data.success) {
                    player.audio.src = getUrl + ".mp3?fName=" + data.fileName + "&mergedTransmissions=false"; //this.baseURL + "/radio/get_audio.cgi?chunk=" + this.playlist[this.current].TalkGroupItemId;
                    player.prevRequest = null;
/*                    if (iOS()) {
                        window.location = getBaseURL() + "/transmissions/" + data.fileName;
                    }
                    else {
                        window.location = getUrl + ".mp3?fName=" + data.fileName + "&mergedTransmissions=false";
                    }*
                }
            },
            error: function (e) {
                $('#loading').hide();
            }
        });*/
        if (this.displayDiv)
        {
            var item = this.playlist[this.current];
            var a = item.ChannelName.split(/[()]/);
            this.displayDiv.innerHTML = "<p>" + a[1] + "- " + a[2] + "</p><p>" + item.RadioDesc + "<br>" + this.getTimePart(item.TransStartUTC) + " (" + item.TransTimeDesc + ")</p>";
            this.displayDiv.style.color = '#000000';
        }
        return true;
    }
    this.filterTransmissions = function (filterId) {
        if (filterId < 0)
            this.filterId = this.playlist[this.current].TalkGroupID;
        else
            this.filterId = filterId;
    }
    this.enableAudio = function () { this.audio.play(); };
    this.audio.onended = function (e) {
        var currItem = this.player.playlist[this.player.current];
        var flag = this.player.changeCounter(true);
        if (this.player.onPlayEnd != null) {
            var stopPlay = this.player.onPlayEnd(currItem, flag ? this.player.playlist[this.player.current] : null);
            if (stopPlay == true)
                return;
        }
        if (flag == false) {
            this.player.playlistEnded();
            return;
        }
        this.player.resetSrc(); 
        this.player.resetCurrentTime();
        this.player.play();
    }
    this.changeCounter = function (inc) {
        if (inc)
            this.current++;
        else
            this.current--;
        if (this.current < 0) {
            this.current = 0;
            return false;
        }
        if (this.current > (this.playlist.length - 1)) {
            this.current = this.playlist.length - 1;
            return false;
        }
        return true;
    }

    this.getTimePart = function (date) {
        debugger
        d = new Date(date);

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        //if (/windows phone/i.test(userAgent)) {
        //    return "Windows Phone";
        //}
        //if (/android/i.test(userAgent)) {
        //    return "Android";
        //}
        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)
        {
            return date.split('T')[1];
        }
        else
        {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf('safari') != -1) {
                if (ua.indexOf('chrome') > -1) {
                   // Chrome
                } else {
                    return date.split('T')[1];
                    // Safari
                }
            }
        }




        h = (d.getHours() < 10 ? '0' : '') + d.getHours();
        m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
        s = (d.getSeconds() < 10 ? '0' : '') + d.getSeconds();
        return h + ':' + m + ':' + s;
    }

    this.audio.onplay = function () {
        var pl = this.player;
        if (pl.conectionerror)
        {
            pl.conectionerror = false;
            pl.displayDiv.innerHTML = pl.displayDivPrevHtml;
            pl.displayDiv.style.color = '#000000';
        }
    }
    this.audio.onerror = function (e, s) {
        // FIXME: Only handle errors related to loading
        var pl = this.player;

        switch (e.target.error.code) {
            case e.target.error.MEDIA_ERR_ABORTED:
            case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
            case e.target.error.MEDIA_ERR_DECODE:
                pl.next();
                return;
            case e.target.error.MEDIA_ERR_NETWORK:
                // retry
                break;
        }

        if (!pl.conectionerror)
        {
            pl.conectionerror = true;
            pl.displayDivPrevHtml = pl.displayDiv.innerHTML;
            pl.displayDiv.innerHTML = "Error in connection";
            pl.displayDiv.style.color = '#FF0000';
        }

        if (pl.timeoutvar)
            clearTimeout(pl.timeoutvar);
        // retry in 1 second
        pl.timeoutvar = setTimeout(function () { pl.audio.load(); pl.audio.play(); }, 1000);
    }
}