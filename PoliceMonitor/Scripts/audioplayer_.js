﻿var AudioPlayer = function (audioControl, display, onplay) {
    this.audio = audioControl;
    this.displayDiv = display;
    this.audio.player = this;
    this.isPlaying = true;
    this.playlistEnd = true;
    this.playlist = [];
    this.current = 0;
    this.filterId = 0;
    this.onPlay = onplay;
    this.play = function () {
        if (this.audio.src == undefined || this.audio.src == '')
            this.resetSrc();
        this.isPlaying = true;
        this.audio.play();
        if (this.onPlay)
            this.onPlay();
    };
    this.pause = function () {
        this.isPlaying = false;
        this.audio.pause();
    }
    this.continuePlay = function () {
        if (this.isPlaying == false)
            return;

        if (this.playlistEnd) {
            this.next();
        }
    }
    this.addToPlaylist = function(items)
    {
        this.playlist = this.playlist.concat(items);
    }
    this.stop = function () {
        this.isPlaying = false;
        this.audio.pause();
        this.audio.currentTime = 0;
    }
    this.previous = function () {
        this.stop();
        var flag = this.changeCounter(false);
        if (flag == false) {
            console.log("Nothing to play");
            return;
        }
        this.resetSrc();
        this.play();
    }
    this.next = function () {
        this.audio.pause();
        this.audio.currentTime = 0;
        var flag = this.changeCounter(true);
        if (flag == false) {
            this.playlistEnd = true;
            console.log("Nothing to play");
            return;
        }
        flag = this.resetSrc();
        if (flag == false) {
            this.playlistEnd = true;
            console.log("Nothing to play");
            return;
        }
        this.playlistEnd = false;
        this.play();
    }
    this.resetSrc = function () {
        if (this.playlist.length < this.current || this.playlist.length <= 0)
            return false;

        if (this.filterId != 0)
        {
            if (this.playlist[this.current].TalkGroupID != this.filterId)
            {
                while (true)
                {
                    var flag = this.changeCounter(true);
                    if (flag == false) {
                        console.log("Nothing to play");
                        return false;
                    }
                    this.playlistEnd = false;
                    if (this.playlist[this.current].TalkGroupID == this.filterId)
                        return true;
                }
            }
        }

        this.audio.src = "http://70.89.197.235:3389/radio/get_audio.cgi?chunk=" + this.playlist[this.current].TalkGroupItemId;
        if (this.displayDiv)
        {
            var item = this.playlist[this.current];
            var a = item.ChannelName.split(/[()]/);
            this.displayDiv.innerHTML = a[1] + " - " + a[2] + "<br\><br\>" + item.RadioDesc + "<br>" + this.getTimePart(item.TransStartUTC) + " (" + item.TransTimeDesc + ")" + item.TalkGroupItemId;
        }
        return true;
    }
    this.filterTransmissions = function (filterId) {
        this.filterId = filterId;
    }
    this.enableAudio = function () { this.audio.play(); };
    this.audio.onended = function (e) {alert("HIII")
        var flag = this.player.changeCounter(true);
        if (flag == false) {
            // playlist end
            this.player.playlistEnd = true;
            return;
        }
        this.player.resetSrc();
        this.player.play();
    }
    this.changeCounter = function (inc) {
        if (inc)
            this.current++;
        else
            this.current--;
        if (this.current < 0) {
            this.current = 0;
            return false;
        }
        if (this.current > (this.playlist.length - 1)) {
            this.current = this.playlist.length - 1;
            return false;
        }
        return true;
    }

    this.getTimePart = function (date) {
        d = new Date(date);
        h = (d.getHours() < 10 ? '0' : '') + d.getHours();
        m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
        s = (d.getSeconds() < 10 ? '0' : '') + d.getSeconds();
        return h + ':' + m + ':' + s;
    }
}