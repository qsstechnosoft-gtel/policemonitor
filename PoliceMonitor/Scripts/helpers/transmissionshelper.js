﻿var helper;
var TransmissionHelper = function (firstName, lastName, userName, baseUrl, markForReviewUrl, markListenUrl, generateMp3Url, downloadMp3Url, getVideoUrl, downloadVideoUrl, downloadMp4File, UploadFiles, PostFb,SearchCity,SearchEvent,DeleteDraft,SaveDraft,GetDraft, scrollabledivId) {
    this.x_row = 0;
    this.y_row = 0;
    this.selectedItem;
    this.player;
    this.firstName = firstName,
    this.lastName = lastName;
    this.userName = userName;
    this.baseUrl = baseUrl;
    this.markForReviewUrl = markForReviewUrl;
    this.markListenUrl = markListenUrl;
    this.generateMp3Url = generateMp3Url;
    this.downloadMp3Url = downloadMp3Url;
    this.getVideoUrl = getVideoUrl;
    this.downloadVideoUrl = downloadVideoUrl;
    this.downloadMp4File = downloadMp4File;
    this.scrollabledivId = scrollabledivId;
    this.UploadFiles = UploadFiles;
    this.PostFb = PostFb;
    this.FbPost = [];
    this.TempFbPost = [];   
    this.SearchCity = SearchCity;
    this.SearchEvent = SearchEvent;
    this.GetDraft = GetDraft;
    this.SaveDraft = SaveDraft;
    this.DeleteDraft = DeleteDraft;
    helper = this;

    this.initialize = function () {
        this.UpdateScrollerHeight();
        $(window).resize(helper.UpdateScrollerHeight);
        this.player = new AudioPlayer(document.getElementById("audioctrl"), this.baseUrl, { style: "" }, this.onPlay, this.onPlayEnd, this.generateMp3Url, this.downloadMp3Url);

        // Scroll to selected itemid or to the bottom if itemid not found.
        var itemId = getUrlParameter('itemid');
        if (!itemId) {
            itemId = $('#myTestID  tr').last().attr('id');
            if(!itemId)
            {
                itemId = $('#myTestID2  tr').last().attr('id');
            }
        }
        if (itemId)
        {
                var w = $(window).width();
                var yposition2 = $('#' + itemId).offset().top;
                if (w < 768) {
                    $('body').animate({ scrollTop: yposition2 - 300 }, 500);
                }
                else {
                    $('#' + this.scrollabledivId).animate({ scrollTop: $('#' + this.scrollabledivId).scrollTop() + yposition2 - 300 }, 500);
                }
        }
        
        //scroll model 
        
        $(".actiondialog").dialog({
            modal: true,
            autoOpen: false,
            title: "Select Action",
            width: 250,
            height: 200
        });

        $("#unchkAll").click(function () {
            $("#transmisisonTable tr").find("td:eq(0)").find("input[name=selectedTransmission]").prop("checked", false);
        });

        $("#markAllBetweenCheck").click(function () {
            var arr = [];
            $("#transmisisonTable tr").find('input[name=selectedTransmission]').each(function (i) {
                if ($(this).is(':checked')) {
                    var rowindex = $(this).closest('tr').index();
                    arr.push(rowindex);
                }
            });
            this.x_row = arr[0];
            this.y_row = arr[arr.length - 1];
            var ind = 0;
            for (ind = this.x_row; ind <= this.y_row; ind++) {
                $("#transmisisonTable tr:eq(" + ind + ")").find("td:eq(0)").find("input[name=selectedTransmission]").prop("checked", true);
            }
        }); 
        $('#autoplayTransmission').bootstrapToggle({
            on: 'Autoplay On',
            off: 'Autoplay Off'
        });
        $('#autoplayTransmissionpop').bootstrapToggle({
            on: 'Autoplay On',
            off: 'Autoplay Off'
        });
        $('.toggleBtn').click(function () {
            $('#searchBar').toggleClass('active');
        });

        //$('select[name="Select_download"]').change(function () {

        //    if (($("#transmisisonTable input[type='checkbox']:checked ").length) > 0) {
        //        if ($(this).val() == "MP3")
        //            helper.DownloadMP3();
        //        if ($(this).val() == "MP4")
        //            $("#movie_options_win").show();
        //    }
        //    else
        //        alert("Please select atleast one Transmission.");
        //    $('select[name="Select_download"]').val("Select");
        //});
        //$("#movie_options_win").draggable();

        //$("#DownlodeMovie").on("click", function () {
        //    $("#movie_options_win").show();
        //});
        //$("#ClosePop").on("click", function () {
        //    $("#movie_options_win").hide();
        //});

        $(".downloadtop").show();
        $("#DateTime").datepicker();
        $('#MP4').on('shown.bs.modal', function () {            
            $("#searchBar").removeClass("active");
        })
        $('#FinalPostFB').on('hidden.bs.modal', function () {
            
            //if ($("#PostFbPage").val() == "false") {
            //  var val= confirm("Do You want to remove All Data!!");
            //    if (val == true) {
            //        helper.FbPost = [];
            //        helper.deleteDrafts();
            //    }
            //}
            //localStorage.removeItem('SessionFb');
        })
        
        //$("iframPop").ready(function () {
        //    //alert("load");
        //    function show_popup() {
        //        helper.showTable();
        //    };
        //    window.setTimeout(show_popup, 5000);
        //    //$("#GripPop").prepend($('#iframPop').contents().find('#transmisisonTable')).delay(50000);
        //    //$("iframPop").hide();
        //});
        $('#PostFB').on('hidden.bs.modal', function () {
            var CheckBox = $("#PostTransmisisonTable").find('input[type="checkbox"]:checked');
            $.each(CheckBox, function (i) {
                $("." + $(this).val()).prop('checked', false);
                $("." + $(this).val()).closest("tr").attr("class", "");
            });
        })


        $("#CityID").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: helper.SearchCity,//"/test/Home/SearchCity",
                    type: "POST",
                    dataType: "json",
                    data: { Prefix: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return { label: item.cities, value: item.cities };
                        }))
                    }
                })
            },
            messages: {
                noResults: "", results: ""
            }
            //source: data
        });
        $("#EventType").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url:helper.SearchEvent, //"/test/Home/SearchEvent",
                    type: "POST",
                    dataType: "json",
                    data: { Prefix: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return { label: item.EventType, value: item.EventType };
                        }))
                    }
                })
            },
            messages: {
                noResults: "", results: ""
            }
            //source: data
        });
        $(".fileUpload").on('change', function () {
            //Get count of selected files
            var countFiles = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $(".image-holder");
            image_holder.empty();
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if (typeof (FileReader) != "undefined") {
                    //loop for each file selected for uploaded.
                    for (var i = 0; i < countFiles; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image"
                            }).appendTo(image_holder);
                        }
                        image_holder.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Please Select only Images!!");
                $(".fileUpload").val("");
            }
        });
    }
    this.onPlayEnd = function (currentItem, nextItem) {
        helper.setCss(currentItem.AudioBtn, true);
        if (nextItem != null) {
            helper.setCss(nextItem.AudioBtn, false);
        }
        if ($("#PostFB").css('display') != "block") {
            if ($("#autoplayTransmission").prop('checked') == false) {
                helper.player.stop();
                helper.player.current = 0;
                helper.player.playlist = [];
                if (nextItem != null) {
                    helper.setCss(nextItem.AudioBtn, true);
                }
                return true;
            }
        }
        if ($("#PostFB").css('display') == "block") {
            if ($("#autoplayTransmissionpop").prop('checked') == false) {
                helper.player.stop();
                helper.player.current = 0;
                helper.player.playlist = [];
                if (nextItem != null) {
                    helper.setCss(nextItem.AudioBtn, true);
                }
                return true;
            }
        }

    }
    this.setCss = function (myCtrl, isPlay) {
        if (isPlay) {
            myCtrl.value = "Play";
            $(myCtrl).removeClass("stop");
            $(myCtrl).addClass("play");
        }
        else {
            myCtrl.value = "Stop";
            $(myCtrl).removeClass("play");
            $(myCtrl).addClass("stop");
        }
    }
   
    this.playAudio = function (myCtrl) {
        debugger
        if (myCtrl.value == "Stop") {
            this.player.stop();
            this.setCss(myCtrl, true);
            return;
        }
        $(".playbtn").val("Play");
        $(".playbtn").removeClass("stop");
        $(".playbtn").addClass("play");
        this.setCss(myCtrl, false);
        ctrl = myCtrl;
        this.player.stop();
        this.player.current = 0;
        this.player.playlist = [];

        this.player.addToPlaylist({ TalkGroupItemId: myCtrl.attributes.audiosrc.value, TalkGroupID: myCtrl.attributes.talkgroupid.value, ChannelName: "", AudioBtn: myCtrl });

        if ($("#autoplayTransmission").prop('checked') == true) {
            var btns = $(myCtrl).closest('tr').nextAll('tr').find('.playbtn');
            btns.each(function (index, item) {
                helper.player.addToPlaylist({ TalkGroupItemId: item.attributes.audiosrc.value, TalkGroupID: item.attributes.talkgroupid.value, ChannelName: "", AudioBtn: item });
            });
        }
        if ($("#autoplayTransmissionpop").prop('checked') == true) {
            var btns = $(myCtrl).closest('tr').nextAll('tr').find('.playbtn');
            btns.each(function (index, item) {
                helper.player.addToPlaylist({ TalkGroupItemId: item.attributes.audiosrc.value, TalkGroupID: item.attributes.talkgroupid.value, ChannelName: "", AudioBtn: item });
            });            
        }        
        if (this.player.audio.src != "") {
            this.player.resetSrc();
        }
        this.player.play();
    }



    //rohit

    this.UplodeImages = function (callback) {
        
        var data = new FormData();
        var files = $(".fileUpload").get(0).files;
        if (files.length > 0) {
            data.append("HelpSectionImages", files[0]);
        }
        else {
            files = $("#fileUploadFB").get(0).files;
            if (files.length > 0) {
                data.append("HelpSectionImages", files[0]);
            }
        }
            $.ajax({
                url: this.UploadFiles,
                type: "POST",
                processData: false,
                contentType: false,
                data: data,
                success: function (response) {
                   
                    if (response == "Please Select Image") {
                        alert("Please select Image File.");
                        return;
                    }

                    if (response != "Error" && response != "No files selected.") {
                        callback(response);
                    }
                    else {
                        if (respond == "No files selected.")
                        { callback(""); }
                        else {
                            callback(null);
                        }
                    }

                },
                error: function (er) {
                    callback(null, er);
                }

            });
        
    }

    this.SaveDrafts = function () {
        var result = JSON.stringify(helper.FbPost);
        $.ajax({
            url: this.SaveDraft,
            //contentType: 'application/json; charset=utf-8',
            type: "POST",
            dataType: "json",
            data: { result: result },
            success: function (msg) {
                if (msg == "save") {
                    alert("save");
                }
                else {
                    alert("Oops something went Wrong!");
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });

    }
    this.Dateconvert = function (newdate) {
            var newDate = new Date(newdate);
            var sMonth = padValue(newDate.getMonth() + 1);
            var sDay = padValue(newDate.getDate());
            var sYear = newDate.getFullYear();
            var sHour = newDate.getHours();
            var sMinute = padValue(newDate.getMinutes());
            var sAMPM = "AM";
            
            var iHourCheck = parseInt(sHour);

            if (iHourCheck > 12) {
                sAMPM = "PM";
                sHour = iHourCheck - 12;
            }
           else if (iHourCheck == 12) {
                sAMPM = "PM";
            }
            else if (iHourCheck === 0) {
                sHour = "12";
            }
            sHour = padValue(sHour);

           return  sMonth + "/" + sDay + "/" + sYear + " " + sHour + ":" + sMinute + ":00 " + sAMPM;
            function padValue(value) {

                return (value < 10) ? "0" + value : value;
            }
    }

    this.AddTime = function () {
        var time = $("#timestamp").val();
        $('#loading').show();
        $('#loading').css("z-index", "2");
        $("#timestamp").val("")
        var transid = $("#TalkURl").val();
        var IframeSrc = $("#iframPop").attr("src").split("&")[1].replace("transStartUTC=", " ");
        IframeSrc= IframeSrc.replace('%2F', '/').replace('%20', ' ').replace('%3A', ':').replace('%2F', '/').replace('%20', ' ').replace('%3A', ':');

        var transid1 = transid.split("itemid=");
        var date = $("#" + transid1[1]).attr("data-date");
        //var newdate = new Date(IframeSrc).setMinutes(time);
        var newdate = new Date(IframeSrc).getTime() + parseInt(time) * 60 * 1000;
        //new Date("2/27/2017 11:46:43 AM").getTime() + 5 * 60 * 1000
        function padValue(value) {

            return (value < 10) ? "0" + value : value;

            }
            {
            var newDate = new Date(newdate);

            var sMonth = padValue(newDate.getMonth() + 1);
            var sDay = padValue(newDate.getDate());
            var sYear = newDate.getFullYear();
            var sHour = newDate.getHours();
            var sMinute = padValue(newDate.getMinutes());
            var sAMPM = "AM";

            var iHourCheck = parseInt(sHour);

            if (iHourCheck > 12) {
                sAMPM = "PM";
                sHour = iHourCheck - 12;
            }
            else if (iHourCheck === 0) {
                sHour = "12";
            }

            sHour = padValue(sHour);

            var final = sMonth + "/" + sDay + "/" + sYear + " " + sHour + ":" + sMinute + " " + sAMPM;
        }
        var Talkurl = transid.split("&");
        var url = Talkurl[0] + "&" + "transStartUTC=" + final + "&" + Talkurl[2];
        $("#iframPop").attr("src", url);

        $("#iframPop").ready(function () {
            function show_popup() {
                if ($('#iframPop').contents().find('#transmisisonTable').length == 0) {
                    window.setTimeout(show_popup, 2000);
                    return;
                }
                else {
                    var CheckBox = $("#GripPop").find('input[type="checkbox"]:checked');
                    var temp = "";
                    $.each(CheckBox, function (j) {
                        temp = temp + "" + this.value + ",";
                    });
                    var tempValue = temp.split(",");


                    $("#GripPop #transmisisonTable tr:last").after($('#iframPop').contents().find('#transmisisonTable > tbody tr:not(:first-child)'));
                    $("#iframPop").attr("scr", "");
                    $('#loading').hide();
                }
            };
            window.setTimeout(show_popup, 2000);
        });
        $("#TimeBefor").modal("hide");
    }

    this.deleteDrafts = function () {
        var result = JSON.stringify(helper.FbPost);
        $.ajax({
            url: this.DeleteDraft,
            //contentType: 'application/json; charset=utf-8',
            type: "POST",
            dataType: "json",
            data: { result: result },
            success: function (msg) {
                if (msg=="delete") {
                    helper.FbPost = [];
                }
                else {
                    alert("Oops something went Wrong!");
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });

    }
    this.GetDrafts = function () {
        var result = JSON.stringify(helper.FbPost);
        $.ajax({
            url: this.GetDraft,
            type: "POST",
            dataType: "json",
            data: { result: result },
            success: function (msg) {
                if (msg == "empty") {
                    helper.FbPost = [];
                }
                else if (msg.length >= 1) {
                    helper.FbPost = JSON.parse(msg);
                }
                else {
                    helper.FbPost = [];
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });

    }
    //this.showTable=function()
    //{
    //    var show = $('#iframPop').contents().find('#transmisisonTable');
    //    //alert($('#iframPop').contents().find('#transmisisonTable'));
    //    //$("#GripPop").prepend($('#iframPop').contents().find('#transmisisonTable'));
    //    $("#GripPop").prepend(show);
    //    $("iframPop").hide();
    //}
    this.PostFacebook = function (e) {
        $('.actiondialog').dialog('close');
        $('#loading').show();
        $('#loading').css("z-index", "2");
        id = this.selectedItem;
        var platform = $(id).text();
        var divObj = $(id).closest('div').children("a");
        var rowdata = $(id).closest('tr');
        var itemid = rowdata.attr("data-itemid");
        $("#PostFB").modal('show');
        var date = $("#" + itemid).attr("data-date").split(" ");
        var date1 = date[0].split("/");
        
        if (parseInt((date[0].split("/"))[0]) < 10)
        {
            date1[0] = "0" + date1[0];
        }
        if (parseInt((date[0].split("/"))[1]) < 10) {
            date1[1] = "0" + date1[1];
        }
        $("#DateTime").val(date1[0]+"/"+date1[1]+"/"+date1[2]);
        var time = date[1].split(":");
        if (date[2].toUpperCase() == 'PM'.toUpperCase()) {
            if (time[0] != '12')
            {
                time[0] = parseInt(time[0]) + 12;
            }

            //alert("rohit");
            
        }
        else if (date[2].toUpperCase() == 'AM'.toUpperCase())  {
            if (time[0] == '12') {
                time[0] = 00;
            }
        }
        var timeset = time[0] + ":" + time[1];
        if (time[0] < 10) {
            $("#TimeInp").val("0" + timeset)
        }
        else {
            $("#TimeInp").val(time[0] + ":" + time[1])
        }
        //$("#iframPop").show();
        var GotoTalkUrl = "";
        var talk = $(location).attr('href');
        GotoTalkUrl = talk.split("/");
        if (talk.split("GoToTalkgroup").length > 1) {
            var temp="";
            for (var i = 3; i < GotoTalkUrl.length; i++)
            {
                temp += "/" + GotoTalkUrl[i];
            }
            temp = temp.replace(/&quot;/g, '"').replace(/&#39;/g, "'").replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
            $("#TalkURl").val(temp);
            $('#iframPop').attr('src', document.location.origin + "" + temp);
        }
        else {
            var Talkurl = $("#" + itemid).children().eq(5).html().split('"');
            var talkurl1 = Talkurl[1];
            talkurl1 = talkurl1.replace(/&quot;/g, '"').replace(/&#39;/g, "'").replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
            $("#TalkURl").val(talkurl1);
            $('#iframPop').attr('src', document.location.origin + "" + talkurl1);
        }
        $("#UserName").val(this.userName);
        $("#Location").val("");
        $("#EventType").val("");
        $("#CityID").val("");
        $("#Description").val("");
        $("#Description").val("");
        $("#CategoriesID").val("");
        $("#APIAccessToken").val("");
        $("#MemberChange").attr('checked', false);
        $("#transmissionID").val("");
        $("#fileUpload").val("");
        $("#image-holder").empty();
        $("." + itemid).prop('checked', true);
        $("." + itemid).closest("tr").attr("class", "info")
        $("#AudioURL").val("http://rtc.radio1033.com/radio/get_audio.cgi?chunk=");
        $("#postBtn").empty();
        $("#postBtn").append("<button style='float:left;' onclick='javascript: helper.Cancel(this);  return false;' id='SaveFB' type='button' class='btn btn-danger'>Cancel</button><button onclick='javascript: helper.ViewPost(this); return false;' type='button' class='btn btn-default'>View Saved Entries</button> <button onclick='javascript: helper.selectMoreFiles(this); return false;' type='button' class='btn  btn-primary'>Save</button>");
        var Previous = helper.GetDrafts();
        if(Previous==undefined)
        {
            helper.FbPost = [];
        }
        $("#GripPop").find('#transmisisonTable').remove();
        $("#iframPop").hide();
        $("#iframPop").ready(function () {
            function show_popup() {
                if ($('#iframPop').contents().find('#transmisisonTable').length == 0) {
                    window.setTimeout(show_popup, 500);
                }
                else {
                    $("#GripPop").prepend($('#iframPop').contents().find('#transmisisonTable'));
                    $("#iframPop").attr("scr", "");
                    //$("#GripPop #" + itemid).children().find('input:checkbox').prop('checked', 'checked');
                    function show_up() {                       
                        if ($("#GripPop #" + itemid).length == 0) {
                            window.setTimeout(show_up, 500);
                            return;
                        }
                        $("#GripPop #transmisisonTable tr").removeClass("info");
                        $("#GripPop #" + itemid).addClass("info");
                        $("#GripPop #" + itemid).children().find('input:checkbox').prop('checked', 'checked');
                        $('#loading').hide();
                        $("#GripPop #" + itemid).get(0).scrollIntoView();
                    }; window.setTimeout(show_up, 500);
                }
            };
            window.setTimeout(show_popup, 500);
        });
    }

    this.Cancel = function (e) {

        var id = e.id;
        if (id == "FinalFB") {
            helper.ViewPost();
        }
        $("#PostFB").modal('hide');
    }

    this.unchkAllPop=function()
    {
        var CheckBox = $("#GripPop").find('input[type="checkbox"]:checked');
        $.each(CheckBox, function (i) {
            $("#transmisisonTable tr").find("td:eq(0)").find("input[name=selectedTransmission]").prop("checked", false);
            //$("." + $(this).val()).closest("tr").attr("class", "");
        });
    }

    this.markAllBetweenCheckPop=function ()
    {
        var arr = [];
        $("#GripPop").find('input[type="checkbox"]:checked').each(function (i) {
            if ($(this).is(':checked')) {
                var rowindex = $(this).closest('tr').index();
                arr.push(rowindex);
            }
        });
        this.x_row = arr[0];
        this.y_row = arr[arr.length - 1];
        var ind = 0;
        for (ind = this.x_row; ind <= this.y_row; ind++) {
            $("#GripPop tr:eq(" + ind + ")").find("td:eq(0)").find('input[type="checkbox"]').prop("checked", true);
        }

    }
    this.SacePostFB = function (e) {
        helper.UplodeImages(function (result, er) {
            var imageUrl = result;
            if (imageUrl != null) {
                for (var i = 0; i < helper.FbPost.length; i++) {
                    for (var j = 0; j < helper.FbPost[i].length; j++) {
                        if (helper.FbPost[i][j].name == "common_img") {
                            helper.FbPost[i][j].value = imageUrl;
                        }
                    }
                }
            }
            helper.SaveDrafts();
            $("#image-holderFB").empty();
            $("#fileUploadFB").val("");
            $("#FinalPostFB").modal('hide');
        });
    }


    this.FinalPostFB = function (e) {
       
        //if ($("#ImageUplode").is(':hidden')==false)
        {
            helper.UplodeImages(function (result, er) {
                var imageUrl = result;
                if (imageUrl != null) {
                    for (var i = 0; i < helper.FbPost.length; i++) {
                        for (var j = 0; j < helper.FbPost[i].length; j++) {
                            if (helper.FbPost[i][j].name == "common_img") {
                                helper.FbPost[i][j].value = imageUrl;
                            }
                        }
                    }
                }
                $(".fileUpload").val("");
                $("#image-holderFB").empty();
                helper.PostFacebookData();
            });
        }

        //else {
        //    helper.PostFacebookData();
        //}
    }
    this.DeletePostFB = function () {
          var val= confirm("Do you want to remove All Data?");
            if (val == true) {
                helper.FbPost = [];
                helper.deleteDrafts();
                $("#FinalPostFB").modal('hide');
            }
            else {
                $("#FinalPostFB").modal('hide');
            }
    }

    this.PostFacebookData = function () {
        $("#FinalPostFB").modal('hide');
        if (helper.FbPost.length == 0)
        {
            alert("Nothing to Post!!");
            return;
        }
        //rohit
            
        var json = JSON.stringify(this.FbPost);
        var result = [];
        var result1 = [];
        for (var item in this.FbPost) {
            var obj = {};
            for (var item1 in this.FbPost[item]) {
                obj[this.FbPost[item][item1].name] = this.FbPost[item][item1].value;
            }
            result.push(obj);
        }
        helper.FbPost = [];
        //this.deleteDrafts();
        //localStorage.removeItem('SessionFb');
        var data = JSON.stringify(result);
        $("#loading").show();
        var PostFBModel = result;
        $.ajax({
            url: this.PostFb,
            //contentType: 'application/json; charset=utf-8',
            type: "POST",
            dataType: "JSON",
            data: { result: result },// { EventType: 'A', Location: 'L' }],
            success: function (msg) {
                console.log(msg);
                //var Response=JSON.parse(msg);
                $("#loading").hide();
                if (msg == "success") {
                    alert("Posted succesfully!");
                    helper.deleteDrafts();
                }
                else {
                    alert(Response.message);
                    helper.deleteDrafts();
                }
                location.reload();
            },
            error: function (msg) {
                alert(msg);
                console.log(msg);
                location.reload();
            }
        });
    }
    this.PostFB = function (e) {
        //helper.UplodeImages();
        if ($("#CategoriesID").val() == "") {
            alert("Please add Some Category!!")
            return;
        }
       
        if (e != false)
            helper.SelectMore(e);
        //if (this.FbPost.length > 1)
        //{
        //    $("#ImageUplode").hide();
        //}
        //else {
        //    $("#ImageUplode").show();
        //}
        $("#fileUploadFB").val("");
       
    }
    this.UpFb = function (e) {
       
        var arrRemove = e.id;
        arrRemove = parseInt(arrRemove.replace("up_", ""));
        arrRemove = arrRemove - 1;
        if (arrRemove > 0) {

            var newArr = [];
            for (i = 0; i < this.FbPost.length; i++) {
                //if (i != arrRemove - 1)
                newArr.push(this.FbPost[i]);
            }
            newArr[arrRemove] = this.FbPost[arrRemove - 1];
            newArr[arrRemove - 1] = this.FbPost[arrRemove];
            this.FbPost = [];
            for (i = 0; i < newArr.length; i++) {
                this.FbPost.push(newArr[i]);
            }
            newArr = [];
            helper.ShowFBpost(false);
        }
        helper.SaveDrafts();
        //localStorage.setItem('SessionFb', JSON.stringify(this.FbPost));
    }


    this.DownFb = function (e) {
        var arrRemove = e.id;
        arrRemove = parseInt(arrRemove.replace("Down_", ""));
        arrRemove = arrRemove - 1;
        if (((this.FbPost.length) - 1) > arrRemove) {
            var newArr = [];
            for (i = 0; i < this.FbPost.length; i++) {
                newArr.push(this.FbPost[i]);
            }
            newArr[arrRemove] = this.FbPost[arrRemove + 1];
            newArr[arrRemove + 1] = this.FbPost[arrRemove];
            this.FbPost = [];
            for (i = 0; i < newArr.length; i++) {
                this.FbPost.push(newArr[i]);
            }
            newArr = [];
            helper.ShowFBpost(false);
        }
        helper.SaveDrafts();
        //localStorage.setItem('SessionFb', JSON.stringify(this.FbPost));
    }

    this.DeleteFB = function (e) {

        var val = confirm("Do You want to remove Post!!");
            if (val == false) {
                return;
            }
        e.closest('tr').remove();
        var arrRemove = e.id;
        arrRemove = parseInt(arrRemove.replace("Delete_", ""));
        var newArr = [];
        for (i = 0; i < this.FbPost.length; i++) {
            if (i != arrRemove - 1)
                newArr.push(this.FbPost[i]);
        }
        this.FbPost = [];
        for (i = 0; i < newArr.length; i++) {
            this.FbPost.push(newArr[i]);
        }
        newArr = [];
        $("#DivFB").empty();
        //var TableData = " <table class='table'> <thead><tr><th>#</th><th>DateTime</th><th>City</th><th>Location</th><th>Events</th><th>Description</th><th>Agency</th><th>UP</th><th>Down</th><th></th><th></th></tr></thead><tbody>";
        //for (i = 0; i < FbPost.length; i++) {
        //    TableData += " <tr><th scope='row'>" + (i + 1) + "</th>";
        //    for (j = 0; j < FbPost[i].length - 2; j++) {
        //        TableData += "<td>" + FbPost[i][j].value + "</td>";
        //    }
        if (helper.FbPost.length > 1) {
            var TableData = "<table class='table'><thead><tr><th>#</th><th>Date</th><th>Time</th><th>Events</th><th></th><th></th></tr></thead><tbody>";
        }
        else {
            var TableData = "<table class='table'><thead><tr><th>#</th><th>Date</th><th>Time</th><th>Events</th></thead><tbody>";
            $("#ImageUplode").show();
        }

        //var TableData = "<table class='table'><thead><tr><th>#</th><th>Date</th><th>time</th><th>Events</th><th>UP</th><th>Down</th><th></th><th></th></tr></thead><tbody>";
        for (i = 0; i < this.FbPost.length; i++) {
            TableData += " <tr><td scope='row'><span>S.No.</span>" + (i + 1) + "</td>";
            for (j = 0; j < this.FbPost[i].length - 2; j++) {
                //if (j == 0 || j == 1 || j == 4) {
                //    TableData += "<td>" + this.FbPost[i][j].value + "</td>";
                //}

                if (j == 0) {
                    TableData += "<td><span>Date</span>" + helper.FbPost[i][j].value + "</td>";
                }
                if (j == 1) {
                    TableData += "<td><span>Time</span>" + helper.FbPost[i][j].value + "</td>";
                }
                if (j == 4) {
                    TableData += "<td><span>Event</span>" + helper.FbPost[i][j].value + "</td>";
                }
            }
            TableData += "<td>" + helper.userName + "</td>";
            if (helper.FbPost.length > 1) {
                TableData += "<td><span></span><small onclick='javascript: helper.UpFb(this); return false;' style='margin-right: 10px;' class='arrowBtn' id='up_" + (i + 1) + "' ><i class='fa fa-arrow-up'></i></small>";
                TableData += "<small onclick='javascript: helper.DownFb(this); return false;' style='margin-right: 10px;' class='arrowBtn' id='Down_" + (i + 1) + "' ><i class='fa fa-arrow-down'></i></small>";
                TableData += "<button onclick='javascript: helper.EditFB(this); return false;' style='margin-right: 10px;'  class='btn mini btn-danger' id='edit_" + (i + 1) + "' >Edit</button>";
                TableData += "<button onclick='javascript: helper.DeleteFB(this); return false;' style='margin-right: 10px;' class='btn mini btn-danger' id='Delete_" + (i + 1) + "' >Delete</button></td></tr>";
            }
            else {
                TableData += "<td><span></span><button onclick='javascript: helper.EditFB(this); return false;' style='margin-right: 10px;'  class='btn mini btn-danger' id='edit_" + (i + 1) + "' >Edit</button>";
                TableData += "<button onclick='javascript: helper.DeleteFB(this); return false;' style='margin-right: 10px;' class='btn mini btn-danger' id='Delete_" + (i + 1) + "' >Delete</button></td></tr>";
            }
            
        }
        TableData += " </tbody></table>";
        helper.SaveDrafts();
        if (helper.FbPost.length == 0)
        {           
            $("#FinalPostFB").modal('hide');
            location.reload();
        }
        //localStorage.setItem('SessionFb', JSON.stringify(this.FbPost));
        $("#DivFB").append(TableData);
    }

    this.EditFB = function (e) {
        var arrEdit = e.id;
        arrEdit = parseInt(arrEdit.replace("edit_", ""));
        helper.TempFbPost = JSON.parse(JSON.stringify(helper.FbPost));
        $("#FinalPostFB").modal('hide');
        $("#PostFB").modal('show');
        $('#loading').show();
        $('#loading').css("z-index", "2");
        $("#DateTime").val(this.FbPost[arrEdit - 1][0].value);
        $("#TimeInp").val(this.FbPost[arrEdit - 1][1].value);
        $("#CityID").val(this.FbPost[arrEdit - 1][2].value);
        $("#Location").val(this.FbPost[arrEdit - 1][3].value);
        $("#EventType").val(this.FbPost[arrEdit - 1][4].value);
        $("#Description").val(this.FbPost[arrEdit - 1][5].value);
        $("#CategoriesID").val(this.FbPost[arrEdit - 1][6].value);
        $("#GripPop").find('#transmisisonTable').remove();
        $('#iframPop').attr("src", "");
        var transId="";
        var image = "";
        $(".image-holder").empty();
        $('#iframPop').hide();
        var count = 0;
        for (var i = 0; i < this.FbPost[arrEdit - 1].length; i++) {
            if (this.FbPost[arrEdit - 1][i].name == "imageurl") {
                if (this.FbPost[arrEdit - 1][i].value != "") {
                    var url = this.FbPost[arrEdit - 1][i].value;
                    image = "<img class='sealImage' style='height: 150px;width: 150px;' alt='Image of Seal' src='../" + url + "'>";
                }
            }
            if (this.FbPost[arrEdit - 1][i].name == "TalkURl") {
                $('#iframPop').attr("src", document.location.origin + "" + this.FbPost[arrEdit - 1][i].value)
            }
            if (this.FbPost[arrEdit - 1][i].name == "transid") {
               transId = (this.FbPost[arrEdit - 1][i].value).split(",");
            }
            if (this.FbPost[arrEdit - 1][i].name == "ismember") {
                count = 1;
                $('#MemberChange').prop("checked", (this.FbPost[arrEdit - 1][i].value));
            }
        }
        if (count == 0)
        {
            $('#MemberChange').prop("checked", false);
        }
        $("#iframPop").ready(function () {
            function show_popup() {
                if ($('#iframPop').contents().find('#transmisisonTable').length == 0) {
                    window.setTimeout(show_popup, 2000);
                }
                else {
                    $("#GripPop").prepend($('#iframPop').contents().find('#transmisisonTable'));
                    $("#iframPop").attr("scr", "");
                    //for (var i = 0; i < transId.length; i++) {
                    //    $("#GripPop #" + transId[i]).children().find('input:checkbox').prop('checked', 'checked');
                    //}
                    function show_up() {                        
                        if ($("#GripPop #" + transId[0]).length == 0) {
                            window.setTimeout(show_up, 500);
                            return;
                        }
                        for (var i = 0; i < transId.length; i++) {
                            $("#GripPop #" + transId[i]).children().find('input:checkbox').prop('checked', 'checked');
                        }
                        $('#loading').hide();
                        $("#GripPop #" + transId[0]).get(0).scrollIntoView();
                    } window.setTimeout(show_up, 500);
                }
            };
            window.setTimeout(show_popup, 2000);
        });
        $("#image-holder").append(image);
        $("#postBtn").empty();
        $("#postBtn").append("<button style='float:left;' onclick='javascript: helper.Cancel(this);  return false;' type='button' id='FinalFB' class='btn btn-danger'>Cancel</button><button onclick='javascript: helper.EditBtn(" + arrEdit + "); return false;' type='button'  class='btn btn-primary'>Save</button>");
    }
    //rohit
    this.EditBtn = function (e) {
        var arrEdit = e;
        helper.FbPost = JSON.parse(JSON.stringify(helper.TempFbPost));
        //helper.UplodeImages();
        this.FbPost[arrEdit - 1][0].value = $("#DateTime").val();
        this.FbPost[arrEdit - 1][1].value = $("#TimeInp").val();
        this.FbPost[arrEdit - 1][2].value = $("#CityID").val();
        this.FbPost[arrEdit - 1][3].value = $("#Location").val();
        this.FbPost[arrEdit - 1][4].value = $("#EventType").val();
        this.FbPost[arrEdit - 1][5].value = $("#Description").val();
        this.FbPost[arrEdit - 1][6].value = $("#CategoriesID").val();
        for (var j = 0; j < helper.FbPost[arrEdit - 1].length; j++) {
            if ($("#MemberChange").prop("checked") == false) {
                if (helper.FbPost[arrEdit - 1][j].name == "ismember") {
                    helper.FbPost[arrEdit - 1].splice(j, 1);
                }
            }
            else {
                if (helper.FbPost[arrEdit - 1][j].name == "ismember") {

                }
                else {
                    var obj = {};
                    obj.name = "ismember";
                    obj.value = "on"
                    this.FbPost[arrEdit - 1].push(obj);
                }
            }
        }

        helper.UplodeImages(function (result, er) {
            var imageUrl = result;
            if (imageUrl != null) {
                
                for (var j = 0; j < helper.FbPost[arrEdit - 1].length; j++) {
                    if (helper.FbPost[arrEdit - 1][j].name == "imageurl") {
                        helper.FbPost[arrEdit - 1][j].value = imageUrl;
                        }
                    }
                }
            
            $("#image-holderFB").empty();            
      





        var CheckBox = $("#GripPop").find('input[type="checkbox"]:checked');
        var count1 = 0; var count2 = 0;
        $.each(CheckBox, function (j) {
            var count = arrEdit - 1;
            for (var i = 0; i < helper.FbPost[count].length; i++) {
                var newUrl = "";
                if (helper.FbPost[count][i].name == "url") {
                    var newUrl = helper.FbPost[count][i].value;
                   
                    if (count1 == 0) {
                        var url = newUrl.split("=");
                        newUrl =  url[0]+"=";
                        newUrl = newUrl + "" + $(this).val();
                    }
                    else {
                        newUrl = newUrl + "," + $(this).val();
                    }
                    count1 += 1;

                    helper.FbPost[count][i].value = newUrl;
                }
                if (helper.FbPost[count][i].name == "transid") {
                    var newUrl = helper.FbPost[count][i].value;
                    if (count2 == 0) {
                        newUrl = "";
                        newUrl = newUrl + "" + $(this).val();
                    }
                    else {
                        newUrl = newUrl + "," + $(this).val();
                    }
                    count2 += 1;

                    helper.FbPost[count][i].value = newUrl;
                }
            }
        });
        $(".image-holder").empty();
        //FbPost[arrEdit - 1][6].value = $("#CategoriesID").val();
        $("#PostFB").modal('hide');
        helper.SaveDrafts();
        //localStorage.setItem('SessionFb', JSON.stringify(this.FbPost));
        helper.ShowFBpost(false);
        });
    }
    //rohit
    this.selectMoreFiles=function(e)
    {
        //debugger;
       
        if($("#CategoriesID").val() == "") 
        {
            alert("Please add Some Category!!")
            return;
        }
        //$("#TalkURl").val($("#iframPop").attr("src"));
            var str = $("#FormPost").serializeArray();
            helper.FbPost.push(str);
            helper.UplodeImages(function (result, er) {
                var imageUrl = result;
                if (imageUrl != null) {
                    for (var item in helper.FbPost[(helper.FbPost.length) - 1])
                    {
                        if (helper.FbPost[(helper.FbPost.length) - 1][item].name == "imageurl") {
                            helper.FbPost[(helper.FbPost.length) - 1][item].value = imageUrl;
                        }
                    }
                }
                var CheckBox = $("#GripPop").find('input[type="checkbox"]:checked');
                var count1 = 0; var count2 = 0;
                $.each(CheckBox, function (j) {
                    var count = helper.FbPost.length - 1;
                    for (var i = 0; i < helper.FbPost[count].length; i++) {
                        var newUrl = "";
                        if (helper.FbPost[count][i].name == "url") {
                            var newUrl = helper.FbPost[count][i].value;
                            if (count1 == 0)
                            {
                                newUrl = newUrl + "" + $(this).val();
                            }
                            else {
                                newUrl = newUrl + "," + $(this).val();
                            }
                            count1 += 1;

                            helper.FbPost[count][i].value = newUrl;
                        }
                        if (helper.FbPost[count][i].name == "transid") {
                            var newUrl = helper.FbPost[count][i].value;
                            if (count2 == 0) {
                                newUrl = newUrl + "" + $(this).val();
                            }
                            else {
                                newUrl = newUrl + "," + $(this).val();
                            }
                            count2 += 1;

                            helper.FbPost[count][i].value = newUrl;
                        }
                    }
                });
                helper.SaveDrafts();
                //localStorage.setItem('SessionFb', JSON.stringify(this.FbPost));
                $("#fileUpload").val("");
                $("#image-holder").empty();
                $("#PostFB").modal('hide');
            });
    }

    this.ViewPost=function(e)
    {
        if ($("#PostFbPage").val()=="true")
        {
            $('#loading').show();
            helper.GetDrafts();
            if (helper.FbPost.length >= 0) {
                function show_up() {
                    
                    
                    helper.ShowFBpost();

                }; window.setTimeout(show_up, 1500);
            }
        }
        else {
            helper.SaveDrafts();
            $("#PostFB").modal('hide');
            helper.ShowFBpost();
        }
        
        //localStorage.setItem('SessionFb', JSON.stringify(this.FbPost));
        
    }
    this.SelectMore = function (e)
    {
       
            var str = $("#FormPost").serializeArray();
            helper.FbPost.push(str);
            helper.UplodeImages(function (result, er) {
                var imageUrl = result;
                if (imageUrl != null) {
                    for (var item in helper.FbPost[(helper.FbPost.length) - 1]) {
                        if (helper.FbPost[(helper.FbPost.length) - 1][item].name == "imageurl") {
                            helper.FbPost[(helper.FbPost.length) - 1][item].value = imageUrl;
                        }
                    }
                } 
                var CheckBox = $("#GripPop").find("#transmisisonTable").find('input[type="checkbox"]:checked');
                $.each(CheckBox, function (j) {
                    var count = helper.FbPost.length - 1;
                    for (var i = 0; i < helper.FbPost[count].length; i++) {
                        if (helper.FbPost[count][i].name == "url") {
                            var newUrl = helper.FbPost[count][i].value;
                            newUrl = newUrl + "," + $(this).val();
                            helper.FbPost[count][i].value = newUrl;
                        }
                        if (helper.FbPost[count][i].name == "transid") {
                            var newUrl = helper.FbPost[count][i].value;
                            newUrl = newUrl + "," + $(this).val();
                            helper.FbPost[count][i].value = newUrl;
                        }
                    }
                });
                helper.SaveDrafts();
                //localStorage.setItem('SessionFb', JSON.stringify(this.FbPost));
                $("#PostFB").modal('hide');
                $("#fileUpload").val("");
                $("#image-holder").empty();
                helper.ShowFBpost();
            });
    }
    this.ShowFBpost=function()
    {
        $('#loading').hide();
        $("#FinalPostFB").modal('show');
        $("#DivFB").empty();
        if (helper.FbPost.length > 1) {
            var TableData = "<table class='table'><thead><tr><th>#</th><th>Date</th><th>Time</th><th>Events</th><th>Posted By</th><th></th><th></th> </tr></thead><tbody>";
        }
        else {
            var TableData = "<table class='table'><thead><tr><th>#</th><th>Date</th><th>Time</th><th>Events</th><th>Posted By</th></thead><tbody>";
        } var image = "";
        for (i = 0; i < helper.FbPost.length; i++) {
            TableData += " <tr><td scope='row'><span>S.No.</span>" + (i + 1) + "</td>";
            for (j = 0; j < helper.FbPost[i].length; j++) {
                //if (j == 0 || j == 1 || j == 4) {
                //    TableData += "<td>" + helper.FbPost[i][j].value + "</td>";
                //}


                if (j == 0) {
                    TableData += "<td><span>Date</span>" + helper.FbPost[i][j].value + "</td>";
                }
                if (j == 1) {
                    TableData += "<td><span>Time</span>" + helper.FbPost[i][j].value + "</td>";
                }
                if (j == 4) {
                    TableData += "<td><span>Event</span>" + helper.FbPost[i][j].value + "</td>";
                }
                if (helper.FbPost[i][j].name == "common_img") {
                    if (helper.FbPost[i][j].value != "") {
                       if (image == "") {
                            image = helper.FbPost[i][j].value;
                        }
                    }
                }
                //if (helper.FbPost[i][j].name == "UserName") {
                //    if (helper.FbPost[i][j].value != "") {
                //        TableData += "<td><span>Posted By</span>" + helper.userName + "</td>";
                //    }
                //}
                
            }
            TableData += "<td><span>Posted By</span>" + helper.userName + "</td>";
            if (helper.FbPost.length >1) {
                TableData += "<td><span></span><small onclick='javascript: helper.UpFb(this); return false;' style='margin-right: 10px;' class='arrowBtn' id='up_" + (i + 1) + "' ><i class='fa fa-arrow-up'></i></small>";
                TableData += "<small onclick='javascript: helper.DownFb(this); return false;' style='margin-right: 10px;' class='arrowBtn' id='Down_" + (i + 1) + "' ><i class='fa fa-arrow-down'></i></small>";
                TableData += "<button onclick='javascript: helper.EditFB(this); return false;' style='margin-right: 10px;'  class='btn mini btn-danger' id='edit_" + (i + 1) + "' >Edit</button>";
                TableData += "<button onclick='javascript: helper.DeleteFB(this); return false;' style='margin-right: 10px;'  class='btn mini btn-danger' id='Delete_" + (i + 1) + "' >Delete</button></td></tr>";
            }
            else {
                TableData += "<td><span></span><button onclick='javascript: helper.EditFB(this); return false;' style='margin-right: 10px;' class='btn mini btn-danger' id='edit_" + (i + 1) + "' >Edit</button>";
                TableData += "<button onclick='javascript: helper.DeleteFB(this); return false;' style='margin-right: 10px;' class='btn mini btn-danger' id='Delete_" + (i + 1) + "' >Delete</button></td></tr>";
            }
            
        }
        
        TableData += " </tbody></table>";
        $("#DivFB").append(TableData);
        $("#fileUpload").val("");
        $(".image-holder").empty();
        if (image != "") {
            $("#image-holderFB").append("<img class='sealImage' style='height: 150px;width: 150px;' alt='Image of Seal' src='../" + image + "'>")
        }
        //helper.ShowFBpost();
    }

    this.DownloadChecked = function (id) {
        $('.actiondialog').dialog('close');
        id = this.selectedItem;
        var platform = $(id).text();
        var divObj = $(id).closest('div').children("a");
        var rowdata = $(id).closest('tr');
        var itemid = rowdata.attr("data-itemid");
        this.DownloadMP3(itemid);
    }
    this.showMenu = function (elm) {
        if (!$("#PostFB").is(':hidden')) {
            $("#FBli").hide();
        }
        else {
            $("#FBli").show();
        }
        this.selectedItem = elm;
        $('.actiondialog').dialog('open');
        return false;
    }
    this.onPlay = function (item) {
        if (!isNullOrBlank(this.firstName) && !isNullOrBlank(this.lastName)) {
            helper.MarkListen(item.AudioBtn, true, helper.firstName + ' ' + helper.lastName);
        } else {
            helper.MarkListen(item.AudioBtn, true, helper.userName);
        }
        setTimeout(function () {
            var rowId = $(item.AudioBtn).closest('tr').attr('id');
            var yposition = $('#' + rowId).offset().top;
            var w = $(window).width();
            if (w < 768 ) {

                if ($("#PostFB").css('display') != "block")
                {
                    if ($("#autoplayTransmission").prop('checked') == true) {
                        $('body').animate({ scrollTop: yposition - 200 }, 500);
                    }
                    $('#' + helper.scrollabledivId + ' tr').removeClass('active');
                    $('#' + helper.scrollabledivId + ' tr#' + rowId).addClass('active');
                }

                if ($("#PostFB").css('display') == "block") 
                {
                    if ($("#autoplayTransmissionpop").prop('checked') == true) {
                        $('body').animate({ scrollTop: yposition - 200 }, 500);
                    }
                    $('#GripPop' + ' tr').removeClass('active');
                    $('#GripPop' + ' tr#' + rowId).addClass('active');
                }


            }
            else {
                if ($("#PostFB").css('display') != "block")
                {
                    if ($("#autoplayTransmission").prop('checked') == true) {
                        $('#' + helper.scrollabledivId + '').animate({ scrollTop: $('#' + helper.scrollabledivId).scrollTop() + yposition - 300 }, 500);
                    }
                    $('#' + helper.scrollabledivId + ' tr').removeClass('active');
                    $('#' + helper.scrollabledivId + ' tr#' + rowId).addClass('active');
                }
                if ($("#PostFB").css('display') == "block") {
                    if ($("#autoplayTransmission").prop('checked') == true) {
                        $('#GripPop').animate({ scrollTop: $('#GripPop').scrollTop() + yposition - 300 }, 500);
                    }
                    $('#GripPop' + ' tr').removeClass('active');
                    $('#GripPop' + ' tr#' + rowId).addClass('active');
                }
            }
        }, 2);
    }
    this.ChangeMarkForReview = function (id) {
        var platform = $(id).text();
        id = this.selectedItem;
        $('.actiondialog').dialog('close');
        var divObj = $(id).closest('div').children("a");
        var rowdata = $(id).closest('tr');
        var itemid = rowdata.attr("data-itemid");
        var transstartmsec = rowdata.attr("data-transstartmsec");
        var translengthmsec = rowdata.attr("data-translengthmsec");
        var radioid = rowdata.attr("data-radioid");
        var talkgroupid = rowdata.attr("data-talkgroupid");
        var radiodesc1 = rowdata.attr("data-radiodesc1");
        var radiodesc2 = rowdata.attr("data-radiodesc2");
        var radiodesc = rowdata.attr("data-radiodesc");
        var transstartutc = rowdata.attr("data-transstartutc");
        var transtimedesc = rowdata.attr("data-transtimedesc");
        var marklisten = rowdata.attr("data-marklisten");
        var reviewstatus = null;

        if (platform == 'Post audio') {
            var reviewstatus = 1;
        }
        else if (platform == 'Post to FB') {
            var reviewstatus = 2;
        }
        else if (platform == 'Follow Up') {
            var reviewstatus = 3;
        }
        else if (platform == 'Clear') {
            var reviewstatus = null;
        }

        var sendInfo = {
            ID: itemid,
            TransStartMSec: transstartmsec,
            TransLengthMSec: translengthmsec,
            RadioID: radioid,
            TalkGroupID: talkgroupid,
            TransStartUTC: transstartutc,
            TransTimeDesc: transtimedesc,
            RadioDesc: radiodesc,
            ReviewStatus: reviewstatus,
            MarkListen: marklisten
        };
        $.ajax({
            type: "POST",
            url: this.markForReviewUrl,
            dataType: "json",
            data: sendInfo,
            success: function (msg) {
                if (platform == 'Post audio') {
                    divObj.html(platform + "<span class='caret'></span>");
                    divObj.attr('style', 'color:red; font-size:12px;');
                }
                else if (platform == 'Post to FB') {
                    divObj.html(platform + "<span class='caret'></span>");
                    divObj.attr('style', 'color:green; font-size:12px;');
                }
                else if (platform == 'Follow Up') {
                    divObj.html(platform + "<span class='caret'></span>");
                    divObj.attr('style', 'color:orange; font-size:12px;');
                }
                else if (platform == 'Clear') {
                    divObj.html('Action' + "<span class='caret'></span>");
                    divObj.attr('style', 'font-size:12px;');
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }
    this.MarkListen = function (id, flag, currentuser) {
        if (isNullOrBlank(id))
            id = this.selectedItem;
        $('.actiondialog').dialog('close');
        var rowdata = $(id).closest('tr');
        var tddata = $(rowdata).find('td:last');
        var itemid = rowdata.attr("data-itemid");
        var transstartmsec = rowdata.attr("data-transstartmsec");
        var translengthmsec = rowdata.attr("data-translengthmsec");
        var radioid = rowdata.attr("data-radioid");
        var talkgroupid = rowdata.attr("data-talkgroupid");
        var radiodesc1 = rowdata.attr("data-radiodesc1");
        var radiodesc2 = rowdata.attr("data-radiodesc2");
        var radiodesc = rowdata.attr("data-radiodesc");
        var transstartutc = rowdata.attr("data-transstartutc");
        var transtimedesc = rowdata.attr("data-transtimedesc");
        var reviewstatus = rowdata.attr("data-reviewstatus");
        var username = rowdata.attr("data-username");
        if (username.indexOf(currentuser) < 0) {
            username = username != '' ? username + "," + currentuser : currentuser;
        }
        var sendInfo = {
            ID: itemid,
            TransStartMSec: transstartmsec,
            TransLengthMSec: translengthmsec,
            RadioID: radioid,
            TalkGroupID: talkgroupid,
            TransStartUTC: transstartutc,
            TransTimeDesc: transtimedesc,
            RadioDesc: radiodesc,
            ReviewStatus: reviewstatus,
            MarkListen: flag
        };
        
        $.ajax({
            type: "POST",
            url: this.markListenUrl,
            dataType: "json",
            data: sendInfo,
            success: function (msg) {
                if (flag == true) {
                    tddata.empty();
                    var contents = "<span title='" + username + "' >Listened</span>";
                    tddata.html(contents);
                }
                else {
                    tddata.empty();
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }

    //this.DownloadMP3 = function () {
    //    $("#MP4").show();
    //}
    this.DownloadMP3 = function (chunk) {
        if (chunk == undefined || chunk == null) {
            if (($("#transmisisonTable input[type='checkbox']:checked ").length) == 0) {
                alert("Please select atleast one Transmission.");
                return;
            }
        }
        $('#loading').show();
        var sendInfo;
        var merged = false;
        if (chunk) {
            sendInfo = chunk;
        }
        else {
            var transmission = [];
            $.each($("input[name='selectedTransmission']:checked"), function () {
                transmission.push($(this).val());
            });
            sendInfo = transmission.join(",");
            merged = true;
        }
        var getUrl = this.downloadMp3Url;
        $.ajax({
            url: this.generateMp3Url,
            type: "POST",
            data: { chunks: sendInfo, mergeTransmissions: merged },
            dataType: "json",
            success: function (data) {
                $('#loading').hide();
                if (data.success) {
                    if (iOS()) {
                        window.location = getBaseURL() + "/transmissions/" + data.fileName;
                    }
                    else {
                        window.location = getUrl + ".mp3?fName=" + data.fileName + "&mergedTransmissions=false";
                    }
                }
            },
            error: function (e) {
                $('#loading').hide();
            }
        });
    }

    this.UpdateScrollerHeight = function () {
        // device detection, if mobile do not resize
        if (isMobile())
            return;
        var h = $(window).height() - 275;
        $('#' + helper.scrollabledivId).height((h < 300) ? 300 : h);
    }
    this.MovieOptionsSet = function (event) {
        debugger
        if (($("#transmisisonTable input[type='checkbox']:checked ").length) == 0) {
            alert("Please select atleast one Transmission.");
            return;
        }
        var form_elem = event.target;
        while (form_elem && form_elem.nodeName != "FORM") {
            form_elem = form_elem.parentNode;
        }
        var talkgroupselect = $("#transmisisonTable input[type='checkbox']:checked ");
        var selectedtalk = "";
        $.each(talkgroupselect, function (i) {
            if (($("#transmisisonTable input[type='checkbox']:checked ").length) > i + 1)
                selectedtalk += $(this).val() + ",";
            else
                selectedtalk += $(this).val();
        })
        var qs = helper.BuildFormQS(form_elem);
        var ts = "radio/get_audio.cgi?" + qs.replace("%23", "") + "&fmt=mp4&chunk=" + selectedtalk;//     .replace("chunk=", "chunk=" + selectedtalk);
        helper.getCall(ts);
    }
    this.getCall = function (ts) {
        debugger
        $("#loading").show();
        $("#loading").css("z-index", 120);
        $.ajax({
            url: helper.getVideoUrl,
            type: "POST",
            dataType: "text",
            data: { url: ts },
            success: function (respItem) {
                {
                    helper.getVideo();
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }
    this.getVideo = function () {
        $.ajax({
            url: this.downloadVideoUrl,
            type: "POST",
            dataType: "text",
            data: {},
            success: function (data) {
               
                $("#loadershow").modal('hide');
                $("#MP4").hide();
                if (data == "fail") {
                    setTimeout(helper.getVideo(), 1000);
                }
                else {
                    var url = data.split("__");
                    $('#loading').hide();
                    {
                        if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
                            window.location = url[1];
                        }
                        else {
                            window.location = helper.downloadMp4File + "?fName=" + url[0] + "&mergedTransmissions=false";
                        }
                    }
                }
            }
        });
    }
    this.BuildFormQS = function (x) {
        if (x.name && x.value !== undefined) {
            return encodeURIComponent(x.name) + '=' + encodeURIComponent(x.value);
        }
        var y = '';
        var i = 0;
        for (; i < x.childElementCount; ++i) {
            var z = helper.BuildFormQS(x.children[i]);
            if (z != '') {
                if (y != '') {
                    y += '&';
                }
                y += z;
            }
        }
        return y;
    }

    this.initialize();
}