﻿function intermediateImg() {
    var elements = $('ul input[type="checkbox"]');
    var chkElements = $('ul input[type="checkbox"]:checked');
    if (elements.length == chkElements.length) {
        if ($('#toggleAll')[0] != undefined) {
            $('#toggleAll')[0].hidden = false;
            $('#toggleAll')[0].checked = true;
        }
        if ($('#middleImg')[0] != undefined) {
            $('#middleImg')[0].hidden = true;
        }
    }
    else if (chkElements.length == 0) {
        if ($('#toggleAll')[0] != undefined) {
            $('#toggleAll')[0].hidden = false;
            $('#toggleAll')[0].checked = false;
        }
        if ($('#middleImg')[0] != undefined) {
            $('#middleImg')[0].hidden = true;
        }
    }
    else {
        if ($('#toggleAll')[0] != undefined) {
            $('#toggleAll')[0].hidden = true;
        }
        if ($('#middleImg')[0]) {
            $('#middleImg')[0].hidden = false;
        }
    }
}

function searchNow()
{
    var flag = false;
    if ($('input[name=radioFilter]:checked').val() == 'hoursDuration')
    {
        var startHr = $('#StartHour').val();
        var endHr = $('#EndHour').val();
        var minTrans = $('#MinTranSeconds').val();
        if (startHr < 1 ) {
            $('.danger').html('Hours Ago should be greater than 0 ')
            flag = true;
        }
         if (endHr < 0 ) {
             $('.danger').html('Hours Duration should be greater than 0')
            flag = true;
        }
        else if (minTrans < 0) {
            $('.danger').html('Min Transmission time should be a positive number')
            flag = true;
        }
    }
    else
    {
        var d1 = $("#datetimepicker1").data("DateTimePicker").date();
        var d2 = $("#datetimepicker2").data("DateTimePicker").date();
        var startTime = $("#StartTime").val();
        var endTime = $("#EndTime").val();
        var minTrans = $('#MinTranSeconds').val();

        if(d1 == null)
        {
            $('.danger').html('Please enter start date')
            flag = true;
        }
        else if (d2 == null) {
            $('.danger').html('Please enter end date')
            flag = true;
        }
        else if (startTime == '') {
            $('.danger').html('Please enter start time')
            flag = true;
        }
        else if (endTime == '') {
            $('.danger').html('Please enter end time')
            flag = true;
        }
        else if (minTrans < 0) {
                $('.danger').html('Min Transmission time should be a positive number')
                flag = true;
        }
    }

    if(flag == false)
    {

        var formId = $('#FormId').serializeArray();
        var fid = [];
        //ASDASDA
        for (var i = 0; i < formId.length; i++) {
            if (formId[i].name == "SelectedTalkGroups") {
                if ($("#" + formId[i].value).prop("checked") == true) {
                    var sysid = ($("#" + formId[i].value).attr("sysid"));                  
                }
                fid.push(sysid);
            }
        }
        $("#HfSystemID").val(fid);


        $('#FormId').submit();
    }
}

function viewTransmissions()
{
    var minTrans = $('#MinTranSeconds').val();
    if ($('[name="radioFilter"]')[0].checked)
    {
        // Hour Duration
        var startHour = $('#StartHour').val();
        var endHour = $('#EndHour').val();
        if(!$.isNumeric(startHour))
        {
            $('.messageDialog').dialog('open');
        }
    }
    else
    {
        // Date time range
    }
}

function resetFields(){
    $('input[type="checkbox"]').prop('hidden', false);
    $('input[type="checkbox"]').prop('checked', '');
    $("[id=middleTGImg]").prop('hidden', true);
    $("[id=middleImg]").prop('hidden', true);
}

function SaveView(id) {
    if (id > -2) {//Edit
        var selText = $("#SelectedFavouriteID option:selected").text();
        var selID = $("#SelectedFavouriteID").val()

        if(selID == '')
        {
            alert("Please select favourite serach item." + selID);
            return;
        }

        $('#viewName').val(selText.trim());
        $('#hdnFavouriteID').val(selID);
        if (id == $("#SelectedFavouriteID").val()) {
            $('#isPrimary').prop('checked', true);
        }

        $('.savedialog').dialog('open');
    }
    else {//Save
        $('#isPrimary').prop('checked', "");
        $('#viewName').val("");
        $('#hdnFavouriteID').val("0");
        $('.savedialog').dialog('open');
    }
}

var flag = false;
function UpdateScrollerHeight()
{
    // device detection, if mobile do not resize
    if (isMobile())
        return;
    var diff = 427;
    if(flag)
    {
        diff = 215;
    }
    var h = $(window).height() - diff;
    $(".scroller").height((h < 300) ? 300 : h);
}

$(function () {
    if (window.radioPage)
        return;
    // set height of select talkgroup scroll
    UpdateScrollerHeight();
    $(window).resize(UpdateScrollerHeight);
    $(".savedialog").dialog({
        modal: true,
        autoOpen: false,
        title: "Save Search List",
        width: 390,
        height: 240
    });

    $(".confirmdialog").dialog({
        modal: true,
        autoOpen: false,
        title: "Confirm",
        width: 400,
        height: 150
    });

    $(".confirmView").click(function () {
        var id = $('#SelectedFavouriteID').val();
        if (id == "" || id <= 0) {
            alert("Please select favourite search item.");
            $('.confirmdialog').dialog("close");
            return;
        }

        $('.confirmdialog').dialog('open');
    });

    $('.Cancel').click(function () {
        $('.confirmdialog').dialog("close");
    });

    $(".hidefilters").click(function () {
        if (!flag) {
            $(".hidefilters").html("+ Show Filters");
        }
        else
        {
            $(".hidefilters").html("- Hide Filters");
        }
        flag = !flag;
        UpdateScrollerHeight();

        $(".filters").toggle();
    });

    var st = $('#StartDatetime').val();
    if (st && st != '')
        $('#StartDatetime').val(moment(moment(st, 'MM/DD/YYYY')).format('MM/DD/YY'))

    st = $('#EndDatetime').val();
    if (st && st != '')
        $('#EndDatetime').val(moment(moment(st, 'MM/DD/YYYY')).format('MM/DD/YY'))

    $('#datetimepicker1').datetimepicker({ format: 'MM/DD/YY' });
    $('#datetimepicker2').datetimepicker({ format: 'MM/DD/YY' });
    $("#SelectLeft option").prop("selected", false);

    var startTime = $("#StartTime").val();
    $.mask.rules.H = /([0-1][0-9]|2[0-3])/;  // 24 hours
    $.mask.masks.time = 'H:59';
    $("#StartTime").setMask('time');
    $("#StartTime").val(startTime);

    $("#StartTime").setMask("29:59")
    .keypress(function () {
        var currentMask = $(this).data('mask').mask;
        var newMask = $(this).val().match(/^2.*/) ? "23:59" : "29:59";
        if (newMask != currentMask) {
            $(this).setMask(newMask);
        }
    });

    var endTime = $("#EndTime").val();
    $("#EndTime").setMask('time');
    $("#EndTime").val(endTime);

    $("#EndTime").setMask("29:59")
    .keypress(function () {
        var currentMask = $(this).data('mask').mask;
        var newMask = $(this).val().match(/^2.*/) ? "23:59" : "29:59";
        if (newMask != currentMask) {
            $(this).setMask(newMask);
        }
    });

    $("#MoveRight,#MoveLeft").click(function (event) {

        var id = $(event.target).attr("id");

        var selectFrom = id == "MoveRight" ? "#SelectLeft" : "#SelectRight";
        var moveTo = id == "MoveRight" ? "#SelectRight" : "#SelectLeft";
        var selectedItems = $(selectFrom + " :selected").toArray();

        $(moveTo).append(selectedItems);

        selectedItems.remove;
        $("#SelectRight option").prop("selected", true);
        $("#SelectLeft option").prop("selected", false);
    });

    if ($("input[name='radioFilter']:checked").val() == 'hoursDuration') {
        $("#hoursDuration").show();
        $("#dateTimeRange").hide();
    }
    else {
        $("#dateTimeRange").show();
        $("#hoursDuration").hide();
    }

    $('input[name="radioFilter"]').click(function () {
        if ($(this).val() == 'hoursDuration') {
            $("#hoursDuration").show();
            $("#dateTimeRange").hide();
        }
        else {
            $("#dateTimeRange").show();
            $("#hoursDuration").hide();
        }
    });
});
