﻿(function ($) {
    function Tree() {
        var $this = this;

        function treeNodeClick() {
            $(document).on('click', '#toggleAll', function () {
                // $(this).closest('div').find('ul input[type="checkbox"]').prop('checked', $(this).is(':checked'));
                $(this).closest('.tree').find('input[type="checkbox"]').prop('checked', $(this).is(':checked'));
            });

            $(document).on('click', '.tree li a input[type="checkbox"]', function () {
                $(this).closest('li').find('ul input[type="checkbox"]').prop('checked', $(this).is(':checked'));
                intermediateImg();
            }).on('click', '.node-item', function () {
                var parentNode = $(this).parents('.tree ul');
                if ($(this).is(':checked')) {
                    var chkElements = parentNode.find('ul input[type="checkbox"]:checked');
                    var elements = parentNode.find('ul input[type="checkbox"]');
                    var middleImage = parentNode.find('#middleTGImg');
                    if (elements.length == chkElements.length) {
                        middleImage[0].hidden = true;
                        parentNode.find('li a .parent').prop('hidden', false);
                        parentNode.find('li a .parent').prop('checked', true);
                    }
                    else if (chkElements.length == 0) {
                        middleImage[0].hidden = false;
                        parentNode.find('li a .parent').prop('checked', false);
                    }
                    else{
                        middleImage[0].hidden = false;
                        parentNode.find('li a .parent').prop('hidden', true);
                    }


                } else {
                    var elements = parentNode.find('ul input[type="checkbox"]:checked');
                    var middleImage = parentNode.find('#middleTGImg');
                    if (elements.length == 0) {
                        middleImage[0].hidden = true;
                        parentNode.find('li a .parent').prop('hidden', false);
                        parentNode.find('li a .parent').prop('checked', false);
                    }
                    else {
                        middleImage[0].hidden = false;
                        parentNode.find('li a .parent').prop('hidden', true);
                    }
                   
                }
                intermediateImg();
            }).on('click', '#middleTGImg', function () {
                var parentNode = $(this).parents('.tree ul');
                var middleImage = parentNode.find('#middleTGImg');
                middleImage[0].hidden = true;
                parentNode.find('li a .parent').prop('hidden', false);
                parentNode.find('li a .parent').prop('checked', true);
                parentNode.closest('ul').find('ul input[type="checkbox"]').prop('checked', true);

                var total = $(this).closest('.tree').find('input[type="checkbox"]');
                var totalChecked = $(this).closest('.tree').find('input[type="checkbox"]:checked');
                if ($('#toggleAll')[0] != undefined) {
                    if (total.length == totalChecked.length && $('#toggleAll')[0].hidden) {
                        $('#toggleAll')[0].hidden = false;
                        $('#toggleAll')[0].checked = true;
                        $('#middleImg')[0].hidden = true;
                    }
                }
            });
        };

        function intermediateImg() {
            var elements = $('ul input[type="checkbox"]');
            var chkElements = $('ul input[type="checkbox"]:checked');
            if (elements.length == chkElements.length) {
                if ($('#toggleAll')[0] != undefined) {
                    $('#toggleAll')[0].hidden = false;
                    $('#toggleAll')[0].checked = true;
                }
                if ($('#middleImg')[0] != undefined) {
                    $('#middleImg')[0].hidden = true;
                }
            }
            else if (chkElements.length == 0) {
                if ($('#toggleAll')[0] != undefined) {
                    $('#toggleAll')[0].hidden = false;
                    $('#toggleAll')[0].checked = false;
                }
                if ($('#middleImg')[0] != undefined) {
                    $('#middleImg')[0].hidden = true;
                }
            }
            else {
                if ($('#toggleAll')[0] != undefined) {
                    $('#toggleAll')[0].hidden = true;
                }
                if ($('#middleImg')[0]) {
                    $('#middleImg')[0].hidden = false;
                }
            }
        }

        function middleImageClick() {
            $(document).on('click', '#middleImg', function () {
                if ($('#toggleAll')[0] != undefined) {
                    $('#toggleAll')[0].hidden = false;
                    $('#toggleAll')[0].checked = true;
                    $('#toggleAll').closest('.tree').find('ul input[type="checkbox"]').prop('checked', true);
                   var tgimg =  $("[id=middleTGImg]");
                   for (i = 0; i < tgimg.length; i++) {
                       if (!tgimg[i].hidden) {
                           tgimg[i].hidden = true
                       }
                   }

                   var hiddenChks = $(this).closest('.tree').find('input[type="checkbox"]:hidden');
                   for (i = 0; i < hiddenChks.length; i++) {
                           hiddenChks[i].hidden = false;
                   }
                }
                $('#middleImg')[0].hidden = true;
            });
        }

        $this.init = function () {
            treeNodeClick();
            middleImageClick();
        }
    }
    $(function () {
        var self = new Tree();
        self.init();
    })
}(jQuery))