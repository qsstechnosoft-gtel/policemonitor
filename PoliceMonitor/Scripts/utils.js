﻿// Gets the date in dd/mm/yyyy format
function getFormattedDate(d)
{
    var dd = d.getDate();
    var mm = d.getMonth() + 1; //January is 0!

    var yyyy = d.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    return dd + '/' + mm + '/' + yyyy;
}

// Gets the time in HH:MM:ss format
function getFormattedDate(d) {
    var dd = d.getDate();
    var mm = d.getMonth() + 1; //January is 0!

    var yyyy = d.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    return dd + '/' + mm + '/' + yyyy;
}

function getAjaxData(url, data, onSuccess, onError)
{
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: data,
        success: function (msg) {
            onSuccess(msg);
        },
        error: function (msg) {
            onError(msg);
        }
    });
}

function arrayToString(arr)
{
    var result = "";
    for(var item in arr)
    {
        result += arr[item] + "<p/>";
    }
    return result;
}

function toHHMMSS(sec_num)
{
    sec_num = Math.floor(sec_num / 1000);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
}

function iOS() {

    var iDevices = [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ];

    if (!!navigator.platform) {
        while (iDevices.length) {
            if (navigator.platform === iDevices.pop()) { return true; }
        }
    }

    return false;
}

function setTimezoneCookie() {
    var timezone_cookie = "timezoneoffset";

    // if the timezone cookie not exists create one.
    if (!$.cookie(timezone_cookie)) {
        // check if the browser supports cookie
        var test_cookie = 'test cookie';
        $.cookie(test_cookie, true);
        // browser supports cookie
        if ($.cookie(test_cookie)) {
            // delete the test cookie
            $.cookie(test_cookie, null);
            // create a new cookie 
            $.cookie(timezone_cookie, new Date().getTimezoneOffset());
            // re-load the page
            location.reload();
        }
    }
    // if the current timezone and the one stored in cookie are different
    // then store the new timezone in the cookie and refresh the page.
    else {
        var storedOffset = parseInt($.cookie(timezone_cookie));
        var currentOffset = new Date().getTimezoneOffset();
        // user may have changed the timezone
        if (storedOffset !== currentOffset) {
            $.cookie(timezone_cookie, new Date().getTimezoneOffset());
            location.reload();
        }
    }
}

function isMobile()
{
    return (window.matchMedia("only screen and (max-width: 760px)").matches);
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function getBaseURL()
{
    var getUrl = window.location;
    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/";
    return baseUrl;
}

function isNullOrBlank(str)
{
    if(str)
    {
        return (str == '');
    }

    return true;
}