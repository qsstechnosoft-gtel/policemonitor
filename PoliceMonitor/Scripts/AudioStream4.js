var AudioStream = function()
{
  this.Paused = false;
  this.ClipsToBeLoaded = [];
  this.ClipBeingLoaded = null;

  this.ClipsToBeDecoded = [];
  this.ClipBeingDecoded = null;

  this.ClipsToBePlayed = [];
  this.ClipBeingPlayed = null;
  this.LastPlayedClip = null;
  this.PlayedClips = [];

};

AudioStream.prototype.ClipIsPlaying = null;
AudioStream.AudioIsEnabled = null;
AudioStream.prototype.MAX_LOADED_CLIPS = 12;
AudioStream.prototype.MAX_PLAYED_CLIPS = 20;
AudioStream.audio_enabled = false;

AudioStream.EnableAudio = function()
{
  if (AudioStream.audio_enabled) {
    return;
  }
  var context = new (window.webkitAudioContext || window.AudioContext)();
  if (context.sampleRate != 44100) {
    var buffer = context.createBuffer(1, 1, 44100);
    var source = context.createBufferSource();
    source.buffer = buffer;
    source.connect(context.destination);
    source.start(0);
    context.close();
    context = new (window.webkitAudioContext || window.AudioContext)();
  }
  AudioStream.audio_context = context;

  if (context.state == 'suspended') {
    context.resume().then(this.FinishEnableAudio.bind(this));
  } else {
    this.FinishEnableAudio();
  }
}

AudioStream.FinishEnableAudio = function()
{
  var source = AudioStream.audio_context.createBufferSource();
  source.connect(AudioStream.audio_context.destination);
  source.buffer = AudioStream.audio_context.createBuffer(1, 100, AudioStream.audio_context.sampleRate);

  source.onended = (function() {
    if (AudioStream.AudioIsEnabled) { AudioStream.AudioIsEnabled(); }
    AudioStream.audio_enabled = true;
  }).bind(this);
  source.start(0);
};



AudioStream.prototype.GetURL = function(x)
{
  return x.url;
};

AudioStream.prototype.ShouldPlayClip = function(x)
{
  return true;
};
AudioStream.prototype.ShouldPlayClipNow = function(x)
{
  return true;
};

AudioStream.prototype.GetGainValues = function(x)
{
  return [{"at":0, "gain":1}];
  //return [{"at":0, "gain":1}, {"at":1, "gain":0}, {"at":2, "gain":1}];
}

AudioStream.prototype.Pause = function()
{
  this.Paused = true;
  if (this.ClipBeingPlayed) {
    this.ClipBeingPlayed.source.stop(0);
    this.ClipsToBePlayed.splice(0,0,this.ClipBeingPlayed);
    this.ClipBeingPlayed = null;
  }
};

AudioStream.prototype.Resume = function()
{
  this.Paused = false;
  this.PlaySomething();
  this.LoadSomething();
};

AudioStream.prototype.Clear = function()
{
  if (this.ClipBeingPlayed) {
    this.ClipBeingPlayed.source.stop(0);
  }
  if (this.ClipBeingLoaded) {
    this.ClipBeingLoaded.cancel = true;
  }
  if (this.ClipBeingDecoded) {
    this.ClipBeingDecoded.cancel = true;
  }
  this.ClipBeingPlayed = null;
  this.ClipsToBePlayed = [];
  this.ClipsToBeDecoded = [];
  this.ClipsToBeLoaded = [];
  this.PlayedClips = [];
  this.LastPlayedClip = null;
};

AudioStream.prototype.Back = function()
{
  this.Pause();
  var go_back_count = 4;
  var i = this.PlayedClips.length - 1;
  for (;i >= 0 && go_back_count > 0; i--) {
    if (this.ShouldPlayClipNow(this.PlayedClips[i])) {
      var x = this.PlayedClips.splice(i,1);
      this.ClipsToBePlayed.splice(0,0,x[0]);
      --go_back_count;
    }
  }
  this.LastPlayedClip = null;
  this.Resume();
}

AudioStream.prototype.LoadSomething = function()
{
  if (this.Paused) {
    return;
  }
  if (this.ClipsToBePlayed.length >= this.MAX_LOADED_CLIPS) {
    return;
  }
  if (this.ClipBeingLoaded != null) {
    return;
  }
  
  var i = 0;
  for (;i < this.ClipsToBeLoaded.length; ++i) {
      this.ClipBeingLoaded = this.ClipsToBeLoaded[i];
      if (!this.ShouldPlayClip(this.ClipBeingLoaded)) {
         //Never play this clip
         this.ClipBeingLoaded = null;
         this.ClipsToBeLoaded.splice(i, 1);
         --i;
         continue;
      }
      
      if (!this.ShouldPlayClipNow(this.ClipBeingLoaded)) {
        //This should be played later
        this.ClipBeingLoaded = null;
        continue;
      }
      
      if (this.ClipBeingLoaded.buffer) {
        //Clip has already been loaded so skip to playing
        this.ClipsToBePlayed.push(this.ClipBeingLoaded);
        this.ClipBeingLoaded = null;
        this.ClipsToBeLoaded.splice(i, 1);
        --i;
        window.setTimeout(this.PlaySomething.bind(this),0);
        continue;
      }
      
      //OK, go with this clip
      this.ClipsToBeLoaded.splice(i, 1);
      --i;


      var url = this.GetURL(this.ClipBeingLoaded);

      var x = new XMLHttpRequest();
      x.open("GET", url, true);
      x.responseType = 'arraybuffer';
      this.ClipBeingLoaded.req = x;
      x.addEventListener('load', this.ClipLoaded.bind(this), false);
      x.addEventListener('error', this.ClipLoadFailed.bind(this), false);
      x.send();
      break;
   }
};

AudioStream.prototype.ClipLoaded = function()
{
   if (!this.ClipBeingLoaded) {
     return;
   }
   if (!this.ClipBeingLoaded.cancel) {
      this.ClipsToBeDecoded.push(this.ClipBeingLoaded);
   }
   this.ClipBeingLoaded = null;
   this.LoadSomething();
   this.DecodeSomething();
   this.PlaySomething();
};

AudioStream.prototype.ClipLoadFailed = function()
{
  this.ClipBeingLoaded = null;
  this.LoadSomething();
  this.DecodeSomething();
  this.PlaySomething();
}

AudioStream.prototype.DecodeSomething = function()
{
  if (this.Paused) {
    return;
  }
  if (this.ClipBeingDecoded) {
    return;
  }
  if (!this.ClipsToBeDecoded.length) {
    return;
  }
  this.ClipBeingDecoded = this.ClipsToBeDecoded.shift();
  this.DecodeClip();
}

AudioStream.prototype.DecodeClip = function()
{
  var cbd = this.ClipBeingDecoded;
  AudioStream.audio_context.decodeAudioData(cbd.req.response, this.ClipDecoded.bind(this));
}

AudioStream.prototype.ClipDecoded = function(buffer)
{
  this.ClipBeingDecoded.buffer = buffer;
  this.ClipsToBePlayed.push(this.ClipBeingDecoded);
  this.ClipBeingDecoded = null;
  this.PlaySomething();
  this.DecodeSomething();
}

AudioStream.prototype.ClipSorter = function(a,b)
{
  return a.start - b.start;
};

AudioStream.prototype.PlayNowChanged = function()
{
  this.ClipsToBeLoaded = this.ClipsToBeLoaded.concat(this.ClipsToBePlayed);
  this.ClipsToBePlayed = [];
  this.ClipsToBeLoaded.sort(this.ClipSorter);
  this.LoadSomething();
};

AudioStream.prototype.PlaySomething = function()
{
  if (!AudioStream.audio_enabled) {
    return;
  }
  if (this.Paused) {
    return;
  }
  if (this.ClipBeingPlayed) {
    return;
  }
  if (!this.ClipsToBePlayed.length) {
    return;
  }
  var i;
  for (i = this.ClipsToBePlayed.length-1; i >= 0; --i) {
    if (!this.ShouldPlayClip(this.ClipsToBePlayed[i])) {
         this.ClipsToBePlayed.splice(i, 1);
    }
  }
  var moved_clip = false;
  for (i = 0; i < this.ClipsToBePlayed.length; ++i) {
    var temp = this.ClipsToBePlayed[i];
    if (!this.ShouldPlayClipNow(temp)) {
      this.ClipsToBeLoaded.push(temp);
      this.ClipsToBePlayed.splice(i, 1);
      --i;
      moved_clip = true;
    }
  }
  if (moved_clip) {
    this.ClipsToBeLoaded.sort(this.ClipSorter);
  }
  
  if (!this.ClipsToBePlayed.length) {
    return;
  }
  
  this.ClipsToBePlayed.sort(this.ClipSorter);

  if (this.LastPlayedClip && this.LastPlayedClip.tg) {
    var target_tg = this.LastPlayedClip.tg;
    var starting_before = this.LastPlayedClip.start + this.LastPlayedClip.len + 8000;
    var i;
    for (i = 0; i < this.ClipsToBePlayed.length; ++i) {
      var x = this.ClipsToBePlayed[i];
      if (x.tg == target_tg && x.start < starting_before) {
        x.hang = true;
        this.ClipBeingPlayed = x;
        this.ClipsToBePlayed.splice(i, 1);
        this.PlayClip();
        return;
      }
    }
  }

  this.ClipBeingPlayed = this.ClipsToBePlayed.shift();
  this.PlayClip();
};



AudioStream.prototype.PlayClip = function()
{
   var cbp = this.ClipBeingPlayed;
   cbp.source = AudioStream.audio_context.createBufferSource();
   var gainNode = AudioStream.audio_context.createGain();
   var gains = this.GetGainValues(cbp);
   var i = 0;
   for (i = 0; i < gains.length; ++i) {
      gainNode.gain.setValueAtTime(gains[i]["gain"], AudioStream.audio_context.currentTime + gains[i]["at"]);
   }
   if (false) {
     var dynCompNode = AudioStream.audio_context.createDynamicsCompressor();
     var preGainNode = AudioStream.audio_context.createGain();
     preGainNode.gain.value = 2;
     cbp.source.connect(preGainNode);
     preGainNode.connect(dynCompNode);
     dynCompNode.connect(gainNode);
     dynCompNode.threshold.value = -15;
     dynCompNode.knee.value = 20;
     dynCompNode.ratio.value = 6;
   } else {
      cbp.source.connect(gainNode);
   }
   gainNode.connect(AudioStream.audio_context.destination);
   cbp.source.onended = this.ClipDone.bind(this);
   cbp.source.buffer = cbp.buffer;
   if (cbp.source.noteOn) {
     cbp.source.noteOn(0);
   } else {
     cbp.source.start(0);
   }
   
  if (this.ClipIsPlaying) {
    this.ClipIsPlaying(cbp);
  }
  
  this.LoadSomething();
}

AudioStream.prototype.ClipDone = function(e)
{
  if (this.ClipBeingPlayed && e.target == this.ClipBeingPlayed.source) {
    this.LastPlayedClip = this.ClipBeingPlayed; 
    this.PlayedClips.push(this.ClipBeingPlayed);
    if (this.PlayedClips.length > this.MAX_PLAYED_CLIPS) {
      this.PlayedClips.shift();
    }
    this.ClipBeingPlayed = null;
  }

  if (this.ClipIsPlaying && this.ClipBeingPlayed == null) {
    this.ClipIsPlaying(null);
  }

  window.setTimeout(this.PlaySomething.bind(this),0);
}

AudioStream.prototype.MostRecentTalkgroup = function()
{
  if (this.ClipBeingPlayed) {
    return this.ClipBeingPlayed.tg;
  }
  if (this.LastPlayedClip) {
    return this.LastPlayedClip.tg;
  }
  return 0;
}

