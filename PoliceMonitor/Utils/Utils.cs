﻿using PoliceMonitor.BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Threading.Tasks;
using PoliceMonitor.Models;
using PoliceMonitor.Repositories;
using PoliceMonitor.Security.Principal;

namespace PoliceMonitor.Utils
{
    public static class Utils
    {
        static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string ToClientTime(this DateTime dt)
        {
            int offset = GetTimezoneOffset();
            if (offset != 0)
            {
                TimeSpan ofs = ToTimezone(offset);
                var timezone = TimeZoneInfo.CreateCustomTimeZone("clienttz", ofs, "dn", "dn");

                dt = dt.AddMinutes(-1 * offset);

                var clientTime = TimeZoneInfo.ConvertTimeFromUtc(dt.ToUniversalTime(), timezone);

                if (clientTime.IsDaylightSavingTime())
                    dt = dt.AddHours(1);

                return dt.ToString();
            }

            if (dt.IsDaylightSavingTime())
                dt = dt.AddHours(1);

            // if there is no offset in session return the datetime in server timezone
            return dt.ToLocalTime().ToString();
        }

        private static TimeSpan ToTimezone(int mins)
        {
            int h = mins / 60;
            int m = mins - (h * 60);
            return new TimeSpan(h, m, 0);
        }

        public static DateTime ToServerTime(this DateTime dt)
        {
            int offset = GetTimezoneOffset();
            if (offset > 0)
            {
                double serverOffset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).TotalMinutes * -1;//browser offeset is different than C#
                dt = dt.AddMinutes(serverOffset - offset);

                return dt;
            }

            // if there is no offset in session return the datetime in server timezone
            return dt;
        }

        internal static void RemoveDuplicates(List<Models.TalkGroupItem> items)
        {
            List<string> tkgids = new List<string>();
            foreach (var item in items)
            {
                if (item.ID.ToString().Contains("-"))
                {
                    tkgids.Add(item.ID.ToString());
                }
            }

            foreach (var id in tkgids)
            {
                var newId = id.Substring(0, id.IndexOf("-"));
                foreach (var item in items)
                {
                    if (item.ID.Equals(newId))
                    {
                        // Found element with same id
                        foreach (var item1 in items)
                        {
                            if (item1.ID.Equals(id))
                            {
                                // remove the talkgroup item with hyphen in id
                                items.Remove(item1);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }

        internal static void RemoveDuplicates(List<Entities.PolledTalkGroupItem> items)
        {
            List<string> tkgids = new List<string>();
            foreach (var item in items)
            {
                if (item.ID.ToString().Contains("-"))
                {
                    tkgids.Add(item.ID.ToString());
                }
            }

            foreach (var id in tkgids)
            {
                var newId = id.Substring(0, id.IndexOf("-"));
                foreach (var item in items)
                {
                    if (item.ID.Equals(newId))
                    {
                        // Found element with same id
                        foreach (var item1 in items)
                        {
                            if (item1.ID.Equals(id))
                            {
                                // remove the talkgroup item with hyphen in id
                                items.Remove(item1);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This function will return the timezone offset for the browser
        /// </summary>
        /// <param name="httpRequest"></param>
        /// <returns></returns>
        internal static int GetTimezoneOffset()
        {
            var timeOffSet = HttpContext.Current.Session["timezoneoffset"];
            if (timeOffSet != null)
                return (int) timeOffSet;
            return 0;
        }

        internal static void SetTimezoneOffset(HttpRequestBase httpRequest, HttpSessionStateBase Session)
        {
            if (httpRequest.Cookies.AllKeys.Contains("timezoneoffset"))
            {
                Session["timezoneoffset"] = int.Parse(httpRequest.Cookies["timezoneoffset"].Value);
            }
        }

        /// <summary>
        /// This function will remove the characters which are not acceptable by windows platform in a file name. This may not 
        /// be exhaustive list but sufficiant for currently used places.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        internal static string RemoveSpecialCharsFromFilename(string fileName)
        {
            return fileName.Replace(':', '_').Replace('?', '_').Replace('*', '_').Replace('"', '_').Replace('<', '_').Replace('>', '_').Replace('|', '_').Replace(',', '_');
        }

        internal static async Task DownloadFile(string URLMP3, string filePath)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Utility.Get().BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.Timeout = new TimeSpan(0, 5, 0);


                HttpResponseMessage response = await client.GetAsync(URLMP3);
                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsByteArrayAsync();
                    var stream = new MemoryStream(responseData);
                    // write data to file
                    try
                    {
                        FileStream fs = new FileStream(filePath, FileMode.Create);
                        fs.Write(responseData, 0, responseData.Length);
                        fs.Close();
                    }
                    catch (Exception e)
                    {
                        logger.Debug(e);
                    }
                }
            }
        }

        private static Random random = new Random();
        internal static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }


        public static List<TalkGroupModel> GetTalkgroupModelList(List<int> selectedRadio,List<int> selectedSystem, CustomPrincipal User, CustomPrincipal UserInfo)
        {
            List<TalkGroupModel> model = new List<TalkGroupModel>();
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            AdminRepository adminRepository = new AdminRepository(User);
            var talkGroups = adminRepository.GetCustomerTalkGroups();
            int i = 1;
            var systId = dbo.TblSystems.ToList();
            foreach (var talkGroup in talkGroups)
            {
                foreach (var x in CacheUtil.GetCustomerTalkgroupsSelectList(User))
                {
                    if (UserInfo.UserTalkGroupIDs.Exists(c => c == int.Parse(x.Value)) == false)
                    {
                        continue;
                    }

                    if (x.Value == talkGroup.TalkGroup)
                    {
                        int lastIndex = x.Text.LastIndexOf(']') + 1;
                        string firstPart = x.Text.Substring(0, lastIndex);
                        string nextPart = x.Text.Substring(lastIndex);

                        //var a = x.Text.Split('(', ')','[',']');
                        var a = firstPart.Split('[', ']');
                        var b = nextPart.Split('(', ')');
                        string county = b[1];// a[3];
                        string sysName = a[1].Trim();
                        var sysID = systId.Where(z => z.Name.Trim() == sysName).ToList();
                        if (sysID[0].ID == talkGroup.SystemID)
                        {
                            //var sysID = dbo.TblSystems.Where(y => y.Name == sysName).ToList();
                            int chkduplicate = model.FindIndex(m => m.ChannelName == county);
                            if (chkduplicate <= -1)
                            {
                                model.Add(new TalkGroupModel() { TalkGroupID = long.Parse(x.Value), ChannelName = county, SystemName = sysName, SysID = sysID[0].ID });
                                i++;
                            }
                            var addChildModel = model.First(m => m.ChannelName == county);
                            var select = false;
                            if (selectedRadio.Contains(int.Parse(x.Value)))
                            {
                                if (selectedSystem.Contains(sysID[0].ID))
                                {
                                    select = true;
                                }

                            }
                            if (sysID[0].ID == 16961)
                            {
                                addChildModel.RadioGroupModel.Add(new RadioGroupModel()
                                {
                                    RadioID = int.Parse(x.Value),
                                    RadioDesc = a[4],
                                    IsSelect = select//(selectedRadio > 0 ? true : false)
                                });
                            }

                            addChildModel.RadioGroupModel.Add(new RadioGroupModel()
                            {
                                RadioID = int.Parse(x.Value),
                                RadioDesc = b[2],
                                //IsSelect = selectedRadio.Contains(int.Parse(x.Value))
                                IsSelect = select
                            });
                            if (selectedRadio.Any())
                            {
                                int selectedListCount = addChildModel.RadioGroupModel.Where(aa => aa.IsSelect == true).ToList().Count;
                                if (selectedListCount == addChildModel.RadioGroupModel.Count)
                                {
                                    addChildModel.IsSelect = true;
                                    addChildModel.AllNotChecked = false;
                                }
                                else if (selectedListCount == 0)
                                {
                                    addChildModel.AllNotChecked = false;
                                }
                                else if (selectedListCount < addChildModel.RadioGroupModel.Count)
                                {
                                    addChildModel.AllNotChecked = true;
                                }
                                else
                                {
                                    addChildModel.AllNotChecked = false;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            List<TalkGroupModel> mm = model.OrderBy(t => t.ChannelName).ToList();
            foreach (TalkGroupModel t in mm)
            {
                t.RadioGroupModel = t.RadioGroupModel.OrderBy(x => x.RadioDesc).ToList();
            }
            return mm;
        }

        internal static IList<TalkGroupModel> GetAllTalkgroupModelList(CustomPrincipal user)
        {
            List<TalkGroup> talkgroups = CacheUtil.GetCustomerTalkgroups(user, true);
            List<TalkGroupModel> models = new List<TalkGroupModel>();
            TalkGroupModel m = new TalkGroupModel();
            foreach (TalkGroup tk in talkgroups)
            {
                if (tk.County == m.ChannelName)
                {
                    RadioGroupModel rgm = new RadioGroupModel();
                    rgm.RadioID = tk.ID;
                    rgm.RadioDesc = tk.Name;
                    m.RadioGroupModel.Add(rgm);
                }
                else
                {
                    m = new TalkGroupModel();
                    m.TalkGroupID = tk.ID;
                    m.ChannelName = tk.County;
                    m.RadioGroupModel = new List<RadioGroupModel>();
                    models.Add(m);
                }
            }
            return models;
        }

        internal static string TwoDigit(int val)
        {
            if (val < 10)
                return "0" + val;
            else
                return val.ToString();
        }

        internal static string GetDateString(DateTime? d, string format)
        {
            if(d != null && d.HasValue)
            {
                DateTime dt = d.Value;
                if(format.Equals("mmddyy"))
                {
                    return TwoDigit(dt.Month + 1) + "/" + TwoDigit(dt.Day) + "/" + (dt.Year - 2000);
                }
                return d.Value.ToString();
            }
            else
                return "";
        }
    }
}