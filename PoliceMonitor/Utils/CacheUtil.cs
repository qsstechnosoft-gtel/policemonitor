﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Security.Principal;
using PoliceMonitor.Repositories;
using System.Web.Mvc;
using PoliceMonitor.BLL;
using PoliceMonitor.Models;

namespace PoliceMonitor.Utils
{
    public static class CacheUtil
    {
        public static string RADIOS = "RADIOS";
        public static string TALKGROUPS = "TALKGROUPS";
        public static string TALKGROUPS1 = "TALKGROUPS1";
        public static string CUSTOMERTALKGROUPS = "CUSTOMERTALKGROUPS";
        public static string ALLTALKGROUPMODELLIST = "ALLTALKGROUPMODELLIST";

        static Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
        static ObjectCache cache = MemoryCache.Default;

        public static List<Entities.Radio> GetRadios()
        {
            if (!cache.Contains(RADIOS))
            {
                // Store data in the cache    
                cache.Add(RADIOS, dbo.Radios.ToList(), GetCacheItemPolicy(1));
            }

            return (List<Entities.Radio>)cache.Get(RADIOS);
        }

        private static CacheItemPolicy GetCacheItemPolicy(int hours)
        {
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(hours);
            return cacheItemPolicy;
        }

        public static List<TalkGroup> GetCustomerTalkgroups(CustomPrincipal User, bool allTalkgroups)
        {
            int customerId = (allTalkgroups)?0:User.CustomerId;
            if (!cache.Contains(CUSTOMERTALKGROUPS + customerId))
            {
                AdminRepository adminRepository = new AdminRepository(User);
                var listTalkGroups = new List<SelectListItem>();

                //List<Entities.TalkGroup> talkgroups;
               
                //List<Entities.TblSystem> tblsystem;

                List<TalkGroup> tk = new List<TalkGroup>();

                bool isSuperAdmin = allTalkgroups || User.IsInRole(Constant.SUPERADMIN_ROLENAME);

                if (isSuperAdmin)
                {                   
                    //talkgroups = dbo.TalkGroups.OrderBy(x => x.County).ThenBy(x => x.Name).ToList();
                    
                    //tblsystem = dbo.TblSystems.ToList();

                    var taklDetails = dbo.TalkGroups.Join(dbo.TblSystems, tg => tg.SystemId, ts => ts.ID, (tg, ts) => new { tg, ts }).OrderBy(o => o.ts.Name).ThenBy(x=>x.tg.County).ThenBy(t => t.tg.Name).ToList();

                    foreach (var talk in taklDetails)
                    {
                        TalkGroup t = new TalkGroup();
                        t.County = talk.tg.County;
                        t.ID = talk.tg.ID;
                        t.Name = talk.tg.Name;
                        t.ServerUrl = talk.tg.ServerUrl;
                        t.SystemId = talk.tg.SystemId;
                        t.TalkgroupID = talk.tg.TalkgroupID;
                        t.SystemName = talk.ts.Name;
                        tk.Add(t);
                    }

                }
                else
                {
                    var talkgroups = dbo.TalkGroups.Join(dbo.CustomerTalkGroups, tkg => tkg.ID.ToString(), ctg => ctg.TalkGroup, (tkg, ctg) => new { TKG = tkg, CTG = ctg }).
                           Where(x => x.TKG.ID.ToString() == x.CTG.TalkGroup && x.CTG.CustomerId == User.CustomerId).Select(x => x.TKG).ToList();
                    var talks = talkgroups.Join(dbo.TblSystems, tg => tg.SystemId, ts => ts.ID, (tg, ts) => new { tg, ts }).OrderBy(o => o.ts.Name).ThenBy(x=>x.tg.County).ThenBy(t => t.tg.Name).ToList();

                    //var data = dbo.TalkGroups.Join(dbo.TblSystems, tg => tg.SystemId, ts => ts.ID, (tg, ts) => new { tg, ts }).
                    //           Join(dbo.CustomerTalkGroups, ctg => ctg.tg.ID, ct => ct.TalkGroup, (ctg, ct) => new { ctg.tg.ID, ct.TalkGroup }).ToList();
                    foreach (var talk in talks)
                    {
                        TalkGroup t = new TalkGroup();
                        t.County = talk.tg.County;
                        t.ID = talk.tg.ID;
                        t.Name = talk.tg.Name;
                        t.ServerUrl = talk.tg.ServerUrl;
                        t.SystemId = talk.tg.SystemId;
                        t.TalkgroupID = talk.tg.TalkgroupID;
                        t.SystemName = talk.ts.Name;
                        tk.Add(t);
                    }


                }

                cache.Add(CUSTOMERTALKGROUPS + customerId, tk, GetCacheItemPolicy(1));
            }

            return (List<TalkGroup>)cache.Get(CUSTOMERTALKGROUPS + customerId);
        }

        internal static List<TalkGroupModel> GetAllTalkgroupModelList(CustomPrincipal user)
        {
            if (!cache.Contains(ALLTALKGROUPMODELLIST))
            {
                List<TalkGroup> talkgroups = CacheUtil.GetCustomerTalkgroups(user, true);
                List<TalkGroupModel> models = new List<TalkGroupModel>();
                TalkGroupModel m = new TalkGroupModel();
                foreach (TalkGroup tk in talkgroups)
                {
                    if (tk.County == m.ChannelName)
                    {
                        RadioGroupModel rgm = new RadioGroupModel();
                        rgm.RadioID = tk.ID;
                        rgm.RadioDesc = tk.Name;
                        m.RadioGroupModel.Add(rgm);
                    }
                    else
                    {
                        m = new TalkGroupModel();
                        m.TalkGroupID = tk.ID;
                        m.ChannelName = tk.County;
                        m.RadioGroupModel = new List<RadioGroupModel>();
                        RadioGroupModel rgm = new RadioGroupModel();
                        rgm.RadioID = tk.ID;
                        rgm.RadioDesc = tk.Name;
                        m.RadioGroupModel.Add(rgm);
                        models.Add(m);
                    }
                }

                cache.Add(ALLTALKGROUPMODELLIST, models, GetCacheItemPolicy(1));
            }
            return (List<TalkGroupModel>)cache.Get(ALLTALKGROUPMODELLIST);
        }


        public static List<SelectListItem> GetCustomerTalkgroupsSelectList(CustomPrincipal User, bool withSystemId=false)
        {
            List<TalkGroup> talkgroups = GetCustomerTalkgroups(User, false);
            var listTalkGroups = new List<SelectListItem>();

            bool isSuperAdmin = User.IsInRole(Constant.SUPERADMIN_ROLENAME);

            foreach (var info in talkgroups)
            {
                string tmp ="["+(string)info.SystemName+"]"+ " (" + (string)info.County + ") " + (string)info.Name ;
                listTalkGroups.Add(new SelectListItem() { Value = ((withSystemId)?info.SystemId + "_":"") + info.ID, Text = string.Format("{0}", tmp) });
            }

            return listTalkGroups;
        }

        public static List<SelectListItem> GetUserTalkgroups(CustomPrincipal User)
        {
            if (!cache.Contains(TALKGROUPS + User.UserId))
            {
                List<TalkGroup> talkgroups = GetCustomerTalkgroups(User, false);
                var listTalkGroups = new List<SelectListItem>();

                bool isSuperAdmin = User.IsInRole(Constant.SUPERADMIN_ROLENAME);

                foreach (var info in talkgroups)
                {
                    string tmp =""+(string)info.SystemName+""+ " (" + (string)info.County + ") " + (string)info.Name;

                    if (isSuperAdmin)
                    {
                        listTalkGroups.Add(new SelectListItem() { Value = "" + info.ID, Text = string.Format("{0}", tmp) });
                    }
                    else
                    {
                        if (User.UserTalkGroupIDs.Exists(c => c == info.ID) == true)
                        {
                            listTalkGroups.Add(new SelectListItem() { Value = "" + info.ID, Text = string.Format("{0}", tmp) });
                        }
                    }
                }

                cache.Add(TALKGROUPS + User.UserId, listTalkGroups, GetCacheItemPolicy(1));
            }

            return (List<SelectListItem>)cache.Get(TALKGROUPS + User.UserId);
        }


        public static List<SelectListItem> GetUserTalkgroups1(CustomPrincipal User)
        {
            if (!cache.Contains(TALKGROUPS1 + User.UserId))
            {
                List<TalkGroup> talkgroups = GetCustomerTalkgroups(User, false);
                var listTalkGroups = new List<SelectListItem>();

                bool isSuperAdmin = User.IsInRole(Constant.SUPERADMIN_ROLENAME);

                foreach (var info in talkgroups)
                {
                    string tmp = " ("+(string)info.County + ") " + (string)info.Name;

                    if (isSuperAdmin)
                    {
                        listTalkGroups.Add(new SelectListItem() { Value = info.SystemId + "_" + info.ID, Text = string.Format("{0}", tmp) });
                    }
                    else
                    {
                        if (User.UserTalkGroupIDs.Exists(c => c == info.ID) == true)
                        {
                            listTalkGroups.Add(new SelectListItem() { Value = info.SystemId+"_" + info.ID, Text = string.Format("{0}", tmp) });
                        }
                    }
                }

                cache.Add(TALKGROUPS1 + User.UserId, listTalkGroups, GetCacheItemPolicy(1));
            }

            return (List<SelectListItem>)cache.Get(TALKGROUPS1 + User.UserId);
        }




        internal static List<Models.TalkGroupModel> GetAllTalkgroupModels(CustomPrincipal user)
        {
            List<Models.TalkGroupModel> models = new List<Models.TalkGroupModel>();
            List<TalkGroup> talkgroups = CacheUtil.GetCustomerTalkgroups(user, true);
            foreach(TalkGroup talkgroup in talkgroups)
            {
                Models.TalkGroupModel model = new Models.TalkGroupModel();
               // model.TalkGroupID = talkgroup.TalkgroupID;
                model.ChannelName = talkgroup.County;
            }
            return models;
        }

        internal static void ResetCache(string key)
        {
            cache.Remove(key);
        }
    }
}