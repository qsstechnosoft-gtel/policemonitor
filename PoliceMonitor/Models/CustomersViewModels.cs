﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoliceMonitor.Models
{
    public class CustomersViewModels
    {
        public int CustomerId { get; set; }

        [Required]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }

        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }

        [Display(Name = "Contact Number")]
        public string ContactPhoneNumber { get; set; }

        [Display(Name = "Unlimited Users")]
        public bool UnlimitedUsers { get; set; }

        [Display(Name = "Users Count")]
        public int? UsersCount { get; set; }

        public bool Active { get; set; }

        public bool DefaultAutoplaySetting { get; set; }
        public string ActiveDisplay
        {
            get
            {
                if (this.Active)
                {
                    return "Active";
                }
                else
                    return "InActive";
            }
        }
        public List<string> SelectedValues { get; set; }
        public List<string> SelectedText { get; set; }
        public IEnumerable<SelectListItem> TalkGroupList { get; set; }        
        public IEnumerable<SelectListItem> PermissionLevelList { get; set; }        
    }
}