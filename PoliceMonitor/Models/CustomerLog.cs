﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PoliceMonitor.Models
{
    public class CustomerLog
    {
        [Required]
        [Display(Name = "Customer")]
        public int Customer { get; set; }

        [Display(Name = "Start Date")]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime SDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime EDate { get; set; }

        public List<CutomerDetail> Detail { get; set; }
    }
    public class CutomerDetail
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int counts { get; set; }

    }
}