﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoliceMonitor.Models
{
    public class Radio11
    {
        public int ID { get; set; }

        string _Description;
        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(_Description) == true)
                    return string.Format("[{0}]", this.ID);
                else return _Description;
            }
            set
            {
                _Description = value;
            }
        }
    }
}
