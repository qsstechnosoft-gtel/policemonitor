﻿using PoliceMonitor.BLL;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PoliceMonitor.Models
{
    public class RadioGroupModel
    {
        public Int64 ID { get; set; }
        public string ChName { get; set; }
        public Int64 RadioID { get; set; }
        public string RadioDesc { get; set; }
        public bool IsSelect { get; set; }
    }

    public class TalkGroupModel
    {
        public TalkGroupModel()
        {
            RadioGroupModel = new List<RadioGroupModel>();
        }
        public Int64 TalkGroupID { get; set; }
        public string ChannelName { get; set; }
        public IList<RadioGroupModel> RadioGroupModel { get; set; }
        public bool IsSelect { get; set; }
        public bool AllNotChecked { get; set; }
        public string SystemName { get; set; }
        public int SysID { get; set; }
    }

    public class PlayBackIndexModel
    {
        public PlayBackIndexModel()
        {
            TalkGroupModel = new List<TalkGroupModel>();
        }
        public IList<TalkGroupModel> TalkGroupModel { get; set; }
        public List<SelectListItem> ListenLiveFavouriteList = new List<SelectListItem>();
        public int SelectedFavouriteID { get; set; }
        public int[] SelectedTalkGroups { get; set; }
        public int[] SelectedSystemID { get; set; }
    }
}
