﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PoliceMonitor.Models
{
    public class AdminViewModels
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Please select at least one customer")]
        [Display(Name = "Customers")]
        public int SelectedCustomerId { get; set; }

        //[Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

 
        [Display(Name = "First Name")]
        public string UserFirstName { get; set; }

       
        [Display(Name = "Last Name")]
        public string UserLastName { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //[Required]
        //[Display(Name = "Address")]
        public string Address { get; set; }

        //[Required]
       // [Display(Name = "City")]
        public string City { get; set; }

        //[Required]
        //[Display(Name = "State")]
        public string State { get; set; }

        //[DataType(DataType.PostalCode, ErrorMessage = "Provided postal code not valid")]
        //[Required]
        [Display(Name = "Postal Code")]
        //[RegularExpression(@"^(\d{4})$", ErrorMessage = "Not a valid postal code")]
        public virtual string PostalCode { get; set; }

        //[DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
        //[Required]
        [Display(Name = "Phone Number")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        public virtual string PhoneNumber { get; set; }

        public bool Active { get; set; }

        public string ActiveDisplay
        {
            get
            {
                if (this.Active)
                {
                    return "Active";
                }
                else
                    return "<font color='red'>InActive</font>";
            }
        }
        public CustomersViewModels Customer { get; set; }

        public IEnumerable<CustomersViewModels> CustomerList { get; set; }      
    }
}