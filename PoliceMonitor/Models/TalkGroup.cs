﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliceMonitor.Models
{
    public class TalkGroup
    {
        public int ID { get; set; }
        public string TalkgroupID { get; set; }
        public string Name { get; set; }
        public string County { get; set; }
        public string ServerUrl { get; set; }
        public int SystemId { get; set; }
        public string SystemName { get; set; }
    }
}