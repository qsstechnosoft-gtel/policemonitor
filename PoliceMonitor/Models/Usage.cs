﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PoliceMonitor.Entities;

namespace PoliceMonitor.Models
{
    public class Usage
    {
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public int TotalTransmissions { get; set; }
        public long Duration { get; set; }
        public int TotalLogins { get; set; }
        public List<Usage> userwiseUsage { get; set; }
    }
}