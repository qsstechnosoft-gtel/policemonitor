﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PoliceMonitor.Entities;
using PoliceMonitor.Security.Base.Controller;
using PoliceMonitor.Security.Attributes;
using PoliceMonitor.Models;
using PoliceMonitor.Repositories;
using PoliceMonitor.BLL;
using System.Threading.Tasks;
using PoliceMonitor.Security.Base.Model;
using PoliceMonitor.Utils;
using PoliceMonitor.Security.Principal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PoliceMonitor.Controllers
{
    public class MyRadiosController : BaseController
    {
        private CustomPrincipal _userInfo;
        protected CustomPrincipal UserInfo
        {
            get
            {
                return _userInfo;
            }
        }

        public MyRadiosController()
        {
            _userInfo = User;
        }

        // GET: CustomerNotification
        [CustomAuthorize(new string[] { Constant.ADMIN_ROLENAME, Constant.SUPERADMIN_ROLENAME })]
        public ActionResult Index()
        {
            if(!UserInfo.HasPermission(PoliceMonitor.Security.Base.Model.Constant.SEARCHBYRADIO))
            {
                AuthorizationContext filterContext = new AuthorizationContext();
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                Response.Redirect("~/Account/AccessDenied");
                return null;
            }

            MyRadiosModel model = GenerateViewModel();

            return View("Index", model);
        }

        private MyRadiosModel GenerateViewModel()
        {
            MyRadiosModel model = new MyRadiosModel();
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            model.Radios = dbo.UserRadios.Where(x => x.userId == UserInfo.UserId).Select(x => x.radioId).ToList();
            return model;
        }

        public string RemoveRadio(string radioid)
        {
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            dbo.UserRadios.RemoveRange(dbo.UserRadios.Where(x => x.userId == UserInfo.UserId && x.radioId == radioid));
            dbo.SaveChanges();

            return "{status: 'success'}";
        }

        [HttpPost]
        [CustomAuthorize(new string[] { Constant.ADMIN_ROLENAME, Constant.SUPERADMIN_ROLENAME })]
        public ActionResult SaveRadios(string RadioIds)
        {
            if (!UserInfo.HasPermission(PoliceMonitor.Security.Base.Model.Constant.SEARCHBYRADIO))
            {
                AuthorizationContext filterContext = new AuthorizationContext();
                filterContext.Result = new RedirectResult("~/Account/AccessDenied");
                Response.Redirect("~/Account/AccessDenied");
                return null;
            }

            MyRadiosModel model = GenerateViewModel();

            if(string.IsNullOrEmpty(RadioIds))
            {
                model.ErrorMessage = "Please enter valid radios";
                return View("Index", model);
            }

            string[] radiosArr = RadioIds.Split(',');
            List<string> radioList = new List<string>();
            foreach(string s in radiosArr)
            {
                long val = 0;
                if (!long.TryParse(s.Trim(), out val))
                {
                    if(s.Contains('-'))
                    {
                        string[] a = s.Split('-');
                        if(a.Count() != 2)
                        {
                            model.ErrorMessage = "Please enter valid radios";
                            return View("Index", model);
                        }
                        foreach (string a1 in a)
                        {
                            if (!long.TryParse(a1.Trim(), out val))
                            {
                                model.ErrorMessage = "Please enter valid radios";
                                return View("Index", model);
                            }
                        }

                        radioList.Add(s);
                        continue;
                    }
                    model.ErrorMessage = "Please enter valid radios";
                    return View("Index", model);
                }
                radioList.Add(val.ToString());
            }
            
            PoliceMonitor.Entities.PoliceMonitorDBEntities dbo = new Entities.PoliceMonitorDBEntities();
            List<string> radios = dbo.UserRadios.Where(x => x.userId == UserInfo.UserId).Select(x => x.radioId).ToList();
            foreach (string val in radioList)
            {
                if (radios.Contains(val))
                    continue;

                UserRadio r = new UserRadio();
                r.userId = UserInfo.UserId;
                r.radioId = val;
                dbo.UserRadios.Add(r);
            }

            dbo.SaveChanges();

            model = GenerateViewModel();

            return View("Index", model);
        }
    }
}
