﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliceMonitor.Models
{
    public class PostFBModel
    {
        public string common_img { get; set; }
        public string APIAccessToken { get; set; }
        public string url { get; set; }
        public string description { get; set; }
        public string event_type { get; set; }
        public string imageurl { get; set; }
        public string location { get; set; }
        public string Time { get; set; }
        public string categoryId { get; set; }
        public string postedOn { get; set; }
        public string transid { get; set; }
        public dynamic ismember { get; set; }
        public string city { get; set; }
        public string postedBy { get; set; }
        public int postid { get; set; }
        public string TalkURl { get; set; }
       // public bool Ismember { get; set; }
    }
}