﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoliceMonitor.Models
{
    public class NotificationViewModels
    {
        public int CustomerNotificationId { get; set; }

        [Required(ErrorMessage = "Please select at least one Case")]
        [Display(Name = "Cases")]
        public int SelectedCaseId { get; set; }
        
        public  IList<SelectListItem> CaseList { 
            get
            {
                return new List<SelectListItem>{ new SelectListItem{Text = "Case1", Value = "1"},
                                                new SelectListItem{Text = "Case2", Value = "2"},
                                                new SelectListItem{Text = "Case3", Value = "3"}
                                             };
            }
        }      



        [Required(ErrorMessage = "Please select at least one customer")]
        [Display(Name = "Customers")]
        public int SelectedCustomerId { get; set; }

        [Display(Name = "Transmissions Count")]
        public int? NotifiyTransmissionsCount { get; set; }

        [Display(Name = "Interval(s)")]
        public int? Interval { get; set; }

        [Display(Name = "First Transmission Wait Interval(s)")]
        public int? AfterFirstTransmissionWaitInterval { get; set; }

        [Display(Name = "Last N Sec Transmissions Count")]
        public int? LastNSecTransmissionsCount { get; set; }

        [Display(Name = "N Sec Interval")]
        public int? NSecInterval { get; set; }

        [Display(Name = "After N Sec Transmission Wait Interval")]
        public int? AfterNSecTransmissionWaitInterval { get; set; }
    
        public CustomersViewModels Customer { get; set; }
        public IEnumerable<CustomersViewModels> CustomerList { get; set; }


    }

    public class NotificationViewModelsNew
    {
        public int CustomerNotificationId { get; set; }

        [Required]
        [Display(Name = "Notification Name")]
        public string NotificationName { get; set; }

        [Required(ErrorMessage = "Please select at least one Case")]
        [Display(Name = "Cases")]
        public int SelectedCaseId { get; set; }

        public IList<SelectListItem> CaseList
        {
            get
            {
                return new List<SelectListItem>{ new SelectListItem{Text = "N transmissions in interval", Value = "1"},
                                                new SelectListItem{Text = "First transmission in interval", Value = "2"},
                                                new SelectListItem{Text = "N transmissions in interval with wait", Value = "3"}
                                             };
            }
        }

        [Display(Name = "Transmissions Count")]
        public int? NotifiyTransmissionsCount { get; set; }

        [Display(Name = "Interval(s)")]
        public int? Interval { get; set; }

        [Display(Name = "First Transmission Wait Interval(s)")]
        public int? AfterFirstTransmissionWaitInterval { get; set; }

        [Display(Name = "Last N SecTransmissions Count")]
        public int? LastNSecTransmissionsCount { get; set; }

        [Display(Name = "N Sec Interval")]
        public int? NSecInterval { get; set; }

        [Display(Name = "After N Sec Transmission Wait Interval")]
        public int? AfterNSecTransmissionWaitInterval { get; set; }

        public CustomersViewModels Customer { get; set; }
        public IEnumerable<SelectListItem> TalkGroupList { get; set; }


        public List<string> SelectedUserValues { get; set; }
        public IEnumerable<SelectListItem> Users { get; set; }

        public List<string> SelectedGroupValues { get; set; }
        public IEnumerable<SelectListItem> GroupList { get; set; }

        public IEnumerable<SelectListItem> RadioList { get; set; }

        public bool Active { get; set; }
    }
}