﻿using PoliceMonitor.BLL;
using System;
using System.ComponentModel.DataAnnotations;

namespace PoliceMonitor.Models
{
    public class TalkGroupItem
    {
        public string ID { get; set; }
        public Int64 TransStartMSec { get; set; }
        public Int64 TransLengthMSec { get; set; }
        public Int64 RadioID { get; set; }
        public string Tones { get; set; }

        public Int64 TalkGroupID { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd/MMM}", ApplyFormatInEditMode = true)]
        public DateTime TransStartUTC
        {
            get
            {
                DateTime MyDT = Utility.Get().ConvertJsonDT(this.TransStartMSec);

                double offset = TimeZone.CurrentTimeZone.GetUtcOffset(MyDT).TotalHours;
                if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
                {
                    //double offset = TimeZone.CurrentTimeZone.GetUtcOffset(MyDT).TotalHours;
                    //// MyDT = MyDT.AddHours(offset);
                    ////double offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).TotalHours;
                    ////MyDT = MyDT.AddHours(offset);
                    ////TimeZoneInfo.ConvertTimeTocs(MyDT, TimeZoneInfo.Local)
                    bool isdaylight = IsDaylight(MyDT.AddHours(offset));
                    if (isdaylight)
                    {
                        DateTime newdate = MyDT;
                        MyDT = new DateTime();
                        MyDT = newdate.AddHours(-1);
                    }
                    else
                    {
                        DateTime newdate = MyDT;
                        MyDT = new DateTime();
                        MyDT = newdate.AddHours(-2);
                    }
                }
               
                //else
                //{
                //   // if(TimeZone.CurrentTimeZone.StandardName == "US Mountain Standard Time")
                //    {
                //        DateTime newdate = MyDT;
                //        MyDT = new DateTime();
                //        MyDT = newdate.AddHours(-2);
                //    }
                //}
                //if(isCurrentlyDaylightSavings)
                //{
                //    DateTime newdate = MyDT;
                //    MyDT = new DateTime();
                //    MyDT = newdate.AddHours(-1);
                //} 
                //if (TimeZoneInfo.Local.IsDaylightSavingTime(MyDT))
                //{
                //    DateTime newdate = MyDT;
                //    MyDT = new DateTime();
                //    MyDT= newdate.AddHours(-1);
                //}
                //else
                return MyDT;
            }
        }


        public static bool IsDaylight(DateTime MyDT)
        {
            return (Between(MyDT, TimeZone.CurrentTimeZone.GetDaylightChanges(MyDT.Year).Start, TimeZone.CurrentTimeZone.GetDaylightChanges(MyDT.Year).End));
        }
        public static bool Between(DateTime input, DateTime date1, DateTime date2)
        {
            return (input > date1 && input < date2);
        }




        public string TransTimeDesc
        {
            get
            {
                if (TransLengthMSec > 0)
                {
                    TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)TransLengthMSec);
                    return string.Format("{0:F1}s", ts.TotalSeconds);
                }
                else
                    return "NA";
            }
        }

        public string RadioDesc { get; set; }

        public string RadioDesc1
        {
            get
            {
                if (RadioDesc == null)
                    return "";
                if (RadioDesc.StartsWith("DISPATCH:::"))
                    return "";

                string Result = "";
                if (string.IsNullOrEmpty(RadioDesc) == false)
                {
                    string str = RadioDesc;
                    char[] arr = str.ToCharArray();
                    Array.Reverse(arr);
                    str = new string(arr);

                    int idx = str.IndexOf(")");
                    int idx2 = str.IndexOf("(");
                    if (idx > -1 && idx2 > -1 && idx2 > idx)
                    {

                        Result = str.Substring(idx + 1, idx2 - (idx + 1));

                        Int32 tmpID = 0;
                        if (int.TryParse(Result, out tmpID) == true)
                            Result = RadioDesc;
                        else
                            Result = "";
                    }
                }

                return Result;
            }
        }
        public string RadioDesc2
        {
            get
            {
                if (RadioDesc == null)
                    return "";

                if (RadioDesc.StartsWith("DISPATCH:::"))
                    return RadioDesc.Substring("DISPATCH:::".Length);

                if (string.IsNullOrEmpty(RadioDesc1) == false)
                    return RadioDesc.Replace(this.RadioDesc1, "");
                else
                    return RadioDesc;
            }

        }

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(UserFirstName) && string.IsNullOrEmpty(UserFirstName))
                    return string.Empty;
                else
                    return UserFirstName + " " + UserLastName;
            }

        }

        public byte? ReviewStatus { get; set; }
        public bool MarkListen { get; set; }
        public string UserName { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserIds { get; set; }
        public string SystemID { get; set; }
    }
}
