﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliceMonitor.Models
{
    public class MyRadiosModel
    {
        public string ErrorMessage { get; set; }
        public List<string> Radios { get; set; }
        public string RadioIds { get; set; }
    }
}