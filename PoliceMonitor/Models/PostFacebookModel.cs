﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoliceMonitor.Models
{
    public class PostFacebookModel
    {              
        public string FBPostUrl { get; set; }
        public string AudioURL { get; set; }
        public string Description { get; set; }
        public string Event { get; set; }
        public string ImageURL { get; set; }
        public string Location { get; set; }
        public int UserID { get; set; }
        public string Categories { get; set; }
        public DateTime PostDate { get; set; }
        public string TransmissionID { get; set; }
        public bool IsMember { get; set; }
        public string City { get; set; }
        public string PostedBy { get; set; }
        public int Postid { get; set; }
    }
}