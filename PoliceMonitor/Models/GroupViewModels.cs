﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PoliceMonitor.Models
{
    public class GroupViewModels
    {
        public int GroupId { get; set; }
        
        [Required]
        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        public IEnumerable<SelectListItem> TalkGroupList { get; set; }
        public IEnumerable<UserViewModels> Users { get; set; }
        
    }
}