﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Collections;

namespace PoliceMonitor.Models
{
    public class TalkGroupView
    {
        public object SelectedItem { get; set; }

        // Grab values from url below instead of hard code one
        // http://70.89.197.235:3389/radio/list_talkgroups.cgi
        public List<SelectListItem> PlayBackFavouriteList  = new List<SelectListItem>();
        public int SelectedFavouriteID { get; set; }
        public List<SelectListItem> TalkGroups  = new List<SelectListItem>();

        public List<SelectListItem> TalkGroupsSelected  = new List<SelectListItem>();

        public List<TalkGroupModel> TalkGroupModel = new List<TalkGroupModel>();
        public List<Entities.City> Cities = new List<Entities.City>();
        public List<Entities.Timeformat> Timeformat = new List<Entities.Timeformat>();

        public int[] SelectedTalkGroups {get;set;}
        public int[] SelectedSystemID { get; set; }
        public string[] SystemName { get; set; }

        public int MinTranSeconds { get; set; }

        public  object Size {get; set;}
       
        public int StartHour { get; set; }
        public int EndHour { get; set; }

        public string ErrorMessage { get; set; }

        public List<TalkGroupItem> FilteredList = new List<TalkGroupItem>();

        public string[] SystemNam { get; set; } 
        
        public string BaseURL { get; set; }
       
       
         public List<SelectListItem> AllTalkGroups  = new List<SelectListItem>();

        public DateTime? StartDatetime { get; set; }
        public DateTime? EndDatetime { get; set; }

        public string StartDatetimeFormatted { get {
            return PoliceMonitor.Utils.Utils.GetDateString(StartDatetime, "mmddyy");
        } }

        public string EndDatetimeFormatted
        {
            get
            {
                return PoliceMonitor.Utils.Utils.GetDateString(EndDatetime, "mmddyy");
            }
        }
        
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string radioFilter { get; set; }
        public string RadioID { get; set; }
        public bool AutoPlay { get; set; }
    }
}
