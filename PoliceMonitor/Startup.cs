﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PoliceMonitor.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace PoliceMonitor
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
